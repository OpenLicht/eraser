package de.tudresden.inf.st.eraser.feedbackloop.execute;

import de.tudresden.inf.st.eraser.feedbackloop.api.Execute;
import de.tudresden.inf.st.eraser.jastadd.model.ItemUpdate;
import de.tudresden.inf.st.eraser.jastadd.model.Root;

/**
 * Reference implementation for Execute.
 *
 * @author rschoene - Initial contribution
 */
public class ExecuteImpl implements Execute {

//  private Root knowledgeBase;

  @Override
  public void setKnowledgeBase(Root knowledgeBase) {
//    this.knowledgeBase = knowledgeBase;
  }

  @Override
  public void updateItems(Iterable<ItemUpdate> updates) {
    for (ItemUpdate preference : updates) {
      preference.apply();
    }
  }
}
