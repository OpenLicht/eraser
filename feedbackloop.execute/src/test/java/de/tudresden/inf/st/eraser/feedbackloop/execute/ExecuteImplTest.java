package de.tudresden.inf.st.eraser.feedbackloop.execute;

import de.tudresden.inf.st.eraser.feedbackloop.api.Execute;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.TestUtils;
import de.tudresden.inf.st.eraser.util.TestUtils.ModelAndItem;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test updating items within the Execute component.
 *
 * @author rschoene - Initial contribution
 */
public class ExecuteImplTest {

  private static final double DELTA = 0.001;

  @Test
  public void testColorControlledByOneNumber() {
    ModelAndItem mai = TestUtils.createModelAndItem(0);
    SmartHomeEntityModel model = mai.model;
    NumberItem numberItem = mai.item;

    ColorItem lamp = new ColorItem();
    lamp.setID("lamp");
    lamp.setState(TupleHSB.of(0, 0, 0), false);
    lamp.enableSendState();
    TestUtils.getDefaultGroup(model).addItem(lamp);

    numberItem.synchronizeWith(lamp);

    Execute execute = new ExecuteImpl();
    execute.setKnowledgeBase(model.getRoot());

    assertEquals(0, numberItem.getState(), DELTA);
    assertEquals(TupleHSB.of(0, 0, 0), lamp.getState());

    ItemUpdate preference = new ItemUpdateColor(lamp, TupleHSB.of(1, 2, 3));
    execute.updateItems(Collections.singletonList(preference));

    assertEquals(3, numberItem.getState(), DELTA);
    assertEquals(TupleHSB.of(1, 2, 3), lamp.getState());
  }

  @Test
  public void testColorControlledByOneBoolean() {
    SmartHomeEntityModel model = TestUtils.createModelAndItem(0).model;

    ItemWithBooleanState button = new SwitchItem();
    button.setID("button");
    button.setState(false, false);
    button.enableSendState();
    TestUtils.getDefaultGroup(model).addItem(button);

    ColorItem lamp = new ColorItem();
    lamp.setID("lamp");
    lamp.setState(TupleHSB.of(0, 0, 0), false);
    lamp.enableSendState();
    TestUtils.getDefaultGroup(model).addItem(lamp);

    button.synchronizeWith(lamp);

    Execute execute = new ExecuteImpl();
    execute.setKnowledgeBase(model.getRoot());

    assertFalse(button.getState());
    assertEquals(TupleHSB.of(0, 0, 0), lamp.getState());

    ItemUpdate preference = new ItemUpdateColor(lamp, TupleHSB.of(1, 2, 3));
    execute.updateItems(Collections.singletonList(preference));

    assertTrue(button.getState());
    assertEquals(TupleHSB.of(1, 2, 3), lamp.getState());
  }

  @Test
  public void testColorControlledByMany() {
    ModelAndItem mai = TestUtils.createModelAndItem(0);
    SmartHomeEntityModel model = mai.model;
    NumberItem numberItem = mai.item;

    Group g = TestUtils.getDefaultGroup(model);

    StringItem stringItem = new StringItem();
    stringItem.setState("0", false);
    g.addItem(stringItem);

    SwitchItem booleanItem = new SwitchItem();
    booleanItem.setState(false, false);
    g.addItem(booleanItem);

    ColorItem colorItem = new ColorItem();
    colorItem.setState(TupleHSB.of(0, 0, 0), false);
    g.addItem(colorItem);

    ColorItem lamp = new ColorItem();
    lamp.setID("lamp");
    lamp.setState(TupleHSB.of(0, 0, 0), false);
    lamp.enableSendState();
    TestUtils.getDefaultGroup(model).addItem(lamp);

    lamp.synchronizeWith(numberItem);
    lamp.synchronizeWith(stringItem);
    lamp.synchronizeWith(booleanItem);
    lamp.synchronizeWith(colorItem);

    Execute execute = new ExecuteImpl();
    execute.setKnowledgeBase(model.getRoot());

    assertEquals(0, numberItem.getState(), DELTA);
    assertEquals("0", stringItem.getState());
    assertFalse(booleanItem.getState());
    assertEquals(TupleHSB.of(0, 0, 0), colorItem.getState());
    assertEquals(TupleHSB.of(0, 0, 0), lamp.getState());

    ItemUpdate preference = new ItemUpdateColor(lamp, TupleHSB.of(1, 2, 3));
    execute.updateItems(Collections.singletonList(preference));

    assertEquals(3, numberItem.getState(), DELTA);
    assertEquals("1,2,3", stringItem.getState());
    assertTrue(booleanItem.getState());
    assertEquals(TupleHSB.of(1, 2, 3), colorItem.getState());
    assertEquals(TupleHSB.of(1, 2, 3), lamp.getState());
  }

}
