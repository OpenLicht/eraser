package de.tudresden.inf.st.eraser.integration_test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.eraser.openhab2.data.ItemData;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.function.Function;

import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Integration test to check openHAB-binding for eraser works well together with eraser itself.
 *
 * @author rschoene - Initial contribution
 */
@RunWith(Parameterized.class)
@Ignore
public class ItemTest {

  private static final String OPENHAB_ITEM_URI = "http://localhost:8080/rest/items/";
  private static final String ERASER_ITEM_URI = "http://localhost:4567/model/items/";

  private static final String DIMMER_ITEM = "dimmer_item";
  private static final String NUMBER_ITEM = "number_item";
  private static final String SWITCH_ITEM = "switch_item";
  private static final String COLOR_ITEM = "color_item";

  @BeforeClass
  public static void ensureItemsAreCreated() throws IOException {
    ensureItemCreated(DIMMER_ITEM, "Dimmer");
    ensureItemCreated(NUMBER_ITEM, "Number");
    ensureItemCreated(SWITCH_ITEM, "Switch");
    ensureItemCreated(COLOR_ITEM, "Color");
  }

  private static void ensureItemCreated(String name, String type) throws IOException {
    // Create at openHAB
    HttpResponse responseOpenHAB = Request.Put(OPENHAB_ITEM_URI + name)
        .bodyForm(Form.form().add("type", type).add("name", name).build())
        .execute().returnResponse();
    assertThat(
        responseOpenHAB.getStatusLine().getStatusCode(),
        either(equalTo(HttpStatus.SC_CREATED))
            .or(equalTo(HttpStatus.SC_OK)));

    // Create at eraser
    HttpResponse responseEraser = Request.Put(ERASER_ITEM_URI + name)
        .bodyString(type + " Item: id=\"" + name + "\"", ContentType.TEXT_PLAIN)
        .execute().returnResponse();
    assertThat(
        responseEraser.getStatusLine().getStatusCode(),
        equalTo(HttpStatus.SC_CREATED));
  }

  @Test
  public void itemAvailable() throws IOException {
    String name = "Tradfri_2_small_tv";
    ItemData itemData = Request.Get( OPENHAB_ITEM_URI + name )
        .execute().handleResponse(
            response -> retrieveResourceFromResponse(response, ItemData.class));
    assertThat(itemData.type, equalTo("Dimmer"));
  }

  @Test
  public void dimmerSetStateAtOpenHAB() throws IOException {
    String name = "Tradfri_2_small_tv";
    double newValue = 3.0;
    // set item state with openHAB REST API
    HttpResponse httpResponse = Request.Put(OPENHAB_ITEM_URI + name + "/state")
        .bodyString(Double.toString(newValue), ContentType.TEXT_PLAIN)
        .execute().returnResponse();
    assertThat(
        httpResponse.getStatusLine().getStatusCode(),
        equalTo(HttpStatus.SC_ACCEPTED));

    // check whether state was set correctly
    String responseOpenHAB = Request.Get(OPENHAB_ITEM_URI + name + "/state")
        .execute().returnContent().asString();
    assertThat(Double.parseDouble(responseOpenHAB), equalTo(newValue));

    // check whether state was updated on eraser side
    String responseEraser = Request.Get(ERASER_ITEM_URI + name + "/state")
        .execute().returnContent().asString();
    assertThat(Double.parseDouble(responseEraser), equalTo(newValue));
  }

  @Test
  public void dimmerSetStateAtEraser() throws IOException {
    String name = "Tradfri_2_small_tv";
    double newValue = 25.0;
    // set item state with eraser REST API
    String uri = ERASER_ITEM_URI + name + "/state";
    System.out.println(uri);
    HttpResponse httpResponse = Request.Put(uri)
        .bodyString(Double.toString(newValue), ContentType.TEXT_PLAIN)
        .execute().returnResponse();
    assertThat(
        httpResponse.getStatusLine().getStatusCode(),
        equalTo(HttpStatus.SC_OK));

    // check whether state was set correctly
    String responseEraser = Request.Get(ERASER_ITEM_URI + name + "/state")
        .execute().returnContent().asString();
    assertThat(Double.parseDouble(responseEraser), equalTo(newValue));

    // check whether state was updated on openHAB side
    String responseOpenHAB = Request.Get(OPENHAB_ITEM_URI + name + "/state")
        .execute().returnContent().asString();
    assertThat(Double.parseDouble(responseOpenHAB), equalTo(newValue));
  }

  private static <T> T retrieveResourceFromResponse(HttpResponse response, Class<T> clazz)
      throws IOException {
    String jsonFromResponse = EntityUtils.toString(response.getEntity());
    ObjectMapper mapper = new ObjectMapper()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper.readValue(jsonFromResponse, clazz);
  }

  private static <T> Object[] d(String name, T initialValue, Function<T, String> toString,
                                Function<String, T> fromString) {
    return new Object[]{name, initialValue, toString, fromString};
  }

  @Parameterized.Parameters(name= "{index}: {0}")
  public static Iterable<Object[]> data() {
    return Arrays.asList(
        d(DIMMER_ITEM, 25.0, d -> Double.toString(d), Double::parseDouble),
        d(NUMBER_ITEM, 4, i -> Integer.toString(i), Integer::parseInt),
        d(SWITCH_ITEM, "ON", Function.identity(), Function.identity()),
        d(COLOR_ITEM, "1,2,3", Function.identity(), Function.identity())
    );
  }

}
