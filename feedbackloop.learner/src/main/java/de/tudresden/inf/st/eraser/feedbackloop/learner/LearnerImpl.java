package de.tudresden.inf.st.eraser.feedbackloop.learner;

import de.tudresden.inf.st.eraser.feedbackloop.api.EncogModel;
import de.tudresden.inf.st.eraser.feedbackloop.api.Learner;
import de.tudresden.inf.st.eraser.jastadd.model.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.neural.flat.FlatNetwork;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.layers.Layer;
import org.encog.util.arrayutil.NormalizedField;
import org.encog.util.csv.CSVFormat;
import org.encog.util.csv.ReadCSV;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Implementation of the Learner.
 *
 * @author Bierzyns - Initial contribution
 */
public class LearnerImpl implements Learner {


  private Root knowledgeBase;
  private static final Logger logger = LogManager.getLogger(LearnerImpl.class);
  private Path csvFolderPath = Paths.get("src", "main", "resources");
  private String modelFolderPath = ".";
  private CSVFormat format = new CSVFormat('.', ',');
  private Map<Integer, Dataset> datasets = new HashMap<>();
  private Map<Integer, Network> models = new HashMap<>();


  @Override
  public void setKnowledgeBase(Root knowledgeBase) {
    this.knowledgeBase = knowledgeBase;
  }

  public void setCsvFolderPath(String csvFolderPath) {
    this.csvFolderPath = Paths.get(csvFolderPath);
  }

  public void setModelFolderPath(String modelFolderPath) {
    this.modelFolderPath = modelFolderPath;
  }

  @Override
  public boolean loadDataSet(String dataSetName, List<Integer> targetColumns, int modelID) {
    Path realDataSetPath = csvFolderPath.resolve(dataSetName);
    logger.debug("Load data set from file {}", realDataSetPath);
    try {
      Dataset set = new Dataset(new ReadCSV(realDataSetPath.toFile().getAbsoluteFile(), false, format), targetColumns);
      datasets.put(modelID, set);
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  @Override
  public boolean loadModelFromFile(File file, int modelID, List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                                   List<Integer> targetMins) {
    logger.debug("Load model from file {}", file);
    models.put(modelID, new Network(file.getAbsolutePath(), modelID, inputMaxes, inputMins, targetMaxes, targetMins));
    return true;
  }

  @Override
  public boolean loadModelFromFile(InputStream input, int modelID, List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                                   List<Integer> targetMins) {
    logger.debug("Load model from input stream");
    models.put(modelID, new Network(input, modelID, inputMaxes, inputMins, targetMaxes, targetMins));
    return true;
  }

  @Override
  public boolean train(int inputCount, int outputCount, int hiddenCount, int hiddenNeuronCount, int modelID,
                       List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                       List<Integer> targetMins) {
    // Method for the initial training of algorithms and models. That uses external data set for training.

    if (datasets.get(modelID) != null) {
      Dataset set = datasets.get(modelID);

      ReadCSV csv = set.getCsv();

      Network model = new Network(inputCount, outputCount, hiddenCount, hiddenNeuronCount, modelID, inputMaxes,
          inputMins, targetMaxes, targetMins);

      ArrayList<Double> input = new ArrayList<>();
      ArrayList<Double> target = new ArrayList<>();

      while (csv.next()) {
        logger.debug("Train next csv row");
        for (int i = 0; i < csv.getColumnCount(); i++) {
          int col_nr = i + 1;
          if (set.getTargetColumns().contains(col_nr)) {
            target.add(csv.getDouble(i));
          } else {
            input.add(csv.getDouble(i));
          }
        }

        model.train(input, target);
        input.clear();
        target.clear();
      }

      models.put(modelID, model);
      model.saveModel(modelFolderPath);

      return true;
    }
    return false;
  }

  @Override
  public boolean train(double[][] data, int inputCount, int outputCount, int hiddenCount, int hiddenNeuronCount, int modelID,
                       List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                       List<Integer> targetMins, List<Integer> targetColumns) {

    Network model = new Network(inputCount, outputCount, hiddenCount, hiddenNeuronCount, modelID, inputMaxes,
        inputMins, targetMaxes, targetMins);

    return reTrainModel(model, data, targetColumns, modelID);
  }


  @Override
  public boolean reTrain(double[][] data, List<Integer> targetColumns, int modelID) {

    Network model = models.get(modelID);

    return reTrainModel(model, data, targetColumns, modelID);
  }

  private boolean reTrainModel(Network model, double[][] data, List<Integer> targetColumns, int modelID) {
    List<Double> input = new ArrayList<>();
    List<Double> target = new ArrayList<>();

    for (int i = 0; i < data.length; i++) {

      for (int j = 0; j < data[0].length; j++) {
        int col_nr = j + 1;
        if (targetColumns.contains(col_nr)) {
          target.add(data[i][j]);
        } else {
          input.add(data[i][j]);
        }

        model.train(input, target);
        input.clear();
        target.clear(); 
      }

    }

    models.put(modelID, model);
    model.saveModel(modelFolderPath);

    return true;
  }


  @Override
  public EncogModel getTrainedModel(int modelID) {
    return fillModel(modelID);
  }

  private EncogModel fillModel(int modelID) {
    EncogModel encogModel = new EncogModel("NN");
    BasicNetwork nn = models.get(modelID).getNetwork();

    ArrayList<Double> weightsList = new ArrayList<>();
    String weights = nn.dumpWeights();
    String[] split = weights.split(",");

    for (int i = 0; i < (split.length); i++) {
      weightsList.add(Double.valueOf(split[i]));
    }

    encogModel.setWeights(weightsList);

    // do not use getLayers() because it is not restored immediately on load from file
    FlatNetwork flat = nn.getFlat();
    List<Layer> layers = new ArrayList<>(flat.getLayerCounts().length);
    logger.debug("layer counts: {}", Arrays.toString(flat.getLayerCounts()));
    for (int j = 0; j < flat.getLayerCounts().length; j++) {
//      boolean hasBias = j != 0 && j != flat.getLayerCounts().length - 1;
      boolean hasBias = flat.getLayerCounts()[j] != flat.getLayerFeedCounts()[j];
      Layer l = new BasicLayer(flat.getActivationFunctions()[j], hasBias, flat.getLayerCounts()[j]);
      l.setBiasActivation(flat.getBiasActivation()[j]);
      layers.add(0, l);
    }

    encogModel.setLayers(layers);

    return encogModel;
  }


  /**
   * @param modelID
   * @return
   */
  public NormalizedField getNormalizerInput(int modelID, int columnNr) {
    return models.get(modelID).getNormalizersIn().get(columnNr);
  }

  /**
   * @param modelID
   * @return
   */
  public NormalizedField getNormalizerTar(int modelID, int columnNr) {
    return models.get(modelID).getNormalizersTar().get(columnNr);
  }

}
