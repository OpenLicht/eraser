package de.tudresden.inf.st.eraser.feedbackloop.learner;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.util.arrayutil.NormalizationAction;
import org.encog.util.arrayutil.NormalizedField;
import org.encog.util.simple.EncogUtility;

/**
 * Network class serves as interface to encog BasicNetwork and holdsfunctions for handling the BasicNetwork (training, input, output and inference)
 *
 * @author Bierzynski - initial contribution
 */
public class Network {
  private static final Logger logger = LogManager.getLogger(Network.class);
  private BasicNetwork network;
  private int modelID;
  private ArrayList<NormalizedField> normalizersIn;
  private ArrayList<NormalizedField> normalizersTar;

  /**
   * Constructor for when the neural network is created from data.
   *
   * @param inputCount        number of neurons in the input layer
   * @param outputCount       number of neurons in the output layer
   * @param hiddenCount       number of hidden layers in the network
   * @param hiddenNeuronCount number of neurons in the hidden layers for now
   * @param modelID           ID of the BasicNetwork.
   * @param inputMaxes        list that contains max values of all input columns (sensors) e.g. light intensity 100
   * @param inputMins         list that contains min values of all input columns (sensors) e.g. light intensity 0
   * @param targetMaxes       list that contains max values of all output columns (results) e.g. brightness 100 for preference learning
   * @param targetMins        list that contains min values of all output columns (results) e.g. brightness 0 for preference learning
   */
  public Network(int inputCount, int outputCount, int hiddenCount, int hiddenNeuronCount, int modelID,
                 List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                 List<Integer> targetMins) {

    normalizersIn = new ArrayList<>();
    normalizersTar = new ArrayList<>();
    this.modelID = modelID;

    network = new BasicNetwork();

    network.addLayer(new BasicLayer(null, true, inputCount));

    for (int i = 0; i < hiddenCount; i++) {
      network.addLayer(new BasicLayer(new ActivationSigmoid(), true, hiddenNeuronCount));
    }

    network.addLayer(new BasicLayer(new ActivationSigmoid(), false, outputCount));
    network.getStructure().finalizeStructure();
    network.reset();

    addNormalizer(inputMaxes, inputMins, normalizersIn);
    addNormalizer(targetMaxes, targetMins, normalizersTar);
  }

  private void addNormalizer(List<Integer> maxes, List<Integer> mins, ArrayList<NormalizedField> normalizers) {
    for (int j = 0; j < maxes.size(); j++) {
      NormalizedField normalizer = new NormalizedField("in_" + j, NormalizationAction.Normalize,
          maxes.get(j), mins.get(j));
      normalizers.add(normalizer);
    }
  }

  /**
   * Constructor for when the neural network is loaded from a file.
   * Please note that the normalizer are note loaded file , because it is assumed that the mins and maxes are saved anyway in the meta data of the data sets or items.
   *
   * @param path        path to the save folder of the model files e.g. C:\models\
   * @param modelID     ID of the BasicNetwork.
   * @param inputMaxes  list that contains max values of all input columns (sensors) e.g. light intensity 100
   * @param inputMins   list that contains min values of all input columns (sensors) e.g. light intensity 0
   * @param targetMaxes list that contains max values of all output columns (results) e.g. brightness 100 for preference learning
   * @param targetMins  list that contains min values of all output columns (results) e.g. brightness 0 for preference learning
   */
  public Network(String path, int modelID, List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                 List<Integer> targetMins) {
    this(() -> (BasicNetwork) EncogDirectoryPersistence.loadObject(new File(path, "NN_" + modelID)), modelID, inputMaxes, inputMins, targetMaxes, targetMins);
  }

  /**
   * Constructor for when the neural network is loaded from an input stream.
   * Please note that the normalizer are note loaded file , because it is assumed that the mins and maxes are saved anyway in the meta data of the data sets or items.
   *
   * @param input       stream to load the model from
   * @param modelID     ID of the BasicNetwork.
   * @param inputMaxes  list that contains max values of all input columns (sensors) e.g. light intensity 100
   * @param inputMins   list that contains min values of all input columns (sensors) e.g. light intensity 0
   * @param targetMaxes list that contains max values of all output columns (results) e.g. brightness 100 for preference learning
   * @param targetMins  list that contains min values of all output columns (results) e.g. brightness 0 for preference learning
   */
  public Network(InputStream input, int modelID, List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                 List<Integer> targetMins) {
    this(() -> (BasicNetwork) EncogDirectoryPersistence.loadObject(input), modelID, inputMaxes, inputMins, targetMaxes, targetMins);
  }

  private Network(LoadEncogModel loader, int modelID, List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
                 List<Integer> targetMins) {
    this.modelID = modelID;

    normalizersIn = new ArrayList<>();
    normalizersTar = new ArrayList<>();

    network = loader.load();

    addNormalizer(inputMaxes, inputMins, normalizersIn);
    addNormalizer(targetMaxes, targetMins, normalizersTar);
  }

  @FunctionalInterface
  interface LoadEncogModel {
    BasicNetwork load();
  }

  /**
   * Method to save the trained {@link BasicNetwork} to a file.
   * File name is always NN_modelID
   *
   * @param savePath path to the folder in which the model file should be placed
   */
  public void saveModel(String savePath) {
    EncogDirectoryPersistence.saveObject(new File(savePath + "NN_" + modelID), network);
  }

  /**
   * Method for training the {@link BasicNetwork}.
   * One row training is implemented here for now to optimize preference learning.
   *
   * @param input  input part of the data row which should be used for training
   * @param target target/output part of the data row which should be used for training
   */
  public void train(List<Double> input, List<Double> target) {
    double[][] INPUT = new double[1][input.size()];
    double[][] IDEAL = new double[1][target.size()];

    for (int i = 0; i < input.size(); i++) {
      INPUT[0][i] = normalizersIn.get(i).normalize(input.get(i));
    }

    for (int j = 0; j < target.size(); j++) {
      IDEAL[0][j] = normalizersTar.get(j).normalize(target.get(j));
    }

    MLDataSet trainingSet = new BasicMLDataSet(INPUT, IDEAL);
    MLTrain train = new Backpropagation(network, trainingSet, 3.5, 0.3);

    do {
      train.iteration();
    } while (train.getError() > 0.005);

    train.finishTraining();
  }

  /**
   * Method that uses the {@link BasicNetwork} to predict/classify/.. something based on an input.
   *
   * @param inputVector data that should be processed
   */
  public double[] computeResult(List<Double> inputVector) {
    double[] output = new double[normalizersTar.size()];
    double[] input = new double[inputVector.size()];

    for (int i = 0; i < inputVector.size(); i++) {
      input[i] = normalizersIn.get(i).normalize(inputVector.get(i));
    }

    network.compute(input, output);

    for (int j = 0; j < normalizersTar.size(); j++) {
      output[j] = normalizersTar.get(j).deNormalize(output[j]);
    }

    return output;
  }

  public BasicNetwork getNetwork() {
    return network;
  }

  public ArrayList<NormalizedField> getNormalizersIn() {
    return normalizersIn;
  }

  public ArrayList<NormalizedField> getNormalizersTar() {
    return normalizersTar;
  }
}
