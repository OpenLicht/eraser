package de.tudresden.inf.st.eraser.feedbackloop.learner;

import de.tudresden.inf.st.eraser.feedbackloop.api.EncogModel;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.engine.network.activation.ActivationFunction;
import org.encog.engine.network.activation.ActivationLinear;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.neural.networks.layers.Layer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Transformation of a {@link EncogModel} into a {@link NeuralNetworkRoot}.
 *
 * @author rschoene - Initial contribution
 */
public class LearnerHelper {

  private static final Logger logger = LogManager.getLogger(LearnerHelper.class);

  // Activation Functions
  private static DoubleArrayDoubleFunction sigmoid = inputs -> Math.signum(Arrays.stream(inputs).sum());
  private static DoubleArrayDoubleFunction tanh = inputs -> Math.tanh(Arrays.stream(inputs).sum());
  private static DoubleArrayDoubleFunction function_one = inputs -> 1.0;

  public static NeuralNetworkRoot transform(EncogModel encogModel) {
    NeuralNetworkRoot result = NeuralNetworkRoot.createEmpty();
    List<Double> weights = encogModel.getWeights();
    logger.debug("Got {} weights", weights.size());

    List<List<Neuron>> allNeurons = new ArrayList<>();
    // inputs
    Layer inputLayer = encogModel.getInputLayer();
    reportLayer("input", inputLayer);
    List<Neuron> inputNeurons = new ArrayList<>();
    for (int i = 0; i < nonBiasNeuronCount(inputLayer); ++i) {
      InputNeuron inputNeuron = new InputNeuron();
      result.addInputNeuron(inputNeuron);
      inputNeurons.add(inputNeuron);
    }
    addBiasIfNeeded(inputLayer, result.getHiddenNeuronList(), inputNeurons);
    allNeurons.add(inputNeurons);

    // hidden layer
    List<Neuron> currentNeurons;
    for (Layer hiddenLayer : encogModel.getHiddenLayers()) {
      reportLayer("one hidden", hiddenLayer);
      currentNeurons = new ArrayList<>();
      allNeurons.add(currentNeurons);
      for (int i = 0; i < nonBiasNeuronCount(hiddenLayer); ++i) {
        HiddenNeuron hiddenNeuron = new HiddenNeuron();
        setActivationFunction(hiddenNeuron, hiddenLayer.getActivationFunction());
        result.addHiddenNeuron(hiddenNeuron);
        currentNeurons.add(hiddenNeuron);
      }
      addBiasIfNeeded(hiddenLayer, result.getHiddenNeuronList(), currentNeurons);
    }

    // output layer
    OutputLayer outputLayer = new OutputLayer();
    Layer modelOutputLayer = encogModel.getOutputLayer();
    reportLayer("output", modelOutputLayer);
    List<Neuron> outputNeurons = new ArrayList<>();
    for (int i = 0; i < nonBiasNeuronCount(modelOutputLayer); ++i) {
      OutputNeuron outputNeuron = new OutputNeuron();
      setActivationFunction(outputNeuron, modelOutputLayer.getActivationFunction());
      outputLayer.addOutputNeuron(outputNeuron);
      outputNeurons.add(outputNeuron);
    }
    result.setOutputLayer(outputLayer);
    allNeurons.add(outputNeurons);
    logger.debug("Created a total of {} neurons",
        allNeurons.stream()
            .map(list -> Integer.toString(list.size()))
            .collect(Collectors.joining("+")));

    // set weights from back to front, and from top to bottom
    int weightIndex = 0;
    for (int layer = allNeurons.size() - 1; layer > 0; --layer) {
      List<Neuron> rightList = allNeurons.get(layer);
      List<Neuron> leftList = allNeurons.get(layer - 1);
      for (int rightIndex = 0; rightIndex < rightList.size(); rightIndex++) {
        for (int leftIndex = 0; leftIndex < leftList.size(); leftIndex++) {
          if (rightList.get(rightIndex) instanceof BiasNeuron) {
            continue;
          }
          leftList.get(leftIndex).connectTo(rightList.get(rightIndex), weights.get(weightIndex++));
        }
      }
    }
    if (weightIndex != weights.size()) {
      logger.error("No all weights used (only {} of {}). Loaded wrong model!", weightIndex, weights.size());
    }

    outputLayer.setCombinator(LearnerHelper::predictor);
    logger.info("Created model with {} input, {} hidden and {} output neurons",
        result.getNumInputNeuron(), result.getNumHiddenNeuron(), result.getOutputLayer().getNumOutputNeuron());
    return result;
  }

  private static void addBiasIfNeeded(Layer layer, JastAddList<HiddenNeuron> neuronList, List<Neuron> localNeuronList) {
    if (layer.hasBias()) {
      BiasNeuron bias = new BiasNeuron();
      neuronList.add(bias);
      localNeuronList.add(bias);
    }
  }

  private static int nonBiasNeuronCount(Layer layer) {
    return layer.getNeuronCount() - (layer.hasBias() ? 1 : 0);
  }

  private static void reportLayer(String name, Layer layer) {
    logger.debug("{} layer has {} neurons {}",
        name, layer.getNeuronCount(), layer.hasBias() ? "(including bias)" : "");
  }

  private static void setActivationFunction(HiddenNeuron neuron, ActivationFunction function) {
    if (function instanceof ActivationTANH) {
      neuron.setActivationFormula(tanh);
    } else if (function instanceof ActivationLinear) {
      neuron.setActivationFormula(function_one);
    } else if (function instanceof ActivationSigmoid) {
      neuron.setActivationFormula(sigmoid);
    } else {
      throw new IllegalArgumentException("Unknown activation function " + function.getClass().getName());
    }
  }

    private static double predictor(double[] inputs) {
    int index = 0;
    double maxInput = StatUtils.max(inputs);
    for (int i = 0; i < inputs.length; i++) {
      if (inputs[i] == maxInput) {
        index = i;
      }
    }
    //outputs from learner
    final double[] outputs = new double[]{2.0, 1.0, 3.0, 0.0};
    return outputs[index];
  }

}
