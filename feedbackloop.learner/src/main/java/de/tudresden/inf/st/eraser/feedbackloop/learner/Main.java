package de.tudresden.inf.st.eraser.feedbackloop.learner;

import de.tudresden.inf.st.eraser.feedbackloop.api.Learner;
import de.tudresden.inf.st.eraser.feedbackloop.api.EncogModel;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@SuppressWarnings("unused")
public class Main {
	private static final Logger logger = LogManager.getLogger(Main.class);
	private static class InitialDataConfig {
		static List<Integer> inputMins = Arrays.asList(
				7,  // min month
				1,  // min day
				10, // min hour
				1   // min minute
		);
		static List<Integer> inputMaxes = Arrays.asList(
				9,  // max month
				31, // max day
				21, // max hour
				60  // max minute
		);
		static List<Integer> targetMins = Collections.singletonList(0);
		static List<Integer> targetMaxes = Collections.singletonList(3);
		static String csv_filename = "initial_data.csv";
		static String encog_filename = Paths.get("src", "main", "resources").toFile().getAbsolutePath() + "/";
		static int inputCount = 4;
		static int outputCount = 1;
		static int hiddenCount = 0;
		static int hiddenNeuronCount = 7;
		static List<Integer> targetColumns = Collections.singletonList(4);
	}

	public static void main(String[] args) {
//		loadFromCsv();
		loadFromEncog();
	}

	private static void loadFromCsv() {
		Learner learner = new LearnerImpl();
		learner.loadDataSet(InitialDataConfig.csv_filename, InitialDataConfig.targetColumns,1);
		learner.train(
				InitialDataConfig.inputCount, InitialDataConfig.outputCount, InitialDataConfig.hiddenCount,
				InitialDataConfig.hiddenNeuronCount, 1, InitialDataConfig.inputMaxes, InitialDataConfig.inputMins,
				InitialDataConfig.targetMaxes, InitialDataConfig.targetMins);

		printModel(learner.getTrainedModel(1));
	}

	private static void loadFromEncog() {
		Learner learner = new LearnerImpl();
		learner.loadModelFromFile(
				new File(InitialDataConfig.encog_filename), 1,
				InitialDataConfig.inputMaxes, InitialDataConfig.inputMins,
				InitialDataConfig.targetMaxes, InitialDataConfig.targetMins);
		printModel(learner.getTrainedModel(1));
    NeuralNetworkRoot eraserModel = LearnerHelper.transform(learner.getTrainedModel(1));
	}

	private static void printModel(EncogModel encogModel) {
		logger.info("Model Type is: " + encogModel.getModelType());
		logger.info("Model Weights are: " + encogModel.getWeights());
		logger.info("Model layers are: " + encogModel.getLayers());
//		logger.info("Model input normal neutrons: " + model.getInputLayerNumber());
//		logger.info("Model input bias neutron: " + model.getInputBias());
//		logger.info("Model hidden normal neutrons: " + model.gethiddenLayerNumber());
//		logger.info("Model hidden bias neutron: " + model.getHiddenBias());
//		logger.info("Model output neutrons: " + model.getOutputLayerNumber());
//		logger.info("Model input activation function: " + model.getInputActivationFunction());
//		logger.info("Model hidden activation function: " + model.getHiddenActivationFunction());
//		logger.info("Model output activation function: " + model.getOutputActivationFunction());
	}

	//(ArrayList<Integer> list3
	private static Root createModel(ArrayList<Double> normalizedInputs, int inputBiasNumber) {

		//create KB Model
		Root root = Root.createEmptyRoot();
		Group group = new Group();
		group.setID("Group1");
		root.getSmartHomeEntityModel().addGroup(group);

		NumberItem monthItem = new NumberItem();
		monthItem.setState(normalizedInputs.get(0));
		monthItem.setID("month");
		monthItem.setLabel("datetime-month");

		NumberItem dayItem = new NumberItem();
		dayItem.setState(normalizedInputs.get(1));
		dayItem.setID("day");
		dayItem.setLabel("datetime-day");

		NumberItem hourItem = new NumberItem();
		hourItem.setState(normalizedInputs.get(2));
		hourItem.setID("hour");
		hourItem.setLabel("datetime-hour");

		NumberItem minuteItem = new NumberItem();
		minuteItem.setState(normalizedInputs.get(3));
		minuteItem.setID("minute");
		minuteItem.setLabel("datetime-minute");

		if (inputBiasNumber == 1) {
			NumberItem biasItem = new NumberItem();
			biasItem.setState(1);
			biasItem.setID("bias");
			biasItem.setLabel("bias");
			group.addItem(biasItem);
		}
		group.addItem(monthItem);
		group.addItem(dayItem);
		group.addItem(hourItem);
		group.addItem(minuteItem);
		return root;
	}

	/**
	 * Purpose: Create a neural network with 3 layers
	 */
	private static void createBrightnessNetwork(ArrayList<Double> all_weights, int inputBiasNumber, int hiddenlayernumber,
												int hiddenlayerbias, ArrayList<Double> normalizedInputsandOutput) {
		Root root = createModel(normalizedInputsandOutput, inputBiasNumber);
		SmartHomeEntityModel model = root.getSmartHomeEntityModel();
		Item monthItem = model.resolveItem("month").orElseThrow(
				() -> new RuntimeException("Month not found"));
		Item dayItem = model.resolveItem("day").orElseThrow(
				() -> new RuntimeException("Day not found"));
		Item hourItem = model.resolveItem("hour").orElseThrow(
				() -> new RuntimeException("Hour not found"));
		Item minuteItem = model.resolveItem("minute").orElseThrow(
				() -> new RuntimeException("Minute not found"));
		Item biasItem = model.resolveItem("bias").orElseThrow(
				() -> new RuntimeException("Bias not found"));

		NeuralNetworkRoot nn = new NeuralNetworkRoot();

		// Activation Functions
		DoubleArrayDoubleFunction sigmoid = inputs -> Math.signum(Arrays.stream(inputs).sum());
		DoubleArrayDoubleFunction tanh = inputs -> Math.tanh(Arrays.stream(inputs).sum());
		DoubleArrayDoubleFunction function_one = inputs -> 1.0;

		//Weights outputs from learner Module
		ArrayList<Double> weights = all_weights;
		// input layer
		InputNeuron month = new InputNeuron();
		month.setItem(monthItem);
		InputNeuron day = new InputNeuron();
		day.setItem(dayItem);
		InputNeuron hour = new InputNeuron();
		hour.setItem(hourItem);
		InputNeuron minute = new InputNeuron();
		minute.setItem(minuteItem);
		InputNeuron bias = new InputNeuron();
		bias.setItem(biasItem);

		nn.addInputNeuron(month);
		nn.addInputNeuron(day);
		nn.addInputNeuron(hour);
		nn.addInputNeuron(minute);
		nn.addInputNeuron(bias);

		// output layer
		OutputLayer outputLayer = new OutputLayer();
		OutputNeuron output0 = new OutputNeuron();
		output0.setActivationFormula(tanh);
		OutputNeuron output1 = new OutputNeuron();
		output1.setActivationFormula(tanh);
		OutputNeuron output2 = new OutputNeuron();
		output2.setActivationFormula(tanh);
		OutputNeuron output3 = new OutputNeuron();
		output3.setActivationFormula(tanh);

		outputLayer.addOutputNeuron(output0);
		outputLayer.addOutputNeuron(output1);
		outputLayer.addOutputNeuron(output2);
		outputLayer.addOutputNeuron(output3);

		outputLayer.setCombinator(inputs -> predictor(inputs, normalizedInputsandOutput));
		//outputLayer.setCombinator(inputs -> StatUtils.max(inputs) );
		nn.setOutputLayer(outputLayer);

		// hidden layer
		int hiddenSum = hiddenlayernumber + hiddenlayerbias;
		HiddenNeuron[] hiddenNeurons = new HiddenNeuron[hiddenSum];
		for (int i = 0; i < (hiddenNeurons.length); i++) {
			if (i == hiddenlayernumber) {
				HiddenNeuron hiddenNeuron = new HiddenNeuron();
				hiddenNeuron.setActivationFormula(function_one);
				hiddenNeurons[i] = hiddenNeuron;
				nn.addHiddenNeuron(hiddenNeuron);
				bias.connectTo(hiddenNeuron, 1.0);
				hiddenNeuron.connectTo(output0, weights.get(i));
				hiddenNeuron.connectTo(output1, weights.get(i + hiddenSum));
				hiddenNeuron.connectTo(output2, weights.get(i + hiddenSum * 2));
				hiddenNeuron.connectTo(output3, weights.get(i + hiddenSum * 3));
			} else {
				HiddenNeuron hiddenNeuron = new HiddenNeuron();
				hiddenNeuron.setActivationFormula(tanh);
				hiddenNeurons[i] = hiddenNeuron;
				nn.addHiddenNeuron(hiddenNeuron);

				month.connectTo(hiddenNeuron, weights.get((hiddenNeurons.length * 4) + i * 5));
				day.connectTo(hiddenNeuron, weights.get((hiddenNeurons.length * 4 + 1) + i * 5));
				hour.connectTo(hiddenNeuron, weights.get((hiddenNeurons.length * 4 + 2) + i * 5));
				minute.connectTo(hiddenNeuron, weights.get((hiddenNeurons.length * 4 + 3) + i * 5));
				bias.connectTo(hiddenNeuron, weights.get((hiddenNeurons.length * 4 + 4) + i * 5));
				hiddenNeuron.connectTo(output0, weights.get(i));
				hiddenNeuron.connectTo(output1, weights.get(i + hiddenSum));
				hiddenNeuron.connectTo(output2, weights.get(i + hiddenSum * 2));
				hiddenNeuron.connectTo(output3, weights.get(i + hiddenSum * 3));
			}
		}
		root.getMachineLearningRoot().setPreferenceLearning(nn);
		System.out.println(root.prettyPrint());

		List<String> output = new ArrayList<>();
		Function<DoubleNumber, String> leafToString = classification -> Double.toString(classification.number);
		Function<NeuralNetworkRoot, DoubleNumber> classify = NeuralNetworkRoot::internalClassify;
		DoubleNumber classification = classify.apply(nn);
		output.add(leafToString.apply(classification));
		System.out.println(output);




	}
		private static double predictor(double[] inputs, ArrayList<Double> normalizedInputsandOutputs){
			int index = 0;
			double maxinput = StatUtils.max(inputs);
			for (int i = 0; i < inputs.length; i++) {
				if (inputs[i] == maxinput) {
					index = i;
				}
			}
			//outputs from learner
			ArrayList<Double> outputs = new ArrayList<Double>(Arrays.asList(normalizedInputsandOutputs.get(4),
					normalizedInputsandOutputs.get(5), normalizedInputsandOutputs.get(6),
					normalizedInputsandOutputs.get(7)));
			double output = outputs.get(index);
			return output;
		}
		//[BasicMLData:0.0,0.0,0.0,0.0]
	//inputs:
	//[BasicMLData:-1.0,0.2666666666666666,-0.6363636363636364,-0.5593220338983051]
	//outputs:
	//[BasicMLData:0.36743405976714366,-0.6610085416169492,-0.9999999998849812,-0.9999999224507112]

	}
