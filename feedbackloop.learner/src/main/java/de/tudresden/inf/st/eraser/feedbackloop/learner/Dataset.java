package de.tudresden.inf.st.eraser.feedbackloop.learner;

import org.encog.util.csv.ReadCSV;

import java.util.List;

/**
 * This class is an representation of a data set in csv format.
 * The data is saved and made accessible by {@link ReadCSV} and the numbers of the target columns is saved as meta-data.
 * 
 *  @author Bierzyns - initial contribution
 * 
 * */
public class Dataset {

    private final ReadCSV csv;
    private final List<Integer> targetColumns;

    public Dataset(ReadCSV csv, List<Integer> targetColumns) {
        this.csv = csv;
        this.targetColumns = targetColumns;
    }

    public int getColumnCount() {
        return csv.getColumnCount();
    }

    public List<Integer> getTargetColumns() {
        return targetColumns;
    }

    public ReadCSV getCsv() {
        return csv;
    }

}
