package de.tudresden.inf.st.eraser.integration;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import de.tudresden.inf.st.eraser.Main;
import de.tudresden.inf.st.eraser.deserializer.ASTNodeDeserializer;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.openhab2.mqtt.MQTTUpdater;
import de.tudresden.inf.st.eraser.serializer.JsonSerializer;
import org.fusesource.mqtt.client.QoS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Integrating openhab-mock and eraser-base.
 *
 * @author rschoene - Initial contribution
 */
public class IntegrationMain {

  private static Logger logger = LogManager.getLogger(IntegrationMain.class);

  public static void main(String[] args) throws InterruptedException {
    CountDownLatch modelLoaded = new CountDownLatch(1);
    Thread mock = new Thread(() -> {
      try {
        boolean ready = modelLoaded.await(5, TimeUnit.SECONDS);
        if (!ready) {
          logger.fatal("Timeout in mock-thread. Exiting");
          return;
        }
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
      // read in csv
      String filename = "/data1.csv";
//      Path csvContent = Paths.get("src", "main", "resources", filename);
      InputStream inputStream = IntegrationMain.class.getResourceAsStream(filename);
      String host = "localhost";
      MqttRoot mqttRoot = new MqttRoot();
      mqttRoot.getHost().setHostName(host);
      // columns: time,topic,qos,message
      try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
           CSVReader reader = new CSVReaderBuilder(inputStreamReader)
               .withCSVParser(new CSVParserBuilder()
                   .withSeparator(',')
                   .withQuoteChar('"')
                   .build())
               .withSkipLines(1)
               .build();
      MQTTSender sender = mqttRoot.getMqttSender()){
        if (!sender.isConnected()) {
          String msg = "MQTT sender is not connected, aborting to avoid waits at publish calls";
          logger.error(msg);
          throw new RuntimeException(msg);
        } else {
          logger.debug("MQTT sender is connected");
        }
        reader.iterator().forEachRemaining(line -> {
          System.out.println(Arrays.toString(line));
          // replay messages
          String topic = line[1];
          String qos = line[2];
          String message = line[3];
          // TODO replay messages in real time, i.e., with delay between messages
          try {
            sender.publish(topic, message, getQoSEnum(qos));
          } catch (Exception e) {
            // abort the whole operation
            throw new RuntimeException(e);
          }
        });
      } catch(Exception e){
        e.printStackTrace();
      }
    });
    Thread eraser = new Thread(() -> {
      final int seconds = 5;
      logger.info("Start!");
      Root model = Main.importFromFile();
//      Root model = importFromLocalFile();
      logger.debug("Got model: {}", model.getSmartHomeEntityModel().description());
      MqttRoot mqttRoot = new MqttRoot();
      mqttRoot.getHost().setHostName("localhost");
      mqttRoot.setIncomingPrefix("oh2/out/");
      MqttTopic irisStateTopic = new MqttTopic();
      irisStateTopic.setTopicString("iris1_item/state");
      Item iris = null;
      for (Item item : model.getSmartHomeEntityModel().items()) {
        if (item.getID().equals("iris1_item")) {
          iris = item;
          break;
        }
      }
      if (iris == null) {
        logger.error("Could not find iris1. Exiting");
        return;
      }
      irisStateTopic.addItem(iris);
      model.setMqttRoot(mqttRoot);
//      JsonSerializer.write(model, "src/main/resources/openhab2-data.json");
      JsonSerializer.write(model, "openhab2-data.json");
      try (MQTTUpdater updater = new MQTTUpdater(model)) {
        logger.info("Processing mqtt updates for {} seconds", seconds);
        updater.start();
        boolean isReady = updater.waitUntilReady(2, TimeUnit.SECONDS);
        if (!isReady) {
          logger.warn("Updater seems not ready yet. Continue, but expect errors.");
        }
        modelLoaded.countDown();
        Thread.sleep(seconds * 1000);
      } catch (IllegalArgumentException | InterruptedException | IOException e) {
        logger.catching(e);
      }
    });
    mock.start();
    eraser.start();
    // now the show begins, and we wait until it's over
    mock.join();
    eraser.join();
  }

  public static Root importFromLocalFile() {
    final String filename = "/openhab2-data.json";
    InputStream inputStream = IntegrationMain.class.getResourceAsStream(filename);
    return ASTNodeDeserializer.read(inputStream);
  }

  public static QoS getQoSEnum(String qos) {
    switch (qos) {
      case "0":
        return QoS.AT_MOST_ONCE;
      case "1":
        return QoS.AT_LEAST_ONCE;
      case "2":
        return QoS.EXACTLY_ONCE;
      default:
        throw new IllegalArgumentException("Invalid QoS: " + qos);
    }
  }

}
