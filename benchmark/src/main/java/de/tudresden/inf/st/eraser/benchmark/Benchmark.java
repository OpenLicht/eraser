package de.tudresden.inf.st.eraser.benchmark;

import com.opencsv.CSVReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Benchmark {

    //read every 5 s from csv
    //activity CSV
    /**
     * Col 1: smartphone acceleration x
     * Col 2: smartphone acceleration y
     * Col 3: smartphone acceleration z
     * Col 4: smartphone rotation x
     * Col 5: smartphone rotation y
     * Col 6: smartphone rotation z
     * Col 7: watch acceleration x
     * Col 8: watch acceleration y
     * Col 9: watch acceleration z
     * Col 10: watch rotation x
     * Col 11: watch rotation y
     * Col 12: watch rotation z/*/
    //preference CSV
    /**
     * Col 1: Activity
     * Col 2: watch brightness range "bright, medium, dimmer, dark"*/

    private String a_csv_file_path;
    private String p_csv_file_path;
    private static final Logger logger = LogManager.getLogger(Benchmark.class);
    private static final String ERASER_ITEM_URI = "http://localhost:4567/model/items/";
    //TODO ITEM_NAME HAS TO BE DISCUSSED
    private String[] ACTIVITY_ITEM_NAME;
    private String[] PREFERENCE_ITEM_NAME;
    //csv_type is activity or preference
    Benchmark(String a_csv_file_path, String p_csv_file_path) {
        this.a_csv_file_path = a_csv_file_path;
        this.p_csv_file_path = p_csv_file_path;
    }
    void start(){
        int TIME_PERIOD = 1000;
        String PREFERENCE_URL = "http://localhost:4567/model/items/iris1_item/state";
        String ACTIVITY_URL = "http://localhost:4567/activity/current";
        File a_file;
        File p_file;
        FileReader a_file_reader;
        FileReader p_file_reader;
        CSVReader a_csv_reader;
        CSVReader p_csv_reader;
        String[] a_next_record;
        String[] p_next_record;
        a_file=new File(a_csv_file_path);
        p_file= new File(p_csv_file_path);
        try {
            a_file_reader =new FileReader(a_file);
            p_file_reader=new FileReader(p_file);
            a_csv_reader = new CSVReader(a_file_reader);
            p_csv_reader = new CSVReader(p_file_reader);
            int i = 0;
            while ((a_next_record = a_csv_reader.readNext())!= null){
                if( i==0 ){
                    getCSVHeader(Arrays.copyOf(a_next_record,12),"a");
                    i++;
                }
                else{
                    String[] values = Arrays.copyOf(a_next_record,12);
                    setNewValue(values, ACTIVITY_ITEM_NAME,"activity");
                    int j = 0;
                    while(((p_next_record = p_csv_reader.readNext()) != null)) {
                        if( j == 0 ){
                            getCSVHeader(Arrays.copyOf(p_next_record,2),"p");
                            j++;
                        }else{
                            String[] values1 = Arrays.copyOf(p_next_record,2);
                            setNewValue(values1, PREFERENCE_ITEM_NAME,"preference");
                            // wait for 1s
                            try{Thread.sleep(TIME_PERIOD);}catch (InterruptedException e){e.printStackTrace();}
                            logger.warn("checking/comparing the result from web server with the CSV");
                            HttpResponse response= Request.Get(ACTIVITY_URL).execute().returnResponse();
                            HttpResponse response1= Request.Get(PREFERENCE_URL).execute().returnResponse();
                            //check response
                            checkResult(response, a_next_record,null);
                            checkResult(response1, p_next_record,a_next_record[12]);
                            logger.warn("checking/comparing finised");
                            //logger.info("checking/comparing finished");
                            break;
                        }
                    }
                }

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setNewValue(String[] values, String[] name, String file_typ){
        if(file_typ.equals("activity"))
        {
            int i = 0;
            for(String value : values){
                String uri = ERASER_ITEM_URI + name[i] + "/state";
                try {
                    HttpResponse httpResponse = Request.Put(uri)
                            .bodyString(value, ContentType.TEXT_PLAIN)
                            .execute().returnResponse();
                    String status=httpResponse.getStatusLine().toString();
                    if(status.contains("200")){
                        logger.info("put activity input name: "+ name[i] +", value: "+ value +" to web server: response 200 ok");
                    }else{
                        logger.info("can not put activity inputs to rest server");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                i+=1;
            }
        }else{
            String uri= ERASER_ITEM_URI + "w_brightness" +"/state";
            try {
                HttpResponse httpResponse = Request.Put(uri)
                        .bodyString(values[1], ContentType.TEXT_PLAIN)
                        .execute().returnResponse();
                String put_response = httpResponse.getStatusLine().toString();
                if (put_response.contains("200")){logger.info("put preference input name: w_brightness, value : " +
                        values[1]+" to web server: response 200 ok");} else{
                    logger.info("can not put w_brightness to rest server");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    private void getCSVHeader(String[] a_values,String csv_typ){
        if (csv_typ.equals("a")){
            ACTIVITY_ITEM_NAME = a_values;
        }else{
            PREFERENCE_ITEM_NAME = a_values;
        }
    }
    private void checkResult(HttpResponse response,String[] record, String activity){
        int status = response.getStatusLine().getStatusCode();
        if(status == 200 && record.length == 13){
            logger.info("activity should be (read direct from CSV): " + record[12]);
            try {
                logger.info("get activity from web server: response 200 ok, value is: "+
                        EntityUtils.toString(response.getEntity()));
            }catch (IOException e){
                e.printStackTrace();
            }
        }else if(status ==200 && record.length == 4){
            logger.info("preference should be (read direct from CSV): " + comparePreferenceOutput(record,activity));
            try {
            logger.info("get the iris1_item preference from web server: response 200 ok, value is: " +
                    EntityUtils.toString(response.getEntity()));
            }catch (IOException e){
                e.printStackTrace();
            }

        }else if(status != 200 &&record.length == 13){
            logger.info("can not get the activity from the web server");

        }
        else if(status != 200 && record.length == 4){
            logger.info("can not get the iris1_item from the web server");
        }else {
            logger.info("unknown check");
        }
    }

    private String comparePreferenceOutput(String[] record, String activity){
        String output = null;
        String brightness=record[1];
        File file =  new File(p_csv_file_path);
        FileReader reader;
        CSVReader csv_reader;
        try{
            reader = new FileReader(file);
            csv_reader=new CSVReader(reader);
            String[] next_record;
            while ((next_record = csv_reader.readNext())!= null){
                if (next_record[0].equals(activity) && next_record[1].equals(brightness)){
                    output = next_record[2]+",100,"+next_record[3];
                }
            }
        }catch (Exception e){e.printStackTrace();}
        return output;
    }
}