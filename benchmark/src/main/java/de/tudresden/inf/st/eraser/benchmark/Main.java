package de.tudresden.inf.st.eraser.benchmark;

public class Main {

    public static void main(String[] args) {
        String A_CSV_FILE_PATH = "../datasets/backup/activity_data.csv";
        String P_CSV_FILE_PATH = "../datasets/backup/preference_data.csv";
        Benchmark benchmark = new Benchmark(A_CSV_FILE_PATH,P_CSV_FILE_PATH);
        benchmark.start();
    }
}

