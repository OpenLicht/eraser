package de.tudresden.inf.st.eraser.openhab_mock;

import com.opencsv.CSVReader;
import de.tudresden.inf.st.eraser.jastadd.model.MQTTSender;
import de.tudresden.inf.st.eraser.jastadd.model.MqttRoot;
import org.fusesource.mqtt.client.QoS;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Mock to send out MQTT message that could have come from openHAB.
 *
 * @author rschoene - Initial contribution
 */
public class MockMain {

  public static void main(String[] args) {
    // read in csv
    String filename = "/data1.csv";
    InputStream inputStream = MockMain.class.getResourceAsStream(filename);
    String host = "localhost";
    MqttRoot mqttRoot = new MqttRoot();
    mqttRoot.setHostByName(host);
    // columns: time,topic,qos,message
    try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
         CSVReader reader = new CSVReader(inputStreamReader, ',', '"', 1);
         MQTTSender sender = mqttRoot.getMqttSender()) {
      reader.iterator().forEachRemaining(line -> {
        System.out.println(Arrays.toString(line));
        // replay messages
        String topic = line[1];
        String qos = line[2];
        String message = line[3];
        try {
          sender.publish(topic, message, getQoSEnum(qos));
        } catch (Exception e) {
          // abort the whole operation
          throw new RuntimeException(e);
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }
    // TODO replay messages in real time, i.e., with delay between messages
  }

  public static QoS getQoSEnum(String qos) {
    switch(qos) {
      case "0": return QoS.AT_MOST_ONCE;
      case "1": return QoS.AT_LEAST_ONCE;
      case "2": return QoS.EXACTLY_ONCE;
      default: throw new IllegalArgumentException("Invalid QoS: " + qos);
    }
  }
}
