package de.tudresden.inf.st.eraser.openhab_mock;

import de.tudresden.inf.st.eraser.commons.color.ColorUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseWheelEvent;

/**
 * Small Swing application to test color conversion from XY to RGB to HSB.
 *
 * @author rschoene - Initial contribution
 */
class HueMock {

  private JLabel colorLabel;
  private LabelAndTextField h;
  private LabelAndTextField s;
  private LabelAndTextField b;
  private LabelAndTextField x;
  private LabelAndTextField y;
  private LabelAndTextField red;
  private LabelAndTextField green;
  private LabelAndTextField blue;

  class LabelAndTextField {
    private final double increment;
    private final boolean isIntegral;
    private JLabel label;
    private JTextField textField;
    private final Runnable updateFunction;

    LabelAndTextField(String labelText, String initialText, int initialColumns, Runnable updateFunction,
                      double increment, boolean isIntegral) {
      this.label = new JLabel(labelText);
      this.textField = new JTextField(initialText, initialColumns);
      this.updateFunction = updateFunction;
      this.increment = increment;
      this.isIntegral = isIntegral;
      textField.addActionListener(e -> update());
      textField.addMouseWheelListener(this::update);
      
      label.setLabelFor(textField);
    }

    private void update(MouseWheelEvent e) {
      // mouse wheel rotated upwards implies negative WheelRotation, thus multiply with 1.0
      double value = doubleValue() + this.increment * (e.getWheelRotation() < 0 ? 1.0 : -1.0);
      if (this.isIntegral) {
        setValue(Long.toString(Math.round(value)));
      } else {
        // round to 1 decimal place
        setValue(Math.round(10.0 * value) / 10.0);
      }
      update();
    }

    private void update() {
      this.updateFunction.run();
    }

    void addTo(Container c) {
      c.add(label);
      c.add(textField);
    }

    int intValue() {
      return Integer.parseInt(textField.getText());
    }

    float floatValue() {
      return Float.parseFloat(textField.getText());
    }

    double doubleValue() {
      return Double.parseDouble(textField.getText());
    }

    void setValue(String value) {
      textField.setText(value);
    }

    void setValue(long value) {
      setValue(Long.toString(value));
    }

    void setValue(double value) {
      setValue(Double.toString(value));
    }
  }

  HueMock() {
    JFrame frame = new JFrame("Hue Mock");
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.setTitle("Example GUI");
    frame.setSize(300,250);
    frame.setLocationRelativeTo(null);

    x = new LabelAndTextField("X", "0", 3,
        this::updateColorFromXY, 0.1, false);
    y = new LabelAndTextField("Y", "0", 3,
        this::updateColorFromXY, 0.1, false);

    JLabel arrowXYtoRGB = new JLabel("→");

    red = new LabelAndTextField("Red", "0", 3,
        this::updateColorFromRGB, 10, true);
    green = new LabelAndTextField("Green", "0", 3,
        this::updateColorFromRGB, 10, true);
    blue = new LabelAndTextField("Blue", "0", 3,
        this::updateColorFromRGB, 10, true);

    JLabel arrowRGBtoHSB = new JLabel("→");

    h = new LabelAndTextField("H", "0", 5,
        this::updateColorFromHSB, 0.1, false);
    s = new LabelAndTextField("S", "0", 5,
        this::updateColorFromHSB, 0.1, false);
    b = new LabelAndTextField("B", "0", 5,
        this::updateColorFromHSB, 0.1, false);

    colorLabel = new JLabel("This is a Hue.");
    colorLabel.setOpaque(true);
    updateColorFromHSB();

    JPanel panel = new JPanel();
    x.addTo(panel);
    y.addTo(panel);
    panel.add(arrowXYtoRGB);
    red.addTo(panel);
    green.addTo(panel);
    blue.addTo(panel);
    panel.add(arrowRGBtoHSB);
    h.addTo(panel);
    s.addTo(panel);
    b.addTo(panel);
    panel.add(colorLabel);
    frame.add(panel);
    frame.pack();

    frame.setVisible(true);
  }

  private void updateColorFromXY() {
    try {
      ColorUtils.convertXYtoRGB(x.doubleValue(), y.doubleValue(), 1.0,
          red::setValue, green::setValue, blue::setValue);
      updateColorFromRGB();
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  private void updateColorFromRGB() {
    try {
      ColorUtils.convertRGBtoHSB(red.intValue(), green.intValue(), blue.intValue(),
          h::setValue, s::setValue, b::setValue);
      updateColorFromHSB();
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  private void updateColorFromHSB() {
    try {
      colorLabel.setBackground(Color.getHSBColor(h.floatValue(), s.floatValue(), b.floatValue()));
      colorLabel.repaint();
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }
}
