package de.tudresden.inf.st.eraser.openhab_mock;

import javax.swing.*;

/**
 * Starts {@link HueMock}.
 *
 * @author rschoene - Initial contribution
 */
public class HueMain {

  public static void main(String[] args) {
    SwingUtilities.invokeLater(HueMock::new);
  }
}
