package de.tudresden.inf.st.eraser.feedbackloop.api;

import de.tudresden.inf.st.eraser.jastadd.model.Root;

/**
 * First phase in the MAPE feedback loop, gathering sensor value changes.
 *
 * @author rschoene - Initial contribution
 */
public interface Monitor {

  void setKnowledgeBase(Root knowledgeBase);

  void setAnalyze(Analyze analyze);

  Analyze getAnalyze();

  default void informAnalyze() {
    getAnalyze().analyzeLatestChanges();
  }
}
