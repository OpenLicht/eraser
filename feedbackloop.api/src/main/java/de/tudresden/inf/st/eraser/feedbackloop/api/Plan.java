package de.tudresden.inf.st.eraser.feedbackloop.api;

import de.tudresden.inf.st.eraser.jastadd.model.Activity;
import de.tudresden.inf.st.eraser.jastadd.model.ItemUpdate;
import de.tudresden.inf.st.eraser.jastadd.model.Root;

/**
 * Third phase in the MAPE feedback loop, planning reconfiguration actions to adapt to recent changes.
 *
 * @author rschoene - Initial contribution
 */
public interface Plan {

  void setKnowledgeBase(Root knowledgeBase);

  void setExecute(Execute execute);

  Execute getExecute();

  void planToMatchPreferences(Activity activity);

  default void informExecute(Iterable<ItemUpdate> updates) {
    getExecute().updateItems(updates);
  }
}
