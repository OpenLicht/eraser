package de.tudresden.inf.st.eraser.feedbackloop.api;

import de.tudresden.inf.st.eraser.jastadd.model.Activity;
import de.tudresden.inf.st.eraser.jastadd.model.Root;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Second phase in the MAPE feedback loop, analyze recent changes w.r.t. their relevance.
 *
 * @author rschoene - Initial contribution
 */
public interface Analyze {

  ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

  void setKnowledgeBase(Root knowledgeBase);

  void setPlan(Plan plan);

  Plan getPlan();

  void analyzeLatestChanges();

  default void startAsThread(long delay, TimeUnit unit) {
    executorService.scheduleWithFixedDelay(this::analyzeLatestChanges, 0, delay, unit);
  }

  default void stop() {
    executorService.shutdownNow();
  }

  default void informPlan(Activity activity) {
    getPlan().planToMatchPreferences(activity);
  }
}
