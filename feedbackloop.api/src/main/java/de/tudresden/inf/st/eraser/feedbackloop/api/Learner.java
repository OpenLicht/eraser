package de.tudresden.inf.st.eraser.feedbackloop.api;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.encog.util.arrayutil.NormalizedField;

import de.tudresden.inf.st.eraser.jastadd.model.Root;

/**
 * 
 * Learner which handles the training and retraining of neural networks 
 * and models used for activity recognition and  preference learning.
 * 
 * @author Bierzyns - initial contribution
 * 
 * */
public interface Learner {

	void setKnowledgeBase(Root knowledgeBase);
	
	/**
	 * Method for loading data set, which used for initial training. This method exists only for development purposes. The csvFolderPath 
	 * variable should be set to ensure that the data is read from the correct place.
	 * 
	 * @param dataSetName - name of data set that is loaded from the data set folder e.g. data1.csv
	 * @param targetColumns - number of the columns that contain label of the corresponding csv data set
	 * @param modelID - ID of the model that will be trained with this data set
	 * @return true - data set loading was successful
	 * */
	boolean loadDataSet(String dataSetName, List<Integer> targetColumns, int modelID);

	/**
	 * Method for loading a neural network from a file.
	 * Please note that the normalizer are note loaded file , because it is assumed that the mins and maxes are saved anyway in the meta data of the data sets or items.
	 *
	 * @param file       file to load the model from
	 * @param modelID - ID of the BasicNetwork.
	 * @param inputMaxes - list that contains max values of all input columns (sensors) e.g. light intensity 100
	 * @param inputMins - list that contains min values of all input columns (sensors) e.g. light intensity 0
	 * @param targetMaxes - list that contains max values of all output columns (results) e.g. brigthness 100 for preference learning
	 * @param targetMins - list that contains min values of all output columns (results) e.g. brigthness 0 for preference learning
	 * @return true - model loading was successful
	 * */
	boolean loadModelFromFile(File file, int modelID, List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
	                          List<Integer> targetMins);

	/**
     * Method for loading a neural network from an input stream.
     * Please note that the normalizer are note loaded file , because it is assumed that the mins and maxes are saved anyway in the meta data of the data sets or items.
     *
   	 * @param input       stream to load the model from
     * @param modelID - ID of the BasicNetwork.
     * @param inputMaxes - list that contains max values of all input columns (sensors) e.g. light intensity 100
     * @param inputMins - list that contains min values of all input columns (sensors) e.g. light intensity 0
     * @param targetMaxes - list that contains max values of all output columns (results) e.g. brigthness 100 for preference learning 
     * @param targetMins - list that contains min values of all output columns (results) e.g. brigthness 0 for preference learning 
     * @return true - model loading was successful
     * */
	boolean loadModelFromFile(InputStream input, int modelID, List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
	                          List<Integer> targetMins);
	
	/**
	 * Method for the initial training of algorithms and models. That uses external data set for training.
	 * 
	 * @param inputCount - number of neurons in the input layer [here to simplify code reading]
     * @param outputCount - number of neurons in the output layer [here to simplify code reading]
     * @param hiddenCount -number of hidden layers in the network
     * @param hiddenNeuronCount - number of neurons in the hidden layers for now 
     * @param modelID - ID of the BasicNetwork.
     * @param inputMaxes - list that contains max values of all input columns (sensors) e.g. light intensity 100
     * @param inputMins - list that contains min values of all input columns (sensors) e.g. light intensity 0
     * @param targetMaxes - list that contains max values of all output columns (results) e.g. brigthness 100 for preference learning 
     * @param targetMins - list that contains min values of all output columns (results) e.g. brigthness 0 for preference learning 
     * 
	 * @return true - training of the model was successful started 
	 * */
	boolean train(int inputCount, int outputCount, int hiddenCount, int hiddenNeuronCount, int modelID,
            List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
            List<Integer> targetMins);
	
	/**
	 * Method for the initial training of algorithms and models. That uses data set provided by Knowledge Base. 
	 * 
	 * @param data - data set on which the training is based on
	 * @param inputCount - number of neurons in the input layer [here to simplify code reading]
     * @param outputCount - number of neurons in the output layer [here to simplify code reading]
     * @param hiddenCount - number of hidden layers in the network
     * @param hiddenNeuronCount - number of neurons in the hidden layers for now 
     * @param modelID - ID of the BasicNetwork.
     * @param inputMaxes - list that contains max values of all input columns (sensors) e.g. light intensity 100
     * @param inputMins - list that contains min values of all input columns (sensors) e.g. light intensity 0
     * @param targetMaxes - list that contains max values of all output columns (results) e.g. brigthness 100 for preference learning 
     * @param targetMins - list that contains min values of all output columns (results) e.g. brigthness 0 for preference learning 
	 * @return true - training of the model was successful started 
	 * */
	boolean train(double[][] data, int inputCount, int outputCount, int hiddenCount, int hiddenNeuronCount, int modelID,
            List<Integer> inputMaxes, List<Integer> inputMins, List<Integer> targetMaxes,
            List<Integer> targetMins, List<Integer> targetColumns);
	
	/**
	 * Method for adapting existing models.
	 * 
	 * @param data - data set on which the retraining is based on
	 * @param targetColumns - number of the columns thta contain labels/target
	 * @param modelID - ID of the model that will be retrained
	 * @return true - retraining of model was started successfully
	 * */
	boolean reTrain(double[][] data, List<Integer> targetColumns, int modelID);
	
	/**
	 * Method for getting the information about an trained model.
	 * 
	 * @param modelID - ID of the model of which information is requested from
	 * @return Model - Object that contains the information of the requested model  
	 * */
	EncogModel getTrainedModel(int modelID);

	/**
	 * 
	 * Method for getting normalizer of a model for a specific column/input.
	 * 
	 * @param modelID - ID of the model of which normalizer is requested from
	 * @param columnNr - number of columns the normalizer can be used for
	 * 
	 * @return {@link NormalizedField} - the normalizer
	 * */
	NormalizedField getNormalizerInput(int modelID, int columnNr);

}
