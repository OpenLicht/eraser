package de.tudresden.inf.st.eraser.feedbackloop.api;

import lombok.Getter;
import lombok.Setter;
import org.encog.neural.networks.layers.Layer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 
 * This class represents an object that contains all information of a trained neural network.
 * 
 * For now: weights and the {@link Layer}s
 * 
 * @author Bierzyns - initial contribution
 * */
@Getter
@Setter
public class EncogModel {
	/**
	 * todo
	 */
	private String modelType;
	private List<Double> weights;
	private List<Layer> layers;

	public EncogModel(String model) {
		modelType = model;
	}

	public Layer getInputLayer() {
		return Objects.requireNonNull(layers, "Layers not set yet").get(0);
	}

	public Layer getOutputLayer() {
		return Objects.requireNonNull(layers, "Layers not set yet").get(layers.size() - 1);
	}

	public List<Layer> getHiddenLayers() {
		return Objects.requireNonNull(layers, "Layers not set yet").subList(1, layers.size() - 1);
	}
}
