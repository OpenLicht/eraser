package de.tudresden.inf.st.eraser.feedbackloop.api;

import de.tudresden.inf.st.eraser.commons.color.ColorUtils.ValuesRGB;
import de.tudresden.inf.st.eraser.jastadd.model.ItemUpdate;
import de.tudresden.inf.st.eraser.jastadd.model.Root;
import de.tudresden.inf.st.eraser.util.Tuple;

import java.util.Map;

/**
 * Fourth phase in the MAPE feedback loop, executing adaptation actions.
 *
 * @author rschoene - Initial contribution
 */
public interface Execute {

  /**
   * Setter for the knowledge base to use
   * @param knowledgeBase the knowledge base to use
   */
  void setKnowledgeBase(Root knowledgeBase);

  /**
   * Updates items according to given updates
   * @param updates tuples containing item and its new HSB value
   */
  void updateItems(Iterable<ItemUpdate> updates);
}
