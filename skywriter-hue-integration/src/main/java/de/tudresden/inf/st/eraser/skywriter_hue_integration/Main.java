package de.tudresden.inf.st.eraser.skywriter_hue_integration;

import beaver.Parser;
import de.tudresden.inf.st.eraser.commons.color.ColorUtils;
import de.tudresden.inf.st.eraser.commons.color.ColorUtils.ValuesRGB;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.openhab2.mqtt.MQTTUpdater;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

  private static Logger logger = LogManager.getLogger(Main.class);

  /** Use thread to make random edits */
  private static final boolean USE_RANDOM_EDITS = false;
  /** Time to wait between random edits (only used, if USE_RANDOM_EDITS is true) */
  private static final int STD_WAIT_IN_MILLI_SECONDS = 2 * 1000;
  /** Use thread to read changes from openHAB */
  private static final boolean USE_READ_FROM_OPENHAB = true;
  /** Use real MQTT sender to propagate changes to openHAB */
  private static final boolean SEND_TO_OPENHAB = true;
  /** Host of MQTT broker (only used, if USE_READ_FROM_OPENHAB is true) */
  private static final String MQTT_HOST = "192.168.1.250";

  // ignore, that result of System.in.read() is not used
  @SuppressWarnings("ResultOfMethodCallIgnored")
  public static void main(String[] args) throws IOException, Parser.Exception {
    // use openHAB-eraser-connection to update hue (automatically done)
    Root root = ParserUtils.load("skywriter-hue.eraser", Main.class);
    SmartHomeEntityModel model = root.getSmartHomeEntityModel();
    Item irisItem = model.resolveItem("iris1_item").orElseThrow(() ->
        new NoSuchElementException("Iris1_item not found"));
    Item skywriter1_x = model.resolveItem("skywriter1_x").orElseThrow(() ->
        new NoSuchElementException("Skywriter1 x not found"));
    Item skywriter1_y = model.resolveItem("skywriter1_y").orElseThrow(() ->
        new NoSuchElementException("Skywriter1 y not found"));
    Item skywriter1_xyz = model.resolveItem("skywriter1_xyz").orElseThrow(() ->
        new NoSuchElementException("Skywriter1 xyz not found"));
    System.out.println(root.prettyPrint());
    // define rule to switch color, based on xyz-to-rgb-to-hsb mapping
    Rule mapXYtoIrisState = new Rule();
    root.addRule(mapXYtoIrisState);
    mapXYtoIrisState.activateFor(skywriter1_xyz);
//    mapXYtoIrisState.addEventFor(skywriter1_y, "y changed");
    SetStateFromItemsAction action = new SetStateFromItemsAction();
    action.addSourceItem(skywriter1_xyz);
    action.setCombinator(items -> updateStateRGB(items.iterator().next()));
    action.setAffectedItem(irisItem);
    mapXYtoIrisState.addAction(action);

    // get mqtt message directly from skywriter, not via binding (which is not working atm)
    Lock abortLock = new ReentrantLock();
    Condition abortCondition = abortLock.newCondition();
    Thread readFromOpenHABThread = new Thread(() -> {
      try (MQTTUpdater updater = new MQTTUpdater(root)) {
        updater.start();
        if (!updater.waitUntilReady(3, TimeUnit.SECONDS)) {
          logger.error("openHAB reader not ready. Aborting.");
          return;
        }
        try {
          logger.debug("oh-read, attempt to lock");
          abortLock.lock();
          abortCondition.await();
        } catch (InterruptedException e) {
          logger.info("Premature exit of openHAB reader thread!");
        } finally {
          abortLock.unlock();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    if (USE_READ_FROM_OPENHAB || SEND_TO_OPENHAB) {
      root.getMqttRoot().setHostByName(MQTT_HOST);
    }
    if (USE_READ_FROM_OPENHAB) {
      readFromOpenHABThread.start();
    }

    Thread randomEditsThread = new Thread(() -> {
      logger.debug("RandEdit starting");
      Random rand = new Random();
      try {
        while (true) {
          try {
            abortLock.lock();
            // combined two things here. 1) wait STD_WAIT_IN_MILLI_SECONDS between random changes
            // and 2) abort early if abortCondition is met
            if (abortCondition.await(STD_WAIT_IN_MILLI_SECONDS, TimeUnit.MILLISECONDS)) {
              break;
            }
          } finally {
            abortLock.unlock();
          }
          boolean changeX = rand.nextBoolean();
          String randomValue = Double.toString(rand.nextDouble());
          logger.debug("Random {}: {}", changeX ? "X" : "Y", randomValue);
          if (changeX) {
            skywriter1_x.setStateFromString(randomValue);
          } else {
            skywriter1_y.setStateFromString(randomValue);
          }
        }
      } catch (InterruptedException e) {
        logger.info("Premature exit of random edits thread!");
      }
    });
    if (USE_RANDOM_EDITS) {
      logger.debug("Starting RandEdit");
      randomEditsThread.start();
    }

    // wait for user to press enter
    System.out.println("Press [Enter] to quit updating the root.");
    System.in.read();

    // and then cancel the threads
    try {
      abortLock.lock();
      abortCondition.signalAll();
    } finally {
      abortLock.unlock();
    }

    // and join everything
    try {
      randomEditsThread.join();
    } catch (InterruptedException e) {
      logger.catching(e);
    }
    try {
      readFromOpenHABThread.join();
    } catch (InterruptedException e) {
      logger.catching(e);
    }
  }

  private static void updateStateHSB(Item skywriter1_x, Item skywriter1_y, Item irisItem) {
    ValuesRGB rgb = ColorUtils.convertXYtoRGB(Double.parseDouble(skywriter1_x.getStateAsString()),
        Double.parseDouble(skywriter1_y.getStateAsString()), 1.0);
//    irisItem.setState(String.format("%d,%d,%d", rgb.red, rgb.green, rgb.blue));
    ColorUtils.ValuesIntegralHSB hsb = ColorUtils.convertRGBtoHSB(rgb).toIntegral().ensureBounds();
    irisItem.setStateFromString(String.format("%d,%d,%d", hsb.hue, hsb.saturation, hsb.brightness));
  }

  private static String updateStateRGB(Item skywriter1_xyz) {
    String[] tokens = skywriter1_xyz.getStateAsString().split(",");
    int[] rgbArray = new int[3];
    for (int i = 0; i < 3; i++) {
      double xyzToken = Double.parseDouble(tokens[i]);
      rgbArray[i] = (int) Math.round(255 * xyzToken);
    }
    ColorUtils.ValuesIntegralHSB hsb = ColorUtils.convertRGBtoHSB(ValuesRGB.of(rgbArray))
        .toIntegral().ensureBounds();
    return String.format("%d,%d,%d", hsb.hue, hsb.saturation, hsb.brightness);
  }
}
