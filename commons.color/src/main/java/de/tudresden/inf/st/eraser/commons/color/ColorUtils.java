package de.tudresden.inf.st.eraser.commons.color;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.awt.*;
import java.util.function.Consumer;

/**
 * Convenience conversion methods between XY, RGB and HSB.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("WeakerAccess")
public class ColorUtils {

  /**
   * Value class comprising three double values X, Y, Z ranging from 0 to 1.
   * The values represent coordinates.
   */
  public static class ValuesXYZ {
    double x, y, z;
    public static ValuesXYZ of(double x, double y, double z) {
      ValuesXYZ result = new ValuesXYZ();
      result.x = x;
      result.y = y;
      result.z = z;
      return result;
    }
  }

  /**
   * Value class comprising three integral values R, G, B ranging from 0 to 255.
   * The values represent red, green and blue.
   */
  public static class ValuesRGB {
    public int red;
    public int green;
    public int blue;
    public static ValuesRGB of(int red, int green, int blue) {
      ValuesRGB result = new ValuesRGB();
      result.red = red;
      result.green = green;
      result.blue = blue;
      return result;
    }
    public static ValuesRGB of(int[] rgb) {
      return of(rgb[0], rgb[1], rgb[2]);
    }
    /** Set all values to a minimum of zero. */
    public ValuesRGB minZero() {
      red = Math.max(0, red);
      green = Math.max(0, green);
      blue = Math.max(0, blue);
      return this;
    }
  }

  /**
   * Value class comprising three double values H, S, B ranging from 0 to 1.
   * The values represent hue, saturation and brightness.
   */
  public static class ValuesHSB {
    public double hue;
    public double saturation;
    public double brightness;
    public static ValuesHSB of(double hue, double saturation, double brightness) {
      ValuesHSB result = new ValuesHSB();
      result.hue = hue;
      result.saturation = saturation;
      result.brightness = brightness;
      return result;
    }
    /** Set all values to a minimum of zero. */
    public ValuesHSB minZero() {
      hue = Math.max(0, hue);
      saturation = Math.max(0, saturation);
      brightness = Math.max(0, brightness);
      return this;
    }
    /** Convert to the same HSB with integral values */
    public ValuesIntegralHSB toIntegral() {
      return ValuesIntegralHSB.of(
          (int) Math.round(360 * hue),
          (int) Math.round(100 * saturation),
          (int) Math.round(100 * brightness)
      );
    }
  }

  /**
   * Value class comprising three integral values H, S, B ranging from 0 either to 360 (H) or 100 (S and B).
   * The values represent hue, saturation and brightness.
   */
  public static class ValuesIntegralHSB {
    public int hue;
    public int saturation;
    public int brightness;
    public static ValuesIntegralHSB of(int hue, int saturation, int brightness) {
      ValuesIntegralHSB result = new ValuesIntegralHSB();
      result.hue = hue;
      result.saturation = saturation;
      result.brightness = brightness;
      return result;
    }
    /** Set all values to a minimum of zero, and a maximum of either 360 (for H) or 100 (for S and B). */
    public ValuesIntegralHSB ensureBounds() {
      hue = Math.min(Math.max(0, hue), 360);
      saturation = Math.min(Math.max(0, saturation), 100);
      brightness = Math.min(Math.max(0, brightness), 100);
      return this;
    }
  }

  /**
   XYZ to RGB [M]-1<br>
    2.0413690 -0.5649464 -0.3446944<br>
   -0.9692660  1.8760108  0.0415560<br>
    0.0134474 -0.1183897  1.0154096<br>
   */
  private static double[][] matrixData = { { 2.0413690, -0.5649464, -0.3446944},
      {-0.9692660,  1.8760108,  0.0415560},
      { 0.0134474, -0.1183897,  1.0154096}};
  private static RealMatrix mInverted = MatrixUtils.createRealMatrix(matrixData);

  public static ValuesRGB convertXYtoRGB(ValuesXYZ values) {
    return convertXYtoRGB(values.x, values.y, values.z);
  }

  public static ValuesRGB convertXYtoRGB(double x, double y, double z) {
    RealMatrix xyz = MatrixUtils.createColumnRealMatrix(new double[] {x, y, z});
    RealMatrix result = mInverted.multiply(xyz);
    double[][] rgb = result.getData();
    return ValuesRGB.of(
        (int) Math.round(255 * rgb[0][0]),  // red
        (int) Math.round(255 * rgb[1][0]),  // green
        (int) Math.round(255 * rgb[2][0])); // blue
  }

  public static void convertXYtoRGB(double x, double y, double z, Consumer<Integer> setRed,
                                    Consumer<Integer> setGreen, Consumer<Integer> setBlue) {
    ValuesRGB result = convertXYtoRGB(x, y, z);
    setRed.accept(result.red);
    setGreen.accept(result.green);
    setBlue.accept(result.blue);
  }

  public static ValuesHSB convertRGBtoHSB(ValuesRGB values) {
    return convertRGBtoHSB(values.red, values.green, values.blue);
  }

  public static ValuesHSB convertRGBtoHSB(int red, int green, int blue) {
    float[] hsb = Color.RGBtoHSB(red, green, blue, null);
    return ValuesHSB.of(hsb[0], hsb[1], hsb[2]);
  }

  public static void convertRGBtoHSB(int red, int green, int blue, Consumer<Double> setHue,
                                     Consumer<Double> setSaturation, Consumer<Double> setBrightness) {
    ValuesHSB values = convertRGBtoHSB(red, green, blue);
    setHue.accept(values.hue);
    setSaturation.accept(values.saturation);
    setBrightness.accept(values.brightness);
  }

}
