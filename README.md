# ERASER - OpenLicht Knowledge Base

## Overview

There are the following subprojects in this repository:

- Base
	- **eraser-base**: The core part of the framework containing the [grammar](/../../blob/main/eraser-base/src/main/jastadd/main.relast), various attributes, the [parser](/../../blob/main/eraser-base/src/main/jastadd/eraser.parser) for model specification files, and Java classes for the [openHAB communication](/../../tree/main/eraser-base/src/main/java/de/tudresden/inf/st/eraser/openhab2) via [MQTT](/../../blob/main/eraser-base/src/main/java/de/tudresden/inf/st/eraser/openhab2/mqtt/MQTTUpdater.java)
- Utility
	- **commons.color**: Utilities for converting color spaces (XYZ, RGB, HSB)
	- **eraser.rest** and **eraser.spark**: REST-API to communicate with the framework
	- **eraser.starter**: The main entry point to start the application
	- **feedbackloop.\***: Component specification and implementation of the MAPE-K feedback loop
	- **project-template**: Template to quickly create new subprojects used by `make-new-project.py`
- Demos
	- **integration** and **openhab-mock**: Playground projects testing some features
	- **skywriter-hue-integration**: The first real demonstrator integrating the [Skywriter gesture recogintion](http://docs.pimoroni.com/skywriter) with a [Hue](http://www.meethue.com/) using a simple rule
	- **benchmark**: Small benchmark to replay previously recoreded sensor events
- Machine Learning Integration (deprecated)
	- org.openhab.action.machinelearn: openHAB connector of the Machine Learning Part
	- org.openlicht.action.reinforcementlearning: Reinforcement-Learning of preferences
	- stub.org.openhab.core.scriptengine.action: Stub to make the other two projects build

## Setup and Building

This project uses Gradle as the build tool. For detailed information, see [setup guidelines](/../../wikis/setup)

## Trivia

The name *Eraser* is a small pun on the term *eRACR*, which is an extension of *RACR* for event recognition. It was chosen to ease the search for a good repository image as it is based on a [film](https://en.wikipedia.org/wiki/Eraser_(film)).
