package de.tudresden.inf.st.eraser.starter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Setting bean.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("WeakerAccess")
class Setting {
  private final static Logger logger = LogManager.getLogger(Setting.class);
  public class Rest {
    /** Start the REST server. Default: true. */
    public boolean use = true;
    /** Port of the REST server. Default: 4567. */
    public int port = 4567;
    /** Add some dummy data for activities and events. Only effective when using the REST server. Default: false. */
    public boolean createDummyMLData = false;
  }
  public class FileContainer {
    public String file;
    /** Whether the file is external (not shipped with the JAR). Default: false. */
    public boolean external = false;
    /** Get the URL to the {@link #file} */
    URL realURL() {
      if (external) {
        try {
          return Paths.get(file).toUri().normalize().toURL();
        } catch (MalformedURLException e) {
          logger.catching(e);
          return null;
        }
      } else {
        return Setting.class.getClassLoader().getResource(file);
      }
    }
  }
  public class MLContainer extends FileContainer {
    /** Factory class to instantiate machine learning handlers */
    public String factory;
    /** Use dummy model in which the current activity is directly editable. Default: false. */
    public boolean dummy = false;
    /** Model id. Default: 1.*/
    public int id = 1;
    public boolean abortOnError = false;
  }
  public class OpenHabContainer {
    /** The URL from which to import and at which openHAB is running */
    public String url;
    /** The metadata namespace for items */
    public String metadataNamespace;
  }
  public enum InitModelWith {
    /** Load the initial model from a file */
    load,
    /** Load the initial model by importing from openHAB */
    openhab
  }
  public Rest rest;
  /** Start the feedback loop. Default: true. */
  public boolean useMAPE = true;
  /** Initialize the knowledge base with a file.
   OpenHAB synchronization is done if MQTT url is set in the processed file.
   <br>
   child "file": File to read in. Expected format = eraser
   */
  public FileContainer load;
  /** Model for activity recognition. If dummy is true, then the file parameter is ignored. */
  public MLContainer activity;
  /** Model for preference learning. If dummy is true, then the file parameter is ignored. */
  public MLContainer preference;
  /** Initialize the knowledge base by importing data from openHAB. */
  public OpenHabContainer openhab;
  /** Get updates from openHAB into the knowledge base. Default: true. */
  public boolean mqttUpdate = true;
  /** Method to initialize model. Possible values: "load", "openhab". Default: "load". */
  public InitModelWith initModelWith = InitModelWith.load;
}
