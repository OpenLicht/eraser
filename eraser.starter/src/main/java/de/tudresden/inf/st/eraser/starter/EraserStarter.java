package de.tudresden.inf.st.eraser.starter;

import beaver.Parser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.tudresden.inf.st.eraser.feedbackloop.analyze.AnalyzeImpl;
import de.tudresden.inf.st.eraser.feedbackloop.api.Analyze;
import de.tudresden.inf.st.eraser.feedbackloop.api.Execute;
import de.tudresden.inf.st.eraser.feedbackloop.api.Plan;
import de.tudresden.inf.st.eraser.feedbackloop.execute.ExecuteImpl;
import de.tudresden.inf.st.eraser.feedbackloop.plan.PlanImpl;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.openhab2.OpenHab2Importer;
import de.tudresden.inf.st.eraser.openhab2.mqtt.MQTTUpdater;
import de.tudresden.inf.st.eraser.spark.Application;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.annotation.Arg;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget.ACTIVITY_RECOGNITION;
import static de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget.PREFERENCE_LEARNING;

/**
 * This Starter combines and starts all modules. This includes:
 *
 * <ul>
 *   <li>Knowledge-Base in <code>eraser-base</code></li>
 *   <li>Feedback loop in <code>feedbackloop.{analyze,plan,execute}</code></li>
 *   <li>REST-API in <code>eraser-rest</code></li>
 * </ul>
 */
public class EraserStarter {

  private static class CommandLineOptions {
    @Arg(dest = "config_file")
    String configFile;
  }

  private static final Logger logger = LogManager.getLogger(EraserStarter.class);

  public static void main(String[] args) {
    EraserStarter starter = new EraserStarter();
    starter.run(args);
  }

  public void run(String[] args) {
    ArgumentParser parser = ArgumentParsers.newFor("eraser").build()
        .defaultHelp(true)
        .description("Starts the knowledge-base of OpenLicht");
    parser.addArgument("-f", "--config-file")
        .help("Path to the configuration YAML file")
        .setDefault("starter-setting.yaml");
    CommandLineOptions commandLineOptions = new CommandLineOptions();
    try {
      parser.parseArgs(args, commandLineOptions);
    } catch (ArgumentParserException e) {
      parser.handleError(e);
      System.exit(1);
    }
    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    File settingsFile = new File(commandLineOptions.configFile);
    Setting settings;
    try {
      settings = mapper.readValue(settingsFile, Setting.class);
    } catch (Exception e) {
      logger.fatal("Could not read settings at '{}'. Exiting.", settingsFile.getAbsolutePath());
      logger.catching(e);
      System.exit(1);
      return;
    }
    logger.info("Starting ERASER " + readVersion());
    boolean startRest = settings.rest.use;

    Root root;
    SmartHomeEntityModel model;
    switch (settings.initModelWith) {
      case openhab:
        OpenHab2Importer importer = new OpenHab2Importer();
        try {
          model = importer.importFrom(new URL(settings.openhab.url));
          root = model.getRoot();
        } catch (MalformedURLException e) {
          logger.error("Could not parse URL {}", settings.openhab.url);
          logger.catching(e);
          System.exit(1);
          return;
        }
        logger.info("Imported model {}", model.description());
        break;
      case load:
      default:
        try {
          root = ParserUtils.load(settings.load.realURL());
          root.getSmartHomeEntityModel();
        } catch (IOException | Parser.Exception e) {
          logger.error("Problems parsing the given file {}", settings.load.file);
          logger.catching(e);
          System.exit(1);
          return;
        }
    }

    MachineLearningHandlerFactory activityFactory = createFactory(ACTIVITY_RECOGNITION, settings.activity, root);
    MachineLearningHandlerFactory preferenceFactory = createFactory(PREFERENCE_LEARNING, settings.preference, root);

    // initialize activity recognition
    MachineLearningRoot machineLearningRoot = root.getMachineLearningRoot();
    root.getMachineLearningRoot().setActivityRecognition(activityFactory.createModel());
    root.getMachineLearningRoot().setPreferenceLearning(preferenceFactory.createModel());

//    machineLearningRoot.getPreferenceLearning().connectItems(settings.preference.items);
    if (!machineLearningRoot.getActivityRecognition().check()) {
      logger.fatal("Invalid activity recognition!");
      System.exit(1);
    }
    if (!machineLearningRoot.getPreferenceLearning().check()) {
      logger.fatal("Invalid preference learning!");
      System.exit(1);
    }

    Lock lock = new ReentrantLock();
    Condition quitCondition = lock.newCondition();

    Analyze analyze = null;
    if (settings.useMAPE) {
      // configure and start mape loop
      logger.info("Starting MAPE loop");
      analyze = new AnalyzeImpl();
      Plan plan = new PlanImpl();
      Execute execute = new ExecuteImpl();

      analyze.setPlan(plan);
      plan.setExecute(execute);

      analyze.setKnowledgeBase(root);
      plan.setKnowledgeBase(root);
      execute.setKnowledgeBase(root);

      analyze.startAsThread(1, TimeUnit.SECONDS);
    } else {
      logger.info("No MAPE loop this time");
    }

    if (settings.mqttUpdate) {
      logger.info("Starting MQTT updater");
      Thread t = new Thread(() -> {
        try (MQTTUpdater updater = new MQTTUpdater(root)) {
          updater.start();
          updater.waitUntilReady(5, TimeUnit.SECONDS);
          lock.lock();
          quitCondition.await();
        } catch (IOException | InterruptedException e) {
          logger.catching(e);
        } finally {
          lock.unlock();
        }
        logger.info("MQTT update stopped");
      }, "MQTT-Updater");
      t.setDaemon(true);
      t.start();
    }

    if (startRest) {
      // start REST-API in new thread
      logger.info("Starting REST server");
      Thread t = new Thread(
          () -> Application.start(settings.rest.port, root, settings.rest.createDummyMLData, lock, quitCondition),
          "REST-API");
      t.setDaemon(true);
      t.start();
      logger.info("Waiting until POST request is send to 'http://localhost:{}/system/exit'", settings.rest.port);
      try {
        lock.lock();
        if (t.isAlive()) {
          quitCondition.await();
        }
      } catch (InterruptedException e) {
        logger.warn("Waiting was interrupted");
      } finally {
        lock.unlock();
      }
    } else {
      logger.info("No REST server this time");
      System.out.println("Hit [Enter] to exit");
      try {
        //noinspection ResultOfMethodCallIgnored
        System.in.read();
      } catch (IOException e) {
        e.printStackTrace();
      }
      System.out.println("Stopping...");
      try {
        lock.lock();
        quitCondition.signalAll();
      } finally {
        lock.unlock();
      }
    }
    if (analyze != null) {
      analyze.stop();
    }
    activityFactory.shutdown();
    preferenceFactory.shutdown();
    InfluxAdapter influxAdapter = root.getInfluxRoot().influxAdapter();
    if (influxAdapter != null) {
      try {
        influxAdapter.close();
      } catch (Exception e) {
        logger.catching(e);
      }
    }
    logger.info("I'm done here.");
  }

  private MachineLearningHandlerFactory createFactory(MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget target, Setting.MLContainer config, Root root) {
    MachineLearningHandlerFactory factory;
    String niceTargetName = target.toString().toLowerCase().replace("_", " ");
    if (config.dummy || config.factory == null) {
      logger.info("Using dummy {}, ignoring other settings for this", niceTargetName);
      factory = new DummyMachineLearningHandlerFactory();
    } else {
      try {
        Class<? extends MachineLearningHandlerFactory> clazz = Class.forName(config.factory)
            .asSubclass(MachineLearningHandlerFactory.class);
        factory = clazz.getDeclaredConstructor().newInstance();
        factory.setKnowledgeBaseRoot(root);
        factory.initializeFor(target, config.realURL());
      } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IOException |
          NoSuchMethodException | InvocationTargetException e) {
        logger.error("Could not instantiate machine learning factory for {} with class '{}'.",
            niceTargetName, config);
        logger.catching(e);
        if (config.abortOnError) {
          logger.fatal("Aborting now as specified in config.");
          System.exit(1);
          return null;
        }
        logger.warn("Using error factory for {}.", niceTargetName);
        factory = MachineLearningHandlerFactory.createErrorFactory();
      }
    }
    return factory;
  }

  private String readVersion() {
    try {
      ResourceBundle resources = ResourceBundle.getBundle("eraser");
      return resources.getString("version");
    } catch (MissingResourceException e) {
      return "version ?";
    }
  }

}
