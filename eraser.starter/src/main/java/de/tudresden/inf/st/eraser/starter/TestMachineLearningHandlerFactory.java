package de.tudresden.inf.st.eraser.starter;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * Testing preference setting
 *
 * @author rschoene - Initial contribution
 */
public class TestMachineLearningHandlerFactory extends MachineLearningHandlerFactory {

  private static final Logger logger = LogManager.getLogger(TestMachineLearningHandlerFactory.class);
  private double offset = 0;
  private double multiplier = 1;

  @Override
  public void initializeFor(MachineLearningHandlerFactoryTarget target, URL configUrl) {
    if (target != MachineLearningHandlerFactoryTarget.PREFERENCE_LEARNING) {
      logger.error("Test Factory only made for preference learning, not {}", target);
    }
    // treat given file as properties file
    try (InputStream input = configUrl.openStream()) {
      Properties prop = new Properties();
      prop.load(input);
      offset = Double.parseDouble(prop.getProperty("offset", "0"));
      multiplier = Double.parseDouble(prop.getProperty("multiplier", "1"));
    } catch (IOException | IllegalArgumentException e) {
      logger.catching(e);
    }
  }

  @Override
  public MachineLearningEncoder createEncoder() {
    return null;
  }

  @Override
  public MachineLearningDecoder createDecoder() {
    return null;
  }

  @Override
  public MachineLearningModel createModel() {
    DummyMachineLearningModel result = DummyMachineLearningModel.createDefault();
    for (Item item : knowledgeBase.getSmartHomeEntityModel().items()) {
      if (item.isColorItem()) {
        ItemUpdateDouble update = new ItemUpdateDouble() {
          // dirty hack to override value based on activity during runtime
          @Override
          public double getNewValue() {
            Activity activity = knowledgeBase.currentActivity().orElse(knowledgeBase.getMachineLearningRoot().getActivity(0));
            return offset + activity.getIdentifier() * multiplier;
          }
        };
        update.setItem(item);
        result.getCurrent().addPreference(update);
      }
    }
    return result;
  }
}
