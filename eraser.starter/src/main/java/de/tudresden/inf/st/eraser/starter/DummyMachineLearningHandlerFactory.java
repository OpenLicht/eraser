package de.tudresden.inf.st.eraser.starter;

import de.tudresden.inf.st.eraser.jastadd.model.*;

import java.net.URL;

/**
 * Factory to create dummy handlers.
 *
 * @author rschoene - Initial contribution
 */
public class DummyMachineLearningHandlerFactory extends MachineLearningHandlerFactory {

  private final DummyMachineLearningModel model = DummyMachineLearningModel.createDefault();

  @Override
  public void initializeFor(MachineLearningHandlerFactoryTarget target, URL configUrl) {
    // not needed
  }

  @Override
  public MachineLearningEncoder createEncoder() {
    // not needed
    return null;
  }

  @Override
  public MachineLearningDecoder createDecoder() {
    // not needed
    return null;
  }

  @Override
  public MachineLearningModel createModel() {
    return model;
  }
}
