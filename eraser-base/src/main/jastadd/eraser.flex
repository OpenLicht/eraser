package de.tudresden.inf.st.eraser.jastadd.scanner;

import de.tudresden.inf.st.eraser.jastadd.parser.EraserParser.Terminals;

%%

// define the signature for the generated scanner
%public
%final
%class EraserScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
  private beaver.Symbol symText(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext().substring(1, yytext().length() - 1));
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r | \r\n
Identifier = [:jletter:][:jletterdigit:]*
Text = \" ([^\"]*) \"

Integer = [:digit:]+ // | "+" [:digit:]+ | "-" [:digit:]+
Real    = [:digit:]+ "." [:digit:]* | "." [:digit:]+

Comment = "//" [^\n\r]+

%%

// discard whitespace information and comments
{WhiteSpace}  { }
{Comment}     { }

// ** token definitions **
// Begin of line with capital letter
"Thing"        { return sym(Terminals.THING); }
"Item"         { return sym(Terminals.ITEM); }
"Group"        { return sym(Terminals.GROUP); }
"ThingType"    { return sym(Terminals.THING_TYPE); }
"Parameter"    { return sym(Terminals.PARAMETER); }
"ChannelType"  { return sym(Terminals.CHANNEL_TYPE); }
"Channel"      { return sym(Terminals.CHANNEL); }
"Mqtt"         { return sym(Terminals.MQTT); }
"Influx"       { return sym(Terminals.INFLUX); }
"ML"           { return sym(Terminals.ML); }
"Rule"         { return sym(Terminals.RULE); }
"SyncState"    { return sym(Terminals.SYNC_STATE); }
// special items (group already has a token definition)
"Activity"     { return sym(Terminals.ACTIVITY); }
"Color"        { return sym(Terminals.COLOR); }
"Contact"      { return sym(Terminals.CONTACT); }
"DateTime"     { return sym(Terminals.DATE_TIME); }
"Dimmer"       { return sym(Terminals.DIMMER); }
"Image"        { return sym(Terminals.IMAGE); }
"Location"     { return sym(Terminals.LOCATION); }
"Number"       { return sym(Terminals.NUMBER); }
"Player"       { return sym(Terminals.PLAYER); }
"RollerShutter"  { return sym(Terminals.ROLLER_SHUTTER); }
"String"       { return sym(Terminals.STRING); }
"Switch"       { return sym(Terminals.SWITCH); }
// within specification
"activities"   { return sym(Terminals.ACTIVITIES); }
"aggregation"  { return sym(Terminals.AGGREGATION); }
"category"     { return sym(Terminals.CATEGORY); }
"channels"     { return sym(Terminals.CHANNELS); }
"channelTypes" { return sym(Terminals.CHANNEL_TYPES); }
"context"      { return sym(Terminals.CONTEXT); }
"dbName"       { return sym(Terminals.DB_NAME); }
"default"      { return sym(Terminals.DEFAULT); }
"description"  { return sym(Terminals.DESCRIPTION); }
"groups"       { return sym(Terminals.GROUPS); }
"host"         { return sym(Terminals.HOST); }
"id"           { return sym(Terminals.ID); }
"incoming"     { return sym(Terminals.INCOMING); }
"items"        { return sym(Terminals.ITEMS); }
"itemType"     { return sym(Terminals.ITEM_TYPE); }
"FrequencySetting" { return sym(Terminals.FREQUENCY_SETTING); }
"performance" { return sym(Terminals.PERFORMANCE); }
"procFreq" { return sym(Terminals.PROCESS_FREQUENCY); }
"label"        { return sym(Terminals.LABEL); }
"links"        { return sym(Terminals.LINKS); }
"metaData"     { return sym(Terminals.META_DATA); }
"outgoing"     { return sym(Terminals.OUTGOING); }
"parameters"   { return sym(Terminals.PARAMETERS); }
"password"     { return sym(Terminals.PASSWORD); }
"readOnly"     { return sym(Terminals.READ_ONLY); }
"required"     { return sym(Terminals.REQUIRED); }
"state"        { return sym(Terminals.STATE); }
"topic"        { return sym(Terminals.TOPIC); }
"type"         { return sym(Terminals.TYPE); }
"user"         { return sym(Terminals.USER); }
// special characters
"="            { return sym(Terminals.EQUALS); }
//"\""           { return sym(Terminals.QUOTE); }
"<"            { return sym(Terminals.LT); }
"<="           { return sym(Terminals.LE); }
"=="           { return sym(Terminals.EQ); }
"!="           { return sym(Terminals.NE); }
">="           { return sym(Terminals.GE); }
">"            { return sym(Terminals.GT); }
"+"            { return sym(Terminals.PLUS); }
"*"            { return sym(Terminals.MULT); }
"-"            { return sym(Terminals.MINUS); }
"/"            { return sym(Terminals.DIV); }
"^"            { return sym(Terminals.POW); }
"!"            { return sym(Terminals.EXCLAMATION); }
"|"            { return sym(Terminals.OR); }
"&"            { return sym(Terminals.AND); }
":"            { return sym(Terminals.COLON); }
","            { return sym(Terminals.COMMA); }
";"            { return sym(Terminals.SEMICOLON); }
"["            { return sym(Terminals.LB_SQUARE); }
"]"            { return sym(Terminals.RB_SQUARE); }
"("            { return sym(Terminals.LB_ROUND); }
")"            { return sym(Terminals.RB_ROUND); }
"{"            { return sym(Terminals.LB_CURLY); }
"}"            { return sym(Terminals.RB_CURLY); }
{Identifier}   { return sym(Terminals.NAME); }
{Text}         { return symText(Terminals.TEXT); }
{Integer}      { return sym(Terminals.INTEGER); }
{Real}         { return sym(Terminals.REAL); }
<<EOF>>        { return sym(Terminals.EOF); }
/* error fallback */
[^]            { throw new Error("Illegal character '"+ yytext() +"' at line " + (yyline+1) + " column " + (yycolumn+1)); }
