aspect Expression {

  //--- eval (logical) ---
  syn boolean LogicalExpression.eval();
  eq ParenthesizedLogicalExpression.eval() = getOperand().eval();
  eq NotExpression.eval() = !getOperand().eval();
  eq ComparingExpression.eval() {
    double leftValue = getLeftOperand().eval();
    double rightValue = getRightOperand().eval();
    switch (getComparator()) {
      case LessThan:           return leftValue <  rightValue;
      case LessOrEqualThan:    return leftValue <= rightValue;
      case Equals:             return leftValue == rightValue;
      case NotEquals:          return leftValue != rightValue;
      case GreaterThan:        return leftValue >  rightValue;
      case GreaterOrEqualThan: return leftValue >= rightValue;
    }
    return false;
  }
  eq AndExpression.eval() = getLeftOperand().eval() && getRightOperand().eval();
  eq OrExpression.eval() = getLeftOperand().eval() || getRightOperand().eval();

  //--- eval (number) ---
  syn double NumberExpression.eval();
  eq AddExpression.eval() = getLeftOperand().eval() + getRightOperand().eval();
  eq SubExpression.eval() = getLeftOperand().eval() - getRightOperand().eval();
  eq MultExpression.eval() = getLeftOperand().eval() * getRightOperand().eval();
  eq DivExpression.eval() = getLeftOperand().eval() / getRightOperand().eval();
  eq PowerExpression.eval() = Math.pow(getLeftOperand().eval(), getRightOperand().eval());
  eq Designator.eval() = getItem().getStateAsDouble();
  eq ParenthesizedNumberExpression.eval() = getOperand().eval();
  eq NumberLiteralExpression.eval() = getValue();

  // is-X
  syn boolean Expression.isLogicalExpression() = false;
  eq LogicalExpression.isLogicalExpression() = true;
  syn boolean Expression.isNumberExpression() = false;
  eq NumberExpression.isNumberExpression() = true;
  // as-X
  syn LogicalExpression Expression.asLogicalExpression() = null;
  eq LogicalExpression.asLogicalExpression() = this;
  syn NumberExpression Expression.asNumberExpression() = null;
  eq NumberExpression.asNumberExpression() = this;
}
