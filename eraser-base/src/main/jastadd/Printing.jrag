import java.util.StringJoiner;

aspect Printing {
  syn String ASTNode.prettyPrint() { throw new UnsupportedOperationException(); }

  String ASTNode.safeID(ModelElement elem) { return elem == null ? "NULL" : elem.getID(); }

  eq Root.prettyPrint() {
    StringBuilder sb = new StringBuilder();
    sb.append(getSmartHomeEntityModel().prettyPrint());
    sb.append(getMqttRoot().prettyPrint());
    sb.append(getInfluxRoot().prettyPrint());
    sb.append(getMachineLearningRoot().prettyPrint());
    return sb.toString();
  }

  //--- SmartHomeEntityModel.prettyPrint() ---
  eq SmartHomeEntityModel.prettyPrint() {
    StringBuilder sb = new StringBuilder();
    for (Thing t : getThingList()) {
      sb.append(t.prettyPrint());
    }
    for (Item i : items()) {
      sb.append(i.prettyPrint());
    }
    for (Group g : groups()) {
      sb.append(g.prettyPrint());
    }
    if (unknownGroup().getNumItem() > 0 ) {
      sb.append(unknownGroup().prettyPrint());
    }
    for (ThingType tt : getThingTypeList()) {
      sb.append(tt.prettyPrint());
    }
    for (ChannelType ct : getChannelTypeList()) {
      sb.append(ct.prettyPrint());
    }
    for (Parameter p : parameters()) {
      sb.append(p.prettyPrint());
    }
    for (Channel c : channels()) {
      sb.append(c.prettyPrint());
    }
    for (FrequencySetting fs : getFrequencySettingList()) {
      sb.append(fs.prettyPrint());
    }
    return sb.toString();
  }

  // Thing: id="" label="" type="" channels=["CHANNEL_ID", "CHANNEL_ID"] ;
  eq Thing.prettyPrint() {
    return new MemberPrinter("Thing")
        .addRequired("id", getID())
        .addNonDefault("label", getLabel())
        .addRequired("type", getType(), ThingType::getID)
        .addIds("channels", getChannelList())
        .build();
  }

  // ITEM_TYPE Item: id="" label="" state="" category="" topic="";
  eq Item.prettyPrint() {
    return new MemberPrinter(prettyPrintType())
        .addRequired("id", getID())
        .addNonDefault("label", getLabel())
        .addRequired("state", getStateAsString())
        .addOptional("category", hasCategory(), () -> getCategory().getName())
        .addOptional("topic", hasTopic(), () -> getTopic().getTopicString())
        .addOptionalPrettyPrint(getMetaData())
        .build();
  }

  syn String Item.prettyPrintType();
  eq ColorItem.prettyPrintType() = "Color Item" ;
  eq ContactItem.prettyPrintType() = "Contact Item" ;
  eq DateTimeItem.prettyPrintType() = "DateTime Item" ;
  eq DimmerItem.prettyPrintType() = "Dimmer Item" ;
  eq ImageItem.prettyPrintType() = "Image Item" ;
  eq LocationItem.prettyPrintType() = "Location Item" ;
  eq NumberItem.prettyPrintType() = "Number Item" ;
  eq PlayerItem.prettyPrintType() = "Player Item" ;
  eq RollerShutterItem.prettyPrintType() = "RollerShutter Item" ;
  eq StringItem.prettyPrintType() = "String Item" ;
  eq SwitchItem.prettyPrintType() = "Switch Item" ;
  eq ActivityItem.prettyPrintType() = "Activity Item" ;
  eq DefaultItem.prettyPrintType() = "Item" ;
  eq ItemPrototype.prettyPrintType() = "!! prototype not converted !!" ;

  // special ActivityItem printing. Always omit state.
  eq ActivityItem.prettyPrint() {
    return new MemberPrinter(prettyPrintType())
        .addRequired("id", getID())
        .addNonDefault("label", getLabel())
        .addOptional("category", hasCategory(), () -> getCategory().getName())
        .addOptional("topic", hasTopic(), () -> getTopic().getTopicString())
        .addOptionalPrettyPrint(getMetaData())
        .build();
  }

  // MetaData: metaData={"key": "value", "key": "value"}
  eq MetaData.prettyPrint() {
    if (getNumKeyValuePair() == 0) {
      return "";
    }
    StringJoiner sj = new StringJoiner(", ", " metaData={", "}");
    for (KeyValuePair keyValuePair : getKeyValuePairList()) {
      sj.add("\"" + keyValuePair.getKey() + "\":\"" + keyValuePair.getValue() + "\"");
    }
    return sj.toString();
  }

  // Group: id="" groups=["GROUP_ID", "GROUP_ID"] items=["ITEM_ID", "ITEM_ID"] aggregation=AGG;
  //        AGG either '"agg-name"', or '"agg-name" ("param1", "param2")'
  eq Group.prettyPrint() {
    return new MemberPrinter("Group")
        .addRequired("id", getID())
        .addNonDefault("label", getLabel())
        .addIds("groups", getNumGroup(), getGroups())
        .addIds("items", getNumItem(), getItems())
        .addOptionalPrettyPrint(getAggregationFunction())
        .build();
  }

  eq SimpleGroupAggregationFunction.prettyPrint() {
    if (getFunctionName() == SimpleGroupAggregationFunctionName.EQUALITY) {
      return "";
    }
    return " aggregation=\"" + getFunctionName().name() + "\"";
  }
  eq ParameterizedGroupAggregationFunction.prettyPrint() {
    StringBuilder sb = new StringBuilder(" aggregation=\"");
    sb.append(getFunctionName().name())
      .append("\" (\"").append(getParam1())
      .append("\", \"").append(getParam2()).append("\")");
    return sb.toString();
  }

  // ThingType: id="" label="" description="" parameters=["PARAM_ID", "PARAM_ID"] channelTypes=["CHANNEL_TYPE_ID", "CHANNEL_TYPE_ID"];
  eq ThingType.prettyPrint() {
    return new MemberPrinter("ThingType")
        .addRequired("id", getID())
        .addNonDefault("label", getLabel())
        .addNonDefault("description", getDescription())
        .addIds("parameters", getParameters())
        .addIds("channelTypes", getChannelTypes())
        .build();
  }

  // Parameter: id="" label="" description="" type="" default="" required;
  eq Parameter.prettyPrint() {
    return new MemberPrinter("Parameter")
        .addRequired("id", getID())
        .addNonDefault("label", getLabel())
        .addNonDefault("description", getDescription())
        .addOptional("type", getType() != null, () -> getType().toString())
        .addNonDefault("context", getContext())
        .addOptional("default", hasDefaultValue(), () -> getDefaultValue().getValue())
        .addFlag("required", getRequired())
        .build();
  }

  // ChannelType: id="" label="" description="" itemType="" category="" readyOnly;
  eq ChannelType.prettyPrint() {
    return new MemberPrinter("ChannelType")
        .addRequired("id", getID())
        .addNonDefault("label", getLabel())
        .addNonDefault("description", getDescription())
        .addOptional("itemType", getItemType() != null, () -> getItemType().name())
        .addOptional("category", getChannelCategory() != null, () -> getChannelCategory().prettyPrint())
        .addFlag("readOnly", getReadOnly())
        .build();
  }

  // ChannelCategory
  syn String DefaultChannelCategory.prettyPrint() = getValue().name();
  syn String SimpleChannelCategory.prettyPrint() = getValue();
  syn String ReferringChannelCategory.prettyPrint() = getChannelCategory().prettyPrint();

  // Channel: id="" type="" links=["ITEM_ID", "ITEM_ID"];
  eq Channel.prettyPrint() {
    return new MemberPrinter("Channel")
        .addRequired("id", getID())
        .addRequired("type", getType(), ChannelType::getID)
        .addIds("links", getLinkedItems())
        .build();
  }
  
  // FrequencySetting: id="" procFrec="";
  eq FrequencySetting.prettyPrint() {
    return new MemberPrinter("FrequencySetting")
        .addNonDefault("id", getID())
        .addNonDefault("procFreq", String.valueOf(getEventProcessingFrequency()))
        .build();
  }

  // ExternalHost: "hostName:port"
  syn String ExternalHost.prettyPrint() {
    if (getHostName().contains(":")) {
      return getHostName();
    }
    return getHostName() + (getPort()>0 ? ":" + getPort() : "");
  }

  // Mqtt: incoming="" outgoing="" host="";
  eq MqttRoot.prettyPrint() {
    ExternalHost host = getHost();
    return new MemberPrinter("Mqtt")
        .addNonDefault("incoming", getIncomingPrefix())
        .addNonDefault("outgoing", getOutgoingPrefix())
        .addOptional("host", host.exists(), () -> host.prettyPrint())
        .build();
  }

  // Influx: user="" password="" dbName="" host="" ;
  eq InfluxRoot.prettyPrint() {
    ExternalHost host = getHost();
    return new MemberPrinter("Influx")
        .addNonDefault("user", host.getUserName(), DEFAULT_USER)
        .addNonDefault("password", host.getPassword(), DEFAULT_PASSWORD)
        .addNonDefault("dbName", getDbName(), DEFAULT_DB_NAME)
        .addOptional("host", host.exists(), () -> host.prettyPrint())
        .build();
  }

  // Activities: { index: "name" }
  eq MachineLearningRoot.prettyPrint() {
    return new MemberPrinter("ML")
        .addNodes("activities", getNumActivity(), getActivityList(),
                  activity -> activity.getIdentifier() + ":\"" + activity.getLabel() + "\"",
                  MemberPrinter.ListBracketType.CURLY)
        .build();
  }

  // Expressions
  syn String ParenthesizedNumberExpression.prettyPrint() = "(" + getOperand().prettyPrint() + ")";
  syn String NumberLiteralExpression.prettyPrint() = Double.toString(getValue());
  syn String AddExpression.prettyPrint() = "(" + getLeftOperand().prettyPrint() + " + " + getRightOperand().prettyPrint() + ")";
  syn String SubExpression.prettyPrint() = "(" + getLeftOperand().prettyPrint() + " - " + getRightOperand().prettyPrint() + ")";
  syn String MultExpression.prettyPrint() = "(" + getLeftOperand().prettyPrint() + " * " + getRightOperand().prettyPrint() + ")";
  syn String DivExpression.prettyPrint() = "(" + getLeftOperand().prettyPrint() + " / " + getRightOperand().prettyPrint() + ")";
  syn String PowerExpression.prettyPrint() = "(" + getLeftOperand().prettyPrint() + " ^ " + getRightOperand().prettyPrint() + ")";
  syn String ParenthesizedLogicalExpression.prettyPrint()  = "(" + getOperand().prettyPrint() + ")";
  syn String NotExpression.prettyPrint()  = "!" + getOperand().prettyPrint();
  eq ComparingExpression.prettyPrint() {
    switch (getComparator()) {
      case NotEquals: return "(" + getLeftOperand().prettyPrint() + " != " + getRightOperand().prettyPrint() + ")";
      case Equals: return "(" + getLeftOperand().prettyPrint() + " == " + getRightOperand().prettyPrint() + ")";
      case LessThan: return "(" + getLeftOperand().prettyPrint() + " < " + getRightOperand().prettyPrint() + ")";
      case GreaterThan: return "(" + getLeftOperand().prettyPrint() + " > " + getRightOperand().prettyPrint() + ")";
      case LessOrEqualThan: return "(" + getLeftOperand().prettyPrint() + " <= " + getRightOperand().prettyPrint() + ")";
      case GreaterOrEqualThan: return "(" + getLeftOperand().prettyPrint() + " >= " + getRightOperand().prettyPrint() + ")";
      default: throw new IllegalArgumentException("Unknown compartor type: " + getComparator());
    }
  }
  syn String AndExpression.prettyPrint() = "(" + getLeftOperand().prettyPrint() + " & " + getRightOperand().prettyPrint() + ")";
  syn String OrExpression.prettyPrint() = "(" + getLeftOperand().prettyPrint() + " | " + getRightOperand().prettyPrint() + ")";
  syn String Designator.prettyPrint() = getItem().getID();

  // Rules
  eq Rule.prettyPrint() {
    return new MemberPrinter("Rule")
        .addIds("TriggeringItems", getObserverList().size(), getObserverList(),
                  io -> io.observedItem().getID())
        .addNodes("Condition", getNumCondition(), getConditionList(),
                  Condition::toString)
        .addNodes("Action", getNumAction(), getActionList(),
                  Action::toString)
        .build();
  }

}
