package de.tudresden.inf.st.eraser.util;

import de.tudresden.inf.st.eraser.jastadd.model.*;

import java.util.ArrayList;

/**
 * Helper class to create models used in tests.
 *
 * @author rschoene - Initial contribution
 */
public class TestUtils {

  public static class ModelAndItem {
    public SmartHomeEntityModel model;
    public NumberItem item;
    static ModelAndItem of(SmartHomeEntityModel model, NumberItem item) {
      ModelAndItem result = new ModelAndItem();
      result.model = model;
      result.item = item;
      return result;
    }
  }

  public static ModelAndItem createModelAndItem(double initialValue) {
    return createModelAndItem(initialValue, false);
  }

  public static ModelAndItem createModelAndItem(double initialValue, boolean useUpdatingItem) {
    Root root = Root.createEmptyRoot();
    Group g = new Group();
    root.getSmartHomeEntityModel().addGroup(g);
    g.setID("group1");

    NumberItem item = addItemTo(g, initialValue, useUpdatingItem);

    return ModelAndItem.of(root.getSmartHomeEntityModel(), item);
  }

  public static SmartHomeEntityModel createModelWithGroup() {
    Root root = Root.createEmptyRoot();
    Group group0 = new Group();
    group0.setID("Group0");
    root.getSmartHomeEntityModel().addGroup(group0);

    return root.getSmartHomeEntityModel();
  }

  public static NumberItem addItemTo(SmartHomeEntityModel model, double initialValue) {
    return addItemTo(model, initialValue, false);
  }

  public static NumberItem addItemTo(SmartHomeEntityModel model, double initialValue, boolean useUpdatingItem) {
    return addItemTo(getDefaultGroup(model), initialValue, useUpdatingItem);
  }

  private static NumberItem addItemTo(Group group, double initialValue, boolean useUpdatingItem) {
    NumberItem item = new NumberItem();
    if (useUpdatingItem) {
      item.enableSendState();
    } else {
      item.disableSendState();
    }
    item.setID("item" + group.getNumItem());
    item.setState(initialValue, false);
    group.addItem(item);
    return item;
  }

  public static Group getDefaultGroup(SmartHomeEntityModel model) {
    // use first found group
    return model.getGroup(0);
  }

}
