package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

/**
 * JSON return type for a Link in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class LinkData {
  public String itemName;
  public String channelUID;
  public Map<String, String> configuration;

  @Override
  public String toString() {
    return "LinkData{" + "itemName='" + itemName + '\'' +
        ", channelUID='" + channelUID + '\'' +
        ", configuration=" + configuration +
        '}';
  }
}
