package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.Map;

/**
 * Abstract JSON return type for either an item or a group in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class AbstractItemData {
  public String type;
  public String name;
  public String label;
  public String category;
  public List<String> tags;
  public List<String> groupNames;
  public String link;
  public String state;
  public String transformedState;
  public StateDescriptionData stateDescription;
  public Boolean editable;
  public Map<String, MetaDataData> metadata;
}
