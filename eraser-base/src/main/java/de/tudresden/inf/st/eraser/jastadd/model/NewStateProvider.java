package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.function.Supplier;

/**
 * Provide a new state in form of a string which is set to the item.
 *
 * @author rschoene - Initial contribution
 */
public interface NewStateProvider extends Supplier<String> {
}
