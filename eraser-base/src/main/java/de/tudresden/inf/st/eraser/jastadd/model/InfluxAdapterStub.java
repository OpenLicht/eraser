package de.tudresden.inf.st.eraser.jastadd.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;

/**
 * Adapter to mock an InfluxDB, instead just logging method invocations.
 *
 * @author rschoene - Initial contribution
 */
public class InfluxAdapterStub extends InfluxAdapter {

  public interface WriteCallback {
    void onWrite(AbstractItemPoint point);
  }

  private Logger logger = LogManager.getLogger(InfluxAdapterStub.class);
  private WriteCallback callback;

  public void setCallback(WriteCallback callback) {
    this.callback = callback;
  }

  @Override
  public InfluxAdapter setHost(ExternalHost host) {
    return this;
  }

  @Override
  public void write(AbstractItemPoint point) {
    logger.info("Stub writing {}", point);
    if (callback != null) {
      callback.onWrite(point);
    }
  }

  @Override
  public List<? extends AbstractItemPoint> query(String from, String id, Class<? extends AbstractItemPoint> clazz) {
    logger.info("Stub returning empty list for {}", id);
    return Collections.emptyList();
  }

  @Override
  public boolean isConnected() {
    return true;
  }

  @Override
  public void deleteDatabase() {
    // empty
  }

  @Override
  public void close() {
    // empty
  }
}
