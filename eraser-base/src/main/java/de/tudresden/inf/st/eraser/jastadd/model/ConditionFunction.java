package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.function.Function;

/**
 * Function used in ECA rules for the condition part.
 *
 * @author rschoene - Initial contribution
 */
public interface ConditionFunction extends Function<Root, Boolean> {
}
