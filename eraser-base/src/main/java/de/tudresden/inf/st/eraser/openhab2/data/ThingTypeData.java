package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * JSON return type for a Thing type in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ThingTypeData {
  public String UID;
  public String label;
  public String description;
  public String category;
  public boolean listed;
  public List<String> supportedBridgeTypeUIDs;
  public boolean bridge;
  public List<ParameterData> configParameters;
}
