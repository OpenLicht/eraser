package de.tudresden.inf.st.eraser.jastadd.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.List;

/**
 * Adapter for internally held machine learning models.
 *
 * @author rschoene - Initial contribution
 */
public class InternalMachineLearningHandler implements MachineLearningEncoder, MachineLearningDecoder {

  private static final Logger logger = LogManager.getLogger(InternalMachineLearningHandler.class);
  private InternalMachineLearningModel model;

  public InternalMachineLearningHandler setModel(InternalMachineLearningModel model) {
    this.model = model;
    return this;
  }

  @Override
  public void setKnowledgeBaseRoot(Root root) {
    // ignored
  }

  @Override
  public void newData(List<Item> changedItems) {
    logger.debug("Ignored new data of {}", changedItems);
  }

  @Override
  public List<Item> getTargets() {
    return model.getTargetItems();
  }

  @Override
  public List<Item> getRelevantItems() {
    return model.getRelevantItems();
  }

  @Override
  public void triggerTraining() {
    logger.debug("Ignored training trigger.");
  }

  @Override
  public MachineLearningResult classify() {
    return model.internalClassify().computePreferences();
  }

  @Override
  public Instant lastModelUpdate() {
    return null;
  }
}
