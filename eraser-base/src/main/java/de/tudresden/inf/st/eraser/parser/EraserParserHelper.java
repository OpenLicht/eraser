package de.tudresden.inf.st.eraser.parser;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.JavaUtils;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.BiConsumer;

/**
 * Resolving names while parsing model files.
 *
 * @author rschoene - Initial contribution
 */
@Deprecated
class EraserParserHelper {
  private Logger logger = LogManager.getLogger(EraserParserHelper.class);

  private Map<String, ThingType> thingTypeMap = new HashMap<>();
  private Map<String, ChannelType> channelTypeMap = new HashMap<>();
  private Map<String, ChannelCategory> channelCategoryMap = new HashMap<>();
  private Map<String, Channel> channelMap = new HashMap<>();
  private Map<String, Parameter> parameterMap = new HashMap<>();
  private Map<String, Item> itemMap = new HashMap<>();
  private Map<String, Group> groupMap = new HashMap<>();
  private Map<String, FrequencySetting> FrequencySettingMap = new HashMap<>();

  private Map<Thing, String> missingThingTypeMap = new HashMap<>();
  private Map<Channel, String> missingChannelTypeMap = new HashMap<>();
  private Map<Item, String> missingTopicMap = new HashMap<>();
  private Map<Item, String> missingItemCategoryMap = new HashMap<>();

  private Map<Designator, String> missingItemForDesignator = new HashMap<>();

  private Map<Thing, Iterable<String>> missingChannelListMap = new HashMap<>();
  private Map<Channel, Iterable<String>> missingItemLinkListMap = new HashMap<>();

  private Map<Group, Iterable<String>> missingSubGroupListMap = new HashMap<>();
  private Map<Group, Iterable<String>> missingItemListMap = new HashMap<>();
  private Map<ThingType, Iterable<String>> missingChannelTypeListMap = new HashMap<>();
  private Map<ThingType, Iterable<String>> missingParameterListMap = new HashMap<>();

  private Set<ASTNode> unusedElements = new HashSet<>();
  private Set<Item> groupedItems = new HashSet<>();

  private List<Item> itemOrder = new LinkedList<>();

  private Root root;

  private static boolean checkUnusedElements = true;
  private static Root initialRoot = null;

  private class ItemPrototype extends DefaultItem {

  }

  /**
   * Changes the behavior of the parser to check for unused elements. (Default: true)
   * @param checkUnusedElements <code>true</code> to check for unused elements, <code>false</code> to skip the check
   */
  public static void setCheckUnusedElements(boolean checkUnusedElements) {
    EraserParserHelper.checkUnusedElements = checkUnusedElements;
  }

  public static void setInitialRoot(Root root) {
    EraserParserHelper.initialRoot = root;
  }

  /**
   * Post processing step after parsing a model, to resolve all references within the model.
   */
  public void resolveReferences() {
    if (this.root == null) {
      // when parsing expressions
      this.root = EraserParserHelper.initialRoot != null ? EraserParserHelper.initialRoot : createRoot();
    }

    if (checkUnusedElements) {
      fillUnused();
    }
    resolve(thingTypeMap, missingThingTypeMap, Thing::setType);
    resolve(channelTypeMap, missingChannelTypeMap, Channel::setType);
    if (itemMap == null || itemMap.isEmpty()) {
      missingItemForDesignator.forEach((designator, itemName) ->
          JavaUtils.ifPresentOrElse(root.getSmartHomeEntityModel().resolveItem(itemName),
              designator::setItem,
              () -> logger.warn("Could not resolve item {} for {}", itemName, designator)));
    } else {
      resolve(itemMap, missingItemForDesignator, Designator::setItem);
    }
//    missingTopicMap.forEach((item, s) -> item.setMqttTopic(s));
    this.root.getMqttRoot().ensureCorrectPrefixes();

    resolveList(channelMap, missingChannelListMap, Thing::addChannel);
    resolveList(itemMap, missingItemLinkListMap, Channel::addLinkedItem);
    resolveList(groupMap, missingSubGroupListMap, Group::addGroup);
    resolveList(itemMap, missingItemListMap, this::addItemToGroup);
    resolveList(channelTypeMap, missingChannelTypeListMap, ThingType::addChannelType);
    resolveList(parameterMap, missingParameterListMap, ThingType::addParameter);


    createUnknownGroupIfNecessary();
//    createChannelCategories();
    createItemCategories();

    if (checkUnusedElements) {
      checkUnusedElements();
    }

    this.root.treeResolveAll();
    this.root.doFullTraversal();
  }

  private void addItemToGroup(Group group, Item item) {
    groupedItems.add(item);
    group.addItem(item);
  }

  private void fillUnused() {
    unusedElements.addAll(thingTypeMap.values());
    unusedElements.addAll(channelTypeMap.values());
    unusedElements.addAll(channelMap.values());
    unusedElements.addAll(parameterMap.values());
//    unusedElements.addAll(topicMap.values());
  }

  /**
   * Create a group for all items not contained in a group. And warn if this was necessary.
   */
  private void createUnknownGroupIfNecessary() {
    if (itemMap.size() > groupedItems.size()) {
      Set<Item> danglingItems = new HashSet<>(itemMap.values());
      danglingItems.removeAll(groupedItems);
      // probably terrible performance
      List<Item> sortedDanglingItems = new LinkedList<>();
      for (Item item : itemOrder) {
        if (danglingItems.contains(item)) {
          sortedDanglingItems.add(item);
        }
      }
      ParserUtils.addToUnknownGroup(this.root.getSmartHomeEntityModel(), sortedDanglingItems);
    }
  }

  private void createItemCategories() {
    Map<String, ItemCategory> newCategories = new HashMap<>();
    missingItemCategoryMap.forEach((item, category) ->
        item.setCategory(newCategories.computeIfAbsent(category, ItemCategory::new)));
    newCategories.values().forEach(node -> root.getSmartHomeEntityModel().addItemCategory(node));
  }

  private void checkUnusedElements() {
    unusedElements.forEach(elem -> logger.info("{} '{}' defined, but not referenced.", elem.getClass().getSimpleName(), ident(elem)));
  }

  private String ident(ASTNode elem) {
    if (elem instanceof ModelElement) {
      return ((ModelElement) elem).getID();
    } else if (elem instanceof MqttTopic) {
      return ((MqttTopic) elem).getTopicString();
    } else if (elem instanceof DefaultChannelCategory) {
      return ((DefaultChannelCategory) elem).getValue().name();
    } else if (elem instanceof SimpleChannelCategory) {
      return ((SimpleChannelCategory) elem).getValue();
    }
    return elem.toString();
  }

  private <Src extends ASTNode, Target extends ASTNode> void resolveList(
      Map<String, Target> resolved, Map<Src, Iterable<String>> missing, BiConsumer<Src, Target> adder) {
    missing.forEach(
        (elem, keyList) -> keyList.forEach(
            key -> resolve0(resolved, key, elem, adder)));
    missing.clear();
  }

  private <Src extends ASTNode, Target extends ASTNode> void resolve(
      Map<String, Target> resolved, Map<Src, String> missing, BiConsumer<Src, Target> setter) {
    missing.forEach(
        (elem, key) -> resolve0(resolved, key, elem, setter));
    missing.clear();
  }

  private <Src extends ASTNode, Target extends ASTNode> void resolve0(
      Map<String, Target> resolved, String key, Src elem, BiConsumer<Src, Target> action) {
    Target value = resolved.get(key);
    if (value == null) {
      logger.warn("Reference in {} {} for '{}' cannot be resolved",
          elem.getClass().getSimpleName(), ident(elem), key);
      return;
    }
    if (checkUnusedElements) {
      unusedElements.remove(value);
    }
    action.accept(elem, value);
  }

  //--- Thing and ThingType ---

  public Thing addThingType(Thing t, String typeName) {
    missingThingTypeMap.put(t, typeName);
    return t;
  }

  public ThingType setChannelTypes(ThingType tt, StringList channelTypeNames) {
    missingChannelTypeListMap.put(tt, channelTypeNames);
    return tt;
  }

  public ThingType setParameters(ThingType tt, StringList parameterNames) {
    missingParameterListMap.put(tt, parameterNames);
    return tt;
  }

  public Thing setChannels(Thing t, StringList channelNames) {
    missingChannelListMap.put(t, channelNames);
    return t;
  }

  public Thing setID(Thing thing, String id) {
    thing.setID(id);
    return thing;
  }

  public FrequencySetting setID(FrequencySetting FrequencySetting, String id) {
    FrequencySetting.setID(id);
    FrequencySettingMap.put(id,FrequencySetting);
    return FrequencySetting;
  }

  public ThingType setID(ThingType thingType, String id) {
    thingType.setID(id);
    thingTypeMap.put(id, thingType);
    return thingType;
  }

  //--- Channel and ChannelType ---

  public ChannelType setItemType(ChannelType ct, String itemTypeName) {
    ct.setItemType(ItemType.valueOf(itemTypeName));
    return ct;
  }

  public Channel setChannelType(Channel c, String channelTypeName) {
    missingChannelTypeMap.put(c, channelTypeName);
    return c;
  }


  public Channel setLinks(Channel c, StringList linkNames) {
    missingItemLinkListMap.put(c, linkNames);
    return c;
  }

  public ChannelType setID(ChannelType channelType, String id) {
    channelType.setID(id);
    channelTypeMap.put(id, channelType);
    return channelType;
  }

  public Channel setID(Channel channel, String id) {
    channel.setID(id);
    channelMap.put(id, channel);
    return channel;
  }

  //--- Item ---

  public Item createItem() {
    ItemPrototype result = new ItemPrototype();
    result.disableSendState();
    return result;
  }

  public Item setCategory(Item item, String categoryName) {
    missingItemCategoryMap.put(item, categoryName);
    return item;
  }

  public Item retype(Item itemWithCorrectType, Item prototype) {
    itemWithCorrectType.setID(prototype.getID());
    itemWithCorrectType.setLabel(prototype.getLabel());
    itemWithCorrectType.setMetaData(prototype.getMetaData());
    itemWithCorrectType.setFrequencySetting(prototype.getFrequencySetting());
    if (!(itemWithCorrectType instanceof ActivityItem)) {
      String state = prototype.getStateAsString();
      itemWithCorrectType.disableSendState();
      if (state.isEmpty()) {
        itemWithCorrectType.setStateToDefault();
      } else {
        itemWithCorrectType.setStateFromString(state);
      }
      itemWithCorrectType.enableSendState();
    }

    moveMissingForRetype(itemWithCorrectType, prototype, missingTopicMap);
    moveMissingForRetype(itemWithCorrectType, prototype, missingItemCategoryMap);

    itemMap.put(prototype.getID(), itemWithCorrectType);

    itemOrder.add(itemWithCorrectType);

    return itemWithCorrectType;
  }

  private <T> void moveMissingForRetype(Item itemWithCorrectType, Item prototype, Map<Item, T> missingXMap) {
    T value = missingXMap.get(prototype);
    if (value != null) {
      missingXMap.put(itemWithCorrectType, value);
    }
    missingXMap.remove(prototype);
  }

  public Item setID(Item item, String id) {
    item.setID(id);
    itemMap.put(id, item);
    return item;
  }

  //--- Group ---

  public Group setSubGroups(Group g, StringList subGroupNames) {
    missingSubGroupListMap.put(g, subGroupNames);
    return g;
  }

  public Group setItems(Group g, StringList itemNames) {
    missingItemListMap.put(g, itemNames);
    return g;
  }

  public Group setSimpleAggregationFunction(Group g, String aggFunctionName) {
    SimpleGroupAggregationFunctionName name = SimpleGroupAggregationFunctionName.valueOf(aggFunctionName.toUpperCase());
    g.setAggregationFunction(new SimpleGroupAggregationFunction(name));
    return g;
  }

  public Group setParameterizedAggregationFunction(Group g, String aggFunctionName, StringList params) {
    ParameterizedGroupAggregationFunctionName name = ParameterizedGroupAggregationFunctionName.valueOf(
        aggFunctionName.toUpperCase());
    List<String> paramList = new ArrayList<>();
    params.iterator().forEachRemaining(paramList::add);
    String param1, param2;
    if (paramList.size() == 2) {
      param1 = paramList.get(0);
      param2 = paramList.get(1);
    } else {
      logger.error("Got {} instead of 2 parameters in group function {}!", paramList.size(), aggFunctionName);
      param1 = "?";
      param2 = "?";
    }
    g.setAggregationFunction(new ParameterizedGroupAggregationFunction(name, param1, param2));
    return g;
  }

  public Group setID(Group group, String id) {
    group.setID(id);
    groupMap.put(id, group);
    return group;
  }

  //--- Parameter ---

  public Parameter setParameterValueType(Parameter p, String pvt) {
    p.setType(ParameterValueType.valueOf(JavaUtils.toTitleCase(pvt)));
    return p;
  }

  public Parameter setDefault(Parameter p, String defaultValue) {
    p.setDefaultValue(new ParameterDefaultValue(defaultValue));
    return p;
  }

  public Parameter setID(Parameter parameter, String id) {
    parameter.setID(id);
    parameterMap.put(id, parameter);
    return parameter;
  }

  //--- MQTT ---

  public Item setTopic(Item item, String mqttTopicName) {
    missingTopicMap.put(item, mqttTopicName);
    return item;
  }

  //--- Activity ---

  public MachineLearningRoot setActivities(MachineLearningRoot mlr, IntegerKeyMap map) {
    for (AbstractMap.SimpleEntry<Integer, String> entry : map) {
      Activity activity = new Activity();
      activity.setIdentifier(entry.getKey());
      activity.setLabel(entry.getValue());
      mlr.addActivity(activity);
    }
    return mlr;
  }

  //--- Root ---

  public Root createRoot() {
    this.root = Root.createEmptyRoot();
    return this.root;
  }

  public Root createRoot(Thing t) {
    Root result = createRoot();
    result.getSmartHomeEntityModel().addThing(t);
    return result;
  }

  public Root createRoot(Group g) {
    Root result = createRoot();
    result.getSmartHomeEntityModel().addGroup(g);
    return result;
  }

  public Root createRoot(ThingType tt) {
    Root result = createRoot();
    result.getSmartHomeEntityModel().addThingType(tt);
    return result;
  }

  public Root createRoot(ChannelType ct) {
    Root result = createRoot();
    result.getSmartHomeEntityModel().addChannelType(ct);
    return result;
  }

  public Root createRoot(MqttRoot mr) {
    Root result = createRoot();
    result.setMqttRoot(mr);
    return result;
  }

  public Root createRoot(InfluxRoot ir) {
    Root result = createRoot();
    result.setInfluxRoot(ir);
    return result;
  }

  public Root createRoot(MachineLearningRoot ml) {
    Root result = createRoot();
    result.setMachineLearningRoot(ml);
    return result;
  }

  public Root createRoot(Rule rule) {
    Root result = createRoot();
    result.addRule(rule);
    return result;
  }

  public Root createRoot(FrequencySetting frequencySetting) {
    Root result = createRoot();
    result.getSmartHomeEntityModel().addFrequencySetting(frequencySetting);
    return result;
  }

  //+++ newStuff (to be categorized) +++
  public Designator createDesignator(String itemName) {
    Designator result = new Designator();
    missingItemForDesignator.put(result, itemName);
    return result;
  }

  public Rule createRule(Condition c, Action a) {
    Rule result = new Rule();
    result.addCondition(c);
    result.addAction(a);
    return result;
  }

}
