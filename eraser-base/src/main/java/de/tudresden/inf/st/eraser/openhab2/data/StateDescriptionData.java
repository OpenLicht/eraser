package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Part of the JSON return type of both Item and channel type in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class StateDescriptionData {
  public Integer minimum;
  public Integer maximum;
  public Integer step;
  public String pattern;
  public Boolean readOnly;
  public List<OptionData> options;
}
