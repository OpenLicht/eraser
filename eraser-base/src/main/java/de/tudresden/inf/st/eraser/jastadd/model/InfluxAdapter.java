package de.tudresden.inf.st.eraser.jastadd.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Adapter handling communication with an InfluxDB.
 *
 * @author rschoene - Initial contribution
 */
public abstract class InfluxAdapter implements AutoCloseable {

  private ExecutorService pool;
  private Logger logger = LogManager.getLogger(InfluxAdapter.class);
  private boolean reallyAsync;

  InfluxAdapter() {
    reset();
  }

  /**
   * Set a new host to connect to. Old connections must be close beforehand.
   * @param host the new external host
   * @return this
   */
  public abstract InfluxAdapter setHost(ExternalHost host);

  /**
   * Write the given point to influx.
   * @param point the point to write
   */
  public abstract void write(AbstractItemPoint point);

  /**
   * Forces this adapter to use threads to compute {@link #asyncQuery(String, String, Class, QueryResultCallback)}.
   */
  public void enableAsyncQuery() {
    this.reallyAsync = true;
  }


  /**
   * Forces this adapter to compute {@link #asyncQuery(String, String, Class, QueryResultCallback)} synchronously.
   */
  public void disableAsyncQuery() {
    this.reallyAsync = false;
  }

  /**
   * Queries a list of measurements.
   * @param from  the kind of measurements, i.e., the kind of states the measurements have
   * @param clazz the clazz to use for a {@link org.influxdb.impl.InfluxDBResultMapper}
   * @return the list of retrieved measurements
   */
  public abstract List<? extends AbstractItemPoint> query(String from, String id, Class<? extends AbstractItemPoint> clazz);

  /**
   * Asynchronously queries a list of measurements using {@link #query(String, String, Class)}
   * @param from     the kind of measurements, i.e., the kind of states the measurements have
   * @param clazz    the clazz to use for a {@link org.influxdb.impl.InfluxDBResultMapper}
   * @param callback the callback to invoke once the result is available
   */
  public void asyncQuery(String from, String id, Class<? extends AbstractItemPoint> clazz, QueryResultCallback callback) {
    if (this.reallyAsync) {
      pool.submit(() -> {
        List<? extends AbstractItemPoint> result = query(from, id, clazz);
        callback.onResult(result);
      });
    } else {
      callback.onResult(query(from, id, clazz));
    }
  }

  /**
   * Checks, whether the InfluxDB is available
   * @return true, if connected and available
   */
  public abstract boolean isConnected();

  /**
   * Deletes the currently used database. Use with care, <b>Data loss possible</b>
   */
  public abstract void deleteDatabase();

  /**
   * Callback invoked when {@link #asyncQuery(String, String, Class, QueryResultCallback)} got its result
   */
  interface QueryResultCallback {
    void onResult(List<? extends AbstractItemPoint> result);
  }

  /**
   * Resets the internal state of this adapter.
   */
  public void reset() {
    if (pool != null) {
      pool.shutdownNow();
    }
    pool = Executors.newFixedThreadPool(2);
    enableAsyncQuery();
  }

  @Override
  public void close() throws Exception {
    logger.info("Shutting down");
    pool.shutdownNow();
  }
}
