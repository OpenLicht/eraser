package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.function.Function;

/**
 * Merge states from given items into one (generic) state represented as a string.
 *
 * @author rschoene - Initial contribution
 */
public interface ItemsToStringFunction extends Function<Iterable<Item>, String> {
}
