package de.tudresden.inf.st.eraser.serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.json.JsonGeneratorImpl;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import de.tudresden.inf.st.eraser.jastadd.model.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ASTNodeSerializer extends StdSerializer<ASTNode> {

  private static final String packageName = Root.class.getPackage().getName();

  public ASTNodeSerializer() {
    this(null);
  }

  public ASTNodeSerializer(Class<ASTNode> t) {
    super(t);
    initSerializers();
  }

  private void initSerializers() {
    serializers.put(ModelElement.class, ((jgen, elem) ->
    {
      jgen.writeStartObject();
      jgen.writeFieldName("ID");
      jgen.writeStartObject();
      jgen.writeStringField("k", "t");
      jgen.writeStringField("t", String.class.getName());
      jgen.writeStringField("v", ((ModelElement) elem).getID());
      jgen.writeEndObject(); // end ID {}
    }));
    serializers.put(MqttTopic.class, ((jgen, elem) ->
    {
      jgen.writeStartObject();
      jgen.writeFieldName("IncomingTopic");
      jgen.writeStartObject();
      jgen.writeStringField("k", "t");
      jgen.writeStringField("t", String.class.getName());
      jgen.writeStringField("v", ((MqttTopic) elem).getIncomingTopic());
      jgen.writeEndObject(); // end IncomingTopic {}
    }));
  }

  Map<Class<?>, SerializeId> serializers = new HashMap<>();

  @Override
  public void serialize(
    ASTNode value, JsonGenerator jgen, SerializerProvider provider)
    throws IOException {

    jgen.writeStartObject();
    jgen.writeStringField("k", "NT");
    jgen.writeStringField("t", value.getClass().getSimpleName());
    jgen.writeObjectFieldStart("c");
    for (Method m : value.getClass().getMethods()) {
      try {
        if (m.getAnnotation(ASTNodeAnnotation.Child.class) != null) {
          jgen.writeFieldName(m.getAnnotation(ASTNodeAnnotation.Child.class).name());
          provider.defaultSerializeValue(m.invoke(value), jgen);
        } else if (m.getAnnotation(ASTNodeAnnotation.Token.class) != null) {
          jgen.writeFieldName(m.getAnnotation(ASTNodeAnnotation.Token.class).name());
          jgen.writeStartObject();
          jgen.writeStringField("k", m.getReturnType().isEnum() ? "enum" : "t");
          String className = m.getReturnType().getName();
          jgen.writeStringField("t", className);
          jgen.writeFieldName("v");
          Object elem = m.invoke(value);
          if (className.startsWith(packageName) && !m.getReturnType().isEnum()) {
            serializeIntrinsicReference(elem, jgen, value, m);
          } else {
            provider.defaultSerializeValue(elem, jgen);
          }
          jgen.writeEndObject();
        } else if (m.getAnnotation(ASTNodeAnnotation.ListChild.class) != null) {
          jgen.writeFieldName(m.getAnnotation(ASTNodeAnnotation.ListChild.class).name());
          provider.defaultSerializeValue(m.invoke(value), jgen);
        } else if (m.getAnnotation(ASTNodeAnnotation.OptChild.class) != null) {
          jgen.writeFieldName(m.getAnnotation(ASTNodeAnnotation.OptChild.class).name());
          provider.defaultSerializeValue(m.invoke(value), jgen);
        }
      } catch (IllegalAccessException | InvocationTargetException e) {
        e.printStackTrace();
      }
    }
    jgen.writeEndObject();
    jgen.writeEndObject();
  }

  private void serializeIntrinsicReference(Object elem, JsonGenerator jgen, ASTNode value, Method m) throws IOException {
    SerializeId specificSerializer;
    if (elem instanceof ModelElement) {
      specificSerializer = serializers.get(ModelElement.class);
    } else if (elem instanceof MqttTopic) {
      specificSerializer = serializers.get(MqttTopic.class);
    } else if (elem == null) {
      throw new JsonGenerationException("Intrinsic reference to null in " +
          value.getClass().getSimpleName() + "." + m.getName(), jgen);
    } else {
      throw new JsonGenerationException("Intrinsic reference to unsupported type " + elem.getClass().getName() +
          " in " + value.getClass().getSimpleName() + "." + m.getName(), jgen);
    }
    jgen.writeStartObject();
    jgen.writeStringField("k", "NTRef");
    jgen.writeStringField("t", elem.getClass().getSimpleName());
    jgen.writeFieldName("c");
    specificSerializer.serialize(jgen, elem);
    jgen.writeEndObject(); // end c {}
    jgen.writeEndObject(); // end v {}
  }

  interface SerializeId {
    void serialize(JsonGenerator jgen, Object elem) throws IOException;
  }
}
