package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * JSON return type for a group in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class GroupItemData extends AbstractItemData {
  public List<String> members;
  public String groupType;
  public FunctionData function;

  public static class FunctionData {
    public String name;
    public List<String> params;
  }
}
