package de.tudresden.inf.st.eraser.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import de.tudresden.inf.st.eraser.jastadd.model.ASTNode;
import de.tudresden.inf.st.eraser.jastadd.model.JastAddList;

import java.io.IOException;

public class ListSerializer extends StdSerializer<JastAddList> {

  public ListSerializer() {
    this(null);
  }

  public ListSerializer(Class<JastAddList> t) {
    super(t);
  }

  @Override
  public void serialize(
      JastAddList value, JsonGenerator jgen, SerializerProvider provider)
    throws IOException {

    jgen.writeStartObject();
    jgen.writeStringField("k", "List");
    jgen.writeArrayFieldStart("c");
    // unchecked cast, because otherwise class clash when adding serializer
    for (ASTNode child : ((JastAddList<ASTNode>) value).astChildren()) {
      provider.defaultSerializeValue(child, jgen);
    }
    jgen.writeEndArray();
    jgen.writeEndObject();
  }
}
