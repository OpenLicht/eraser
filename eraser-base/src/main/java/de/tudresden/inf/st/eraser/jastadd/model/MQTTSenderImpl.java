package de.tudresden.inf.st.eraser.jastadd.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fusesource.mqtt.client.*;

import java.net.URI;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of a MQTT sender using <code>org.fusesource.mqtt.client</code>.
 *
 * @author rschoene - Initial contribution
 */
public class MQTTSenderImpl implements MQTTSender {

  private final Logger logger = LogManager.getLogger(MQTTSenderImpl.class);;
  /** The connection to the MQTT broker. */
  private FutureConnection connection;

  /** Timeout for connect/disconnect methods */
  private long connectTimeout;
  /** Unit of timeout for connect/disconnect methods */
  private TimeUnit connectTimeoutUnit;

  /** Timeout for publish method */
  private long publishTimeout;
  /** Unit of timeout for publish method */
  private TimeUnit publishTimeoutUnit;

  @Override
  public MQTTSender setHost(ExternalHost host) {
    /* The host running the MQTT broker. */
    URI hostUri = URI.create("tcp://" + host.getHostName() + ":" + host.getPort());
    logger.debug("Host is {}", hostUri);
    MQTT mqtt = new MQTT();
    String username = host.getUserName();
    String password = host.getPassword();
    if (username != null && !username.isEmpty()) {
      mqtt.setUserName(username);
    }
    if (password != null && !password.isEmpty()) {
      mqtt.setPassword(password);
    }
    mqtt.setHost(hostUri);
    connection = mqtt.futureConnection();
    setConnectTimeout(2, TimeUnit.SECONDS);
    setPublishTimeout(1, TimeUnit.SECONDS);
    ensureConnected();
    return this;
  }

  @Override
  public void setConnectTimeout(long connectTimeout, TimeUnit connectTimeoutUnit) {
    this.connectTimeout = connectTimeout;
    this.connectTimeoutUnit = connectTimeoutUnit;
  }

  @Override
  public void setPublishTimeout(long publishTimeout, TimeUnit publishTimeoutUnit) {
    this.publishTimeout = publishTimeout;
    this.publishTimeoutUnit = publishTimeoutUnit;
  }

  @Override
  public void publish(String topic, String message, QoS qos) throws Exception {
    if (ensureConnected()) {
      logger.debug("Send: {} -> {}", topic, message);
      connection.publish(topic, message.getBytes(), qos, false).await(publishTimeout, publishTimeoutUnit);
    }
  }

  /**
   * Ensures an established connection.
   * If already connected, return immediately. Otherwise try to connect.
   * @return <code>true</code> if the connected was established successfully, <code>false</code> if there was an error
   */
  private boolean ensureConnected() {
    if (!isConnected()) {
      try {
        connection.connect().await(connectTimeout, connectTimeoutUnit);
      } catch (Exception e) {
        logger.warn("Could not connect", e);
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isConnected() {
    return connection != null && connection.isConnected();
  }

  @Override
  public void close() throws Exception {
    if (connection == null) {
      logger.warn("Stopping without connection.");
      return;
    }
    if (isConnected()) {
      connection.disconnect().await(connectTimeout, connectTimeoutUnit);
    }
  }
}
