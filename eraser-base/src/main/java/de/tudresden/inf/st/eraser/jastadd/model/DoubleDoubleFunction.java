package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.function.Function;

/**
 * Function, that takes a double as input and outputs a double.
 *
 * @author rschoene - Initial contribution
 */
public interface DoubleDoubleFunction extends Function<Double, Double> {
}
