package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.function.Consumer;

/**
 * Consumer used to make edits in the action part of ECA rules.
 *
 * @author rschoene - Initial contribution
 */
public interface Action2EditConsumer extends Consumer<Item> {
}
