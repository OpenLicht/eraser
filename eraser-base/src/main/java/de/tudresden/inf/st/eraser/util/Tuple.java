package de.tudresden.inf.st.eraser.util;

import java.util.Objects;

/**
 * Simple class holding two values.
 *
 * @author rschoene - Initial contribution
 */
public class Tuple<X, Y> {
  public X x;
  public Y y;

  public Tuple(X x, Y y) {
    this.x = x;
    this.y = y;
  }

  public static <X, Y> Tuple<X, Y> of(X x, Y y) {
    return new Tuple<>(x, y);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Tuple<?, ?> tuple = (Tuple<?, ?>) o;
    return Objects.equals(x, tuple.x) &&
        Objects.equals(y, tuple.y);
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }
}
