package de.tudresden.inf.st.eraser.util;

import beaver.Parser;
import beaver.Scanner;
import beaver.Symbol;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.jastadd.parser.EraserParser;
import de.tudresden.inf.st.eraser.jastadd.scanner.EraserScanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Objects;

/**
 * Utility methods involving scanner and parser of the models.
 *
 * @author rschoene - Initial contribution
 */
public class ParserUtils {

  private static boolean verboseLoading = false;
  private static final Logger logger = LogManager.getLogger(ParserUtils.class);
  private static final ObjectMapper OBJECT_MAPPER_INSTANCE = new ObjectMapper()
      .enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS)
      .enable(SerializationFeature.INDENT_OUTPUT);

  private interface ReaderProvider {
    Reader provide() throws IOException;
  }

  private static class ReaderProviderByName implements ReaderProvider {
    private final String filename;
    private final Class<?> clazz;

    ReaderProviderByName(String filename, Class<?> clazz) {
      this.filename = filename;
      this.clazz = clazz;
    }

    @Override
    public Reader provide() throws IOException {
      return getReader(filename, clazz);
    }
  }

  private static class ReaderProviderByURL implements ReaderProvider {
    private final URL url;

    ReaderProviderByURL(URL url) {
      this.url = url;
    }

    @Override
    public Reader provide() throws IOException {
      return new InputStreamReader(url.openStream());
    }
  }

  /**
   * Print read tokens before loading the model.
   * This will effectively parse the input two times, thus slowing the operation.
   * @param verboseLoading <code>true</code> to print the model, <code>false</code> to silently load the model (default)
   */
  public static void setVerboseLoading(boolean verboseLoading) {
    ParserUtils.verboseLoading = verboseLoading;
  }

  /**
   * Loads a model in a local file with the given path.
   * @param fileName path to the file, either absolute, or relative to the current directory
   * @return the parsed model
   * @throws IOException if the file could not be found, or opened
   * @throws Parser.Exception if the file contains a malformed model
   */
  public static Root load(String fileName) throws IOException, Parser.Exception {
    return load(fileName, null);
  }

  /**
   * Loads a model in a local file with the given path.
   * @param file the file, either absolute, or relative to the current directory
   * @return the parsed model
   * @throws IOException if the file could not be found, or opened
   * @throws Parser.Exception if the file contains a malformed model
   */
  public static Root load(File file) throws IOException, Parser.Exception {
    return load(file.getAbsolutePath(), null);
  }

  /**
   * Loads a model in a file with the given path.
   * Resolving of relative paths depends on the value of clazz.
   * @param fileName  path to the file, either absolute, or relative.
   * @param clazz     {@code null} to search relative to current directory, otherwise search relative to resources location of given class
   * @return the parsed model
   * @throws IOException if the file could not be found, or opened
   * @throws Parser.Exception if the file contains a malformed model
   */
  public static Root load(String fileName, Class<?> clazz) throws IOException, Parser.Exception {
    logger.info("Loading model DSL file '{}'", fileName);
    return load(new ReaderProviderByName(fileName, clazz));
  }

  /**
   * Loads a model in a file from the given URL.
   * @param url an URL pointing to a file
   * @return the parsed model
   * @throws IOException if the file could not be found, or opened
   * @throws Parser.Exception if the file contains a malformed model
   */
  public static Root load(URL url) throws IOException, Parser.Exception {
    logger.info("Loading model DSL from '{}'", url);
    return load(new ReaderProviderByURL(url));
  }

  private static Root load(ReaderProvider readerProvider) throws IOException, Parser.Exception {
    Reader reader = readerProvider.provide();
    if (verboseLoading) {
      EraserScanner scanner = new EraserScanner(reader);
      try {
        Symbol token;
        while ((token = scanner.nextToken()).getId() != EraserParser.Terminals.EOF) {
          logger.debug("start: {}, end: {}, id: {}, value: {}",
              token.getStart(), token.getEnd(), EraserParser.Terminals.NAMES[token.getId()], token.value);
        }
      } catch (Scanner.Exception e) {
        e.printStackTrace();
      } finally {
        try {
          reader.reset();
        } catch (IOException resetEx) {
          reader = readerProvider.provide();
        }
      }
    }

    EraserScanner scanner = new EraserScanner(reader);
    EraserParser parser = new EraserParser();
    Root result = parser.parseRoot(scanner);
    reader.close();
    return result;
  }

  private static Reader getReader(String fileName, Class<?> clazz) throws IOException {
    return clazz == null ? getLocalReaderFor(fileName) : getClassReaderFor(fileName, clazz);
  }

  private static Reader getClassReaderFor(String fileName, Class<?> clazz) throws IOException {
    URL url = clazz.getClassLoader().getResource(fileName);
    return new BufferedReader(new InputStreamReader(
        Objects.requireNonNull(url, "Could not open file " + fileName)
            .openStream()));
  }

  private static Reader getLocalReaderFor(String fileName) throws IOException {
    try {
      return Files.newBufferedReader(Paths.get(fileName));
    } catch (IOException e) {
      logger.error("Error. Searching at {}", Paths.get(fileName).toAbsolutePath());
      throw e;
    }
  }

  /**
   * Add all dangling items to unknown group.
   * @param model         The model to operate on
   * @param danglingItems A list of items to add to the new group
   */
  public static void addToUnknownGroup(SmartHomeEntityModel model, Collection<Item> danglingItems) {
    Group unknownGroup = model.unknownGroup();
    danglingItems.forEach(unknownGroup::addItem);
    logger.info("Updated unknown group {}", unknownGroup.prettyPrint().trim());
  }

  public static NumberExpression parseNumberExpression(String expression_string) throws IOException, Parser.Exception {
    return parseNumberExpression(expression_string, null);
  }

  public static LogicalExpression parseLogicalExpression(String expression_string) throws IOException, Parser.Exception {
    return parseLogicalExpression(expression_string, null);
  }

  public static NumberExpression parseNumberExpression(String expression_string, Root root) throws IOException, Parser.Exception {
    return (NumberExpression) parseExpression(expression_string, EraserParser.AltGoals.number_expression, root);
  }

  public static LogicalExpression parseLogicalExpression(String expression_string, Root root) throws IOException, Parser.Exception {
    return (LogicalExpression) parseExpression(expression_string, EraserParser.AltGoals.logical_expression, root);
  }

  private static Expression parseExpression(String expression_string, short alt_goal, Root root) throws IOException, Parser.Exception {
    StringReader reader = new StringReader(expression_string);
    if (verboseLoading) {
      EraserScanner scanner = new EraserScanner(reader);
      try {
        Symbol token;
        while ((token = scanner.nextToken()).getId() != EraserParser.Terminals.EOF) {
          logger.debug("start: {}, end: {}, id: {}, value: {}",
              token.getStart(), token.getEnd(), EraserParser.Terminals.NAMES[token.getId()], token.value);
        }
      } catch (Scanner.Exception e) {
        e.printStackTrace();
      }
    }
    EraserParser.setNextInitialRoot(root);
    reader = new StringReader(expression_string);
    EraserScanner scanner = new EraserScanner(reader);
    EraserParser parser = new EraserParser();
    Expression result = parser.parseExpression(scanner, alt_goal);
    reader.close();
    return result;
  }

  public static Item parseItem(String definition)
      throws IllegalArgumentException, IOException, Parser.Exception {
    StringReader reader = new StringReader(definition);
    EraserScanner scanner = new EraserScanner(reader);
    EraserParser parser = new EraserParser();
    Root root = parser.parseRoot(scanner);
    reader.close();
    int size = root.getSmartHomeEntityModel().items().size();
    if (size == 0) {
      throw new IllegalArgumentException("Model does not contain any items!");
    }
    if (size > 1) {
      logger.warn("Model does contain {} items, ignoring all but the first.", size);
    }
    return root.getSmartHomeEntityModel().items().get(0);
  }

  public static <T> T loadFrom(URL location, Class<T> valueType) throws IOException {
    return OBJECT_MAPPER_INSTANCE.readValue(location, valueType);
  }

  public static <T> void saveTo(T value, URL location) throws IOException {
    if (location.getProtocol().equals("file")) {
      // write to (local) file directly
      OBJECT_MAPPER_INSTANCE.writeValue(new File(location.getFile()), value);
    } else {
      OBJECT_MAPPER_INSTANCE.writeValue(location.openConnection().getOutputStream(), value);
    }
  }

}
