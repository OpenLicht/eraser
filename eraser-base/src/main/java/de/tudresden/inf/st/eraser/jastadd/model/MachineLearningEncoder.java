package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.List;

/**
 * This interface represents the connection from knowledge base to one machine learning model.
 * It takes information from the knowledge base, and encodes them to a representation that is readable both for
 * the used technique and the purpose of the machine learning model.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
public interface MachineLearningEncoder extends MachineLearningSetRoot {

  /**
   * Update when new data is available.
   * @param changedItems A list of items whose state has changed
   */
  void newData(List<Item> changedItems);

  // to be discussed, in which form this is specified

  /**
   * Get the items that this model is supposed to change.
   * @return the list of targeted items
   */
  List<Item> getTargets();

  // to be discussed, in which form this is specified

  /**
   * Get the items which are relevant for the decision making of this model.
   * @return the list of items relevant for decision making
   */
  List<Item> getRelevantItems();

  // to be discussed, if this is necessary

  /**
   * Explicit hint for this model to start/trigger training. The model might ignore this hint.
   */
  void triggerTraining();

}
