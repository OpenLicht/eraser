package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.function.Function;

/**
 * Function, that takes an array of doubles as input and outputs a double.
 *
 * @author rschoene - Initial contribution
 */
public interface DoubleArrayDoubleFunction extends Function<double[], Double> {
}
