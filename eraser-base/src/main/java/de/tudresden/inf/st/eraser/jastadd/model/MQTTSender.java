package de.tudresden.inf.st.eraser.jastadd.model;

import org.fusesource.mqtt.client.QoS;

import java.util.concurrent.TimeUnit;

/**
 * Small helper to publish messages to a MQTT broker.
 *
 * @author rschoene - Initial contribution
 */
public interface MQTTSender extends AutoCloseable {

  /**
   * Sets the host running the MQTT broker (no username/password set).
   * @param host host name (IP address or domain name) and port
   */
  MQTTSender setHost(ExternalHost host);


  /**
   * Set the timeout used for connecting and disconnecting.
   * @param connectTimeout     Timeout value
   * @param connectTimeoutUnit Timeout unit
   */
  void setConnectTimeout(long connectTimeout, TimeUnit connectTimeoutUnit);

  /**
   * Set the timeout used for publishing messages.
   * @param publishTimeout     Timeout value
   * @param publishTimeoutUnit Timeout unit
   */
  void setPublishTimeout(long publishTimeout, TimeUnit publishTimeoutUnit);

  /**
   * Publishes a message in a topic at most once.
   * @param topic   the topic to publish at
   * @param message the message to publish
   * @throws Exception if the underlying connection throws an error
   */
  default void publish(String topic, String message) throws Exception {
    this.publish(topic, message, QoS.AT_MOST_ONCE);
  }

  /**
   * Publishes a message in a topic with the given quality of service (QoS).
   * @param topic   the topic to publish at
   * @param message the message to publish
   * @param qos     the needed quality of service (at most once, at least once, exactly once)
   * @throws Exception if the underlying connection throws an error
   */
  void publish(String topic, String message, QoS qos) throws Exception;

  /**
   * Checks, whether the connection to the host (set in the constructor) is established.
   * @return <code>true</code> if this sender is connected to the host
   */
  boolean isConnected();

  @Override
  void close() throws Exception;
}
