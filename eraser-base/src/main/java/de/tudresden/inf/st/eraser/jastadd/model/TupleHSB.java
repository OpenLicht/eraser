package de.tudresden.inf.st.eraser.jastadd.model;

import java.util.Objects;

/**
 * Value class comprising three integral values hue, saturation, brightness ranging from 0 to 255.
 *
 * @author rschoene - Initial contribution
 */
public class TupleHSB implements Cloneable {
  private int hue;
  private int saturation;
  private int brightness;
  public static TupleHSB of(int hue, int saturation, int brightness) {
    TupleHSB result = new TupleHSB();
    result.hue = hue % 360;
    result.saturation = ensureBetweenZeroAndHundred(saturation);
    result.brightness = ensureBetweenZeroAndHundred(brightness);
    return result;
  }

  private static int ensureBetweenZeroAndHundred(int value) {
    return Math.max(0, Math.min(value, 100));
  }

  public int getHue() {
    return hue;
  }

  public int getSaturation() {
    return saturation;
  }

  public int getBrightness() {
    return brightness;
  }

  public TupleHSB withDifferentHue(int hue) {
    return TupleHSB.of(hue, this.saturation, this.brightness);
  }

  public TupleHSB withDifferentSaturation(int saturation) {
    return TupleHSB.of(this.hue, saturation, this.brightness);
  }

  public TupleHSB withDifferentBrightness(int brightness) {
    return TupleHSB.of(this.hue, this.saturation, brightness);
  }

  public String toString() {
    return String.format("%s,%s,%s", hue, saturation, brightness);
  }

  public static TupleHSB parse(String s) {
    String[] tokens = s.split(",");
    return of(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));
  }

  @SuppressWarnings("MethodDoesntCallSuperMethod")
  public TupleHSB clone() {
    return TupleHSB.of(hue, saturation, brightness);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TupleHSB tupleHSB = (TupleHSB) o;
    return hue == tupleHSB.hue &&
        saturation == tupleHSB.saturation &&
        brightness == tupleHSB.brightness;
  }

  @Override
  public int hashCode() {
    return Objects.hash(hue, saturation, brightness);
  }
}
