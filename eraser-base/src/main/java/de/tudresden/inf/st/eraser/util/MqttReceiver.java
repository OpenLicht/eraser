package de.tudresden.inf.st.eraser.util;

import de.tudresden.inf.st.eraser.jastadd.model.ExternalHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.*;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;

/**
 * Subscribe to topics, receive and store messages.
 *
 * @author rschoene - Initial contribution
 */
public class MqttReceiver implements AutoCloseable {

  private final Logger logger;

  /** The host running the MQTT broker. */
  private URI host;
  private String username;
  private String password;
  /** The connection to the MQTT broker. */
  private CallbackConnection connection;
  /** Whether we are subscribed to the topics yet */
  private Condition readyCondition;
  private Lock readyLock;
  private boolean ready;
  private BiConsumer<String, String> onMessageCallback;
  private String[] topics;
  private QoS qos;

  public MqttReceiver() {
    this.logger = LogManager.getLogger(MqttReceiver.class);
    this.readyLock = new ReentrantLock();
    this.readyCondition = readyLock.newCondition();
    this.ready = false;
    qos = QoS.AT_LEAST_ONCE;
  }

  /**
   * Sets the host to receive messages from
   */
  public void setHost(ExternalHost externalHost) {
    this.host = URI.create("tcp://" + externalHost.getHostName() + ":" + externalHost.getPort());
    logger.debug("Host is {}", externalHost.getHostName());
  }


  public void setOnMessage(BiConsumer<String, String> callback) {
    this.onMessageCallback = callback;
  }

  public void setTopicsForSubscription(String... topics) {
    this.topics = topics;
  }

  public void setQoSForSubscription(QoS qos) {
    this.qos = qos;
  }

  /**
   * Waits until this updater is ready to receive MQTT messages.
   * If it already is ready, return immediately with the value <code>true</code>.
   * Otherwise waits for the given amount of time, and either return <code>true</code> within the timespan,
   * if it got ready, or <code>false</code> upon a timeout.
   * @param time the maximum time to wait
   * @param unit the time unit of the time argument
   * @return whether this updater is ready
   */
  public boolean waitUntilReady(long time, TimeUnit unit) {
    try {
      readyLock.lock();
      if (ready) {
        return true;
      }
      return readyCondition.await(time, unit);
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      readyLock.unlock();
    }
    return false;
  }

  /**
   * Starts the updating process, i.e., subscribe to the required topics, and updates the model.
   * @throws IOException if could not connect, or could not subscribe to a topic
   */
  public void start() throws IOException {
    if (ready) {
      return;
    }
    Objects.requireNonNull(this.host, "Host need to be set!");
    MQTT mqtt = new MQTT();
    mqtt.setHost(this.host);
    mqtt.setPassword(this.password);
    mqtt.setUserName(this.username);
    connection = mqtt.callbackConnection();
    AtomicReference<Throwable> error = new AtomicReference<>();
    connection.listener(new ExtendedListener() {
      public void onConnected() {
        logger.debug("Connected");
      }

      @Override
      public void onDisconnected() {
        logger.debug("Disconnected");
      }

      @Override
      public void onPublish(UTF8Buffer topic, Buffer body, Callback<Callback<Void>> ack) {
        String topicString = topic.toString();
        String message = body.ascii().toString();
//        logger.debug("{}: {}", topicString, message);
        onMessageCallback.accept(topicString, message);
        ack.onSuccess(null);  // always acknowledge message
      }

      @Override
      public void onPublish(UTF8Buffer topicBuffer, Buffer body, Runnable ack) {
        logger.warn("onPublish should not be called");
      }

      @Override
      public void onFailure(Throwable cause) {
//        logger.catching(cause);
        error.set(cause);
      }
    });
    throwIf(error);
    connection.connect(new Callback<Void>() {
      @Override
      public void onSuccess(Void value) {
        connection.publish("components", "Eraser is listening".getBytes(), QoS.AT_LEAST_ONCE, false, new Callback<Void>() {
          @Override
          public void onSuccess(Void value) {
            logger.debug("success sending welcome message");
          }

          @Override
          public void onFailure(Throwable value) {
            logger.debug("failure sending welcome message", value);
          }
        });
        Topic[] topicArray = Arrays.stream(topics).map(topicName -> new Topic(topicName, qos)).toArray(Topic[]::new);
        logger.info("Connected, subscribing to {} topic(s) now.", topicArray.length);
        connection.subscribe(topicArray, new Callback<byte[]>() {
          @Override
          public void onSuccess(byte[] qoses) {
            logger.debug("Subscribed, qoses: {}", qoses);
            try {
              readyLock.lock();
              ready = true;
              readyCondition.signalAll();
            } finally {
              readyLock.unlock();
            }
          }

          @Override
          public void onFailure(Throwable cause) {
            logger.error("Could not subscribe", cause);
          }
        });
      }

      @Override
      public void onFailure(Throwable cause) {
//        logger.error("Could not connect", cause);
        error.set(cause);
      }
    });
    throwIf(error);
  }

  private void throwIf(AtomicReference<Throwable> error) throws IOException {
    if (error.get() != null) {
      throw new IOException(error.get());
    }
  }

  public void close() {
    if (connection == null) {
      logger.warn("Stopping without connection. Was start() called?");
      return;
    }
    connection.disconnect(new Callback<Void>() {
      @Override
      public void onSuccess(Void value) {
        logger.info("Disconnected from {}", host);
      }

      @Override
      public void onFailure(Throwable ignored) {
        // Disconnects never fail.
      }
    });
  }
}
