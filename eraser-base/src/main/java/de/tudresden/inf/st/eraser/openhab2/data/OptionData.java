package de.tudresden.inf.st.eraser.openhab2.data;

/**
 * Part of the JSON return type of both StateDescription and parameter in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
public class OptionData {
  public String label;
  public String value;
}
