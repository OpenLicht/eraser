package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.Map;

/**
 * JSON return type for a Thing in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ThingData {
  public String label;
  public String bridgeUID;
  public Map<String, String> configuration;
  public Map<String, String> properties;
  public String UID;
  public String thingTypeUID;
  public List<ChannelData> channels;
  public String location;
  public StatusInfoData statusInfo;
  public FirmwareStatusData firmwareStatus;
  public Boolean editable;

  public static class ChannelData {
    public String uid;
    public String id;
    public String channelTypeUID;
    public String itemType;
    public String kind;
    public String label;
    public String description;
    public List<String> linkedItems;
    public List<String> defaultTags;
    public Map<String, String> properties;
    public Map<String, String> configuration;
  }

  public static class StatusInfoData {
    public String status;
    public String statusDetail;
    public String description;
  }

  public class FirmwareStatusData {
    public String status;
    public String updatableVersion;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("ThingData{");
    sb.append("label='").append(label).append('\'');
    sb.append(", bridgeUID='").append(bridgeUID).append('\'');
    sb.append(", configuration=").append(configuration);
    sb.append(", properties=").append(properties);
    sb.append(", UID='").append(UID).append('\'');
    sb.append(", thingTypeUID='").append(thingTypeUID).append('\'');
    sb.append(", channels=").append(channels);
    sb.append(", location='").append(location).append('\'');
    sb.append(", statusInfo=").append(statusInfo);
    sb.append(", firmwareStatus=").append(firmwareStatus);
    sb.append(", editable=").append(editable);
    sb.append('}');
    return sb.toString();
  }
}
