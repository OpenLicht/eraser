package de.tudresden.inf.st.eraser.deserializer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Deserialize JSON into an ASTNode.
 * @author jmey - Initial contribution on 5/15/17.
 * @author rschoene - Extension for reference resolving and null values
 */
public class ASTNodeDeserializer extends StdDeserializer<ASTNode> {

  interface ReadASTNode {
    ASTNode read() throws IOException;
  }

  private static final String packageName = Root.class.getPackage().getName();

  private static ObjectMapper mapper;
  private static ASTNodeDeserializer deser;

  private final Logger logger = LogManager.getLogger(ASTNodeDeserializer.class);

  private Map<ASTNode, Map<String, JsonNode>> toBeResolved = new HashMap<>();

  private void addResolver(Map<String, ResolveAstNode> r, Class<?> c, BiFunction<Root, String, Optional<? extends ASTNode>> f, String terminalName) {
    r.put(c.getName(), ((node, model) -> f.apply(model, termValue(node, terminalName))));
  }

  private void addResolverForSmartHomeEntityModel(Map<String, ResolveAstNodeForSmartHomeEntityModel> r, Class<?> c, BiFunction<SmartHomeEntityModel, String, Optional<? extends ASTNode>> f, String terminalName) {
    r.put(c.getName(), ((node, model) -> f.apply(model, termValue(node, terminalName))));
  }

  private Map<String, ResolveAstNode> resolvers = new HashMap<>();
  private Map<String, ResolveAstNodeForSmartHomeEntityModel> resolversForSmartHomeEntityModel = new HashMap<>();

  private void initResolvers() {
    addResolverForSmartHomeEntityModel(resolversForSmartHomeEntityModel, ThingType.class, SmartHomeEntityModel::resolveThingType, "ID");
    addResolverForSmartHomeEntityModel(resolversForSmartHomeEntityModel, ChannelType.class, SmartHomeEntityModel::resolveChannelType, "ID");
    addResolverForSmartHomeEntityModel(resolversForSmartHomeEntityModel, Item.class, SmartHomeEntityModel::resolveItem, "ID");
    addResolver(resolvers, MqttTopic.class, Root::resolveMqttTopic, "IncomingTopic");
  }

  private static void init() {
    if (mapper == null) {
      mapper = new ObjectMapper();
      SimpleModule module = new SimpleModule();
      deser = new ASTNodeDeserializer();
      module.addDeserializer(ASTNode.class, deser);
      mapper.registerModule(module);
    }
  }

  private String termValue(JsonNode node, String terminalName) throws IOException {
    return (String) deserializeTerminal(node.get("v").get("c").get(terminalName));
  }

  public ASTNodeDeserializer() {
    this(null);
  }

  public ASTNodeDeserializer(Class<?> vc) {
    super(vc);
    initResolvers();
  }

  private static Optional<Root> read(ReadASTNode reader) {
    init();
    try {
      ASTNode readValue = reader.read();

      if (readValue instanceof Root) {
        deser.finishResolving((Root) readValue);
        return Optional.of((Root) readValue);
      } else {
        throw new RuntimeException("Could not read a complete model");
      }

    } catch (IOException e) {
      LogManager.getLogger(ASTNodeDeserializer.class).catching(e);
    }
    return Optional.empty();
  }

  public static Root read(File file) {
    return read(() -> mapper.readValue(file, ASTNode.class))
        .orElseThrow(() -> new RuntimeException("Could not read the model file " + file.getName()));
  }

  public static Root read(InputStream inputStream) {
    return read(() -> mapper.readValue(inputStream, ASTNode.class))
        .orElseThrow(() -> new RuntimeException("Could not read the model from input."));
  }

  private void finishResolving(Root model) throws IOException {
    for (Map.Entry<ASTNode, Map<String, JsonNode>> entry : toBeResolved.entrySet()) {
      ASTNode incompleteNT = entry.getKey();
      for (Map.Entry<String, JsonNode> inner : entry.getValue().entrySet()) {
        String type = inner.getValue().get("t").asText();
        logger.trace("Resolving at node {} for child {} of type {} to {}",
            incompleteNT, inner.getKey(), type, inner.getValue());
        ResolveAstNode resolver = resolvers.get(type);
        if (resolver == null) {
          throw new RuntimeException("No resolver found for type " + type);
        }
        Optional<? extends ASTNode> resolvedNode = resolver.resolve(inner.getValue(), model);
        resolvedNode.ifPresent(n -> setChild(incompleteNT, inner.getKey(), n));
      }
    }
    toBeResolved.clear();
  }

  private void setChild(ASTNode incompleteNT, String childName, ASTNode newValue) {
    try {
      incompleteNT.getClass().getMethod("set" + childName, newValue.getClass()).invoke(incompleteNT, newValue);
    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new DeserializationException("Can not resolve child " + childName + " on " + incompleteNT, e);
    }
  }

  @Override
  public ASTNode deserialize(JsonParser jp, DeserializationContext ctxt)
    throws IOException {

    JsonNode node = jp.getCodec().readTree(jp);

    return (ASTNode) deserializeObject(node);
  }

  private Object deserializeObject(JsonNode node) throws IOException {
    if (node.isObject()) {
      String kind = node.get("k").asText();
      switch (kind) {
        case "NT":
          return deserializeNonterminal(node);
        case "List":
          return deserializeList(node);
        case "Opt":
          return deserializeOpt(node);
        case "t":
          return deserializeTerminal(node);
        case "enum":
          return deserializeEnum(node);
        case "NTRef":
          return deserializeIntrinsicReference(node);
        default:
          throw new DeserializationException("cannot deserialize node of unknown kind " + kind);
      }
    } else {
      if (node.isNull()) {
        return null;
      }
      throw new DeserializationException("cannot deserialize non-object node as object node!");
    }
  }

  private ASTNode deserializeNonterminal(JsonNode node) throws IOException {

    // get the type we want to create
    String type = node.get("t").asText();
    Class<?> typeClass;
    try {
      typeClass = Class.forName(packageName + "." + type);
    } catch (ClassNotFoundException e) {
      throw new DeserializationException("Unable to find class of type " + type + " in package " + packageName, e);
    }

    // create the instance
    ASTNode instance;
    try {
      instance = (ASTNode) (typeClass.getConstructor().newInstance());
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new DeserializationException("Unable to construct a nonterminal of type " + typeClass.getCanonicalName(), e);
    }

    // call every setter we have a field for
    Iterator<String> f = node.get("c").fieldNames();
    while (f.hasNext()) {
      String fieldName = f.next();

      // serialize the parameter
      Object parameter = deserializeObject(node.get("c").get(fieldName));
      if (parameter == null) {
        // skip setter, as null is the default value
        continue;
      }

      // find the setter to call
      boolean isList = node.get("c").get(fieldName).get("k").asText().equals("List");
      boolean isOpt = node.get("c").get(fieldName).get("k").asText().equals("Opt");
      // ... by getting its name
      String setterName = "set" + fieldName + (isList ? "List" : "") + (isOpt ? "Opt" : "");
      // ... and its type
      Class<?> setterType;
      if (isList) {
        setterType = JastAddList.class;
      } else if (isOpt) {
        setterType = Opt.class;
      } else {
        setterType = parameter.getClass();
      }
      Class<?> originalSettType = setterType;

      if (setterType.equals(ResolveLater.class)) {
        ResolveLater rl = (ResolveLater) parameter;
        toBeResolved.computeIfAbsent(instance, k -> new HashMap<>()).put(fieldName, rl.node);
        logger.trace("resolve later. {}, childName: {}", instance, fieldName);
        continue;
      }

      // get the method
      Method method = null;
      while(setterType != null && method == null) {
        try {
          method = typeClass.getMethod(setterName, setterType);
        } catch (NoSuchMethodException e1) {
          try {
            if (setterType.equals(Integer.class)) {
              method = typeClass.getMethod(setterName, int.class);
            } else if (setterType.equals(Double.class)) {
              method = typeClass.getMethod(setterName, double.class);
            } else if (setterType.equals(Long.class)) {
              method = typeClass.getMethod(setterName, long.class);
            } else if (setterType.equals(Character.class)) {
              method = typeClass.getMethod(setterName, char.class);
            } else if (setterType.equals(Boolean.class)) {
              method = typeClass.getMethod(setterName, boolean.class);
            } else if (setterType.equals(Float.class)) {
              method = typeClass.getMethod(setterName, float.class);
            }
            setterType = setterType.getSuperclass();
          } catch (NoSuchMethodException e2) {
            throw new DeserializationException("Unable to set value of " + fieldName + " with setter " + setterName, e2);
          }
        }
      }
      if (method == null) {
        throw new DeserializationException("Unable to set value of " + fieldName + " with setter " + setterName + " of type " + originalSettType.getSimpleName() + "!");
      }

      // invoke the method on the instance with the parameter
      try {
        method.invoke(instance, parameter);
      } catch (IllegalAccessException | InvocationTargetException e) {
        throw new DeserializationException("Unable to set value of " + fieldName + " with setter " + setterName, e);
      }
    }

    // finally, return the instance
    return instance;
  }

  private ASTNode deserializeOpt(JsonNode node) throws IOException {
    if (node.has("c")) {
      // opts can only contain Nonterminals
      ASTNode value = deserializeNonterminal(node.get("c"));
      return new Opt<ASTNode>(value);

    } else {
      return new Opt();
    }
  }

  private Object deserializeTerminal(JsonNode node) throws IOException {
    if (node == null) {
      logger.warn("Got null for parsing terminal.");
      return null;
    }
    // get the type name
    JsonNode typeNode = node.get("t");
    if (typeNode == null) {
      throw new IOException("Node has no field 't': " + node);
    }
    String typeName = typeNode.asText();

    // first try the builtin types
    switch (typeName) {
      case "int":
      case "Integer":
        return node.get("v").asInt();
      case "float":
      case "Float":
        return (float) node.get("v").asDouble();
      case "boolean":
      case "Boolean":
        return node.get("v").asBoolean();
      case "double":
      case "Double":
        return node.get("v").asDouble();
      case "java.lang.String":
      case "String":
        return node.get("v").asText();
      case "long":
      case "Long":
        return node.get("v").asLong();
      default:
        if (typeName.startsWith(packageName)) {
          // this is a runtime-reference, store it to be later resolved
          return new ResolveLater(node);
        }
        throw new DeserializationException("cannot create object of type " + typeName);
    }
  }

  private Enum deserializeEnum(JsonNode node) {
    // check for null
    if (node.get("v").isNull()) {
      return null;
    }

    // get the type name
    String typeName = node.get("t").asText();

    Class<?> type;
    try {
      type = Class.forName(typeName);
    } catch (ClassNotFoundException e) {
      throw new DeserializationException("cannot create enum of type " + typeName, e);
    }

    Method valueOf;
    try {
      valueOf = type.getMethod("valueOf", String.class);
    } catch (NoSuchMethodException e) {
      throw new DeserializationException("cannot call valueOf() on enum of type " + typeName, e);
    }
    try {
      return (Enum) valueOf.invoke(null, node.get("v").asText());
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new DeserializationException("cannot call valueOf() on enum of type " + typeName, e);
    }
  }

  private JastAddList deserializeList(JsonNode node) throws IOException {
    JastAddList<ASTNode> list = new JastAddList<>();
    Iterator<JsonNode> it = node.get("c").elements();
    while (it.hasNext()) {
      JsonNode child = it.next();
      // lists can only contain Nonterminals
      list.add(deserializeNonterminal(child));
    }
    return list;
  }

  private Object deserializeIntrinsicReference(JsonNode node) {
    return new ResolveLater(node);
  }

  }
interface ResolveAstNode {
  Optional<? extends ASTNode> resolve(JsonNode node, Root model) throws IOException;
}


interface ResolveAstNodeForSmartHomeEntityModel {
  Optional<? extends ASTNode> resolve(JsonNode node, SmartHomeEntityModel model) throws IOException;
}

class ResolveLater {
  JsonNode node;

  ResolveLater(JsonNode node) {
    this.node = node;
  }
}
