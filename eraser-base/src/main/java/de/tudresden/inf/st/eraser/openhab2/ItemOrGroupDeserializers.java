package de.tudresden.inf.st.eraser.openhab2;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.tudresden.inf.st.eraser.openhab2.data.AbstractItemData;
import de.tudresden.inf.st.eraser.openhab2.data.GroupItemData;
import de.tudresden.inf.st.eraser.openhab2.data.ItemData;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Custom deserializer to distinguish groups and normal items.
 * Inspired by <a href="http://www.robinhowlett.com/blog/2015/03/19/custom-jackson-polymorphic-deserialization-without-type-metadata/">robin</a>
 *
 * @author rschoene - Initial contribution
 */
public class ItemOrGroupDeserializers extends StdDeserializer<AbstractItemData> {

  protected ItemOrGroupDeserializers() {
    super(AbstractItemData.class);
  }

  @Override
  public AbstractItemData deserialize(JsonParser jp, DeserializationContext context) throws IOException {
    ObjectMapper mapper = (ObjectMapper) jp.getCodec();
    ObjectNode obj = mapper.readTree(jp);
    Iterator<Map.Entry<String, JsonNode>> elementsIterator = obj.fields();

    Class<? extends AbstractItemData> clazz = ItemData.class;
    while (elementsIterator.hasNext()) {
      Map.Entry<String, JsonNode> element = elementsIterator.next();
      String name = element.getKey();
      if ("members".equals(name)) {
        clazz = GroupItemData.class;
        break;
      }
    }
    return mapper.treeToValue(obj, clazz);
  }

}
