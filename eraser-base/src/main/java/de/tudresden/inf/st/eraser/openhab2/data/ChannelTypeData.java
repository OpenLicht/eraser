package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * JSON return type for a channel type in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ChannelTypeData {
  public List<ParameterData> parameters;
  public List<ParameterGroupData> parameterGroups;
  public String description;
  public String label;
  public String category;
  public String itemType;
  public String kind;
  public StateDescriptionData stateDescription;
  public List<String> tags;
  public String UID;
  public Boolean advanced;

  public static class ParameterGroupData {
    public String name;
    public String context;
    public Boolean advanced;
    public String label;
    public String description;
  }
}
