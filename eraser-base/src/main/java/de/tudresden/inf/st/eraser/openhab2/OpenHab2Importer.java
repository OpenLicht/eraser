package de.tudresden.inf.st.eraser.openhab2;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.openhab2.data.*;
import de.tudresden.inf.st.eraser.util.JavaUtils;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import de.tudresden.inf.st.eraser.util.Tuple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Import AST from running openHAB 2 server.
 * @author rschoene - Initial contribution
 */
public class OpenHab2Importer {

  protected static final String thingTypesUrl = "http://%s/rest/thing-types";
  protected static final String thingTypeDetailUrl = "http://%s/rest/thing-types/%s";
  protected static final String channelTypeUrl = "http://%s/rest/channel-types";
  protected static final String thingsUrl = "http://%s/rest/things";
  protected static final String itemsUrl = "http://%s/rest/items";
  protected static final String linksUrl = "http://%s/rest/links";

  private static final String NULL_STATE = "NULL";

  private final Logger logger;
  private final Set<String> nonDefaultChannelCategories;

  public OpenHab2Importer() {
    logger = LogManager.getLogger(OpenHab2Importer.class);
    nonDefaultChannelCategories = new HashSet<>();
  }

  public SmartHomeEntityModel importFrom(String host, int port) {
    /*
    Plan:
    - requesting: thing-types, channel-types, things, items, links
     */
    String hostAndPort = host + ":" + port;
    ObjectMapper mapper = new ObjectMapper();

    SimpleModule module = new SimpleModule("GroupOrItemDataModule");
    JsonDeserializer<? extends AbstractItemData> deserializer = new ItemOrGroupDeserializers();
    module.addDeserializer(AbstractItemData.class, deserializer);

    mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
    mapper.registerModule(module);

    try {
      Root root = Root.createEmptyRoot();
      SmartHomeEntityModel model = root.getSmartHomeEntityModel();
      ThingTypeData[] thingTypeList = mapper.readValue(makeURL(thingTypesUrl, hostAndPort), ThingTypeData[].class);
      logger.info("Read a total of {} thing type(s).", thingTypeList.length);
      update(model, thingTypeList);

      ChannelTypeData[] channelTypeList = mapper.readValue(makeURL(channelTypeUrl, hostAndPort), ChannelTypeData[].class);
      logger.info("Read a total of {} channel type(s).", channelTypeList.length);
      update(model, channelTypeList);

      ThingData[] thingList = mapper.readValue(makeURL(thingsUrl, hostAndPort), ThingData[].class);
      logger.info("Read a total of {} thing(s).", thingList.length);
      update(model, thingList);

      AbstractItemData[] itemList = mapper.readValue(makeURL(itemsUrl, hostAndPort), AbstractItemData[].class);
      logger.info("Read a total of {} item(s) including groups.", itemList.length);
      update(model, itemList);

      root.treeResolveAll();
      root.doSafeFullTraversal();

      LinkData[] linkList = mapper.readValue(makeURL(linksUrl, hostAndPort), LinkData[].class);
      logger.info("Read a total of {} link(s).", linkList.length);
      update(model, linkList);

      // get parameters of thing types
      for (ThingType thingType : model.getThingTypeList()) {
        ThingTypeData data = mapper.readValue(makeURL(thingTypeDetailUrl, hostAndPort, thingType.getID()), ThingTypeData.class);
        update(thingType, data);
      }
      return model;
    } catch (IOException e) {
      logger.catching(e);
      return null;
    }
  }

  protected URL makeURL(String formatUrlString, String hostAndPort) throws MalformedURLException {
    return URI.create(String.format(formatUrlString, hostAndPort)).toURL();
  }

  protected URL makeURL(String formatUrlString, String hostAndPort, String id) throws MalformedURLException {
    return URI.create(String.format(formatUrlString, hostAndPort, id)).toURL();
  }

  private void update(SmartHomeEntityModel model, ThingTypeData[] thingTypeList) {
    for (ThingTypeData thingTypeData : thingTypeList) {
      ThingType thingType = new ThingType();
      thingType.setID(thingTypeData.UID);
      thingType.setLabel(thingTypeData.label);
      thingType.setDescription(thingTypeData.description);
      model.addThingType(thingType);
    }
  }

  private void update(SmartHomeEntityModel model, ChannelTypeData[] channelTypeList) {
    for (ChannelTypeData channelTypeData : channelTypeList) {
      ChannelType channelType = new ChannelType();
      model.addChannelType(channelType);
      channelType.setID(channelTypeData.UID);
      channelType.setLabel(channelTypeData.label);
      channelType.setDescription(channelTypeData.description);
      maybeSetItemType(channelType, channelTypeData.itemType);
      maybeSetChannelCategory(channelType, channelTypeData.category);
      maybeSetReadOnly(channelType, channelTypeData.stateDescription);
    }
  }

  private void maybeSetItemType(ChannelType channelType, String itemType) {
    if (itemType == null) {
      return;
    }
    try {
      channelType.setItemType(ItemType.valueOf(itemType));
    } catch (IllegalArgumentException e) {
      logger.warn("Could not find ItemType for '{}'", itemType);
      if (logger.isDebugEnabled()) {
        logger.debug("Possible values: {}",
            Arrays.stream(ItemType.values())
                .map(ItemType::toString)
                .collect(Collectors.joining(", ")));
      }
    }
  }

  private void maybeSetChannelCategory(ChannelType channelType, String category) {
    if (category == null) {
      return;
    }
    JavaUtils.ifPresentOrElse(channelType.getRoot().getSmartHomeEntityModel().resolveDefaultChannelCategory(category),
        dcc -> channelType.setChannelCategory(new ReferringChannelCategory(dcc)),
        () -> {
          // channel category was not found
          // store in unresolved and only warn once about it
          if (nonDefaultChannelCategories.add(category)) {
            logger.warn("Could not find ChannelCategory for '{}'", category);
          }
          channelType.setChannelCategory(new SimpleChannelCategory(category));
        });
  }

  private void maybeSetReadOnly(ChannelType channelType, StateDescriptionData stateDescription) {
    if (stateDescription != null) {
      channelType.setReadOnly(stateDescription.readOnly);
    }
  }

  private void update(SmartHomeEntityModel model, ThingData[] thingList) {
    for (ThingData thingData : thingList) {
      Thing thing = new Thing();
      thing.setID(thingData.UID);
      thing.setLabel(thingData.label);
      ifPresent(model.resolveThingType(thingData.thingTypeUID), "ThingType", thingData,
          thing::setType);
      model.addThing(thing);
      // add channels
      for (ThingData.ChannelData channelData : thingData.channels) {
        Channel channel = new Channel();
        channel.setID(channelData.uid);
        ifPresent(model.resolveChannelType(channelData.channelTypeUID),
            "channelType", channelData, channel::setType);
        model.addChannel(channel);
        // now set relation
        thing.addChannel(channel);
      }
    }
  }

  private void update(SmartHomeEntityModel model, AbstractItemData[] itemList) {
    List<Tuple<Group, GroupItemData>> groupsWithMembers = new ArrayList<>();
    List<Tuple<Group, GroupItemData>> groupsInGroups = new ArrayList<>();
    List<Tuple<Item, AbstractItemData>> itemsInGroups = new ArrayList<>();
    List<Item> itemsWithoutGroup = new ArrayList<>();
    for (AbstractItemData itemData : itemList) {
      if (itemData instanceof GroupItemData) {
        // create a new group
        Group group = new Group();
        GroupItemData groupItemData = (GroupItemData) itemData;
        if (groupItemData.members.size() > 0) {
          groupsWithMembers.add(Tuple.of(group, groupItemData));
        }
        if (groupItemData.groupNames.size() > 0) {
          groupsInGroups.add(Tuple.of(group, groupItemData));
        }
        if (groupItemData.function != null) {
          try {
            // try, if simple name matches
            SimpleGroupAggregationFunctionName name = SimpleGroupAggregationFunctionName.valueOf(
                groupItemData.function.name);
            group.setAggregationFunction(new SimpleGroupAggregationFunction(name));
          } catch (IllegalArgumentException simpleEx) {
            // so it was a parameterized.
            try {
              ParameterizedGroupAggregationFunctionName name = ParameterizedGroupAggregationFunctionName.valueOf(
                  groupItemData.function.name);
              group.setAggregationFunction(new ParameterizedGroupAggregationFunction(name,
                  groupItemData.function.params.get(0), groupItemData.function.params.get(1)));
            } catch (IllegalArgumentException paramEx) {
              logger.warn("Could not resolve group aggregation function name '{}'! Using default equality.",
                  groupItemData.function.name);
            }
          }
        }
        group.setID(itemData.name);
        if (!itemData.label.isEmpty()) {
          group.setLabel(itemData.label);
        }
        model.addGroup(group);
      } else {
        // create a new item
        Item item = createItem(itemData.type);
        item.setID(itemData.name);
        item.setLabel(itemData.label);
        if (itemData.category != null && !itemData.category.isEmpty()) {
          item.setCategory(model.resolveItemCategory(itemData.category).orElseGet(() -> {
            ItemCategory category = new ItemCategory(itemData.category);
            model.addItemCategory(category);
            return category;
          }));
        }
        // if state is not set in openHAB, then use default state for the item
        item.disableSendState();
        if (itemData.state.equals(NULL_STATE)) {
          item.setStateToDefault();
        } else {
          item.setStateFromString(itemData.state, false);
        }
        item.enableSendState();
        if (itemData.metadata != null) {
          MetaData metaData = new MetaData();
          item.setMetaData(metaData);
          for (Map.Entry<String, MetaDataData> entry : itemData.metadata.entrySet()) {
            logger.debug("Add metadata for namespace {}", entry.getKey());
            for (Map.Entry<String, String> metaDataEntry : entry.getValue().config.entrySet()) {
              metaData.add(metaDataEntry.getKey(), metaDataEntry.getValue());
            }
          }
        }
        if (itemData.groupNames.size() > 0) {
          itemsInGroups.add(Tuple.of(item, itemData));
        } else {
          itemsWithoutGroup.add(item);
        }
      }
    }
    for (Tuple<Group, GroupItemData> tuple : groupsWithMembers) {
      for (String member : tuple.y.members) {
        // either an item
        ifPresent(model.resolveItem(member), "member", tuple.y, tuple.x::addItem);
        // or a group
        ifPresent(model.resolveGroup(member), "member", tuple.y, tuple.x::addGroup);
      }
    }
    for (Tuple<Group, GroupItemData> tuple : groupsInGroups) {
      for (String parentGroup : tuple.y.groupNames) {
        ifPresent(model.resolveGroup(parentGroup), "parentGroup", tuple.y,
            parent -> parent.addGroup(tuple.x));
      }
    }
    for (Tuple<Item, AbstractItemData> tuple : itemsInGroups) {
      for (String parentGroup : tuple.y.groupNames) {
        ifPresent(model.resolveGroup(parentGroup), "parentGroup", tuple.y,
            parent -> parent.addItem(tuple.x));
      }
    }
    if (!itemsWithoutGroup.isEmpty()) {
      ParserUtils.addToUnknownGroup(model, itemsWithoutGroup);
    }
  }

  private Item createItem(String type) {
    switch (type) {
      case "Color": return new ColorItem();
      case "Contact": return new ContactItem();
      case "DateTime": return new DateTimeItem();
      case "Dimmer": return new DimmerItem();
      case "Image": return new ImageItem();
      case "Location": return new LocationItem();
      case "Number": return new NumberItem();
      case "Player": return new PlayerItem();
      case "RollerShutter": return new RollerShutterItem();
      case "String": return new StringItem();
      case "Switch": return new SwitchItem();
      default: return new DefaultItem();
    }
  }

  private void update(SmartHomeEntityModel model, LinkData[] linkList) {
    for (LinkData linkData : linkList) {
      ifPresent(model.resolveChannel(linkData.channelUID), "Channel", linkData,
          channel -> ifPresent(model.resolveItem(linkData.itemName), "Item", linkData, channel::addLinkedItem));
    }
  }

  private void update(ThingType thingType, ThingTypeData data) {
    // just set parameters
    if (data == null || data.configParameters == null) {
      logger.warn("No parameters given for thing type {}", thingType.getID());
      return;
    }
    for (ParameterData parameterData : data.configParameters) {
      Parameter parameter = new Parameter();
      parameter.setID(parameterData.name);
      parameter.setLabel(parameterData.label);
      parameter.setDescription(parameterData.description);
      parameter.setContext(parameterData.context);
      if (parameterData.defaultValue != null) {
        parameter.setDefaultValue(new ParameterDefaultValue(parameterData.defaultValue));
      }
      parameter.setRequired(parameterData.required);
      parameter.setType(ParameterValueType.valueOf(titleCase(parameterData.type)));
      thingType.addParameter(parameter);
    }
  }

  private String titleCase(String type) {
    return Character.toTitleCase(type.charAt(0)) + type.substring(1).toLowerCase();
  }

  @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
  private <T> void ifPresent(Optional<T> opt, String type, Object data,
                             Consumer<? super T> consumer) {
    if (opt.isPresent()) {
      consumer.accept(opt.get());
    } else {
      logger.warn("Missing or unresolved {} in {}", type, data);
    }
  }

  public SmartHomeEntityModel importFrom(URL baseUrl) {
    return importFrom(baseUrl.getHost(),
        baseUrl.getPort() == -1 ? baseUrl.getDefaultPort() : baseUrl.getPort());
  }
}
