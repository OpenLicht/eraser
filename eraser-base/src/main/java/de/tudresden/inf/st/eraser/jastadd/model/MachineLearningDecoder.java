package de.tudresden.inf.st.eraser.jastadd.model;

import java.time.Instant;

/**
 * This interface represents the connection from a machine learning model back to the knowledge base.
 * It decodes the output of the machine learning model and outputs the result of the classification.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
public interface MachineLearningDecoder extends MachineLearningSetRoot {

  /**
   * Execute the machine learning model and returns the classification result.
   * @return the result of the classification
   */
  MachineLearningResult classify();

  // less important

  /**
   * Returns the time when the model was last updated, i.e., when the last training was completed.
   * @return the time when the model was last updated, or <code>null</code> if the model was not trained yet
   */
  Instant lastModelUpdate();

}
