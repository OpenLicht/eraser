package de.tudresden.inf.st.eraser;

import beaver.Parser;
import de.tudresden.inf.st.eraser.deserializer.ASTNodeDeserializer;
import de.tudresden.inf.st.eraser.jastadd.model.Root;
import de.tudresden.inf.st.eraser.openhab2.OpenHab2Importer;
import de.tudresden.inf.st.eraser.openhab2.mqtt.MQTTUpdater;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;

/**
 * Main entry point for testing eraser.
 * @author rschoene - Initial contribution
 */
@SuppressWarnings({"unused", "RedundantThrows"})
public class Main {

  public static void main(String[] args) throws IOException, Parser.Exception {
//    testSimple();
//    Root model = testParser();
//    Root model = importFromOpenHab();
//    testPrinterWith(model);
//    testUpdaterWith(model);
  }

  private static double readFromSystemIn(BufferedReader in, String prompt) throws IOException {
    System.out.println(prompt);
    String line = in.readLine();
    return Double.parseDouble(line);
  }

  private static Root testParser() throws IOException, Parser.Exception {
//    return ParserUtils.load("openhab-data.eraser", Main.class);
    ParserUtils.setVerboseLoading(true);
    return ParserUtils.load("easy-data.eraser", Main.class);
  }

  private static void testPrinterWith(Root model) {
    model.flushTreeCache();
    System.out.println("Got model: " + model.getSmartHomeEntityModel().description());
    System.out.println("PrettyPrinted:");
    System.out.println(model.prettyPrint());
  }

  private static void testUpdater() {
    Root model;
//    model = importFromOpenHab();
    model = importFromFile();
    System.out.println("Got model: " + model.getSmartHomeEntityModel().description());
//    JsonSerializer.write(model, "openhab2-data.json");
    testUpdaterWith(model);
  }

  private static void testUpdaterWith(Root model) {
    final int seconds = 10;
    System.out.println("Start!");
    try (MQTTUpdater updater = new MQTTUpdater(model)) {
      LogManager.getLogger(Main.class).info("Processing mqtt updates for {} seconds", seconds);
      updater.start();
      Thread.sleep(seconds * 1000);
    } catch (IllegalArgumentException | InterruptedException | IOException e) {
      LogManager.getLogger(Main.class).catching(e);
    }
  }

  private static void testSimple() {
    String s = "hingType: id=";
    for (Character c : s.toCharArray()) {
      System.out.println("'" + c + "'.isJavaIdentifierPart: " + Character.isJavaIdentifierPart(c));
    }
    System.exit(0);
  }

  public static Root importFromOpenHab() {
    OpenHab2Importer importer = new OpenHab2Importer();
    return importer.importFrom("192.168.1.250", 8080).getRoot();
  }

  public static Root importFromFile() {
    final String filename = "/openhab2-data.json";
    InputStream inputStream = Main.class.getResourceAsStream(filename);
    return ASTNodeDeserializer.read(inputStream);
  }
}
