package de.tudresden.inf.st.eraser.jastadd.model;

/**
 * Common interface for both {@link MachineLearningDecoder} and {@link MachineLearningEncoder}.
 *
 * @author rschoene - Initial contribution
 */
public interface MachineLearningSetRoot {

  /**
   * Informs this instance of the knowledge base.
   * This method is called before any other of the interface methods.
   * @param root The root node of the knowledge base
   */
  void setKnowledgeBaseRoot(Root root);

}
