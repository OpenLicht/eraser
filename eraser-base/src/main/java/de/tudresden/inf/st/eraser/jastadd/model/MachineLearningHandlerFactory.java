package de.tudresden.inf.st.eraser.jastadd.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

/**
 * Factory to create new handlers ({@link MachineLearningEncoder} and {@link MachineLearningDecoder}).
 *
 * <p>
 *   The protocol to use this class is as follows:
 *   <ul>
 *     <li>Set the model using {@link #setKnowledgeBaseRoot(Root) setKnowledgeBaseRoot()}</li>
 *     <li>Initialize using a config using {@link #initializeFor(MachineLearningHandlerFactoryTarget, URL) initializeFor()}</li>
 *     <li>Create the models using {@link #createModel()}</li>
 *     <li>Once finished, shutdown the factory using {@link #shutdown()}</li>
 *   </ul>
 * </p>
 *
 * @author rschoene - Initial contribution
 */
public abstract class MachineLearningHandlerFactory implements MachineLearningSetRoot {

  protected Root knowledgeBase;

  public enum MachineLearningHandlerFactoryTarget {
    ACTIVITY_RECOGNITION,
    PREFERENCE_LEARNING
  }

  @Override
  public void setKnowledgeBaseRoot(Root root) {
    this.knowledgeBase = root;
  }

  /**
   * Initialize this factory for a certain target and a configuration.
   * @param target    The target for which the model should be used for
   * @param configUrl The location of the configuration
   * @throws IOException If an error happen during loading the configuration
   * @throws ClassNotFoundException If an error happens while applying the configuration
   */
  public abstract void initializeFor(MachineLearningHandlerFactoryTarget target, URL configUrl) throws IOException, ClassNotFoundException;

  /**
   * Creates just the {@link MachineLearningEncoder}.
   * <p><b>Note: </b> {@link #createModel()} should be used instead of this method.</p>
   * @return the new encoder
   */
  public abstract MachineLearningEncoder createEncoder();

  /**
   * Creates just the {@link MachineLearningDecoder}.
   * <p><b>Note: </b> {@link #createModel()} should be used instead of this method.</p>
   * @return the new decoder
   */
  public abstract MachineLearningDecoder createDecoder();

  /**
   * Creates a new model.
   * To create an {@link InternalMachineLearningModel}, this method needs to be overridden.
   * @return the created machine learning model
   */
  public MachineLearningModel createModel() {
    ExternalMachineLearningModel result = new ExternalMachineLearningModel();
    result.setEncoder(createEncoder());
    result.setDecoder(createDecoder());
    return result;
  }

  /**
   * Shuts down this factory, and free held resources, if any.
   * Subclasses should override the default empty implementation, if needed.
   */
  public void shutdown() {
    // empty by default
  }

  /**
   * Creates a new factory to be used, if there was an error during configuration of a factory
   * @return a new factory logging warning messages upon each invoked method
   */
  public static MachineLearningHandlerFactory createErrorFactory() {
    return new MachineLearningHandlerFactory() {
      private final Logger logger = LogManager.getLogger(MachineLearningHandlerFactory.class);
      @Override
      public void initializeFor(MachineLearningHandlerFactoryTarget target, URL configUrl) {
        logger.warn("initializeFor called for ErrorFactory");
      }

      @Override
      public MachineLearningEncoder createEncoder() {
        return new MachineLearningEncoder() {
          @Override
          public void newData(List<Item> changedItems) {
            logger.warn("newData called for encoder of ErrorFactory");
          }

          @Override
          public List<Item> getTargets() {
            logger.warn("getTargets called for encoder of ErrorFactory");
            return Collections.emptyList();
          }

          @Override
          public List<Item> getRelevantItems() {
            logger.warn("getRelevantItems called for encoder of ErrorFactory");
            return Collections.emptyList();
          }

          @Override
          public void triggerTraining() {
            logger.warn("triggerTraining called for encoder of ErrorFactory");
          }

          @Override
          public void setKnowledgeBaseRoot(Root root) {
            logger.warn("setKnowledgeBaseRoot called for encoder of ErrorFactory");
          }
        };
      }

      @Override
      public MachineLearningDecoder createDecoder() {
        return new MachineLearningDecoder() {
          @Override
          public MachineLearningResult classify() {
            logger.warn("classify called for decoder of ErrorFactory");
            return new MachineLearningResult();
          }

          @Override
          public Instant lastModelUpdate() {
            logger.warn("lastModelUpdate called for decoder of ErrorFactory");
            return null;
          }

          @Override
          public void setKnowledgeBaseRoot(Root root) {
            logger.warn("setKnowledgeBaseRoot called for decoder of ErrorFactory");
          }
        };
      }
    };
  }
}
