package de.tudresden.inf.st.eraser.openhab2.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * JSON return type for a parameter in openHAB 2, used within a channel type.
 *
 * @author rschoene - Initial contribution
 */
//@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ParameterData {
  public String context;
  public String defaultValue;
  public String description;
  public String label;
  public String name;
  public Boolean required;
  public String type;
//  public Integer min;
  public Integer max;
//  public Integer stepsize;
  public String pattern;
  public Boolean readOnly;
//  public Boolean multiple;
//  public Integer multipleLimit;
//  public String groupName;
//  public Boolean advanced;
//  public Boolean verify;
//  public Boolean limitToOptions;
  public String unit;
//  public String unitLabel;
//  public List<OptionData> options;
//  public List<FilterCriteriaData> filterCriteria;

//  public static class FilterCriteriaData {
//    public String name;
//    public String value;
//  }
}
