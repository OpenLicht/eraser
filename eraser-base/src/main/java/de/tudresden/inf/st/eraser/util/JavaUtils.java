package de.tudresden.inf.st.eraser.util;

import de.tudresden.inf.st.eraser.jastadd.model.ASTNode;
import de.tudresden.inf.st.eraser.jastadd.model.JastAddList;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * General utils, or future methods not available in Java 1.8 yet.
 *
 * @author rschoene - Initial contribution
 */
public class JavaUtils {

  @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
  public static <T> void ifPresentOrElse(Optional<T> optional, Consumer<? super T> action, Runnable emptyAction) {
    if (optional.isPresent()) {
      action.accept(optional.get());
    } else {
      emptyAction.run();
    }
  }

  @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
  public static <T, R> R ifPresentOrElseReturn(Optional<T> optional, Function<? super T, R> action, Supplier<R> emptyAction) {
    if (optional.isPresent()) {
      return action.apply(optional.get());
    } else {
      return emptyAction.get();
    }
  }

  public static <T extends ASTNode> List<T> toJavaList(JastAddList<T> jastAddList) {
    return toStream(jastAddList).collect(Collectors.toList());
  }

  public static <T extends ASTNode> Stream<T> toStream(JastAddList<T> jastAddList) {
    return StreamSupport.stream(jastAddList.spliterator(), false);
  }

  public static String toTitleCase(String s) {
    if (s == null || s.isEmpty()) {
      return s;
    }
    return s.substring(0, 1).toUpperCase() + s.substring(1);
  }

}
