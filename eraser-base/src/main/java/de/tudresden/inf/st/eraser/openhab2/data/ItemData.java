package de.tudresden.inf.st.eraser.openhab2.data;

/**
 * JSON return type for an item in openHAB 2.
 *
 * @author rschoene - Initial contribution
 */
public class ItemData extends AbstractItemData {
  // there are no special fields
}
