package de.tudresden.inf.st.eraser.openhab2.mqtt;

import de.tudresden.inf.st.eraser.jastadd.model.ExternalHost;
import de.tudresden.inf.st.eraser.jastadd.model.Item;
import de.tudresden.inf.st.eraser.jastadd.model.Root;
import de.tudresden.inf.st.eraser.util.MqttReceiver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fusesource.mqtt.client.QoS;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Update an imported model by subscribing to MQTT topics.
 *
 * @author rschoene - Initial contribution
 */
public class MQTTUpdater implements AutoCloseable {

  private final Logger logger;

  /** Receiver actually receiving messages. */
  private MqttReceiver delegatee;

  public MQTTUpdater() {
    this.logger = LogManager.getLogger(MQTTUpdater.class);
    this.delegatee = new MqttReceiver();
  }

  public MQTTUpdater(Root root) throws IllegalArgumentException {
    this();
    this.setRoot(root);
  }

  /**
   * Sets the model root to update
   * @param root the model root to update
   */
  public void setRoot(Root root) {
    ExternalHost host = root.getMqttRoot().getHost();
    delegatee.setHost(host);
    delegatee.setOnMessage((topicString, message)->
        root.getMqttRoot().resolveTopic(topicString).ifPresent(topic ->
            topic.getItems().forEach(
                item -> itemUpdate(item, message))));
    delegatee.setTopicsForSubscription(root.getMqttRoot().getIncomingPrefix() + "#");
    delegatee.setQoSForSubscription(QoS.AT_LEAST_ONCE);
  }

  private void itemUpdate(Item item, String state) {
    String oldState = item.getStateAsString();
    if (oldState == null || !oldState.equals(state)) {
      this.logger.debug("Update state of {} [{}] from '{}' to '{}'.",
          item.getLabel(), item.getID(), oldState, state);
      item.setStateFromString(state, false);
    }
  }

  /**
   * Waits until this updater is ready to receive MQTT messages.
   * If it already is ready, return immediately with the value <code>true</code>.
   * Otherwise waits for the given amount of time, and either return <code>true</code> within the timespan,
   * if it got ready, or <code>false</code> upon a timeout.
   * @param time the maximum time to wait
   * @param unit the time unit of the time argument
   * @return whether this updater is ready
   */
  public boolean waitUntilReady(long time, TimeUnit unit) {
    return delegatee.waitUntilReady(time, unit);
  }

  /**
   * Starts the updating process, i.e., subscribe to the required topics, and updates the model.
   * @throws IOException if could not connect, or could not subscribe to a topic
   */
  public void start() throws IOException {
    delegatee.start();
  }

  public void close() {
    delegatee.close();
  }
}
