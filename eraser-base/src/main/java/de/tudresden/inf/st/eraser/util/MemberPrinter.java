package de.tudresden.inf.st.eraser.util;

import de.tudresden.inf.st.eraser.jastadd.model.ASTNode;
import de.tudresden.inf.st.eraser.jastadd.model.ModelElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Fluent API to pretty-print ASTNodes in a line-based fashion.
 *
 * @author rschoene - Initial contribution
 */
public class MemberPrinter {

  private static final Logger logger = LogManager.getLogger(MemberPrinter.class);
  private static boolean addSemicolonNewlineWhenBuild = true;
  private static boolean emitNothingOnEmptyBody = true;
  private boolean empty = true;
  private final StringBuilder sb;

  public enum ListBracketType {
    SQUARE("[", "]"),
    ROUND("(", ")"),
    CURLY("{", "}");

    private final String begin;
    private final String end;

    ListBracketType(String begin, String end) {
      this.begin = begin;
      this.end = end;
    }
  }

  public MemberPrinter(String elementName) {
    this.sb = new StringBuilder();
    if (elementName != null && !elementName.isEmpty()) {
      sb.append(elementName).append(":");
    }
  }

  public static void setAddSemicolonNewlineWhenBuild(boolean addSemicolonNewlineWhenBuild) {
    MemberPrinter.addSemicolonNewlineWhenBuild = addSemicolonNewlineWhenBuild;
  }

  public static void setEmitNothingOnEmptyBody(boolean emitNothingOnEmptyBody) {
    MemberPrinter.emitNothingOnEmptyBody = emitNothingOnEmptyBody;
  }

  public String build() {
    if (empty) {
      if (emitNothingOnEmptyBody) {
        return "";
      } else {
        logger.debug("Emitting empty body {}", sb);
      }
    }
    if (addSemicolonNewlineWhenBuild) {
      sb.append(" ;\n");
    }
    return sb.toString();
  }

  /**
   * Given a list of elements, concat the quoted id's of those elements, separated by commas,
   * and appends it to the given StringBuilder.
   * @param listOfElements the list of elements to concat
   */
  private void concatIds(Iterable<? extends ModelElement> listOfElements) {
    concatNodes(listOfElements, ModelElement::getID, true);
  }

  /**
   * Given a list of elements, concat the quoted id's of those elements, separated by commas,
   * and appends it to the given StringBuilder.
   * @param listOfNodes the list of nodes
   * @param mapping a function to map a node to a ModelElement
   */
  private <T extends ASTNode<?>> void concatIds(Iterable<T> listOfNodes,
                                             Function<T, String> mapping) {
    concatNodes(listOfNodes, mapping, true);
  }

  /**
   * Given a list of nodes, apply a function on each of those, concat the output (separated by commas),
   * and appends it to the given StringBuilder.
   * @param listOfNodes the list of nodes
   * @param mapping a function to map a node to a String
   */
  private <T extends ASTNode<?>> void concatNodes(Iterable<T> listOfNodes,
                                               Function<T, String> mapping,
                                               boolean quote) {
    boolean first = true;
    for (T t : listOfNodes) {
      if (first) {
        first = false;
      } else {
        sb.append(", ");
      }
      if (quote) {
        sb.append("\"");
      }
      sb.append(mapping.apply(t));
      if (quote) {
        sb.append("\"");
      }
    }
  }

  /**
   * Appends model elements to the builder, if their number is positive.
   * @param name           The name of the member
   * @param listOfElements The list of model elements
   * @return this
   */
  public MemberPrinter addIds(String name, List<? extends ModelElement> listOfElements) {
    return addIds(name, listOfElements.size(), listOfElements);
  }

  /**
   * Appends model elements to the builder, if count is positive.
   * @param name           The name of the member
   * @param count          The number of items in the listOfElements
   * @param listOfElements The list of model elements
   * @return this
   */
  public MemberPrinter addIds(String name, int count, Iterable<? extends ModelElement> listOfElements) {
    if (count > 0) {
      sb.append(' ').append(name).append("=[");
      concatIds(listOfElements);
      sb.append("]");
      this.empty = false;
    }
    return this;
  }

  /**
   * Appends nodes to the builder, if count is positive.
   * @param name        The name of the member
   * @param count       The number of items in the listOfNodes
   * @param listOfNodes The list of nodes
   * @param mapping     A function to map a node to a ModelElement
   * @return this
   */
  public <T extends ASTNode<?>> MemberPrinter addIds(
      String name, int count, Iterable<T> listOfNodes,
      Function<T, String> mapping) {
    if (count > 0) {
      sb.append(' ').append(name).append("=[");
      concatIds(listOfNodes, mapping);
      sb.append("]");
      this.empty = false;
    }
    return this;
  }

  /**
   * Applies the mapping to each node and appends the results to the builder, if count is positive.
   * The list is enclosed with square brackets.
   * @param name        The name of the member
   * @param count       The number of items in the listOfNodes
   * @param listOfNodes The list of nodes
   * @param mapping     A function to map a node to a String
   * @param <T>         The type of all nodes
   * @return this
   */
  public <T extends ASTNode<?>> MemberPrinter addNodes(String name, int count, Iterable<T> listOfNodes,
                                Function<T, String> mapping) {
    return addNodes(name, count, listOfNodes, mapping, ListBracketType.SQUARE);
  }

  /**
   * Applies the mapping to each node and appends the results to the builder, if count is positive
   * @param name        The name of the member
   * @param count       The number of items in the listOfNodes
   * @param listOfNodes The list of nodes
   * @param mapping     A function to map a node to a String
   * @param <T>         The type of all nodes
   * @param bracketType The type of brackets to enclose the list with
   * @return this
   */
  public <T extends ASTNode<?>> MemberPrinter addNodes(String name, int count, Iterable<T> listOfNodes,
                                                    Function<T, String> mapping, ListBracketType bracketType) {
    if (count > 0) {
      sb.append(' ').append(name).append("=").append(bracketType.begin);
      concatNodes(listOfNodes, mapping, false);
      sb.append(bracketType.end);
      this.empty = false;
    }
    return this;
  }

  /**
   * Appends a value to the builder, if the value is present.
   * A function is used to get the value, such that it is only evaluated if the value is present
   * @param name        The name of the member
   * @param isPresent   Whether the optional value is present
   * @param valueGetter A function to get the value
   * @return this
   */
  public MemberPrinter addOptional(String name, boolean isPresent, Supplier<String> valueGetter) {
    if (isPresent) {
      return add(name, valueGetter.get());
    }
    return this;
  }

  /**
   * Appends the result of calling {@link ASTNode#prettyPrint()} for the child, if non-<code>null</code>.
   * @param child       The child to append
   * @return this
   */
  public MemberPrinter addOptionalPrettyPrint(ASTNode<?> child) {
    if (child != null) {
      this.empty = false;
      sb.append(child.prettyPrint());
    }
    return this;
  }

  /**
   * Appends a the name of the flag, if it is set
   * @param name        The name of the member
   * @param isSet       Whether the flag is set
   * @return this
   */
  public MemberPrinter addFlag(String name, boolean isSet) {
    if (isSet) {
      this.empty = false;
      sb.append(' ').append(name);
    }
    return this;
  }

  /**
   * Appends a value to the builder, if the value is not <code>null</code> and not empty
   * @param name        The name of the member
   * @param actualValue The value to append
   * @return this
   */
  public MemberPrinter addNonDefault(String name, String actualValue) {
    return addNonDefault(name, actualValue, "");
  }

  /**
   * Appends a value to the builder, if the value is not the given default
   * @param name         The name of the member
   * @param actualValue  The value to append
   * @param defaultValue The default value
   * @return this
   */
  public MemberPrinter addNonDefault(String name, Object actualValue, Object defaultValue) {
    if (!actualValue.equals(defaultValue)) {
      this.empty = false;
      return add(name, actualValue);
    }
    return this;
  }

  /**
   * Appends a value to the builder, and issue a warning if it is either <code>null</code> or empty.
   * @param name         The name of the member
   * @param actualValue  The value to append
   * @return this
   */
  public MemberPrinter addRequired(String name, Object actualValue) {
    if (actualValue == null || actualValue.toString().isEmpty()) {
      logger.warn("Member \"{}\" not defined while printing", name);
      actualValue = "";
    }
    return add(name, actualValue);
  }

  /**
   * Appends a value to the builder, and issue a warning if it is either <code>null</code> or empty.
   * @param name         The name of the member
   * @param objectValue  An object to append
   * @param mapping      A function to get the real value from the object
   * @return this
   */
  public <T> MemberPrinter addRequired(String name, T objectValue, Function<T, String> mapping) {
    String actualValue;
    if (objectValue == null) {
      logger.warn("Member \"{}\" not defined while printing", name);
      actualValue = "";
    } else {
      actualValue = mapping.apply(objectValue);
      if (actualValue.isEmpty()) {
        logger.warn("Member \"{}\" empty while printing", name);
      }
    }
    return add(name, actualValue);
  }

  private MemberPrinter add(String name, Object actualValue) {
    this.empty = false;
    sb.append(' ').append(name).append("=\"").append(actualValue).append("\"");
    return this;
  }

}
