package de.tudresden.inf.st.eraser.jastadd.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBException;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import java.util.List;

/**
 * Adapter to handle the connection to a real InfluxDB.
 *
 * @author rschoene - Initial contribution
 */
public class InfluxAdapterImpl extends InfluxAdapter {

  private static final String user = "root";
  private static final String password = "root";
  private static final String dbName = "jastaddHistory";

  private InfluxDB influxDB;
  private InfluxDBResultMapper resultMapper;
  private Logger logger = LogManager.getLogger(InfluxAdapterImpl.class);

  public InfluxAdapterImpl() {
    resultMapper = new InfluxDBResultMapper();
  }

  @Override
  public InfluxAdapter setHost(ExternalHost host) {
    try {
      reset();
    } catch (Exception e) {
      logger.catching(e);
    }
//    String url = String.format("http://%s:%s", host.getHostName(), host.getPort());
    influxDB = InfluxDBFactory.connect(host.urlAsString(), user, password);
    influxDB.setDatabase(dbName);
    return this;
  }

  @SuppressWarnings("deprecation")
  @Override
  public void write(AbstractItemPoint point) {
    // create database, if it not already exists
    if (!influxDB.databaseExists(dbName)) {
      influxDB.createDatabase(dbName);
      // create default retention policy
      String rpName = "aRetentionPolicy";
      influxDB.createRetentionPolicy(rpName, dbName, "30d", "30m", 2, true);
      influxDB.setRetentionPolicy(rpName);
    }
    influxDB.write(point.build());
    logger.info("Wrote {}", point);
  }

  @Override
  public boolean isConnected() {
    try {
      Pong response = influxDB.ping();
      return response.isGood();
    } catch (InfluxDBException e) {
      return false;
    }
  }

  @SuppressWarnings("deprecation")
  @Override
  public void deleteDatabase() {
    influxDB.deleteDatabase(dbName);
  }

  @Override
  public List<? extends AbstractItemPoint> query(String from, String id, Class<? extends AbstractItemPoint> clazz) {
    String command = String.format("SELECT id, state FROM %s WHERE id = '%s'", from, id);
    Query q = new Query(command, dbName);
    QueryResult queryResult = influxDB.query(q);
    logger.info("Queried {} in {} for {} => {} result(s)", from, dbName, id, queryResult.getResults().size());
    return resultMapper.toPOJO(queryResult, clazz);
  }

  @Override
  public void reset() {
    super.reset();
    if (influxDB != null) {
      influxDB.close();
    }
  }

  @Override
  public void close() throws Exception {
    super.close();
    if (influxDB != null) {
      influxDB.close();
    }
  }
}
