package de.tudresden.inf.st.eraser.jastadd.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fusesource.mqtt.client.QoS;

import java.util.concurrent.TimeUnit;

/**
 * Stub implementation of a MQTT sender only printing publish messages.
 *
 * @author rschoene - Initial contribution
 */
public class MQTTSenderStub implements MQTTSender {

  public interface PublishCallback {
    void onPublish(String topic, String message, QoS qos);
  }

  private Logger logger = LogManager.getLogger(MQTTSenderStub.class);
  private PublishCallback callback;

  @Override
  public MQTTSender setHost(ExternalHost host) {
    return this;
  }


  public void setCallback(PublishCallback callback) {
    this.callback = callback;
  }

  @Override
  public void setConnectTimeout(long connectTimeout, TimeUnit connectTimeoutUnit) {
    // empty
  }

  @Override
  public void setPublishTimeout(long publishTimeout, TimeUnit publishTimeoutUnit) {
    // empty
  }

  @Override
  public void publish(String topic, String message, QoS qos) {
    // ignore QoS for now
    logger.info("{}: {}", topic, message);
    if (callback != null) {
      callback.onPublish(topic, message, qos);
    }
  }

  @Override
  public boolean isConnected() {
    return true;
  }

  @Override
  public void close() {
    // empty
  }
}
