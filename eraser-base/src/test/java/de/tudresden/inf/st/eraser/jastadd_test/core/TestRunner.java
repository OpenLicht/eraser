/* Copyright (c) 2005-2015, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tudresden.inf.st.eraser.jastadd_test.core;

import beaver.Parser;
import de.tudresden.inf.st.eraser.jastadd.model.Root;
import de.tudresden.inf.st.eraser.util.ParserUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Utility methods for running JastAdd unit tests.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class TestRunner {

  private static int TEST_TIMEOUT = 5000;
  private static final String SYS_LINE_SEP = System.getProperty("line.separator");

  static {
    // Set up test timeout.
    TEST_TIMEOUT = Integer.parseInt(System.getProperty("org.jastadd.testTimeout", "5000"));
  }

  /**
   * Run test with given JastAdd configuration.
   * @param config test case specific configuration
   */
  public static void runTest(TestConfiguration config)
      throws Exception {

    Result expected = config.expected;

    // Parse input model
    Root root;
    try {
      root = parseModel(config);
    } catch (Parser.Exception e) {
      if (expected == Result.PARSE_FAILED) return;
      // otherwise rethrow error
      throw e;
    }

    if (expected == Result.PARSE_FAILED) {
      fail("Parsing the model should have failed, but was successful!");
    }

    if (root == null) {
      fail("Parsing the model should have passed, but model was null!");
    }

    if (expected == Result.PARSE_PASSED) return;

    if (config.expected == Result.PARSE_ERR_OUTPUT) {
      compareParseErrOutput(config.tmpDir, config.testDir);
      return;
    }

    printAndCompare(config, root);
  }

  protected static void printAndCompare(TestConfiguration config, Root root) {
    Result expected = config.expected;
    // Print model.
    String output;
    try {
      output = printModel(root, config);
    } catch (Exception e) {
      if (expected == Result.PRINT_FAILED) return;
        // otherwise rethrow error
        throw e;
    }
    if (expected == Result.PRINT_FAILED) {
      fail("Printing the model should have failed, but was successful!");
    }

    // Compare the output with the expected output.
    compareOutput(output, config);
  }

  private static Root parseModel(TestConfiguration config) throws IOException, Parser.Exception {
    // input file is always input.eraser, except testProperties defines a value for the key input
    String inputFileName = config.testProperties.getProperty(
        "input", "input.eraser");
    File inputFile = new File(config.testDir, inputFileName);
    return ParserUtils.load(inputFile);
 }

  /**
   * Compare the error output from Parser
   */
  protected static void compareParseErrOutput(File tmpDir, File testDir) {
    try {
      File expected = expectedJastAddErrorOutput(testDir);
      File actual = new File(tmpDir, "jastadd.err");
      assertEquals(
          readFileToString(expected),
          readFileToString(actual),
          "Error output files differ");
    } catch (IOException e) {
      fail("IOException occurred while comparing JastAdd error output: " + e.getMessage());
    }
  }

  private static File expectedJastAddErrorOutput(File testDir) {
    boolean windows = System.getProperty("os.name").startsWith("Windows");
    if (windows) {
      // First try .win file.
      File file = new File(testDir, "jastadd.err.expected.win");
      if (file.isFile()) {
        return file;
      }
    }
    // Read default file:
    return new File(testDir, "jastadd.err.expected");
  }

  private static String printModel(Root root, TestConfiguration config) {
    return root.prettyPrint();
  }

  /**
   * Compare the generated output to the expected output
   */
  private static void compareOutput(String actual, TestConfiguration config) {
    try {
      // output file is always output.eraser, except testProperties defines a value for the key output
      String expectedFileLocation = config.testProperties.getProperty(
          "output", "output.eraser");
      File expectedOutput = new File(config.testDir, expectedFileLocation);
      if (!expectedOutput.exists()) {
        fail("Missing file: " + expectedFileLocation);
      }
      String expected = readFileToString(expectedOutput);
      assertEquals(expected, actual, "Output differs!");
    } catch (IOException e) {
      fail("IOException occurred while comparing output: " + e.getMessage());
    }
  }

  /**
   * Reads an entire file to a string object.
   *
   * <p>If the file does not exist an empty string is returned.
   *
   * <p>The system dependent line separator char sequence is replaced by
   * the newline character.
   *
   * @param file the file to read from
   * @return file contents as string
   * @throws IOException if an I/O error occurs
   */
  private static String readFileToString(File file) throws IOException {
    if (!file.isFile()) {
      return "";
    }
    String content = new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);
    return normalizeText(content);
  }

  /**
   * Normalize line endings and replace back-slashes with slashes.
   * This is used to avoid insignificant platform differences from
   * altering test results.
   * @param text text to normalize
   * @return normalized text string
   */
  private static String normalizeText(String text) {
    return text.replace(SYS_LINE_SEP, "\n").replace('\\', '/');
  }

}
