package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.TestUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Testing the decision tree aspect of the knowledge base.
 *
 * @author rschoene - Initial contribution
 */
public class DecisionTreeTest {

  @Test
  public void simpleDecision() {
    TestUtils.ModelAndItem mai = TestUtils.createModelAndItem(4);

    DecisionTreeRoot dtroot = new DecisionTreeRoot();
    mai.model.getRoot().getMachineLearningRoot().setActivityRecognition(dtroot);
    DecisionTreeLeaf isLessThanFour = newLeaf("less than four");
    DecisionTreeLeaf isFourOrGreater = newLeaf("four or greater");
    ItemStateNumberCheck check = new ItemStateNumberCheck(ComparatorType.LessThan, 4f);
    check.setItem(mai.item);
    dtroot.setRootRule(newRule(isLessThanFour, isFourOrGreater, "check item1", check));

    // current value is four, so return value should be "four or greater"
    Leaf leaf = dtroot.internalClassify();
    assertEquals(isFourOrGreater, leaf);

    // change value to 5, so return value should still be "four or greater"
    mai.item.setState(5);
    leaf = dtroot.internalClassify();
    assertEquals(isFourOrGreater, leaf);

    // change value to 2, so return value should now be "less than four"
    mai.item.setState(2);
    leaf = dtroot.internalClassify();
    assertEquals(isLessThanFour, leaf);
  }

  @Test
  public void tripleDecision() {
    TestUtils.ModelAndItem mai = TestUtils.createModelAndItem(20);

    DecisionTreeRoot dtroot = new DecisionTreeRoot();
    mai.model.getRoot().getMachineLearningRoot().setActivityRecognition(dtroot);

    DecisionTreeLeaf isLessThan25 = newLeaf("less than 25");
    DecisionTreeLeaf is25OrGreater = newLeaf("25 or greater");
    ItemStateNumberCheck check25 = new ItemStateNumberCheck(ComparatorType.LessThan, 25f);
    ItemStateCheckRule rule25 = newRule(isLessThan25, is25OrGreater, "25-item1", check25);

    DecisionTreeLeaf isLessThan75 = newLeaf("75 or less");
    DecisionTreeLeaf is75OrGreater = newLeaf("greater than 75");
    ItemStateNumberCheck check75 = new ItemStateNumberCheck(ComparatorType.LessOrEqualThan, 75f);
    ItemStateCheckRule rule75 = newRule(isLessThan75, is75OrGreater, "75-item1", check75);

    ItemStateNumberCheck check50 = new ItemStateNumberCheck(ComparatorType.LessThan, 50f);

    check25.setItem(mai.item);
    check50.setItem(mai.item);
    check75.setItem(mai.item);
    dtroot.setRootRule(newRule(rule25, rule75, "50-item1", check50));

    // current value is 20, so return value should be "less than 25"
    Leaf leaf = dtroot.internalClassify();
    assertEquals(isLessThan25, leaf);

    // change value to 25, so return value should still be "25 or greater"
    mai.item.setState(25);
    leaf = dtroot.internalClassify();
    assertEquals(is25OrGreater, leaf);

    // change value to 100, so return value should now be "greater than 75"
    mai.item.setState(100);
    leaf = dtroot.internalClassify();
    assertEquals(is75OrGreater, leaf);
  }

  @Test
  public void testNotEquals() {
    testSingleComparator(ComparatorType.NotEquals, 20,
        TestResult.of(20, false),
        TestResult.of(10, true),
        TestResult.of(30, true));
  }

  @Test
  public void testEquals() {
    testSingleComparator(ComparatorType.Equals, 20,
        TestResult.of(20, true),
        TestResult.of(10, false),
        TestResult.of(30, false));
  }

  @Test
  public void testLessThan() {
    testSingleComparator(ComparatorType.LessThan, 40,
        TestResult.of(20, true),
        TestResult.of(40, false),
        TestResult.of(50, false));
  }

  @Test
  public void testGreaterThan() {
    testSingleComparator(ComparatorType.GreaterThan, 40,
        TestResult.of(20, false),
        TestResult.of(40, false),
        TestResult.of(50, true));
  }

  @Test
  public void testLessOrEqualsThan() {
    testSingleComparator(ComparatorType.LessOrEqualThan, 40,
        TestResult.of(20, true),
        TestResult.of(40, true),
        TestResult.of(50, false));
  }

  @Test
  public void testGreaterOrEqualsThan() {
    testSingleComparator(ComparatorType.GreaterOrEqualThan, 40,
        TestResult.of(20, false),
        TestResult.of(40, true),
        TestResult.of(50, true));
  }

  private void testSingleComparator(ComparatorType comparatorType, float expected, TestResult... testResults) {
    TestUtils.ModelAndItem mai = TestUtils.createModelAndItem(0);

    DecisionTreeRoot dtroot = new DecisionTreeRoot();
    mai.model.getRoot().getMachineLearningRoot().setActivityRecognition(dtroot);
    DecisionTreeLeaf left = newLeaf("left");
    DecisionTreeLeaf right = newLeaf("right");
    ItemStateNumberCheck check = new ItemStateNumberCheck(comparatorType, expected);
    check.setItem(mai.item);
    dtroot.setRootRule(newRule(left, right, "check", check));

    for (TestResult result : testResults) {
      mai.item.setState(Math.round(result.value));
      Leaf leaf = dtroot.internalClassify();
      assertEquals(result.chooseLeft ? leaf : right, leaf);
    }
  }

  private ItemStateCheckRule newRule(DecisionTreeElement left, DecisionTreeElement right, String label,
                                     ItemStateNumberCheck check, ItemUpdate... updates) {
    ItemStateCheckRule result = new ItemStateCheckRule();
    result.setLeft(left);
    result.setRight(right);
    result.setLabel(label);
    result.setItemStateCheck(check);
    for (ItemUpdate update : updates) {
      result.addPreference(update);
    }
    return result;
  }

  private DecisionTreeLeaf newLeaf(String label, ItemUpdate... updates) {
    DecisionTreeLeaf result = new DecisionTreeLeaf();
    result.setLabel(label);
    for (ItemUpdate update : updates) {
      result.addPreference(update);
    }
    return result;
  }

  static class TestResult {
    float value;
    boolean chooseLeft;
    static TestResult of(float value, boolean chooseLeft) {
      TestResult result = new TestResult();
      result.value = value;
      result.chooseLeft = chooseLeft;
      return result;
    }
  }

}
