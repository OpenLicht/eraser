package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.TestUtils;
import de.tudresden.inf.st.eraser.util.TestUtils.ModelAndItem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

/**
 * Testing item history aspect, and connection to influx db.
 *
 * @author rschoene - Initial contribution
 */
@Tag("influx")
public class InfluxTest {

  public static String getInfluxHost() {
    if (System.getenv("GITLAB_CI") != null) {
      // we are in the CI, so use "influx" as host
      return "influx";
    } {
      // else assume a locally running influx container
      return "localhost";
    }
  }

  private static final double DELTA = 0.001;
  private final List<DoubleStatePoint> points = new ArrayList<>();
  private static final double firstState = 2.0;
  private static final double secondState = 4.0;
  private static final double thirdState = 3.0;

  private ModelAndItem mai;

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  public void oneItem(boolean useStub) {
    setNewModel(useStub);
    NumberItem item = mai.item;
    // set state once
    item.setState(firstState);
    assertSameOrdered(query(useStub, item),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item.getID()));

    // set state again
    item.setState(secondState);
    assertSameOrdered(query(useStub, item),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(2), secondState, item.getID()));

    // set state a third time
    item.setState(thirdState);
    assertSameOrdered(query(useStub, item),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(2), secondState, item.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(3), thirdState, item.getID()));
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  public void twoItems(boolean useStub) {
    setNewModel(useStub);
    NumberItem item1 = mai.item;
    NumberItem item2 = TestUtils.addItemTo(mai.model, 1.0, true);
    // set state once for first item
    item1.setState(firstState);
    assertSameOrdered(query(useStub, item1, item2),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item1.getID()));

    // set state again for first item
    item1.setState(secondState);
    assertSameOrdered(query(useStub, item1, item2),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(2), secondState, item1.getID()));

    // set state for second item
    item2.setState(firstState);
    assertSameOrdered(query(useStub, item1, item2),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(2), secondState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(3), firstState, item2.getID()));

    // set third state for first item
    item1.setState(thirdState);
    assertSameOrdered(query(useStub, item1, item2),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(2), secondState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(3), firstState, item2.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(4), thirdState, item1.getID()));

    // set second and third state for second item
    item2.setState(secondState);
    item2.setState(thirdState);
    assertSameOrdered(query(useStub, item1, item2),
        DoubleStatePoint.of(Instant.ofEpochSecond(1), firstState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(2), secondState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(3), firstState, item2.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(4), thirdState, item1.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(5), secondState, item2.getID()),
        DoubleStatePoint.of(Instant.ofEpochSecond(6), thirdState, item2.getID()));
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  public void justAdapter(boolean useStub) {
    setNewModel(useStub);
    InfluxAdapter influxAdapter = getInfluxRoot().influxAdapter();
    assertTrue(influxAdapter.isConnected(), "Adapter not connected");
    influxAdapter.deleteDatabase();

    // write one point
    Instant now = Instant.now();
    double state = 42.0;
    String id = "forty-two";
    influxAdapter.write(DoubleStatePoint.of(now, state, id));

    // retrieve all points
    List<? extends AbstractItemPoint> result;
    if (useStub) {
      result = points;
    } else {
      result = influxAdapter.query(DoubleStatePoint.NAME, id, DoubleStatePoint.class);
    }
    assertEquals(1, result.size(), "Did not get one result item");
    assertEquals(DoubleStatePoint.class, result.get(0).getClass(), "Wrong class");
    DoubleStatePoint point = (DoubleStatePoint) result.get(0);
    assertEquals(now, point.getTime(), "Time differs");
    assertEquals(id, point.getId(), "ID differs");
    assertEquals(state, point.getState(), DELTA, "State differs");
  }

  private void assertSameOrdered(List<DoubleStatePoint> actual, DoubleStatePoint... expected) {
    assertEquals(expected.length, actual.size(), "Number of items differs!");
    // sort actual and expected points by time
    List<DoubleStatePoint> actualOrdered = new ArrayList<>(actual);
    actualOrdered.sort(Comparator.comparing(AbstractItemPoint::getTime));
    List<DoubleStatePoint> expectedOrdered = Arrays.asList(expected);
    expectedOrdered.sort(Comparator.comparing(AbstractItemPoint::getTime));
    // and then compare them
    for (int i = 0; i < actualOrdered.size(); i++) {
      assertEquals(expectedOrdered.get(i).getState(), actualOrdered.get(i).getState(), DELTA,
          "State of item " + i + " differs!");
      assertEquals(expectedOrdered.get(i).getId(), actualOrdered.get(i).getId(),
          "Id of item " + i + " differs!");
    }
  }

  private ModelAndItem createModel(boolean useStub) {
    ModelAndItem mai = TestUtils.createModelAndItem(1.0, true);
    InfluxRoot influxRoot;
    if (useStub) {
      influxRoot = InfluxRoot.createDefault();
      // now a SenderStub is being used
      ((InfluxAdapterStub) influxRoot.influxAdapter()).setCallback(
          point -> points.add((DoubleStatePoint) point));
    } else {
      influxRoot = InfluxRoot.createDefault();
      // use container running influx
      influxRoot.setDbName(InfluxTest.class.getSimpleName());
      influxRoot.getHost().setHostName(getInfluxHost());
    }
    mai.model.getRoot().setInfluxRoot(influxRoot);
    assumeTrue(influxRoot.influxAdapter().isConnected());
    influxRoot.influxAdapter().deleteDatabase();
    return mai;
  }

  private InfluxRoot getInfluxRoot() {
    return mai.model.getRoot().getInfluxRoot();
  }

  private List<DoubleStatePoint> query(boolean useStub, ItemWithDoubleState... allItems) {
    if (useStub) {
      return points;
    } else {
      List<DoubleStatePoint> allMessages = new ArrayList<>();
      for (ItemWithDoubleState item : allItems) {
        allMessages.addAll(item.getHistory());
      }
      return allMessages;
    }
  }

  private void setNewModel(boolean useStub) {
    // BeforeEach does not work with parameterized tests :(
    mai = createModel(useStub);
    getInfluxRoot().influxAdapter().disableAsyncQuery();
  }

  @AfterEach
  public void closeInfluxAdapter() throws Exception {
    if (mai != null && mai.model != null) {
      getInfluxRoot().influxAdapter().close();
    }
  }
}
