package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.TestUtils;
import de.tudresden.inf.st.eraser.util.TestUtils.ModelAndItem;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testing wrapper methods of {@link Item}.
 *
 * @author rschoene - Initial contribution
 */
public class ItemTests {

  @Test
  public void testItemWithBooleanStateEquals() {
    ItemWithBooleanState sut = createItem(SwitchItem::new);

    sut.setState(true);
    assertTrue(sut.stateEquals(true), "State 'true' should match 'true'");
    assertFalse(sut.stateEquals(false), "State 'true' should not match 'false'");

    sut.setState(false);
    assertFalse(sut.stateEquals(true), "State 'false' should not match 'true'");
    assertTrue(sut.stateEquals(false), "State 'false' should match 'false'");
  }

  @Test
  public void testItemWithStringStateEquals() {
    ItemWithStringState sut = createItem(ImageItem::new);

    sut.setState("correct");
    assertTrue(sut.stateEquals("correct"),
        "State 'correct' should match 'correct'");
    assertFalse(sut.stateEquals("something else"),
        "State 'correct' should not match 'something else'");
  }

  @Test
  public void testItemWithDoubleStateEquals() {
    ItemWithDoubleState sut = createItem(NumberItem::new);

    sut.setState(3.0);
    assertTrue(sut.stateEquals(3.0), "State '3.0' should match '3.0'");
    assertFalse(sut.stateEquals(7.0), "State '3.0' should not match '7.0'");

    sut.setState(4.0);
    assertFalse(sut.stateEquals(3.0), "State '4.0' should not match '3.0'");
    assertFalse(sut.stateEquals(7.0), "State '4.0' should not match '7.0'");
  }

  @Test
  public void testItemWithTupleHSBStateEquals() {
    ColorItem sut = createItem(ColorItem::new);

    sut.setState(TupleHSB.of(1, 2, 3));
    assertTrue(sut.stateEquals(TupleHSB.of(1, 2, 3)),
        "State 'TupleHSB(1,2,3)' should match 'TupleHSB(1,2,3)'");
    assertFalse(sut.stateEquals(TupleHSB.of(1, 2, 4)),
        "State 'TupleHSB(1,2,3)' should not match 'TupleHSB(1,2,4)'");
    assertFalse(sut.stateEquals(TupleHSB.of(9, 2, 3)),
        "State 'TupleHSB(1,2,3)' should not match 'TupleHSB(9,2,3)'");
    assertFalse(sut.stateEquals(TupleHSB.of(1, 17, 3)),
        "State 'TupleHSB(1,2,3)' should not match 'TupleHSB(1,17,3)'");
    assertFalse(sut.stateEquals(TupleHSB.of(99, 99, 3)),
        "State 'TupleHSB(1,2,3)' should not match 'TupleHSB(99,99,3)'");
    assertFalse(sut.stateEquals(TupleHSB.of(5, 5, 5)),
        "State 'TupleHSB(1,2,3)' should not match 'TupleHSB(5,5,5)'");

    sut.setState(TupleHSB.of(347, 80, 95));
    assertTrue(sut.stateEquals(TupleHSB.of(347, 80, 95)),
        "State 'TupleHSB(357,80,95)' should match 'TupleHSB(357,80,95)'");
    assertFalse(sut.stateEquals(TupleHSB.of(1, 2, 4)),
        "State 'TupleHSB(357,80,95)' should not match 'TupleHSB(1,2,4)'");
    assertFalse(sut.stateEquals(TupleHSB.of(9, 2, 3)),
        "State 'TupleHSB(357,80,95)' should not match 'TupleHSB(9,2,3)'");
    assertFalse(sut.stateEquals(TupleHSB.of(1, 17, 3)),
        "State 'TupleHSB(357,80,95)' should not match 'TupleHSB(1,17,3)'");
    assertFalse(sut.stateEquals(TupleHSB.of(99, 99, 3)),
        "State 'TupleHSB(357,80,95)' should not match 'TupleHSB(99,99,3)'");
    assertFalse(sut.stateEquals(TupleHSB.of(5, 5, 5)),
        "State 'TupleHSB(357,80,95)' should not match 'TupleHSB(5,5,5)'");
  }

  @Test
  public void testItemWithDateStateEquals() {
    DateTimeItem sut = createItem(DateTimeItem::new);

    sut.setState(Instant.ofEpochMilli(1543415826));
    assertTrue(sut.stateEquals(Instant.ofEpochMilli(1543415826)),
        "State 'Date(1543415826)' should match 'Date(1543415826)'");
    assertFalse(sut.stateEquals(Instant.ofEpochMilli(4)),
        "State 'Date(1543415826)' should not match 'Date(4)'");
  }

  @FunctionalInterface
  private interface CreateItem<T extends Item> {
    T create();
  }

  private <T extends  Item> T createItem(CreateItem<T> creator) {
    // create a root with default group and one unused item
    ModelAndItem mai = TestUtils.createModelAndItem(0);
    // create wanted item, add it to default group, and return it
    T result = creator.create();
    TestUtils.getDefaultGroup(mai.model).addItem(result);
    return result;
  }

}
