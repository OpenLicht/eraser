package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.model.SmartHomeEntityModel;
import de.tudresden.inf.st.eraser.jastadd_test.core.*;
import de.tudresden.inf.st.eraser.openhab2.OpenHab2Importer;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * Testing import from openHAB using {@link OpenHab2Importer}.
 *
 * @author rschoene - Initial contribution
 */
public class OpenHabImporterTest {

  private static final String HOST = "localhost";

  static class OpenHab2ImporterFromFile extends OpenHab2Importer {
    private final String directory;

    OpenHab2ImporterFromFile(String directory) {
      this.directory = directory;
    }

    @Override
    protected URL makeURL(String formatUrlString, String hostAndPort) throws MalformedURLException {
      // read from files instead of making an http request
      switch (formatUrlString) {
        case thingTypesUrl: return makeFileUrl("thing-types.json", "empty-list.json");
        case channelTypeUrl: return makeFileUrl("channel-types.json", "empty-list.json");
        case thingsUrl: return makeFileUrl("things.json", "empty-list.json");
        case itemsUrl: return makeFileUrl("items.json", "empty-list.json");
        case linksUrl: return makeFileUrl("links.json", "empty-list.json");
        default: throw new IllegalArgumentException("Unsupported formatURLString: " + formatUrlString);
      }
    }

    @Override
    protected URL makeURL(String formatUrlString, String hostAndPort, String id) throws MalformedURLException {
      return makeFileUrl(id + ".json", "empty.json");
    }

    private URL makeFileUrl(String fileName, String emptyFileName) throws MalformedURLException {
      Path path = null;
      try {
        path = Paths.get(directory, fileName);
      } catch (InvalidPathException e) {
        // use empty file
      }
      if (path == null || !path.toFile().exists() || !path.toFile().isFile()) {
        path = Paths.get(directory, "..", emptyFileName);
      }
      return path.toUri().toURL();
    }
  }

  static class OpenHabImportRunner extends TestRunner {
    public static void runTest(TestConfiguration config) {

      Result expected = config.getExpected();

      OpenHab2Importer importer = new OpenHab2ImporterFromFile(config.getDir().getAbsolutePath());

      // call the modified importer, with the static host name, and an arbitrary chosen port
      // port will not be used during the test
      SmartHomeEntityModel model = importer.importFrom(HOST, 80);

      if (model == null) {
        if (expected == Result.PARSE_FAILED) return;
        // else fail
        fail("Expected to successfully import model, but returned model is null!");
      } else {
        switch (expected) {
          case PARSE_PASSED:
            return;
          case PARSE_FAILED:
            fail("Expected to fail parsing, but was successful!");
          case PARSE_ERR_OUTPUT:
            compareParseErrOutput(config.getTempDir(), config.getDir());
            return;
        }
      }

      printAndCompare(config, model.getRoot());
    }
  }

  /**
   * Run the OpenHabImporterTest
   * @param unitTest The test to run.
   */
  @ParameterizedTest
  @MethodSource("getTests")
  public void runTest(TestConfiguration unitTest) {
    OpenHabImportRunner.runTest(unitTest);
  }

  public static List<TestConfiguration> getTests() {
    TestProperties properties = new TestProperties();
    properties.put("jastadd3", "false");
    properties.setTestRoot("src/test/resources/openhabtest");
//    properties.exclude(Tests.FAILING);
//    properties.exclude(Tests.UNSTABLE);
    return Util.getTests(properties);
  }
}
