package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.MqttReceiver;
import de.tudresden.inf.st.eraser.util.TestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

/**
 * Test for everything related to MQTT.
 *
 * @author rschoene - Initial contribution
 */
@Tag("mqtt")
public class MqttTests {

  public static String getMqttHost() {
    if (System.getenv("GITLAB_CI") != null) {
      // we are in the CI, so use "mqtt" as host
      return "mqtt";
    } {
      // else assume a locally running mqtt broker
      return "localhost";
    }
  }

  private static final String outgoingPrefix = "out";
  private static final String firstPart = "a";
  private static final String alternativeFirstPart = "x";
  private static final String secondPart = "b";
  private static final double firstState = 1.0;
  private static final double secondState = 2.0;
  private static final double thirdState = 3.0;

  private final List<String> messages = new ArrayList<>();
  private static final Logger logger = LogManager.getLogger(MqttTests.class);

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  public void resolve1(boolean useStub) {
    ModelItemAndTwoTopics modelAB = createAB(useStub);
    MqttRoot sut = modelAB.model.getRoot().getMqttRoot();

    // incoming mqtt topic might be "inc/a/" or "inc/a/b"

    assertTrue(sut.resolveTopic("inc/a").isPresent());
    assertEquals(modelAB.firstTopic, sut.resolveTopic("inc/a").get(), "Could not resolve a.");
    assertTrue(sut.resolveTopic("inc/a/b").isPresent());
    assertEquals(modelAB.secondTopic, sut.resolveTopic("inc/a/b").get(), "Could not resolve a/b.");
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  public void brokerConnected(boolean useStub) throws Exception {
    ModelItemAndTwoTopics modelAB = createAB(useStub);
    MqttRoot sut = modelAB.model.getRoot().getMqttRoot();
//    MqttRoot mqttRoot = new MqttRoot();
//    mqttRoot.setHostByName("localhost");
//    MQTTSender sender = new MQTTSenderImpl().setHost(mqttRoot.getHost());
    MQTTSender sender = sut.getMqttSender();
    assumeTrue(sender.isConnected(), "Broker is not connected");
//    assertTrue(sender.isConnected());
    sender.publish("test", "me");
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  public void itemUpdateSend1(boolean useStub) throws Exception {
    String expectedTopic = outgoingPrefix + "/" + firstPart + "/" + secondPart;

    ModelItemAndTwoTopics modelAB = createAB(useStub);
    assertSenderConnected(modelAB);

    NumberItem sut = modelAB.item;
    sut.setTopic(modelAB.secondTopic);

    createMqttReceiver(useStub, expectedTopic);

    sut.setState(firstState);

    assertTimeoutEquals(2, 1, messages::size);
    assertEquals(Double.toString(firstState), messages.get(0));

    sut.setState(secondState);

    assertTimeoutEquals(2, 2, messages::size);
    assertEquals(Double.toString(secondState), messages.get(1));
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  public void itemUpdateSend2(boolean useStub) throws Exception {
    String expectedTopic1 = outgoingPrefix + "/" + firstPart + "/" + secondPart;
    String expectedTopic2 = outgoingPrefix + "/" + alternativeFirstPart + "/" + secondPart;

    ModelItemAndTwoTopics modelAB = createAB(useStub);
    assertSenderConnected(modelAB);

    NumberItem item1 = modelAB.item;
    item1.setTopic(modelAB.secondTopic);

    MqttTopic alternativeB = createAndAddMqttTopic(modelAB.model.getRoot().getMqttRoot(),alternativeFirstPart + "/" + secondPart);

    NumberItem item2 = TestUtils.addItemTo(modelAB.model, 0, true);
    item2.setTopic(alternativeB);

    createMqttReceiver(useStub, expectedTopic1, expectedTopic2);

    item1.setState(firstState);

    item2.setState(firstState);

    assertTimeoutEquals(2, 2, messages::size);
    assertEquals(Double.toString(firstState), messages.get(0));
    assertEquals(Double.toString(firstState), messages.get(1));

    item1.setState(secondState);
    item2.setState(thirdState);

    assertTimeoutEquals(3, 4, messages::size);
    // TODO actually this does not test, whether the topic was correct for each state
    assertThat(messages).contains(Double.toString(secondState));
    assertThat(messages).contains(Double.toString(thirdState));
  }

  private void assertSenderConnected(ModelItemAndTwoTopics modelAB) {
    MqttRoot mqttRoot = modelAB.model.getRoot().getMqttRoot();
    if (!mqttRoot.getMqttSender().isConnected()) {
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        logger.catching(e);
      }
    }
    assertTrue(mqttRoot.getMqttSender().isConnected(), "Broker is not connected");
  }

  private void createMqttReceiver(boolean useStub, String... expectedTopics) throws IOException {
    if (useStub) {
      // do not need receiver, as messages are directly written by MqttSenderStub and callback
      messages.clear();
      return;
    }
    MqttReceiver receiver = new MqttReceiver();
    List<String> expectedTopicList = Arrays.asList(expectedTopics);
    receiver.setHost(ExternalHost.of(getMqttHost(),1883));
    receiver.setTopicsForSubscription(expectedTopics);
    receiver.setOnMessage((topic, message) -> {
      assertThat(expectedTopicList).contains(topic);
      messages.add(message);
    });
    receiver.start();
    receiver.waitUntilReady(2, TimeUnit.SECONDS);
  }

  private ModelItemAndTwoTopics createAB(boolean useStub) {
    TestUtils.ModelAndItem mai = TestUtils.createModelAndItem(0, true);
    SmartHomeEntityModel model = mai.model;
    MqttRoot mqttRoot = new MqttRoot();
    mqttRoot.setIncomingPrefix("inc");
    mqttRoot.setOutgoingPrefix(outgoingPrefix);
    if (useStub) {
      
      // now a SenderStub is being used
      ((MQTTSenderStub) mqttRoot.getMqttSender()).setCallback(((topic, message, qos) -> messages.add(message)));
    } else {
      mqttRoot.getHost().setHostName(getMqttHost()).setPort(1883);
    }
    MqttTopic a = createAndAddMqttTopic(mqttRoot, firstPart);
    MqttTopic ab = createAndAddMqttTopic(mqttRoot, firstPart + "/" + secondPart);
    mqttRoot.ensureCorrectPrefixes();
    model.getRoot().setMqttRoot(mqttRoot);
    return ModelItemAndTwoTopics.of(model, mai.item, a, ab);
  }

  static class ModelItemAndTwoTopics {
    SmartHomeEntityModel model;
    NumberItem item;
    MqttTopic firstTopic;
    MqttTopic secondTopic;
    static ModelItemAndTwoTopics of(SmartHomeEntityModel model, NumberItem item, MqttTopic firstTopic, MqttTopic secondTopic) {
      ModelItemAndTwoTopics result = new ModelItemAndTwoTopics();
      result.model = model;
      result.item = item;
      result.firstTopic = firstTopic;
      result.secondTopic = secondTopic;
      return result;
    }
  }

  private MqttTopic createAndAddMqttTopic(MqttRoot mqttRoot, String suffix) {
    MqttTopic result = new MqttTopic();
    result.setTopicString(suffix);
    mqttRoot.addTopic(result);
    return result;
  }

  private <T> void assertTimeoutEquals(long seconds, T expected, Supplier<T> actualProvider) throws InterruptedException {
    if (expected == actualProvider.get()) {
      // already matched right now. return immediately.
      return;
    }
    long targetEndTime = System.nanoTime() + TimeUnit.SECONDS.toNanos(seconds);
    while (System.nanoTime() < targetEndTime) {
      // this is indeed busy waiting in favour of new dependencies handling it
      //noinspection BusyWait
      Thread.sleep(100);
      if (expected == actualProvider.get()) {
        break;
      }
    }
    // final assessment, throw exception if not matched. Or pass, if previously matched.
    assertEquals(expected, actualProvider.get());
  }

}
