package de.tudresden.inf.st.eraser;

import beaver.Parser;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test correct evaluation of NumberExpression and LogicalExpression.
 *
 * @author rschoene - Initial contribution
 */
public class ExpressionEvalTest {

  @Test
  public void plusExpression() throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression("(3 + 4)");
    assertEquals(7.0, sut.eval());
  }

  @Test
  public void minusExpression() throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression("(4.1 - 12.5)");
    assertEquals(-8.4, sut.eval());
  }

  @Test
  public void mulExpression() throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression("(3 * 4)");
    assertEquals(12.0, sut.eval());
  }

  @Test
  public void divExpression() throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression("(1.1 / 4.0)");
    assertEquals(0.275, sut.eval());
  }

  @Test
  public void powerExpression() throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression("(16 ^ 0.5)");
    assertEquals(4.0, sut.eval());
  }

  @Test
  public void parenthesizedExpression() throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression("(3)");
    assertEquals(3.0, sut.eval());
  }

  @Test
  public void complexExpression() throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression("((3 + 4) * (1 / (12 - 8)))");
    assertEquals(1.75, sut.eval());
    MultExpression multExpression = (MultExpression) sut;

    // 3+4
    assertThat(multExpression.getLeftOperand()).isInstanceOf(AddExpression.class);
    assertEquals(7.0, multExpression.getLeftOperand().eval());

    // 1/(12-8)
    assertThat(multExpression.getRightOperand()).isInstanceOf(DivExpression.class);
    DivExpression divExpression = (DivExpression) multExpression.getRightOperand();
    assertEquals(0.25, divExpression.eval());

    // 12-8
    assertEquals(4.0, divExpression.getRightOperand().eval());
  }

  @Test
  public void expressionWithItem() throws IOException, Parser.Exception {
    double itemValue = 5.3;
    Item referenceItem = ParserUtils.parseItem("Number Item: id=\"myItem\" state=\"" + itemValue + "\";");
    NumberExpression sut = ParserUtils.parseNumberExpression("(myItem * 3)", referenceItem.getRoot());
    assertEquals(itemValue * 3, sut.eval());

    // set item state to new value
    itemValue = 17;
    referenceItem.setStateFromDouble(itemValue);
    assertEquals(itemValue * 3, sut.eval());
  }

  @Test
  public void comparingExpressions() throws IOException, Parser.Exception {
    comparingExpression(1, "<",  2, true);
    comparingExpression(2, "<",  2, false);
    comparingExpression(3, "<",  2, false);

    comparingExpression(3, "<=", 4, true);
    comparingExpression(4, "<=", 4, true);
    comparingExpression(5, "<=", 4, false);

    comparingExpression(5, "==", 6, false);
    comparingExpression(6, "==", 6, true);

    comparingExpression(7, "!=", 8, true);
    comparingExpression(8, "!=", 8, false);

    comparingExpression(9, ">=", 10, false);
    comparingExpression(10, ">=", 10, true);
    comparingExpression(11, ">=", 10, true);

    comparingExpression(11,">",   12, false);
    comparingExpression(12,">",   12, false);
    comparingExpression(13,">",   12, true);
  }

  private void comparingExpression(double left, String actualComparatorString, double right, boolean expectedResult) throws IOException, Parser.Exception {
    String expression = String.format("(%s %s %s)", left, actualComparatorString, right);
    LogicalExpression sut = ParserUtils.parseLogicalExpression(expression);
    assertEquals(expectedResult, sut.eval());
  }

  @Test
  public void notExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = ParserUtils.parseLogicalExpression("!(0==0)");
    assertFalse(sut.eval());
    LogicalExpression sut2 = ParserUtils.parseLogicalExpression("!!(0==0)");
    assertTrue(sut2.eval());
  }

  @Test
  public void andExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = ParserUtils.parseLogicalExpression("((0==0) & (0==0))");
    assertTrue(sut.eval());

    LogicalExpression sut2 = ParserUtils.parseLogicalExpression("((0==0) & (0==1))");
    assertFalse(sut2.eval());

    LogicalExpression sut3 = ParserUtils.parseLogicalExpression("((0==1) & (0==0))");
    assertFalse(sut3.eval());

    LogicalExpression sut4 = ParserUtils.parseLogicalExpression("((0==1) & (0==1))");
    assertFalse(sut4.eval());
  }

  @Test
  public void orExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = ParserUtils.parseLogicalExpression("((0==0) | (0==0))");
    assertTrue(sut.eval());

    LogicalExpression sut2 = ParserUtils.parseLogicalExpression("((0==0) | (0==1))");
    assertTrue(sut2.eval());

    LogicalExpression sut3 = ParserUtils.parseLogicalExpression("((0==1) | (0==0))");
    assertTrue(sut3.eval());

    LogicalExpression sut4 = ParserUtils.parseLogicalExpression("((0==1) | (0==1))");
    assertFalse(sut4.eval());
  }

  @Test
  public void parenthesizedLogicalExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = ParserUtils.parseLogicalExpression("((0==0))");
    assertTrue(sut.eval());
    ParenthesizedLogicalExpression parenthesizedLogicalExpression = (ParenthesizedLogicalExpression) sut;
    assertTrue(parenthesizedLogicalExpression.getOperand().eval());
  }
}
