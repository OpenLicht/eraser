package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.TestUtils;
import de.tudresden.inf.st.eraser.util.TestUtils.ModelAndItem;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Testing the neuronal network aspect of the knowledge base.
 *
 * @author rschoene - Initial contribution
 */
public class NeuralNetworkTest {

  @Test
  public void simpleNetwork() {
    ModelAndItem mai = TestUtils.createModelAndItem(1);

    /* I..Input, H..Hidden, O..Output
     * (I) -> (H) -  0.4 -> (O1)
     *            \- 0.6 -> (O2)
     *  f=    4*x           x>4?
     */

    NeuralNetworkRoot neuralNetworkRoot = NeuralNetworkRoot.createEmpty();
    mai.model.getRoot().getMachineLearningRoot().setPreferenceLearning(neuralNetworkRoot);
    InputNeuron inputNeuron = new InputNeuron();
    inputNeuron.setItem(mai.item);
    HiddenNeuron hiddenNeuron = new HiddenNeuron();
    hiddenNeuron.setActivationFormula(inputs -> 4 * inputs[0]);
    OutputNeuron outputNeuron1 = new OutputNeuron();
    outputNeuron1.setLabel("first");
    outputNeuron1.setActivationFormula(inputs -> inputs[0] > 4 ? 1d : 0d);
    OutputNeuron outputNeuron2 = new OutputNeuron();
    outputNeuron2.setLabel("second");
    outputNeuron2.setActivationFormula(inputs -> inputs[0] > 4 ? 1d : 0d);

    inputNeuron.connectTo(hiddenNeuron, 1);
    hiddenNeuron.connectTo(outputNeuron1, 0.4);
    hiddenNeuron.connectTo(outputNeuron2, 0.6);

    neuralNetworkRoot.addInputNeuron(inputNeuron);
    neuralNetworkRoot.addHiddenNeuron(hiddenNeuron);
    OutputLayer outputLayer = new OutputLayer();
    outputLayer.addOutputNeuron(outputNeuron1);
    outputLayer.addOutputNeuron(outputNeuron2);
//    outputLayer.setCombinator(inputs -> pickLargest(inputs, outputLayer.getOutputNeuronList()));
    outputLayer.setCombinator(this::combineBinaryValues);
    neuralNetworkRoot.setOutputLayer(outputLayer);

    // Current value is 1, so return value is 1 * 4 * 0.6 = 2.4, 1 * 4 * 0.4 = 1.6, both are less than 4. so 0.
    Leaf leaf = neuralNetworkRoot.internalClassify();
    assertLeafEqual(0, leaf);

    // Current value is 2, so return value is 2 * 4 * 0.6 = 4.8, 2 * 4 * 0.4 = 3.2, first is less than 4. so 1.
    mai.item.setState(2);
    leaf = neuralNetworkRoot.internalClassify();
    assertLeafEqual(1, leaf);

    // Current value is 5, so return value is 5 * 4 * 0.6 = 12, 5 * 4 * 0.4 = 8, both are greater than 4. so 3.
    mai.item.setState(5);
    leaf = neuralNetworkRoot.internalClassify();
    assertLeafEqual(3, leaf);
  }

  private void assertLeafEqual(int expected, Leaf actualLeaf) {
    assertEquals(Double.toString(expected), actualLeaf.getLabel());
  }

  private Double combineBinaryValues(double[] outputs) {
    /*
    threat outputs as bits, i.e. they should be either 0 or 1.
    then combine them in LSB (least significant bit first)
    */
    int n = 0;
    for (double d : outputs) {
      n = (n << 1) | (d == 0 ? 0 : 1);
    }
    return (double) n;
  }

//  private Leaf pickLargest(double[] inputs, List<OutputNeuron> outputNeuronList) {
//    int indexOfLargest = 0;
//    for (int i = 0; i < inputs.length; i++) {
//      indexOfLargest = inputs[i] > inputs[indexOfLargest] ? i : indexOfLargest;
//    }
//    return outputNeuronList.getChild(indexOfLargest);
//  }

}
