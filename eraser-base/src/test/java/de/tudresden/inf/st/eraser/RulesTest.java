package de.tudresden.inf.st.eraser;

import beaver.Parser;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.jastadd.model.Action;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import de.tudresden.inf.st.eraser.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

import static de.tudresden.inf.st.eraser.util.TestUtils.getDefaultGroup;
import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing the simple rule engine.
 *
 * @author rschoene - Initial contribution
 */
public class RulesTest {

  private static final double DELTA = 0.01d;

  static class CountingAction extends NoopAction {
    final Map<Item, AtomicInteger> counters = new HashMap<>();

    CountingAction() {
      reset();
    }

    private AtomicInteger getAtomic(Item item) {
      return counters.computeIfAbsent(item, unused -> new AtomicInteger(0));
    }

    @Override
    public void applyFor(Item item) {
      getAtomic(item).addAndGet(1);
    }

    int get(Item item) {
      return getAtomic(item).get();
    }

    void reset() {
      counters.clear();
    }
  }

  @Test
  public void testUnconditionalLambdaAction() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;

    Rule rule = new Rule();
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(0, counter.get(item), "Counter not initialized correctly");

    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");

    setState(item, 5);
    assertEquals(1, counter.get(item), "Set state to same value should not trigger the rule");

    setState(item, 3);
    assertEquals(2, counter.get(item), "Change of item state should trigger the rule");
  }

  @Test
  public void testIdempotentActivation() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(4);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;

    Rule rule = new Rule();
    root.addRule(rule);

    rule.activateFor(item);
    rule.activateFor(item);

    assertEquals(1, rule.getObserverList().size(), "Rule was not activated exactly once (observer count)");
    int ruleCount = 0;
    for (ItemObserver observer : rule.getObserverList()) {
      ruleCount += observer.getTriggeredRuleList().size();
    }
    assertEquals(1, ruleCount, "Rule was not activated exactly once (rule count)");
  }

  @Test
  public void testTwoRulesForOneItem() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(4);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;

    CountingAction counter1 = new CountingAction();
    Rule ruleA = new Rule();
    ruleA.addAction(counter1);
    root.addRule(ruleA);

    Rule ruleB = new Rule();
    CountingAction counter2 = new CountingAction();
    ruleB.addAction(counter2);
    root.addRule(ruleB);

    ruleA.activateFor(item);
    ruleB.activateFor(item);

    assertEquals(0, counter1.get(item), "First counter not initialized correctly");
    assertEquals(0, counter2.get(item), "Second counter not initialized correctly");

    setState(item, 5);
    assertEquals(1, counter1.get(item), "Change of item state should trigger the first rule");
    assertEquals(1, counter2.get(item), "Change of item state should trigger the second rule");

    setState(item, 5);
    assertEquals(1, counter1.get(item), "Set state to same value should not trigger the first rule");
    assertEquals(1, counter2.get(item), "Set state to same value should not trigger the second rule");

    setState(item, 3);
    assertEquals(2, counter1.get(item), "Change of item state should trigger the first rule");
    assertEquals(2, counter2.get(item), "Change of item state should trigger the second rule");
  }

  @Test
  public void testOneRuleForTwoItems() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(4);
    Root root = modelAndItem.model.getRoot();
    NumberItem item1 = modelAndItem.item;
    NumberItem item2 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);

    Rule rule = new Rule();
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);

    rule.activateFor(item1);
    rule.activateFor(item2);

    // Items: 4, 4. Expected counters: 0, 0.
    assertEquals(0, counter.get(item1), "Counter not initialized correctly for first item");
    assertEquals(0, counter.get(item2), "Counter not initialized correctly for second item");

    // Items: 5, 4. Expected counters: 1, 0.
    setState(item1, 5);
    assertEquals(1, counter.get(item1), "Change of first item state should trigger the rule once");
    assertEquals(0, counter.get(item2), "Change of first item state should not trigger the rule for second item");

    // Items: 5, 4. Expected counters: 1, 0.
    setState(item1, 5);
    assertEquals(1, counter.get(item1), "Set state of first item to same value should not trigger the rule");
    assertEquals(0, counter.get(item2), "Change of first item state should not trigger the rule for second item");

    // Items: 5, 3. Expected counters: 1, 1.
    setState(item2, 3);
    assertEquals(1, counter.get(item2), "Change of second item state should trigger the rule");
    assertEquals(1, counter.get(item1), "Change of second item state should not trigger the rule for first item");

    // Items: 5, 3. Expected counters: 1, 1.
    setState(item1, 5);
    assertEquals(1, counter.get(item1), "Set state of first item to same value should still not trigger the rule");
    assertEquals(1, counter.get(item2), "Change of first item state should still not trigger the rule for second item");

    // Items: 6, 3. Expected counters: 2, 1.
    setState(item1, 6);
    assertEquals(2, counter.get(item1), "Set state of first item should trigger the rule");
    assertEquals(1, counter.get(item2), "Change of first item state should still not trigger the rule for second item");

    // Items: 6, 3. Expected counters: 2, 1.
    setState(item2, 3);
    assertEquals(1, counter.get(item2), "Set state of second item to same value should not trigger the rule");
    assertEquals(2, counter.get(item1), "Change of second item state should not trigger the rule for first item");

    // Items: 6, 0. Expected counters: 2, 2.
    setState(item2, 0);
    assertEquals(2, counter.get(item2), "Change of second item state should trigger the rule");
    assertEquals(2, counter.get(item1), "Change of second item state should not trigger the rule for first item");
  }

  @Test
  public void testRemoveActivation() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(4);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;

    Rule rule = new Rule();
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);

    rule.activateFor(item);

    assertEquals(0, counter.get(item), "Counter not initialized correctly");

    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");

    rule.deactivateFor(item);

    setState(item, 3);
    assertEquals(1, counter.get(item), "Change of item state should not change the counter anymore");
  }

  @Test
  public void testNumberConditions() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(2);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;

    Rule rule = new Rule();
    ItemStateNumberCheck check1 = new ItemStateNumberCheck(ComparatorType.GreaterOrEqualThan, 4);
    ItemStateNumberCheck check2 = new ItemStateNumberCheck(ComparatorType.LessThan, 6);
    rule.addCondition(new ItemStateCheckCondition(check1));
    rule.addCondition(new ItemStateCheckCondition(check2));
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(0, counter.get(item), "Counter not initialized correctly");

    setState(item, 3);
    assertEquals(0, counter.get(item), "Change of item to 3 should not trigger the rule, check1 violated");

    setState(item, 4);
    assertEquals(1, counter.get(item), "Change of item to 4 should trigger the rule");

    setState(item, 5);
    assertEquals(2, counter.get(item), "Change of item to 5 should trigger the rule");

    setState(item, 6);
    assertEquals(2, counter.get(item), "Change of item to 6 should not trigger the rule, check2 violated");

    setState(item, 7);
    assertEquals(2, counter.get(item), "Change of item to 7 should not trigger the rule, check2 violated");
  }


  @Test
  public void testStateSyncGroupRewriteStructure() {
    // init StateSyncGroup
    StateSyncGroup group = new StateSyncGroup();

    ArrayList<Item> items = new ArrayList<>();

    // create model and an item
    TestUtils.ModelAndItem mai = TestUtils.createModelAndItem(0, true);
    NumberItem item0 = mai.item;
    group.addTargetItem(item0);
    items.add(item0);

    // init more items
    for (int i=1;i<=4;i++) {
      NumberItem item = TestUtils.addItemTo(mai.model, i, true);
      item.setID("item"+i);
      group.addTargetItem(item);
      items.add(item);
    }

    // add StateSyncGroup as rule to root
    mai.model.getRoot().addRule(group);



    // trace rewritten rule and its actions
    Rule rewrittenRule = mai.model.getRoot().getRules().getChild(0);


    ImmutableList<Action> actions = ImmutableList.copyOf(rewrittenRule.getActionList().iterator());
    ImmutableList<ItemObserver> observers = ImmutableList.copyOf(rewrittenRule.getObservers().iterator());

    // check general structure
    assertEquals(ImmutableList.copyOf(rewrittenRule.getConditionList().iterator()).size(), 0);
    assertEquals(actions.size(), 5);

    // check actions and observers
    for (int i=0;i<items.size();i++) {
      assertTrue(actions.get(i) instanceof SetStateFromTriggeringItemAction);
      assertTrue(observers.contains(items.get(i).getItemObserver()));
    }



  }

  private static void addItemToModel(SmartHomeEntityModel model, Item item) {
    getDefaultGroup(model).addItem(item);
  }

  @Test
  public void testColorItemStateSyncGroup() {
    StateSyncGroup group = new StateSyncGroup();

    //init model and 3 items
    SmartHomeEntityModel model = TestUtils.createModelWithGroup();

    ColorItem colorItem1 = new ColorItem();
    addItemToModel(model,colorItem1);
    group.addTargetItem(colorItem1);

    ColorItem colorItem2 = new ColorItem();
    addItemToModel(model,colorItem2);
    group.addTargetItem(colorItem2);

    ColorItem colorItem3 = new ColorItem();
    addItemToModel(model,colorItem3);
    group.addTargetItem(colorItem3);


    // add StateSyncGroup as rule to root and trigger rewrite
    model.getRoot().addRule(group);
    model.getRoot().doSafeFullTraversal();


    colorItem1.setState(TupleHSB.parse("0,0,100"));
    assertEquals(colorItem1.getState(),TupleHSB.parse("0,0,100"));
    assertEquals(colorItem2.getState(),TupleHSB.parse("0,0,100"));
    assertEquals(colorItem3.getState(),TupleHSB.parse("0,0,100"));

    colorItem3.setState(TupleHSB.parse("0,0,7"));
    assertEquals(colorItem1.getState(),TupleHSB.parse("0,0,7"));
    assertEquals(colorItem2.getState(),TupleHSB.parse("0,0,7"));
    assertEquals(colorItem3.getState(),TupleHSB.parse("0,0,7"));


  }

  @Test
  public void testDateTimeItemStateSyncGroup() {
    StateSyncGroup group = new StateSyncGroup();

    //init model and 3 items
    SmartHomeEntityModel model = TestUtils.createModelWithGroup();

    DateTimeItem dateTimeItem1 = new DateTimeItem();
    addItemToModel(model,dateTimeItem1);
    group.addTargetItem(dateTimeItem1);

    DateTimeItem dateTimeItem2 = new DateTimeItem();
    addItemToModel(model,dateTimeItem2);
    group.addTargetItem(dateTimeItem2);

    DateTimeItem dateTimeItem3 = new DateTimeItem();
    addItemToModel(model,dateTimeItem3);
    group.addTargetItem(dateTimeItem3);


    // add StateSyncGroup as rule to root and trigger rewrite
    model.getRoot().addRule(group);
    model.getRoot().doSafeFullTraversal();

    Instant i1 = Instant.now();
    dateTimeItem1.setState(i1);
    assertEquals(dateTimeItem1.getState(),i1);
    assertEquals(dateTimeItem2.getState(),i1);
    assertEquals(dateTimeItem3.getState(),i1);

    Instant i2 = Instant.now();
    dateTimeItem3.setState(i2);
    assertEquals(dateTimeItem1.getState(),i2);
    assertEquals(dateTimeItem2.getState(),i2);
    assertEquals(dateTimeItem3.getState(),i2);
  }

  /**
   * Also for DimmerItem, RollerShutterItem, ActivityItem
   */
  @Test
  public void testDoubleStateItemStateSyncGroup() {
    StateSyncGroup group = new StateSyncGroup();

    //init model and 3 items
    SmartHomeEntityModel model = TestUtils.createModelWithGroup();

    NumberItem numberItem1 = new NumberItem();
    addItemToModel(model,numberItem1);
    group.addTargetItem(numberItem1);

    NumberItem numberItem2 = new NumberItem();
    addItemToModel(model,numberItem2);
    group.addTargetItem(numberItem2);

    NumberItem numberItem3 = new NumberItem();
    addItemToModel(model,numberItem3);
    group.addTargetItem(numberItem3);


    // add StateSyncGroup as rule to root and trigger rewrite
    model.getRoot().addRule(group);
    model.getRoot().doSafeFullTraversal();


    numberItem1.setState(123);
    assertEquals(numberItem1.getState(),123);
    assertEquals(numberItem2.getState(),123);
    assertEquals(numberItem3.getState(),123);

    numberItem2.setState(42);
    assertEquals(numberItem1.getState(),42);
    assertEquals(numberItem2.getState(),42);
    assertEquals(numberItem3.getState(),42);
  }

  /**
   * Also for ImageItem, LocationItem, PlayerItem, DefaultItem
   */
  @Test
  public void testStringStateItemStateSyncGroup() {
    StateSyncGroup group = new StateSyncGroup();

    //init model and 3 items
    SmartHomeEntityModel model = TestUtils.createModelWithGroup();

    StringItem stringItem1 = new StringItem();
    addItemToModel(model,stringItem1);
    group.addTargetItem(stringItem1);

    StringItem stringItem2 = new StringItem();
    addItemToModel(model,stringItem2);
    group.addTargetItem(stringItem2);

    StringItem stringItem3 = new StringItem();
    addItemToModel(model,stringItem3);
    group.addTargetItem(stringItem3);


    // add StateSyncGroup as rule to root and trigger rewrite
    model.getRoot().addRule(group);
    model.getRoot().doSafeFullTraversal();


    stringItem1.setState("123");
    assertEquals(stringItem1.getState(),"123");
    assertEquals(stringItem2.getState(),"123");
    assertEquals(stringItem3.getState(),"123");

    stringItem2.setState("Hermes");
    assertEquals(stringItem1.getState(),"Hermes");
    assertEquals(stringItem2.getState(),"Hermes");
    assertEquals(stringItem3.getState(),"Hermes");


  }


  /**
   * Also for ContactItem
   */
  @Test
  public void testBooleanStateItemStateSyncGroup() {
    StateSyncGroup group = new StateSyncGroup();

    //init model and 3 items
    SmartHomeEntityModel model = TestUtils.createModelWithGroup();

    SwitchItem switchItem1 = new SwitchItem();
    addItemToModel(model,switchItem1);
    group.addTargetItem(switchItem1);

    SwitchItem switchItem2 = new SwitchItem();
    addItemToModel(model,switchItem2);
    group.addTargetItem(switchItem2);

    SwitchItem switchItem3 = new SwitchItem();
    addItemToModel(model,switchItem3);
    group.addTargetItem(switchItem3);


    // add StateSyncGroup as rule to root and trigger rewrite
    model.getRoot().addRule(group);
    model.getRoot().doSafeFullTraversal();


    switchItem3.setState(false);
    assertFalse(switchItem1.getState());
    assertFalse(switchItem2.getState());
    assertFalse(switchItem3.getState());

    switchItem1.setState(true);
    assertTrue(switchItem1.getState());
    assertTrue(switchItem2.getState());
    assertTrue(switchItem3.getState());

  }


  @Test
  public void testTwoActions() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(2);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;

    Rule rule = new Rule();
    CountingAction counter1 = new CountingAction();
    rule.addAction(counter1);
    CountingAction counter2 = new CountingAction();
    rule.addAction(counter2);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(0, counter1.get(item), "First counter not initialized correctly");
    assertEquals(0, counter2.get(item), "Second counter not initialized correctly");

    setState(item, 3);
    assertEquals(1, counter1.get(item), "Change of item state should trigger the rule for first counter");
    assertEquals(1, counter2.get(item), "Change of item state should trigger the rule for second counter");

    setState(item, 3);
    assertEquals(1, counter1.get(item), "Change of item to same state should not trigger the rule for first counter");
    assertEquals(1, counter2.get(item), "Change of item to same state should not trigger the rule for second counter");
  }

  @Test
  public void testChainedRules() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(2);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;
    NumberItem item2 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);

    Rule ruleA = new Rule();
    CountingAction counter1 = new CountingAction();
    ruleA.addAction(counter1);

    Rule ruleB = new Rule();
    CountingAction counter2 = new CountingAction();
    ruleB.addAction(counter2);

    ruleA.addAction(new TriggerRuleAction(ruleB));

    root.addRule(ruleA);
    root.addRule(ruleB);
    ruleA.activateFor(item);
    ruleB.activateFor(item2);

    assertEquals(0, counter1.get(item), "First counter not initialized correctly for first item");
    assertEquals(0, counter1.get(item2), "First counter not initialized correctly for second item");
    assertEquals(0, counter2.get(item), "Second counter not initialized correctly for first item");
    assertEquals(0, counter2.get(item2), "Second counter not initialized correctly for second item");

    setState(item, 3);
    assertEquals(1, counter1.get(item), "Change of first item state should trigger the first rule");
    assertEquals(1, counter2.get(item), "Change of first item state should trigger the second rule");
    assertEquals(0, counter2.get(item2), "Change of first item state should not trigger the second rule for second item");

    setState(item, 3);
    assertEquals(1, counter1.get(item), "Change of item to same state should not trigger the first rule");
    assertEquals(1, counter2.get(item), "Change of item to same state should not trigger the second rule");
    assertEquals(0, counter2.get(item2), "Change of first item state should not trigger the second rule for second item");

    setState(item2, 7);
    assertEquals(1, counter1.get(item), "Change of second item state should not trigger the first rule");
    assertEquals(1, counter2.get(item), "Change of second item state should not trigger the second rule for the first item");
    assertEquals(1, counter2.get(item2), "Change of second item state should trigger the second rule for the second item");
  }

  @Test
  public void testSetStateFromConstantStringAction() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;
    NumberItem item2 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);

    Rule rule = new Rule();
    rule.addAction(new SetStateFromConstantStringAction(item2, "5"));
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(4, item2.asItemWithDoubleState().getState(), DELTA, "Affected item not initialized correctly");

    setState(item, 25);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(5, item2.asItemWithDoubleState().getState(), DELTA, "Change of item state should set the state of the affected item");
  }

  static class ValuedStateProvider implements NewStateProvider {
    int value;
    @Override
    public String get() {
      return Integer.toString(value);
    }
  }

  @Test
  public void testSetStateFromLambdaAction() {
    ValuedStateProvider provider = new ValuedStateProvider();
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(0);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;
    NumberItem item2 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 3, true);

    Rule rule = new Rule();
    rule.addAction(new SetStateFromLambdaAction(item2, provider));
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(3, item2.asItemWithDoubleState().getState(), DELTA, "Affected item not initialized correctly");

    provider.value = 4;
    setState(item, 1);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(4, item2.asItemWithDoubleState().getState(), DELTA, "Change of item state should set the state of the affected item");

    provider.value = 4;
    setState(item, 2);
    assertEquals(2, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(4, item2.asItemWithDoubleState().getState(), DELTA, "Change of item state should set the state of the affected item");

    provider.value = 5;
    setState(item, 2);
    assertEquals(2, counter.get(item), "Change of item to same state should not trigger the rule");
    assertEquals(4, item2.asItemWithDoubleState().getState(), DELTA, "Change of item to same state should not set the state of the affected item");

    provider.value = 5;
    setState(item, 3);
    assertEquals(3, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(5, item2.asItemWithDoubleState().getState(), DELTA, "Change of item state should set the state of the affected item");
  }

  @Test
  public void testSetStateFromTriggeringItemAction() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;
    StringItem item2 = addStringItem(root.getSmartHomeEntityModel(), "0");

    Rule rule = new Rule();
    CountingAction counter = new CountingAction();
    rule.addAction(new SetStateFromTriggeringItemAction(item2));
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(0, counter.get(item), "Counter not initialized correctly");
    assertEquals("0", item2.getState(), "Affected item not initialized correctly");

    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");
    assertEquals("5.0", item2.getState(), "Change of item state should set the state of the affected item");

    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item state should not trigger the rule");
    assertEquals("5.0", item2.getState(), "Change of item state should not set the state of the affected item");

    setState(item, 7);
    assertEquals(2, counter.get(item), "Change of item state should trigger the rule");
    assertEquals("7.0", item2.getState(), "Change of item state should set the state of the affected item");
  }

  @Test
  public void testSetStateFromItemsAction() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;
    NumberItem item2 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);
    StringItem affectedItem = addStringItem(root.getSmartHomeEntityModel(), "1");

    Rule rule = new Rule();
    SetStateFromItemsAction action = new SetStateFromItemsAction(items ->
        Long.toString(StreamSupport.stream(items.spliterator(), false)
            .mapToLong(inner -> (long) inner.asItemWithDoubleState().getState())
            .sum()));
    action.addSourceItem(item);
    action.addSourceItem(item2);
    action.setAffectedItem(affectedItem);
    rule.addAction(action);
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(0, counter.get(item), "Counter not initialized correctly");
    assertEquals("1", affectedItem.getState(), "Affected item not initialized correctly");

    // 5 + 4 = 9
    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");
    assertEquals("9", affectedItem.getState(), "Change of item state should set the state of the affected item");

    // still 5 + 4 = 9, as rule does not trigger for item2
    setState(item2, 5);
    assertEquals(1, counter.get(item), "Change of item2 state should not trigger the rule");
    assertEquals("9", affectedItem.getState(), "Change of item2 state should not set the state of the affected item");

    // still 5 + 4 = 9, as rule should not trigger
    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item to same state should not trigger the rule");
    assertEquals("9", affectedItem.getState(), "Change of item to same state should not set the state of the affected item");

    // 7 + 5 = 12
    setState(item, 7);
    assertEquals(2, counter.get(item), "Change of item state should trigger the rule");
    assertEquals("12", affectedItem.getState(), "Change of item state should set the state of the affected item");

    // add new item to sum
    NumberItem item3 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), -4, true);
    action.addSourceItem(item3);

    // still 7 + 5 = 12, as rule should not trigger
    setState(item, 7);
    assertEquals(2, counter.get(item), "Change of item to same state should not trigger the rule");
    assertEquals("12", affectedItem.getState(), "Change of item to same state should not set the state of the affected item");

    // 8 + 5 - 4 = 9
    setState(item, 8);
    assertEquals(3, counter.get(item), "Change of item state should trigger the rule");
    assertEquals("9", affectedItem.getState(), "Change of item state should set the state of the affected item");
  }

  @Test
  public void testAddDoubleToStateAction() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;
    NumberItem affectedItem = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);

    Rule rule = new Rule();
    rule.addAction(new AddDoubleToStateAction(affectedItem, 2));
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(0, counter.get(item), "Counter not initialized correctly");
    assertEquals(4, affectedItem.getState(), DELTA, "Affected item not initialized correctly");

    // 4 + 2 = 6
    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(6, affectedItem.getState(), DELTA, "Change of item state should set the state of the affected item");

    // still 6
    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item to same state should not trigger the rule");
    assertEquals(6, affectedItem.getState(), DELTA, "Change of item to same state should not set the state of the affected item");

    // 6 + 2 = 8
    setState(item, -2);
    assertEquals(2, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(8, affectedItem.getState(), DELTA, "Change of item state should set the state of the affected item");
  }

  @Test
  public void testMultiplyDoubleToStateAction() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item = modelAndItem.item;
    NumberItem affectedItem = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);

    Rule rule = new Rule();
    rule.addAction(new MultiplyDoubleToStateAction(affectedItem, 2));
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    root.addRule(rule);
    rule.activateFor(item);

    assertEquals(0, counter.get(item), "Counter not initialized correctly");
    assertEquals(4, affectedItem.getState(), DELTA, "Affected item not initialized correctly");

    // 4 * 2 = 8
    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(8, affectedItem.getState(), DELTA, "Change of item state should set the state of the affected item");

    // still 8
    setState(item, 5);
    assertEquals(1, counter.get(item), "Change of item to same state should not trigger the rule");
    assertEquals(8, affectedItem.getState(), DELTA, "Change of item to same state should not set the state of the affected item");

    // 8 * 2 = 16
    setState(item, 0);
    assertEquals(2, counter.get(item), "Change of item state should trigger the rule");
    assertEquals(16, affectedItem.getState(), DELTA, "Change of item state should set the state of the affected item");
  }

  @Test
  public void testChainAddMultiplyActions() {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item1 = modelAndItem.item;
    NumberItem item2 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);
    NumberItem affectedItem = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 5, true);

    Rule ruleA = new Rule();
    ruleA.addAction(new AddDoubleToStateAction(affectedItem, 2));
    CountingAction counterA = new CountingAction();
    ruleA.addAction(counterA);

    Rule ruleB = new Rule();
    ruleB.addAction(new MultiplyDoubleToStateAction(affectedItem, 3));
    CountingAction counterB = new CountingAction();
    ruleB.addAction(counterB);

    ruleA.addAction(new TriggerRuleAction(ruleB));

    root.addRule(ruleA);
    root.addRule(ruleB);
    ruleA.activateFor(item1);
    ruleB.activateFor(item2);

    assertEquals(0, counterA.get(item1), "CounterA not initialized correctly for first item");
    assertEquals(0, counterA.get(item2), "CounterA not initialized correctly for second item");
    assertEquals(0, counterB.get(item1), "CounterB not initialized correctly for first item");
    assertEquals(0, counterB.get(item2), "CounterB not initialized correctly for second item");
    assertEquals(5, affectedItem.getState(), DELTA, "Affected item not initialized correctly");

    // First, 5 + 2 = 7. Then, 7 * 3 = 21
    setState(item1, 5);
    assertEquals(1, counterA.get(item1), "Change of item state should trigger the ruleA");
    assertEquals(1, counterB.get(item1), "Change of item state should also trigger the ruleB");
    assertEquals(21, affectedItem.getState(), DELTA, "Change of item state should set the state of the affected item");

    // still 21
    setState(item1, 5);
    assertEquals(1, counterA.get(item1), "Change of item to same state should not trigger the ruleA");
    assertEquals(1, counterB.get(item1), "Change of item to same state should not trigger the ruleB");
    assertEquals(21, affectedItem.getState(), DELTA, "Change of item to same state should not set the state of the affected item");

    // Only, 21 * 3 = 63
    setState(item2, 1);
    assertEquals(1, counterA.get(item1), "Change of second item state should not trigger the ruleA");
    assertEquals(1, counterB.get(item1), "Change of second item state should not trigger the ruleB for first item");
    assertEquals(1, counterB.get(item2), "Change of second item state should trigger the ruleB for second item");
    assertEquals(63, affectedItem.getState(), DELTA, "Change of second item state should set the state of the affected item");
  }

  @Test
  public void testSetFromExpression() throws IOException, Parser.Exception {
    TestUtils.ModelAndItem modelAndItem = createModelAndItem(3);
    Root root = modelAndItem.model.getRoot();
    NumberItem item1 = modelAndItem.item;
    NumberItem item2 = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 4, true);
    NumberItem affectedItem = TestUtils.addItemTo(root.getSmartHomeEntityModel(), 5, true);

    Rule rule = new Rule();
    SetStateFromExpression action = new SetStateFromExpression();
    // TODO item1 should be referred to as triggering item
    action.setNumberExpression(ParserUtils.parseNumberExpression("(" + item1.getID() + " + " + item2.getID() + ")", root));
    action.setAffectedItem(affectedItem);
    rule.addAction(action);
    CountingAction counter = new CountingAction();
    rule.addAction(counter);

    root.addRule(rule);
    rule.activateFor(item1);

    assertEquals(0, counter.get(item1), "Counter not initialized correctly");
    assertEquals(4, item2.getState(), DELTA, "Second item not initialized correctly");
    assertEquals(5, affectedItem.getState(), DELTA, "Affected item not initialized correctly");

    // 5 + 4 = 9
    setState(item1, 5);
    assertEquals(1, counter.get(item1), "Change of item state should trigger the rule");
    assertEquals(9, affectedItem.getState(), DELTA, "Change of item state should set the state of the affected item");

    // still 9
    setState(item1, 5);
    assertEquals(1, counter.get(item1), "Change of item to same state should not trigger the rule");
    assertEquals(9, affectedItem.getState(), DELTA, "Change of item to same state should not set the state of the affected item");

    // still 9 (changes of item2 do not trigger the rule)
    setState(item2, 1);
    assertEquals(1, counter.get(item1), "Change of second item to same state should not trigger the rule");
    assertEquals(9, affectedItem.getState(), DELTA, "Change of second item to same state should not set the state of the affected item");

    // 0 + 1 = 1
    setState(item1, 0);
    assertEquals(2, counter.get(item1), "Change of item state should trigger the rule");
    assertEquals(1, affectedItem.getState(), DELTA, "Change of item state should set the state of the affected item");
  }

  @Disabled("Not working reliably, need to be made more robust")
  @Test
  public void testCronJobRule() {
    Rule rule = new Rule();
    CountingAction counter = new CountingAction();
    rule.addAction(counter);

    assertEquals(0, counter.get(null), "Counter not initialized correctly");

    ScheduledFuture f = rule.activateEvery(50, TimeUnit.MILLISECONDS);
    waitMillis(160);
    assertTrue(f.cancel(true), "Rule cron job could not be cancelled");
    // ----------------------- 160ms -----------------------
    // + -- 50ms -- + -- 50ms -- + -- 50 ms -- + -- 10 ms --
    // 4 times executed (+), with no initial delay
    assertEquals(4, counter.get(null), "Rule was not executed four times");

    counter.reset();
    assertEquals(0, counter.get(null), "Counter not reset correctly");

    f = rule.activateEvery(80, 50, TimeUnit.MILLISECONDS);
    waitMillis(150);
    assertTrue(f.cancel(true), "Rule cron job could not be cancelled");
    // ------------------ 150ms ------------------
    // ----- 80ms ----- + -- 50ms -- + -- 20 ms --
    // 2 times executed (+), with given delay
    assertEquals(2, counter.get(null), "Rule was not executed two times");
  }

  @Test
  public void testFrequencySetting() {

    TestUtils.ModelAndItem mai = createModelAndItem(0);
    NumberItem numberItem = mai.item;

    FrequencySetting frequencySetting = new FrequencySetting();
    frequencySetting.setEventProcessingFrequency(10);
    numberItem.setFrequencySetting(frequencySetting);

    Rule rule = new Rule();
    CountingAction counter = new CountingAction();
    rule.addAction(counter);
    rule.activateFor(numberItem);
    numberItem.setState(1);
    numberItem.setState(2);
    assertEquals(1, counter.get(numberItem), "Action was triggered although FrequencySetting too small");
    counter.reset();
    waitMillis(100);
    numberItem.setState(3);
    assertEquals(1, counter.get(numberItem), "Action wasn't triggered although frequency FrequencySetting is small enough");
    counter.reset();
    numberItem.setState(4);
    numberItem.setState(5);
    assertEquals(0, counter.get(numberItem), "Action was triggered although FrequencySetting too small");
    counter.reset();

  }

  private static void waitMillis(int millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Sleeping was interrupted!");
    }
  }

  private StringItem addStringItem(SmartHomeEntityModel model, String initialValue) {
    StringItem item = new StringItem();
    Group group = getDefaultGroup(model);
    item.setID("item" + group.getNumItem());
    item.setState(initialValue, false);
    group.addItem(item);
    return item;
  }

  private TestUtils.ModelAndItem createModelAndItem(long initialValue) {
    return TestUtils.createModelAndItem(initialValue, true);
  }

  private void setState(NumberItem item, long newState) {
    item.setState(newState);
  }

}
