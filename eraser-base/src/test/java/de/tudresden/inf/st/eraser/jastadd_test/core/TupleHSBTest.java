package de.tudresden.inf.st.eraser.jastadd_test.core;

import de.tudresden.inf.st.eraser.jastadd.model.TupleHSB;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Testing the class {@link de.tudresden.inf.st.eraser.jastadd.model.TupleHSB}.
 *
 * @author rschoene - Initial contribution
 */
public class TupleHSBTest {

  @Test
  public void testBounds() {
    checkWithinBounds(TupleHSB.of(1,2,3));
    checkWithinBounds(TupleHSB.of(340,2,3));
    checkWithinBounds(TupleHSB.of(999,2,3));
    checkWithinBounds(TupleHSB.of(11,200,3));
    checkWithinBounds(TupleHSB.of(240,200,3));
    checkWithinBounds(TupleHSB.of(899,200,3));
    checkWithinBounds(TupleHSB.of(21,2,300));
    checkWithinBounds(TupleHSB.of(140,2,300));
    checkWithinBounds(TupleHSB.of(799,2,300));
  }

  @Test
  public void testEquals() {
    TupleHSB one = TupleHSB.of(1,2,3);
    TupleHSB two = TupleHSB.of(1,2,3);
    TupleHSB three = TupleHSB.of(99,99,99);
    assertEquals(one, two);
    assertNotEquals(one, three);
    assertNotEquals(two, three);

    TupleHSB oneEqualModuloHue = TupleHSB.of(361,2,3);
    assertEquals(one, oneEqualModuloHue);

    TupleHSB bigSB = TupleHSB.of(50,100,100);
    TupleHSB biggerS = TupleHSB.of(50,123,100);
    TupleHSB biggerB = TupleHSB.of(50,123,6484);
    assertEquals(bigSB, biggerS);
    assertEquals(bigSB, biggerB);
  }

  @Test
  public void testWithDifferent() {
    TupleHSB one = TupleHSB.of(1,2,3);
    TupleHSB oneDifferentHue = one.withDifferentHue(43);
    TupleHSB oneDifferentSaturation = one.withDifferentSaturation(43);
    TupleHSB oneDifferentBrightness = one.withDifferentBrightness(43);
    TupleHSB oneEqualModuloHue = one.withDifferentHue(721);

    assertNotEquals(one, oneDifferentHue);
    assertNotEquals(one, oneDifferentSaturation);
    assertNotEquals(one, oneDifferentBrightness);
    assertNotEquals(oneDifferentHue, oneDifferentSaturation);
    assertNotEquals(oneDifferentHue, oneDifferentBrightness);
    assertNotEquals(oneDifferentSaturation, oneDifferentBrightness);

    assertEquals(one, oneEqualModuloHue);
  }

  @Test
  public void testPrint() {
    TupleHSB one = TupleHSB.of(1,2,3);
    String expectedForOne = "1,2,3";
    assertEquals(expectedForOne, one.toString());

    TupleHSB two = TupleHSB.of(341,92,555);
    String expectedForTwo = "341,92,100";
    assertEquals(expectedForTwo, two.toString());
  }

  @Test
  public void testParse() {
    String one = "3,2,1";
    TupleHSB expectedForOne = TupleHSB.of(3, 2, 1);
    assertEquals(expectedForOne, TupleHSB.parse(one));

    String two = "399,201,17";
    TupleHSB expectedForTwo = TupleHSB.of(39, 100, 17);
    assertEquals(expectedForTwo, TupleHSB.parse(two));
  }

  @Test
  public void testClone() {
    TupleHSB one = TupleHSB.of(361,2,3);
    TupleHSB two = TupleHSB.of(50,123,100);
    TupleHSB clone = one.clone();

    assertEquals(one, clone);
    assertNotEquals(one, two);
    assertNotEquals(clone, two);
  }

  private void checkWithinBounds(TupleHSB hsb) {
    assertThat(hsb.getHue()).isGreaterThanOrEqualTo(0);
    assertThat(hsb.getHue()).isLessThanOrEqualTo(360);
    assertThat(hsb.getSaturation()).isGreaterThanOrEqualTo(0);
    assertThat(hsb.getSaturation()).isLessThanOrEqualTo(100);
    assertThat(hsb.getBrightness()).isGreaterThanOrEqualTo(0);
    assertThat(hsb.getBrightness()).isLessThanOrEqualTo(100);
  }

}
