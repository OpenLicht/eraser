package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.TestUtils;
import de.tudresden.inf.st.eraser.util.TestUtils.ModelAndItem;
import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing the simple rule engine.
 *
 * @author rschoene - Initial contribution
 */
public class ControllingItemTest {

  private static final double DELTA = 0.001;

  @Test
  public void testOneToOneNumber() {
    ModelAndItem mai = TestUtils.createModelAndItem(0, true);
    NumberItem item1 = mai.item;
    NumberItem item2 = TestUtils.addItemTo(mai.model, 4, true);
    item1.addControlling(item2);

    assertEquals(0, item1.getState(), DELTA);
    assertEquals(4, item2.getState(), DELTA);

    item1.setState(5);
    assertEquals(5, item1.getState(), DELTA);
    assertEquals(5, item2.getState(), DELTA, "Item was not controlled correctly");
  }
  @Test
  public void testRemoveControlling() {
    ModelAndItem mai = TestUtils.createModelAndItem(0, true);
    NumberItem controlledItem = mai.item;
    NumberItem controllingItem = TestUtils.addItemTo(mai.model, 4, true);
    controllingItem.addControlling(controlledItem);
    controllingItem.removeControlling(controlledItem);

    controllingItem.setState(8);

    assertEquals(8, controllingItem.getState(), DELTA);
    assertEquals(0, controlledItem.getState(), DELTA, "Item was still controlled although the controlling was removed");
  }

  @Test
  public void testOneToManyNumber() {
    ModelAndItem mai = TestUtils.createModelAndItem(0, true);
    NumberItem item1 = mai.item;
    NumberItem item2 = TestUtils.addItemTo(mai.model, 3, true);
    NumberItem item3 = TestUtils.addItemTo(mai.model, 4, true);
    NumberItem item4 = TestUtils.addItemTo(mai.model, 5, true);
    item1.addControlling(item2);
    item1.addControlling(item3);
    item1.addControlling(item4);

    assertEquals(0, item1.getState(), DELTA);
    assertEquals(3, item2.getState(), DELTA);
    assertEquals(4, item3.getState(), DELTA);
    assertEquals(5, item4.getState(), DELTA);

    item1.setState(5);
    assertEquals(5, item1.getState(), DELTA);
    assertEquals(5, item2.getState(), DELTA, "Item2 was not controlled correctly");
    assertEquals(5, item3.getState(), DELTA, "Item3 was not controlled correctly");
    assertEquals(5, item4.getState(), DELTA, "Item4 was not controlled correctly");
  }

  @Test
  public void testManyToOneColor() {
    ModelAndItem mai = TestUtils.createModelAndItem(0);
    SmartHomeEntityModel model = mai.model;
    NumberItem numberItem = mai.item;

    Group g = TestUtils.getDefaultGroup(model);

    StringItem stringItem = initAndAddItem(g, new StringItem(),
        item -> item.setState("0"));

    SwitchItem booleanItem = initAndAddItem(g, new SwitchItem(),
        item -> item.setState(false));

    ColorItem colorItem = initAndAddItem(g, new ColorItem(),
        item -> item.setState(TupleHSB.of(0, 0, 0)));

    ColorItem target = initAndAddItem(g, new ColorItem(),
        item -> item.setState(TupleHSB.of(0, 0, 0)));

    target.addControlledBy(numberItem);
    target.addControlledBy(stringItem);
    target.addControlledBy(booleanItem);
    target.addControlledBy(colorItem);

    assertEquals(0, numberItem.getState(), DELTA);
    assertEquals("0", stringItem.getState());
    assertFalse(booleanItem.getState());
    assertEquals(TupleHSB.of(0, 0, 0), colorItem.getState());
    assertEquals(TupleHSB.of(0, 0, 0), target.getState());

    // number 5 -> set brightness to 5
    numberItem.setState(5);
    assertEquals(5, numberItem.getState(), DELTA);
    assertEquals(TupleHSB.of(0, 0, 5), target.getState(), "Item was not controlled correctly");

    // string 30 -> set brightness to 30
    stringItem.setState("30");
    assertEquals("30", stringItem.getState());
    assertEquals(TupleHSB.of(0, 0, 30), target.getState(), "Item was not controlled correctly");

    // string 30,20,10 -> set HSB to (30,20,10)
    stringItem.setState("30,20,10");
    assertEquals("30,20,10", stringItem.getState());
    assertEquals(TupleHSB.of(30, 20, 10), target.getState(), "Item was not controlled correctly");

    // boolean true -> brightness should still be 10 because it's already turned on
    booleanItem.setState(true);
    assertTrue(booleanItem.getState());
    assertEquals(TupleHSB.of(30, 20, 10), target.getState(), "Item was not controlled correctly");

    // color (33,33,33) -> set brightness to (33,33,33)
    colorItem.setState(TupleHSB.of(33, 33, 33));
    assertEquals(TupleHSB.of(33, 33, 33), colorItem.getState());
    assertEquals(TupleHSB.of(33, 33, 33), target.getState(), "Item was not controlled correctly");

    // number 44 -> set brightness to 44
    numberItem.setState(44);
    assertEquals(44, numberItem.getState(), DELTA);
    assertEquals(TupleHSB.of(33, 33, 44), target.getState(), "Item was not controlled correctly");
  }

  private <T extends Item> T initAndAddItem(Group group, T item, Consumer<T> setState) {
    item.setID("item" + group.getNumItem());
    group.addItem(item);
    setState.accept(item);
    item.enableSendState();
    return item;
  }


  @Test
  public void testCircularControlling() {
    ModelAndItem mai = TestUtils.createModelAndItem(0, true);
    NumberItem item1 = mai.item;
    NumberItem item2 = TestUtils.addItemTo(mai.model, 4, true);
    item1.addControlling(item2);
    item2.addControlling(item1);

    assertEquals(0, item1.getState(), DELTA);
    assertEquals(4, item2.getState(), DELTA);

    item1.setState(5);
    assertEquals(5, item1.getState(), DELTA, "Item was not controlled correctly");
    assertEquals(5, item2.getState(), DELTA, "Item was not controlled correctly");

    item2.setState(3);
    assertEquals(3, item1.getState(), DELTA, "Item was not controlled correctly");
    assertEquals(3, item2.getState(), DELTA, "Item was not controlled correctly");
  }
}
