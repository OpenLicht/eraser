/* Copyright (c) 2005-2015, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tudresden.inf.st.eraser.jastadd_test.core;

import java.io.*;
import java.util.*;

/**
 * Utility methods for JastAdd testing
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class Util {

  public static String platformIndependentPath(String regularPath) {
    return regularPath.replace('\\', '/');
  }

//  public static final String TEST_ROOT = "src/test/resources/tests";
//  public static final String TEST_ROOT = "tests";

  /**
   * Find all test directories
   * @param testRoot  root directory of tests
   * @param tests     list of test configurations to add
   * @param excludes  paths to exclude
   */
  private static void addChildTestDirs(File testRoot, List<TestConfiguration> tests,
                                       Collection<String> excludes) {
    if (testRoot == null) {
      return;
    }
    File[] files = testRoot.listFiles();
    if (files == null) {
      System.err.println("Not a directory: " + testRoot);
      return;
    }
    for (File child: files) {
      addTestDir(platformIndependentPath(testRoot.getPath()), child, tests, excludes);
    }
  }

  private static void addTestDir(String testRoot, File dir,
                                 List<TestConfiguration> tests, Collection<String> excludes) {

    if (dir.isDirectory()) {
      String path = dir.getPath().replace(File.separatorChar, '/');
      if (path.startsWith(testRoot + "/"))
        path = path.substring(testRoot.length()+1);
      for (String exclude: excludes) {
        if (path.startsWith(exclude)) {
          return;
        }
      }
      File inputFile = new File(dir, "input.eraser");
      File propertiesFile = new File(dir, "Test.properties");
      if (inputFile.isFile() || propertiesFile.isFile()) {
        if (!skipTest(dir)) {
          addUnitTests(testRoot, tests, path, dir);
        }
      } else {
        addChildTestDirs(dir, tests, excludes);
      }
    }
  }

  /**
   * Add separate test for each option set.
   */
  private static void addUnitTests(String testRoot, List<TestConfiguration> tests, String path, File dir) {
    Properties testProperties = Util.getTestProperties(dir);
    String optionsProperty = testProperties.getProperty("options", "");
    String extraOptions = testProperties.getProperty("extraoptions", "");
    String[] options = optionsProperty.split("\\|", -1);
    int index = 1;
    for (String option : options) {
      TestOptions u = new TestOptions((option + " " + extraOptions).trim(),
          options.length > 1, index);
      TestConfiguration config = new TestConfiguration(testRoot, path, u);
      tests.add(config);
      index++;
    }
  }

  private static void addByPattern(File root, String pattern,
                                   List<TestConfiguration> tests, Collection<String> excludes) {
    if (pattern.isEmpty()) {
      addTestDir(root.getPath(), root, tests, excludes);
    } else {
      int index = pattern.indexOf('/');
      String part, rest;
      if (index == -1) {
        part = pattern;
        rest = "";
      } else {
        part = pattern.substring(0, index);
        rest = pattern.substring(index + 1);
      }
      if (part.indexOf('*') == -1) {
        addByPattern(new File(root, part), rest, tests, excludes);
      } else if (part.equals("**")) {
        addByPattern(root, rest, tests, excludes);
        addByPattern(root, "*/**/" + rest, tests, excludes);
      } else if (root.isDirectory()) {
        for (File file: root.listFiles()) {
          if (patternMatch(file.getName().toCharArray(), 0, part.toCharArray(), 0)) {
            addByPattern(file, rest, tests, excludes);
          }
        }
      }
    }
  }

  private static boolean patternMatch(char[] name, int ni, char[] pattern, int pi) {
    if (ni >= name.length && pi >= pattern.length) {
      return true;
    }

    char p = pattern[pi];
    if (p == '*') {
      if (ni >= name.length) {
        return patternMatch(name, ni, pattern, pi + 1);
      } else {
        return patternMatch(name, ni + 1, pattern, pi)
            || patternMatch(name, ni + 1, pattern, pi + 1);
      }
    } else {
      if (ni >= name.length) {
        return false;
      } else {
        char n = name[ni];
        return n == p && patternMatch(name, ni + 1, pattern, pi + 1);
      }
    }
  }

  /**
   * @param testDir
   * @return <code>true</code> if the test should be skipped
   */
  private static boolean skipTest(File testDir) {
    return false;
  }

  /**
   * @param properties
   * @return A collection of String arrays containing the test directories
   */
  public static List<TestConfiguration> getTests(TestProperties properties) {
    List<TestConfiguration> tests = new ArrayList<>();

    Collection<String> includes = properties.includes();
    Collection<String> excludes = properties.excludes();

    if (includes.isEmpty()) {
      addTestDir(properties.getTestRoot(), new File(properties.getTestRoot()), tests, excludes);
    } else {
      for (String include: includes) {
        addByPattern(new File(properties.getTestRoot()), platformIndependentPath(include), tests, excludes);
      }
    }

    for (TestConfiguration test : tests) {
      test.addOptions(properties.getProperty("extraoptions", ""));
    }

    // Sort the tests lexicographically.
    tests.sort(Comparator.comparing(TestConfiguration::toString));
    return tests;
  }

  /**
   * @param propertiesFile
   * @return The properties loaded from the given file
   */
  private static TestProperties getProperties(File propertiesFile) {
    TestProperties properties = new TestProperties();
    try {
      FileInputStream in = new FileInputStream(propertiesFile);
      properties.load(in);
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return properties;
  }

  /**
   * @param sourceFile
   * @return Test configuration loaded from comments
   */
  private static TestProperties readPropertyComments(File sourceFile) {
    TestProperties properties = new TestProperties();
    try {
      Scanner scanner = new Scanner(new FileInputStream(sourceFile));
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        if (line.startsWith("//")) {
          int index = line.indexOf('.');
          if (index == 2 || index == 3) {
            // this is a property definition
            String property = line.substring(index+1).trim();
            if (!property.isEmpty()) {
              properties.load(new StringReader(property));
            }
          }
        } else {
          break;
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return properties;
  }

  /**
   * Try to read the Test.properties file, if that fails read properties from
   * Test.java comments.
   * @param testDir
   * @return test properties
   */
  public static TestProperties getTestProperties(File testDir) {
    File propertiesFile = new File(testDir, "Test.properties");
    if (propertiesFile.isFile()) {
      // Read test config from the .properties file.
      return Util.getProperties(propertiesFile);
    } else {
      File sourceFile = new File(testDir, "Test.java");
      if (sourceFile.isFile()) {
        // Read test config from the .java file.
        return Util.readPropertyComments(sourceFile);
      }
      sourceFile = new File(testDir, "Test.jrag");
      if (sourceFile.isFile()) {
        // Read test config from the .jrag file.
        return Util.readPropertyComments(sourceFile);
      }
      return new TestProperties();
    }
  }
}
