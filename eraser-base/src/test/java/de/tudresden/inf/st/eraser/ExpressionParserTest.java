package de.tudresden.inf.st.eraser;

import beaver.Parser;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test correct parsing of NumberExpression and LogicalExpression.
 *
 * @author rschoene - Initial contribution
 */
public class ExpressionParserTest {

  private static final String TRUE_EXPRESSION = "(0.0==0.0)";

  @Test
  public void plusExpression() throws IOException, Parser.Exception {
    NumberExpression sut = parseWithRoundTripNumberExpression("(3 + 4)");
    assertThat(sut).isInstanceOf(AddExpression.class);
    AddExpression addExpression = (AddExpression) sut;
    assertThat(addExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression left = (NumberLiteralExpression) addExpression.getLeftOperand();
    assertEquals(3.0, left.getValue());
    assertThat(addExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression right = (NumberLiteralExpression) addExpression.getRightOperand();
    assertEquals(4.0, right.getValue());
  }

  @Test
  public void minusExpression() throws IOException, Parser.Exception {
    NumberExpression sut = parseWithRoundTripNumberExpression("(12.5 - 4.1)");
    assertThat(sut).isInstanceOf(SubExpression.class);
    SubExpression subExpression = (SubExpression) sut;
    assertThat(subExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression left = (NumberLiteralExpression) subExpression.getLeftOperand();
    assertEquals(12.5, left.getValue());
    assertThat(subExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression right = (NumberLiteralExpression) subExpression.getRightOperand();
    assertEquals(4.1, right.getValue());
  }

  @Test
  public void multExpression() throws IOException, Parser.Exception {
    NumberExpression sut = parseWithRoundTripNumberExpression("(0 * 0)");
    assertThat(sut).isInstanceOf(MultExpression.class);
    MultExpression multExpression = (MultExpression) sut;
    assertThat(multExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression left = (NumberLiteralExpression) multExpression.getLeftOperand();
    assertEquals(0.0, left.getValue());
    assertThat(multExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression right = (NumberLiteralExpression) multExpression.getRightOperand();
    assertEquals(0.0, right.getValue());
  }

  @Test
  public void divExpression() throws IOException, Parser.Exception {
    NumberExpression sut = parseWithRoundTripNumberExpression("(1.1 / 0.0)");
    assertThat(sut).isInstanceOf(DivExpression.class);
    DivExpression divExpression = (DivExpression) sut;
    assertThat(divExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression left = (NumberLiteralExpression) divExpression.getLeftOperand();
    assertEquals(1.1, left.getValue());
    assertThat(divExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression right = (NumberLiteralExpression) divExpression.getRightOperand();
    assertEquals(0.0, right.getValue());
  }

  @Test
  public void powerExpression() throws IOException, Parser.Exception {
    NumberExpression sut = parseWithRoundTripNumberExpression("(3 ^ 0.5)");
    assertThat(sut).isInstanceOf(PowerExpression.class);
    PowerExpression powExpression = (PowerExpression) sut;
    assertThat(powExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression left = (NumberLiteralExpression) powExpression.getLeftOperand();
    assertEquals(3.0, left.getValue());
    assertThat(powExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression right = (NumberLiteralExpression) powExpression.getRightOperand();
    assertEquals(0.5, right.getValue());
  }

  @Test
  public void parenthesizedExpression() throws IOException, Parser.Exception {
    NumberExpression sut = parseWithRoundTripNumberExpression("(3)");
    assertThat(sut).isInstanceOf(ParenthesizedNumberExpression.class);
    ParenthesizedNumberExpression parenExpression = (ParenthesizedNumberExpression) sut;
    assertThat(parenExpression.getOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression left = (NumberLiteralExpression) parenExpression.getOperand();
    assertEquals(3.0, left.getValue());
  }

  @Test
  public void complexExpression() throws IOException, Parser.Exception {
    NumberExpression sut = parseWithRoundTripNumberExpression("((3 + 4) * (1 / (12 - 8)))");
    assertThat(sut).isInstanceOf(MultExpression.class);
    MultExpression multExpression = (MultExpression) sut;

    // 3+4
    assertThat(multExpression.getLeftOperand()).isInstanceOf(AddExpression.class);
    AddExpression addExpression = (AddExpression) multExpression.getLeftOperand();
    assertThat(addExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression left = (NumberLiteralExpression) addExpression.getLeftOperand();
    assertEquals(3.0, left.getValue());
    assertThat(addExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression right = (NumberLiteralExpression) addExpression.getRightOperand();
    assertEquals(4.0, right.getValue());

    // 1/(12-8)
    assertThat(multExpression.getRightOperand()).isInstanceOf(DivExpression.class);
    DivExpression divExpression = (DivExpression) multExpression.getRightOperand();

    assertThat(divExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression leftOfDiv = (NumberLiteralExpression) divExpression.getLeftOperand();
    assertEquals(1.0, leftOfDiv.getValue());

    // 12-8
    assertThat(divExpression.getRightOperand()).isInstanceOf(SubExpression.class);
    SubExpression rightofDiv = (SubExpression) divExpression.getRightOperand();
    assertThat(rightofDiv.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression leftOfSSub = (NumberLiteralExpression) rightofDiv.getLeftOperand();
    assertEquals(12.0, leftOfSSub.getValue());
    assertThat(rightofDiv.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression rightOfSub = (NumberLiteralExpression) rightofDiv.getRightOperand();
    assertEquals(8.0, rightOfSub.getValue());
  }

  @Test
  public void expressionWithItem() throws IOException, Parser.Exception {
    Item referenceItem = ParserUtils.parseItem("Number Item: id=\"myItem\";");
    NumberExpression sut = parseWithRoundTripNumberExpression("(myItem * 3)", referenceItem.getRoot());
    assertThat(sut).isInstanceOf(MultExpression.class);
    MultExpression multExpression = (MultExpression) sut;
    assertThat(multExpression.getLeftOperand()).isInstanceOf(Designator.class);
    Designator left = (Designator) multExpression.getLeftOperand();
    assertEquals(referenceItem, left.getItem());
    assertThat(multExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression right = (NumberLiteralExpression) multExpression.getRightOperand();
    assertEquals(3.0, right.getValue());
  }

  @Test
  public void comparingExpressions() throws IOException, Parser.Exception {
    comparingExpression("<",  ComparatorType.LessThan, 1, 2);
    comparingExpression("<=", ComparatorType.LessOrEqualThan, 3, 4);
    comparingExpression("==", ComparatorType.Equals, 5, 6);
    comparingExpression("!=", ComparatorType.NotEquals, 7, 8);
    comparingExpression(">=", ComparatorType.GreaterOrEqualThan, 9, 10);
    comparingExpression(">",  ComparatorType.GreaterThan, 11, 12);
  }

  private void comparingExpression(String actualComparatorString, ComparatorType expectedComparatorType, double left, double right) throws IOException, Parser.Exception {
    String expression = String.format("(%s %s %s)", left, actualComparatorString, right);
    LogicalExpression sut = parseWithRoundTripLogicalExpression(expression);
    assertThat(sut).isInstanceOf(ComparingExpression.class);
    ComparingExpression comparingExpression = (ComparingExpression) sut;
    assertThat(comparingExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression leftExpression = (NumberLiteralExpression) comparingExpression.getLeftOperand();
    assertEquals(left, leftExpression.getValue());
    assertThat(comparingExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression rightExpression = (NumberLiteralExpression) comparingExpression.getRightOperand();
    assertEquals(right, rightExpression.getValue());
    assertEquals(expectedComparatorType, comparingExpression.getComparator());
  }

  @Test
  public void notExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = parseWithRoundTripLogicalExpression("!" + TRUE_EXPRESSION);
    assertThat(sut).isInstanceOf(NotExpression.class);
    NotExpression notExpression = (NotExpression) sut;
    checkZeroEqualsZero(notExpression.getOperand());
  }

  @Test
  public void andExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = parseWithRoundTripLogicalExpression("(" + TRUE_EXPRESSION + " & " + TRUE_EXPRESSION + ")");
    assertThat(sut).isInstanceOf(AndExpression.class);
    AndExpression notExpression = (AndExpression) sut;
    checkZeroEqualsZero(notExpression.getLeftOperand());
    checkZeroEqualsZero(notExpression.getRightOperand());
  }

  @Test
  public void orExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = parseWithRoundTripLogicalExpression("(" + TRUE_EXPRESSION + " | " + TRUE_EXPRESSION + ")");
    assertThat(sut).isInstanceOf(OrExpression.class);
    OrExpression notExpression = (OrExpression) sut;
    checkZeroEqualsZero(notExpression.getLeftOperand());
    checkZeroEqualsZero(notExpression.getRightOperand());
  }

  @Test
  public void parenthesizedLogicalExpression() throws IOException, Parser.Exception {
    LogicalExpression sut = parseWithRoundTripLogicalExpression("(" + TRUE_EXPRESSION + ")");
    assertThat(sut).isInstanceOf(ParenthesizedLogicalExpression.class);
    ParenthesizedLogicalExpression parenthesizedLogicalExpression = (ParenthesizedLogicalExpression) sut;
    checkZeroEqualsZero(parenthesizedLogicalExpression.getOperand());
  }

  private NumberExpression parseWithRoundTripNumberExpression(String numberExpression) throws IOException, Parser.Exception {
    return parseWithRoundTripNumberExpression(numberExpression, null);
  }

  private NumberExpression parseWithRoundTripNumberExpression(String numberExpression, Root root) throws IOException, Parser.Exception {
    NumberExpression sut = ParserUtils.parseNumberExpression(numberExpression, root);
    String first = sut.prettyPrint();
    NumberExpression reParsed = ParserUtils.parseNumberExpression(first, root);
    String second = reParsed.prettyPrint();
    assertEquals(second, first);
    return sut;
  }

  private LogicalExpression parseWithRoundTripLogicalExpression(String logicalExpression) throws IOException, Parser.Exception {
    LogicalExpression sut = ParserUtils.parseLogicalExpression(logicalExpression);
    String first = sut.prettyPrint();
    LogicalExpression reParsed = ParserUtils.parseLogicalExpression(first);
    String second = reParsed.prettyPrint();
    assertEquals(second, first);
    return sut;
  }

  private void checkZeroEqualsZero(LogicalExpression logicalExpression) {
    assertThat(logicalExpression).isInstanceOf(ComparingExpression.class);
    ComparingExpression comparingExpression = (ComparingExpression) logicalExpression;
    assertThat(comparingExpression.getLeftOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression leftOfSSub = (NumberLiteralExpression) comparingExpression.getLeftOperand();
    assertEquals(0.0, leftOfSSub.getValue());
    assertThat(comparingExpression.getRightOperand()).isInstanceOf(NumberLiteralExpression.class);
    NumberLiteralExpression rightOfSub = (NumberLiteralExpression) comparingExpression.getRightOperand();
    assertEquals(0.0, rightOfSub.getValue());
  }
}
