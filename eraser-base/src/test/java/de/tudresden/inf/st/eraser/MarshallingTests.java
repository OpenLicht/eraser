package de.tudresden.inf.st.eraser;

import de.tudresden.inf.st.eraser.jastadd.parser.EraserParser;
import de.tudresden.inf.st.eraser.jastadd_test.core.TestConfiguration;
import de.tudresden.inf.st.eraser.jastadd_test.core.TestProperties;
import de.tudresden.inf.st.eraser.jastadd_test.core.TestRunner;
import de.tudresden.inf.st.eraser.jastadd_test.core.Util;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

/**
 * Testing round-trip parsing-printing-comparing.
 *
 * @author rschoene - Initial contribution
 */
public class MarshallingTests {

  /**
   * Run the JastAdd test
   * @param unitTest The test to run.
   */
  @ParameterizedTest
  @MethodSource("getTests")
  public void runTest(TestConfiguration unitTest) throws Exception {
    EraserParser.setCheckUnusedElements(false);
    ParserUtils.setVerboseLoading(false);
    TestRunner.runTest(unitTest);
  }

  public static List<TestConfiguration> getTests() {
    TestProperties properties = new TestProperties();
    properties.put("jastadd3", "false");
    properties.put("options", "indent=tab");
    properties.setTestRoot("src/test/resources/tests");
//    properties.exclude(Tests.FAILING);
//    properties.exclude(Tests.UNSTABLE);
    return Util.getTests(properties);
  }

}
