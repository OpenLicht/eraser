eraserVersionFileName = '../eraser.starter/src/main/resources/eraser.properties'


def get_version():
    with open(eraserVersionFileName) as eraserVersionFile:
        versionFileContent = eraserVersionFile.read()
    return versionFileContent[versionFileContent.rindex('version=') + 8:].strip()


def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    """
    env.conf['site_name'] = 'Documentation of Eraser ' + get_version()

    @env.macro
    def eraser_version():
        return get_version()


if __name__ == '__main__':
    print(get_version())
