# Learner

_This document is only available in German_

Der Learner ist ein Interface zur Encog Bibliothek. Er dient nur dazu Modelle zu traineren und zu Laufzeit des Systems anzupassen. Im Folgenden zeigen wir Beispiele wie der Learner zu Verwenden ist.

## Training mit externen Datensatz

```java
learner.setCsvFolderPath("/csvs");

ArrayList<Integer> targetColumns = new ArrayList<>();
targetColumns.add(5);
targetColumns.add(7);

int modelID = 1;

learner.loadDataSet("test1", targetColumns, modelID);

ArrayList<Integer> inputMaxes = new ArrayList<>();
inputMaxes.add(100); // e.g., first Columns is light intensity sensor with max value of 100
// ...

ArrayList<Integer> inputMins = new ArrayList<>();
inputMins.add(0); // e.g., first Columns is light intensity sensor with min value of 0
// ...

ArrayList<Integer> inputMaxes = new ArrayList<>();
targetMaxes.add(255); // e.g., first output value is R value of lamp with max value of 255
// ...

ArrayList<Integer> inputMins = new ArrayList<>();
targetMins.add(0); // e.g., first output value is R value of lamp with min value of 0
// ...

int inputCount = 5; 
int outputCount = 2;
int hiddenCount = 1;
int hiddenNeuronCount = 4;

learner.train(inputCount, outputCount, hiddenCount, hiddenNeuronCount, imodelID, inputMaxes, inputMins,  targetMaxes, targetMins);

Model model = learner.getTrainedModel(modelID);
```

## Training mit Daten von interner Datenbank

```java
ArrayList<Integer> targetColumns = new ArrayList<>();
targetColumns.add(5);
targetColumns.add(7);

int modelID = 1;

ArrayList<Integer> inputMaxes = new ArrayList<>();
inputMaxes.add(100); // e.g., first Columns is light intensity sensor with max value of 100
// ...

ArrayList<Integer> inputMins = new ArrayList<>();
inputMins.add(0); // e.g., first Columns is light intensity sensor with min value of 0
// ...

ArrayList<Integer> inputMaxes = new ArrayList<>();
targetMaxes.add(255); // e.g., first output value is R value of lamp with max value of 255
// ...

ArrayList<Integer> inputMins = new ArrayList<>();
targetMins.add(0); // e.g., first output value is R value of lamp with min value of 0
...

int inputCount = 5; 
int outputCount = 2;
int hiddenCount = 1;
int hiddenNeuronCount = 4;

Table table = database.getData("query"); // Database Klasse, Methode und Table existieren nicht, nur ein Beispiel

double [][] data = table.toArray(); // Methode und Table existieren nicht, nur ein Beispiel

learner.train(data, inputCount, outputCount, hiddenCount, hiddenNeuronCount, imodelID, inputMaxes, inputMins,  targetMaxes, targetMins, targetColumns);

Model model = learner.getTrainedModel(modelID);
```

## Retraining

```java
ArrayList<Integer> targetColumns = new ArrayList<>();
targetColumns.add(5);
targetColumns.add(7);

int modelID = 1;

data[][] dataVector = database.getNewestVector(); // Database Klasse und Methode existieren nicht nur ein Beispiel

learner.reTrain(dataVector, targetColumns, modelID);
```

## Inference

Bei der Nutzung der von Machine Learning muss in vielen Fällen die Einagbe und Ausgaben normalisiert bzw. denormalisiert werden.
Die Normalisierer können vom Learner angefragt werden mit getNormalizerInput und getNormalizerTar angefragt werden.
