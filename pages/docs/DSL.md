# The eraser-DSL

Find a general description in the [Model description](Model-description) page, and the parser ([dev](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/eraser-base/src/main/jastadd/eraser.parser), [main](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/main/eraser-base/src/main/jastadd/eraser.parser)) and printing ([dev](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/eraser-base/src/main/jastadd/Printing.jrag), [main](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/main/eraser-base/src/main/jastadd/Printing.jrag)).

The DSL is line-based, so on each line, one model element will be defined.
All shown specifications for one element are optional and can be reordered, but at least `id` must be present to identify the element.
References to other elements are done by using their IDs as strings, and can be forward references.
The following elements can be described (in the main branch if not otherwise noted):

## ChannelType

```
ChannelType: id="" label="" description="" itemType="" category="" readyOnly;
```

- The item type has to be one of the [item names defined for Eclipse Smart Home](https://www.eclipse.org/smarthome/documentation/concepts/items.html)
- The category can either be one of the [categories defined for Eclipse Smart Home](https://www.eclipse.org/smarthome/documentation/concepts/categories.html#channel-categories), or a custom one.
- Both item type and category are defined as enums ([dev](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/eraser-base/src/main/jastadd/enums.jadd), [main](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/main/eraser-base/src/main/jastadd/enums.jadd))
- The flag `readOnly` is currently without any effect

## Channel

```
Channel: id="" type="" links=["ITEM_ID", "ITEM_ID"];
```

- `type` must reference a [ChannelType](#channeltype), and must be set
- `links` must reference [items](#item)

## Group

```
Group: id="" groups=["GROUP_ID", "GROUP_ID"] items=["ITEM_ID", "ITEM_ID"] aggregation=AGG;
```

- `groups` must reference other [groups](#group)
- `items` must reference [items](#item)
- `AGG` in `aggregation` can either be one of `EQUALITY`, `AVG`, `MAX`, `MIN`, `SUM`; or one of `AND`, `OR`, `NAND`, `NOR`, followed by a list of arguments surrounded by round brackets, e.g., `aggregation = AVG` or `aggregation = AND ("off", "on")`. The semantics are described in the [openHAB docs](https://www.openhab.org/docs/configuration/items.html#group-type-and-state)

## Item

```
ITEM_TYPE Item: id="" label="" state="" category="" topic="" controls=["ITEM_ID", "ITEM_ID"] metaData={"key":"value"};
```

- `ITEM_TYPE` can be one of the following: `Color`, `Contact`, `DateTime`, `Dimmer`, `Image`, `Location`, `Number`, `Player`, `RollerShutter`, `String`, `Switch`. If left out, the item will behave as if `String` was used.
- `state` is given as a string and will be interpreted depending on the type of the item
- `topic` implicitely defines a [MQTT](mqtt) topic which is used to update the item in both directions. The full topic is prefixed with `incoming`-prefix if receiving updates from openHAB, and `outgoing`-prefix if sending updates to openHAB
- `category` is currently not used
- `controls` is only available on `dev`. It defines connection of one item controls the state of another item as the state of the first item changes.
- `metaData` contains key-value-pairs comprising its meta data

## Parameter

```
Parameter: id="" label="" description="" type="" default="" required;
```

- `type` has to be one of the following: `Boolean`, `Decimal`, `Integer`, `Text`
- `default` is currently an arbitrary String, thus it is not checked whether it matches the type

## ThingType

```
ThingType: id="" label="" description="" parameters=["PARAM_ID", "PARAM_ID"] channelTypes=["CHANNEL_TYPE_ID", "CHANNEL_TYPE_ID"];
```

- `parameters` must reference [parameters](#parameter)
- `channelTypes` must reference [channel types](#channeltype)

## Thing

```
Thing: id="" label="" type="" channels=["CHANNEL_ID", "CHANNEL_ID"] ;
```

- `type` must reference a [thing type](#thingtype), and must be set
- `channels` must reference [channels](#channel)

## Mqtt

```
Mqtt: incoming="" outgoing="" host="";
```

- MQTT is used to communicate/synchronize with openHAB
- `incoming` defines the prefix for received MQTT messages, see [topic description in item](#item). Defaults to an empty string, which should be avoided.
- `outgoing` defines the prefix for sent MQTT messages, see [topic description in item](#item). Defaults to an empty string, which should be avoided.
- `host` defines the URL of the host running an MQTT broker. The port will be set to `1883` and cannot be changed.

## Influx

```
Influx: user="" password="" dbName="" host="" ;
```

- Influx is used to store historical item states
- `user` and `password` define connection credentials
- `dbName` defines the name of the database to use
- `host` defines the URL of the host running an InfluxDB. The port will be set to `8086` and cannot be changed.
