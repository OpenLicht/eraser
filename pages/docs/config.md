# Configuration

This page describes the main configuration options when starter eraser.
Whenever a file is specified here, it will be a combination of `file` and `external`. With `external` set to `true`, the given path is used to access the file system, using the directory of the `eraser.starter` module as current directory for relative paths. If `external` is set to `false` the file is looked up in the bundled JAR file.

## Starting configuration `starter-settings.yaml`

In this file, everything is being configured, i.e., which components will be started, and how they are set up.
It has global options (mostly boolean) and sections for components, both declared on the top-level.

### Global options

|       Name      |   Type  |                            Description                             | Default |
|-----------------|---------|--------------------------------------------------------------------|---------|
| `useMAPE`       | Boolean | Start the feedback loop                                            | True    |
| `sharedLearner` | Boolean | Use the same learner instance for activity and preference learning | False   |
| `mqttUpdate`    | Boolean | Get updates from openHAB into the knowledge base                   | True    |
| `initModelWith` | Choice  | Method to initialize model. Possible values: "load", "openhab"     | "load"  |

### Section `rest`

This section configures the REST server providing an API to interact with eraser.

|         Name        |   Type  |                                       Description                                        | Default |
|---------------------|---------|------------------------------------------------------------------------------------------|---------|
| `use`               | Boolean | Start the REST server                                                                    | True    |
| `port`              | Integer | Port of the REST server                                                                  | 4567    |
| `createDummyMLData` | Boolean | Add some dummy data for activities and events. Only effective when using the REST server | False   |

### Section `load`

This section defines how to initialize eraser when using the [option `initModelWith`](#global-options) with the value "load".

|    Name    |   Type   |                            Description                            | Default |
|------------|----------|-------------------------------------------------------------------|---------|
| `file`     | Filename | File to read in, expected format: `.eraser`                       | _None_  |
| `external` | Boolean  | False: Use file bundled in the JAR. True: Use file in filesystem. | False   |

### Sections `activity` and `preference`

These section have equivalent options and define the activity recogntion taking place in the Analyze phase, and the preference learning triggered in the Plan phase respectively.

|    Name    |   Type   |                            Description                             | Default |
|------------|----------|--------------------------------------------------------------------|---------|
| `file`     | Filename | File to read in, expected format: `.eraser`                        | _None_  |
| `external` | Boolean  | False: Use file bundled in the JAR. True: Use file in filesystem.  | False   |
| `dummy`    | Boolean  | Use dummy model in which the current activity is directly editable | false   |
| `id`       | Integer  | Model id. Currently unused                                         | 1       |


### Section `openhab`

This section configures the communication with openHAB.

|         Name        |  Type  |                         Description                          | Default |
|---------------------|--------|--------------------------------------------------------------|---------|
| `url`               | String | The URL from which to import and at which openHAB is running | _None_  |
| `metadataNamespace` | String | The metadata namespace used for items                        | _None_  |

## Configuring start of eraser

To build eraser, execute

```bash
./gradlew :eraser.starter:installDist
```

This will create bundled JARs and a script to launch the starter. Both will be located in `eraser.starter/build/install/eraser.starter/`.
To start, change to this directory and invoke

```bash
./bin/eraser.starter -f starter-setting.yaml
```

Beforehand, the configuration has to be changed according to your needs.
