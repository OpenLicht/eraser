# OpenHAB Bindings

## openHAB 2.0-2.3 and MQTT 1.x

In the old openHAB versions, there used to be the MQTT event bus, which publishes every change of the state of an item to a channel dedicated to this item, and receives updates for the state analogously.

The configuration file needed is located in `/etc/openhab2/services/mqtt-eventbus.cfg` with the following content:

```bash
broker=mosquitto_local
statePublishTopic=oh2/out/${item}/state
commandSubscribeTopic=oh2/in/${item}/state
```

If you have an item named `iris1_item`, then every change (e.g., using the PaperUI) will be published to `oh2/out/iris1_item/state`.
And whenever a message is published to `oh2/in/iris1_item/state`, its payload will be used to update the state of the item `iris1_item`.

## openHAB 2.4+ and MQTT 2.x

In the more recent versions of openHAB, the old MQTT binding was ported to the new architecture.
However, the event bus feature [was dropped in favour of simple rules](https://community.openhab.org/t/roadmap-mqtt-binding/70657/12).

Now the following setup is required, involving one new channel, one new group and two new rules.

### A new MQTT channel to receive updates

To enable a trigger for all new MQTT messages, a new channel has to be created in the Thing of the respective broker.
Navigate to the Thing of the broker in the PaperUI, and click the "+" near "Channels" to create a new one using the following settings:

- Channel type: "Publish trigger" (will change the editable fields)
- Channel Id: Choose any appropriate name and stick to it. In the following, `receiveEverythingChannel` will be used.
- Label: Pick a description, like "Receive everything".
- MQTT Topic: This is the topic to subscribe to, and has to match the `outgoing` value in the MQTT settings of Eraser. The default in most examples is to use `oh2/in/#`.
- Payload condition: Is left empty.
- Separator character: The character to separate the last topic and the payload. In the following, "#" is used (not to be confused with the `#` used for the topic above, which matches any topic with the given prefix)

### A new group for relevant items

To enable a trigger to publish new states, a new group has to be created with all relevant items as children.
Navigate to Configuration > Items in the PaperUI. If this is not visible, disable the "Simple Mode" found at Configuration > System > Item Linking. This enables the manual editing of items and their links to channels.

Create a new group with the following settings:

- Name: Choose any appropriate name and stick to it. In the following, `PublishAllToMqttGroup` will be used.
- Label: Pick a description, like "Items whose state will be published to MQTT".
- Category: Is left empty.
- Type: `Group`
- Base type: `None` (Other types would enable an aggregation function, which is not needed here)

### A rule to process received updates

Add the following rule to your rule file in `./etc/openhab2/rules/`:

```java
rule "Receive all"
when
	Channel "mqtt:systemBroker:embedded-mqtt-broker:receiveEverythingChannel" triggered
then
	// The receivedEvent String contains unneeded elements like the mqtt topic, we only need everything after the "/" as this is were item name an
	// logInfo("one.rules", "receivedEvent: {}", receivedEvent.toString())
	val payload = receivedEvent.toString.split(" ", 3).get(2)
	val parts = payload.split("/")
	// logInfo("one.rules", "parts: {}", parts)
	val lastTopicPartAndRealPayload = parts.get(parts.length - 1)
	// logInfo("one.rules", "lastTopicPartAndRealPayload: {}", lastTopicPartAndRealPayload)
	val topicAndPayloadTokens = lastTopicPartAndRealPayload.split("#", 2)
	// logInfo("one.rules", "topicAndPayloadTokens: {}", topicAndPayloadTokens)
	sendCommand(topicAndPayloadTokens.get(0), topicAndPayloadTokens.get(1));
end
```

Note the matching channel id for an system broker with the id `embedded-mqtt-broker`. If you use a normal broker with the id `myMQTTBroker`, it would be `mqtt:broker:myMQTTBroker:receiveEverythingChannel` instead.
The rule processes the received information, which is a String containing the topic and payload, separated by the formerly chosen Separator character `#`.
Lastly, it updates the item matching the last part of the topic with the payload.

### A rule to publish updates

```java
rule "Publish all"
when
	Member of PublishAllToMqttGroup changed
then
	val actions = getActions("mqtt","mqtt:systemBroker:embedded-mqtt-broker")
	actions.publishMQTT("oh2/out/" + triggeringItem.name, triggeringItem.state.toString);
end
```

Note the matching group name `PublishAllToMqttGroup`, the matching name of the system broker `embedded-mqtt-broker` (see [above](#a-rule-to-process-received-updates) for normal broker), and the correct topic to publish to.
The topic has to match the `incoming` value in the MQTT settings of Eraser. The default in most examples is to use `oh2/out/`.
