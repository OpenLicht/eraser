# Machine Learning Basics

The relevant parts of the grammar ([dev](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/eraser-base/src/main/jastadd/main.relast) | [main](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/eraser-base/src/main/jastadd/main.relast)) are the following (updated on Feburary, 11th 2019):

## General information machine learning

```
abstract MachineLearningModel ::= <OutputApplication:DoubleDoubleFunction> ;
abstract ItemPreference ::= ;
ItemPreferenceColor : ItemPreference ::= <PreferredHSB:TupleHSB> ;
ItemPreferenceDouble : ItemPreference ::= <PreferredValue:double> ;
rel ItemPreference.Item -> Item ;

// neural network
NeuralNetworkRoot : MachineLearningModel ::= InputNeuron* HiddenNeuron* OutputLayer ;
OutputLayer ::= OutputNeuron* <Combinator:DoubleArrayDoubleFunction> ;
abstract Neuron ::= Output:NeuronConnection* ;
NeuronConnection ::= <Weight:double> ;
InputNeuron : Neuron ;
HiddenNeuron : Neuron ::= <ActivationFormula:DoubleArrayDoubleFunction> ;
OutputNeuron : HiddenNeuron ::= <Label:String> ;
rel NeuronConnection.Neuron <-> Neuron.Input* ;
rel InputNeuron.Item -> Item ;
rel OutputLayer.AffectedItem -> Item ;

// decision tree
DecisionTreeRoot : MachineLearningModel ::= RootRule:DecisionTreeRule ;
abstract DecisionTreeElement ::= Preference:ItemPreference*;
abstract DecisionTreeRule : DecisionTreeElement ::= Left:DecisionTreeElement Right:DecisionTreeElement <Label:String> ;
ItemStateCheckRule : DecisionTreeRule ::= ItemStateCheck ;
abstract ItemStateCheck ::= <Comparator:ComparatorType> ;
ItemStateNumberCheck : ItemStateCheck ::= <Value:double> ;
ItemStateStringCheck : ItemStateCheck ::= <Value:String> ;
DecisionTreeLeaf : DecisionTreeElement ::= <ActivityIdentifier:int> <Label:String> ;

// dummy model
DummyMachineLearningModel : MachineLearningModel ::= <Current:DecisionTreeLeaf> ;
```

A machine learning model implements a classification task, thus it has an attribute `classify` returning a general class `Leaf` whose type depends to the specific model.
This leaf represents the result of a classification, and comprises a double value and a set of `ItemPreference`s.
Such a double value can be scaled using the `OutputApplication`, which is a function mapping one double value to another.
If the neural network only outputs values from 0 to 3, but a range from 0 to 100 is needed, the OutputApplication function can multiply the result with 33.

## Neural networks

A neural network is, generally speaken, a set of neurons connected through weighted connections.
Those neurons are typically put into layers such that connections of neurons are only possible between neurons in adjecent layers.
Currently in `eraser`, only the output layer is explicitely modelled.

Neurons are either Input-, Hidden-, or OutputNeurons.
InputNeurons typically form the first (implicit) layer and are connected to Items, thus get their state from the state of the connected Item.
HiddenNeurons are between input and output, and have an individual `ActivationFormula` taking all input values from the neuron and return its state.
OutputNeurons also have an activation formula, and are children of the `OutputLayer`.
In this output layer, a `Combinator` is defined, which merges the values of all output neurons into one double value as result of the whole neural network.

### Construction of a neural network

Most of the code snippets in this section are taken from [NeuralNetworkTest in the `dev` branch](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/eraser-base/src/test/java/de/tudresden/inf/st/eraser/NeuralNetworkTest.java).
To construct a neural network, first the root element needs to be created using `createEmpty`, which sets the output application function to the identity function:

```
NeuralNetworkRoot neuralNetworkRoot = NeuralNetworkRoot.createEmpty()
```

Then all neurons are created using the matching type, e.g.:

```java
InputNeuron inputNeuron = new InputNeuron();
HiddenNeuron hiddenNeuron = new HiddenNeuron();
hiddenNeuron.setActivationFormula(inputs -> 4 * inputs[0]);
OutputNeuron outputNeuron1 = new OutputNeuron();
outputNeuron1.setLabel("first");
outputNeuron1.setActivationFormula(inputs -> inputs[0] > 4 ? 1d : 0d);
```

Afterwards, the connections are set using the convenience method `connectTo` taking the target neuron and a weight:

```java
inputNeuron.connectTo(hiddenNeuron, 1);
hiddenNeuron.connectTo(outputNeuron1, 0.4);
```

As an important next step, the output layer and all neurons are added to the model:

```java
neuralNetworkRoot.addInputNeuron(inputNeuron);
neuralNetworkRoot.addHiddenNeuron(hiddenNeuron);
OutputLayer outputLayer = new OutputLayer();
outputLayer.addOutputNeuron(outputNeuron1);
neuralNetworkRoot.setOutputLayer(outputLayer);
```

Connect the items to the neural network.
This will assign the first item in the list to the first input neuron, the second to the second, and so on.

```java
Item item;
neuralNetworkRoot.connectItems(Arrays.asList(item));
```

The combinator function is set using a lambda expression:

```java
outputLayer.setCombinator(outputs -> {
  int n = 0;
  for (double d : outputs) {
    n = (n << 1) | (d == 0 ? 0 : 1);
  }
  return (double) n;
});
```

Finally, the affected item needs to be set in the output layer:

```java
Item affectedItem;
outputLayer.setAffectedItem(affectedItem);
```

To verify, that the network was correctly built, the `check` method can be used.
It will check the various formulas (output, combinator, activation), valid connections, set weights and connected items.
The return value is `true` if no errors were found, and `false` otherwise.
In case of any warnings or errors, they are printed out using the logger.

```java
boolean everythingValid = neuralNetworkRoot.check();
```

### Using a neural network

Once a model has been built, it can be used either to recognize activities, or to learn preferences:

```java
Root model;
// either recognize activities, ...
model.getMachineLearningRoot().setActivityRecognition(neuralNetworkRoot);
// ... or learn preferences
model.getMachineLearningRoot().setPreferenceLearning(neuralNetworkRoot);
```

Once add the complete model, the classification can be invoked:

```java
Leaf leaf = neuralNetworkRoot.classify();
```

Depending on the use case, the leaf can either be interpreted as an activity, or as an item preference.
For an activity, the classification result is used as an acitivty identifier:

```java
int identifier = leaf.getActivityIdentifier();
Optional<Activity> activity = model.resolveActivity(identifier);
```

Alternatively, the result can be a preference as written in the following.
In case of a neural network, currently only one item will be affected (the one set as AffectedItem in the output layer).

```java
List<ItemPreference> preferences = leaf.computePreferences();
for (ItemPreference p : preferences) {
	p.apply();
}
```

## Decision Trees

A decision tree is a tree of decisions.
Every decision rule refers to an item and compares the state of this item to a given constant.
The leaf of the tree don't contain any decisions anymore, instead they capture the classification result.

### Construction of a decision tree

Like above, most of the code was taken from the according test, in this case the [DecisionTreeTest](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/eraser-base/src/test/java/de/tudresden/inf/st/eraser/DecisionTreeTest.java).

To begin with, construct a new `DecisionTreeRoot`.

```java
DecisionTreeRoot dtroot = new DecisionTreeRoot();
```

Next, create your first decision.
In this case, the rule will decided whether the state of the referenced item is below or above 4.
Instead of the leafs, other rules could be inserted referencing the same or another Item.

```java
DecisionTreeLeaf isLessThanFour = new DecisionTreeLeaf();
DecisionTreeLeaf isFourOrGreater = new DecisionTreeLeaf();
ItemStateNumberCheck check = new ItemStateNumberCheck(ComparatorType.LessThan, 4f);
JastAddList<ItemPreference> preferences = new JastAddList();
dtroot.setRootRule(new Rule(preferences, isLessThanFour, isFourOrGreater, "check item1", check));
```

For every leaf, its label, the resulting activity identifier and item preferences have to be set.

```java
Item affectedItem;
isLessThanFour.setLabel("less than four");
isLessThanFour.setActivityIdentifier(1);
isLessThanFour.addItemPreference(new ItemPreferenceDouble(2, affectedItem));
isFourOrGreater.setLabel("four or greater");
isFourOrGreater.setActivityIdentifier(3);
isFourOrGreater.addItemPreference(new ItemPreferenceColor(new TupleHSB(1, 2, 3), affectedItem));
```

The automatic connection of items to all elements of a decision tree is currently not supported.
Instead the items can only be set manually.

```java
Item item;
try {
	DecisionTreeRoot.connectItems(Arrays.asList(item));
} catch(UnsupportedOperationException e) {
	check.setItem(item);
}
```

### Using a decision tree

The result of the classification is the leaf at the end of the "decision path".
If the item has for example a state of 3, the leaf `isLessThanFour` will be returned.

```java
Leaf leaf = dtroot.classify();
```

A `DecisionTreeLeaf` directly has an activity identifier and item preferences attached to it, thus can be used in the same way as described [above](#using-a-neural-network).
