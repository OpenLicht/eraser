# Working with the model

The root is mostly the entry point to all needed information, and can be obtained from any element in the model using `getRoot()`.
From there, one has mostly "descend" into the relevant part of the model, e.g., if information is needed about items, then `getOpenHAB2Model()` yields this part of the model.

There are various attributes to resolve model entities by their ID, e.g., `resolveThing(String thingName)`, `resolveItem(String itemName)`. Those return an `java.util.Optional` with the respective type, e.g., `Optional<Item>` to represent non-matched IDs.

## OpenHAB specifics

As described in the [description of the model](Model-description), item type is reified, so all items with a double state are of type `ItemWithDoubleState` and only this type has a `getState` returning its state (as a double value).
There is a convenience way to get the state as a double, or String value using `getStateAsDouble` and `getStateAsString` defined on `Item` and implement in a meaningful way by the different subclasses.
Analogously, state can be set from different type, e.g., from a String, or a boolean value.

Synchronisation between eraser and openHAB itself currently includes only states of items. Therefore, any structural changes (e.g., addition or removal of an item) or change of other attributes such as description of items or things, is **not tracked nor supported**.

## Documentation

When working and especially when extending eraser, it is highly recommended to use the [API documentation](ragdoc/index.html) as a reference manual.
