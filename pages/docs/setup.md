# Setup and Building eraser

This project uses Gradle as the build tool. Please use the provided Gradle wrappers instead of an installed one.

- Clone the repository, either using https (`https://git-st.inf.tu-dresden.de/OpenLicht/eraser.git`) or ssh (`git@git-st.inf.tu-dresden.de:OpenLicht/eraser.git`)
- (Optional) Build all subprojects: `./gradlew build`

## Troubleshooting

### Lombok

Some of the modules use [Lombok](https://projectlombok.org/). It has to be set up according to your platform, e.g., for IntelliJ the [plugin has to be installed](https://projectlombok.org/setup/intellij).
Afterwards, the annotation processor has to be enabled for the project (which sould be warned about by IntelliJ).

### Java Version

All submodules use Java `1.8`. Please ensure using an appropriate compiler.
