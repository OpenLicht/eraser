# Documentation of Eraser

First, setup eraser following the README in the main repository: [dev](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/dev/README.md) | [main](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/blob/main/README.md)
To configure the system, read the [configuration page](config).

For an overview of the model, check out the [Model description](Model-description).
If you need more details, dive into the [inner workings](Inner-workings) (now with colorful images!), [insights for working with the model](working) the [description of machine learning](MachineLearning) (with code examples) or the [description of Learner implementation](Learner) (only in German).
To be able to read in models, a [dedicated DSL](DSL) must be used.

Further, the updated [configuration settings regarding MQTT 1.x and 2.x](mqtt) as well as the [Setup guide to connect to openHAB](openhab-binding) (only in German) are found in this wiki.
For contributing, please see [contribution guideline](contributing).
