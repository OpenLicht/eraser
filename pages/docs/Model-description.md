# Model description

The latest version of the model: [main](https://git-st.inf.tu-dresden.de/rschoene/eraser/blob/main/eraser-base/src/main/jastadd/main.relast) | [dev](https://git-st.inf.tu-dresden.de/rschoene/eraser/blob/dev/eraser-base/src/main/jastadd/main.relast)

Below, all parts of the model are described in more detail. Each part resides in a separate grammar file.

## Main

`Root ::= SmartHomeEntityModel User* MqttRoot InfluxRoot MachineLearningRoot Rule* Location* ;`

- `SmartHomeEntityModel`: Smart home entities
- `User*`: Users in the system (work in progress)
- `MqttRoot`: Everything needed for MQTT (communication with openHAB)
- `InfluxRoot`: Everything needed for Influx (time series database for item state history)
- `MachineLearningModelRoot`: The learned models for activity recognition and preference resolving
- `Rule*`: Self-made ECA rules
- `Location*`: Locations associated with users, activities, preferences (work in progress)

## Smart Home Entity Model

`SmartHomeEntityModel ::= Thing* Group* ThingType* Parameter* ChannelType* Channel* ItemCategory* /ActivityItem:Item/ FrequencySetting* ;`

The structure is mostly extracted from openHAB. There are some abstract superclasses to share common token, such as `ID`, `label` or `description`. Some differences to openHAB include:

- Links are not modelled explicitly, but instead the items are directly referenced within channels
- Item type was reified instead of a token describing the type. This has the advantage, that the state can be used in a more type-safe way. On the downside, more code is required in some instances, such as for item history, and there is no easy `getState` method.

Explanations for the other types:

- **Things** represent physical entities, such as a Hue lamp, a complex (or simple) sensor, a monitor, a webservice. They are typed and described by ThingTypes.
- **ThingTypes** describe the blueprint for a Thing, like a class does for objects in OO languages. They also have **ChannelTypes** describing possible capabilities.
- **Items** are the capabilities of the things, e.g., the color temperature of a lamp, the measured temperature value of a sensor, a method of a webservice (the last one is probably a bad design). They have a type as described above.
- Things have **Channels** (one for each ChannelType of their ThingType) linking their effective capabilities to items via **Links**, such that those items represent the current value of this capability. A channel has a **ChannelCategory** describing the capability, e.g., `BatteryLevel`, `Switch`, `Presence`
- **Groups** contain other groups, and other items

## MQTT

- Topics are a flat list, referencing items to be updated upon a message in this topic
- All topics have one common prefix to subscribe to topics, and another common prefix to publish topics

## Influx

Basically, here are just some tokens containing information for connection to Influx, such as username, password, host

## Machine Learning

Handling of machine learning models currently undergoes a change.

There is a distinction between internal and external models, both implement an encoder interface to get data into those models, and decoder interface to extract data from the models.
There are two internal models which are supported: decision trees and neural networks.

Decision tree are modelled straight-forward, there are inner nodes and leaves. Inner nodes reference an item and check a certain condition for this item to decide whether to follow the left or the right path of the inner node. The classification is equal to the leaf node at the end of the path.

Neural networks are also straight-forward, but probably inefficient for bigger networks in the current implementation. Every neuron is a non-terminal which has ingoing and outgoing connections (using bidirectional relations), input neurons reference an item and propagate its state, hidden and output neurons have an activation function, bias neurons always return a constant value. The output layer contains all output neurons and has a combinator function to calculate the classification result.

## Rules

Simple ECA rules can be modelled. A rule has a list of conditions and a list of actions. An event can be triggered by some items (via their ItemObserver), i.e., whenever an item changes its state to a different value. Conditions use a logical expression, which can contain simple mathematical expressions, comparisons, and simple logical operations. Actions are organized currently by their purpose, but are wll be simplified to reference only affected items and a number expression to calculate the new value for those items.

### StateSyncGroup

#### What does it do?
Item states can be synchronized through StateSyncGroups. Once an item belongs to such a group, all of its state changes will be applied and then sent to other items in that group.
The behavior of the other items in the group following the order to change the state is up to the item type. This is how state transfers between different item types can be managed. For example if a ColorItem (currently at `1%` brightness) and a SwitchItem (currently at value `false`) are in a StateSyncGroup and the SwitchItem is changing to `true` then the ColorItem won't change to 100%. Instead, it keeps its old value (`1%`).

#### How does it work?
The StateSyncGroup as an entity does only exist while parsing. After resolving all references, it is rewritten (using JastAdd rewrites). As a result, an ECA rule emerges that has conditions and actions whose functionality correspond to the explained behavior in the previous paragraph.
