# Contributing

Please also refer to the [API documentation](ragdoc/index.html)

Steps to create the initial multi-project setup from a single-project setup

- create a new Gradle project named `eraser` using IntelliJ
- manually create subdirectory `eraser-base`, move `src/` to this directory
- change `build.gradle` to

```Groovy
allprojects  {
	group = 'de.tudresden.inf.st'
	version = '1.0.0-SNAPSHOT'
}

subprojects {
	apply plugin: 'java'
	sourceCompatibility = 1.8
	targetCompatibility = 1.8

	task packageSources(type: Jar) {
		classifier = 'sources'
		from sourceSets.main.allSource
	}

	artifacts.archives packageSources
	configurations {
		testArtifacts.extendsFrom testRuntime
	}

	task testJar(type: Jar) {
		classifier "test"
		from sourceSets.test.output
	}

	artifacts {
		testArtifacts testJar
	}
}
```
- create new `eraser-base/build.gradle` with the following content

```Groovy
repositories {
    mavenCentral()
}

apply plugin: 'jastadd'

dependencies {
    testCompile group: 'junit', name: 'junit', version: '4.12'
}

buildscript {
    repositories.mavenLocal()
    repositories.mavenCentral()
    dependencies {
        classpath 'org.jastadd:jastaddgradle:1.12.2'
    }
}

jastadd {
    configureModuleBuild()
    modules "jastadd_modules"

    module = "expressions"

    astPackage = 'de.tudresden.inf.st.eraser.jastadd.model'
    genDir = 'src/gen/java'

    buildInfoDir = 'src/gen-res'
}
```
- create new directory `eraser-base/src/main/jastadd` and put in JADD and JRAG files
- create new `eraser-base/jastadd_modules` with the following content:

```Groovy
module("expressions") {

	java {
		basedir "src/main/java/"
		include "**/*.java"
	}

	jastadd {
		basedir "src/main/jastadd/"
		include "**/*.ast"
		include "**/*.jadd"
		include "**/*.jrag"
	}
}
```
- optionally create a `.gitignore` to not commit `.idea` and `.gradle` directories
- optionally create a Main Java class
