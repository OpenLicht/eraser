# Inner workings

Please also refer to the [API documentation](ragdoc/index.html)

## openHAB synchronization via MQTT

MQTTRoot contains `incomingPrefix`, `outgoingPrefix` and `host` tokens.

Items have a MQTT-topic associated, both from which they get updates via MQTT and publish changes of their state via MQTT.

Eraser will subscribe to `incomingPrefix/#`, i.e., every update of topics starting with `incomingPrefix` is listened to.
Also, if the a new host is set (`MQTTRoot.Host`), the connection is re-established.
Listening means, that upon a new message, the topic string is resolved to a non-terminal MQTT-topic using `Root.resolveTopic`. This topic non-terminal has a reference to its item, which in turn has its state updated with the body of the MQTT message. Each different item type has its own way to set its state from a string (`Item.setStateFromString`).

Whenever a state of an item is set from within the KB, this change will be published via MQTT, if `shouldSendState` is set to true, or the default setting (`DefaultShouldSendState`) is set to true.
The topic will be prefix with `outgoingPrefix`. Every item type has its own way to print its state to a string (`Item.getStateAsString`), which is used as a MQTT message body.


## Item state history via InfluxDB

Connecting to Influx is done with the Java binding `org.influxdb:influxdb-java`. To represent the data to send and receive, `ItemPoints` are used defined in the `ItemHistoryPoints.jrag` aspect file.
There is one class for each item type capturing the specifics for translating item states to data points.

A pretty cool way to add new behavior when setting the item state is to use JastAdd's refinement:

```
refine MQTT public void Item.sendState() throws Exception {
  refined();
  getRoot().getInfluxRoot().influxAdapter().write(pointFromState());
}
```

This adds writing a new data point to influx after the former behavior (which includes sending the update via MQTT, as described above).
However, those two are intended to be independent of each other.

For retrieving the history, an asynchronous approach was chosen and is described in the following. The sequence diagram shows the important methods called, where ItemType denotes a specific subclass of Item, e.g., `ColorItem` or `ItemWithDoubleState`, and Item denotes the general, common superclass `Item`.

![item-history](img/item-history.png)

To get the history, `getHistory` is called on a specific item returning a list of datapoints of a matching type, e.g., a list of `DoubleStatePoint` for an `ItemWithDoubleState`.

The code in `getHistory` is actuall the same for every type, namely: `return proxied(this::_history);`
This calls `proxied` (shown in yellow above) which first asynchronously updates the history and returns the current value using `_history()` (shown in orange).
For updating the history, first `historyUpdating` is checked, and if appropriate, the current InfluxAdapter is used to invoke `asyncQuery` and use the return value to set a new `history_token`.
This token contains the history, and is returned by the `_history` attribute. If data was never fetched before, an empty list is returned.

## Rules

For events in rules to trigger, `ItemObserver` objects are used. The following figure shows three rules (A, B, C) triggered by two different items (`item1` und `item2`):

![rules-object](img/rules-object.png)

To get those wirings, the following code can be used:

```java
Root model = createModel();
NumberedItem item1 = createNumberedItem();
ColorItem item2 = createColorItem();
addToModel(model, item1);
addToModel(model, item2);

Rule rA = new Rule();
ItemStateNumberCheck check1 = new ItemStateNumberCheck(ComparatorType.GreaterOrEqualThan, 4);
ItemStateNumberCheck check2 = new ItemStateNumberCheck(ComparatorType.LessThan, 6);
rA.addCondition(new ItemStateCheckCondition(check1));
rA.addCondition(new ItemStateCheckCondition(check2));
rA.setAction(new LambdaAction(item -> System.out.println("A was triggered by " + item.getLabel())));
rA.setAction(new LambdaAction(item -> item.disableSendState()));
rA.activateFor(item1);

Rule rB = new Rule();
Rule rC = new Rule();
// .. conditions, actions of B and C
rB.activateFor(item1);
rB.activateFor(item2);
rC.activateFor(item1);
```

Changes to `item1` trigger all three rules, whereas changing `item2` only triggers the second rule B.
Triggering a rule works as follows:

![rules-sequence](img/rules-sequence.png)

When the state of `item1` is changed, its observer checks, whether the new change is different to the previous change. If that is the case, it triggers all associated rules, i.e, it calls `rule.trigger()` for every rule in the `TriggeredRuleList`.
The rule checks all conditions (if any), and if all hold, executes all actions (if any).

In this shown case, `item1` was changed, thus all rules `rA`, `rB`, and `rC` are triggered, if the state has changed. The full sequence send for the two latter events is not shown above for brevity.

### Condition Types

Currently only one condition is supported, which compares the state of an ItemWithDoubleState with a given constant. It borrows its functionality from `ItemStateCheck`, which has a comparator (`>=`, `>`, `=`, `!=`, `<`, `<=`) and a double constant.

### Action Types

| Name | Children | Description |
|------|----------|-------------|
| Action | _none_ | Abstract super class for all actions |
| LambdaAction | <ul><li>`<Lambda:Action2EditConsumer>` An arbitrary lambda consuming an `Item`</li></ul> | Does arbitrary stuff, use only if none of the classes below fit. |
| TriggerRuleAction | <ul><li>`rel Rule -> Rule` Relation to the rule to trigger</li></ul> | Triggers another rule passing the same item to it. |
| _SetStateAction_ (abstract) | <ul><li>`rel AffectedItem -> Item` The item to change the state of</li></ul> | Abstract class for changing the state of one affected item. |
| SetStateFromConstantStringAction | <ul><li>`rel AffectedItem -> Item` (from SetStateAction)</li><li>`<NewState:String>` The new state to set</li></ul> | Sets the given constant as the new state for the affected item |
| SetStateFromLambdaAction | <ul><li>`rel AffectedItem -> Item` (from SetStateAction)</li><li>`<NewStateProvider:NewStateProvider>` A lambda to get the new state</li></ul> | Sets the given variable as the new state for the affected item |
| SetStateFromTriggeringItemAction | <ul><li>`rel AffectedItem -> Item` (from SetStateAction)</li></ul> | Sets the state of the triggering item as the new state for the affected item |
| SetStateFromItemsAction | <ul><li>`rel AffectedItem -> Item` (from SetStateAction)</li><li>`<Combinator:ItemsToStringFunction>` A function taking a list of items and returning a string</li><li>`rel SourceItem* -> Item` A list of input items</li></ul> | Uses the combinator to combine all input items to a new state, which is set for the affected item |
| AddDoubleToStateAction | <ul><li>`rel AffectedItem -> Item` (from SetStateAction)</li><li>`<Increment:double>` A constant value to add</li></ul> | Increments the state of the affected item by the given constant increment |
| MultiplyDoubleToStateAction | <ul><li>`rel AffectedItem -> Item` (from SetStateAction)</li><li>`<Multiplier:double>` A constant value to multipy</li></ul> | Multiplies the state of the affected item by the given constant multiplier |
