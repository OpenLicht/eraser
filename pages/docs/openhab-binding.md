# Setup für Knowledge-Base

_This document is only available in German_

## Eraser-Seite

Grundlage zum Starten der Knowledge-Base ist die Datei `eraser.starter-1.0.0-SNAPSHOT.zip`, welche die eraser-Module, deren Abhängigkeiten sowie ein Start-Skript enthält.

Zusätzlich ist eine Konfigurationsdatei im YAML-Format notwendig. Die Standard-Konfiguration ist folgende (mit den Standard-Werten in Kommentaren):

```yaml
# Settings for Eraser.

# Start the feedback loop. Default: true.
useMAPE: true

rest:
  # Start the REST server. Default: true.
  use: true
  # Port of the REST server. Default: 4567.
  port: 4567
  # Add some dummy data for activities and events. Only effective when using the REST server. Default: false.
  createDummyMLData: false

# Initialize the knowledge base with a file.
# OpenHAB synchronization is done if MQTT url is set in the processed file
load:
  # File to read in. Expected format = eraser
#  # Option 1: Use built-in file
#  file: starter.eraser
#  external: false
  # Option 2: Use external file
  file: starter.eraser
  external: true

# Model for activity recognition. If dummy is true, then the file parameter is ignored.
activity:
  # File to read in. Expected format = eg
  file: activity.eg
  # Use dummy model in which the current activity is directly editable. Default: false.
  dummy: true
  # Model id. Default: 1.
  id: 1

# Model for preference learning. If dummy is true, then the file parameter is ignored.
preference:
  # File to read in. Expected format = eg
  file: preference.eg
  # Use dummy model in which the current activity is directly editable. Default: false.
  dummy: true
  # Model id. Default: 1.
  id: 1
  # Items to connect to inputs
  items:
    - datetime_month
    - datetime_day
    - datetime_hour
    - datetime_minute
    - bias
    - activity
  # Item to change with classification result
  affectedItem: iris1_item

# Initialize the knowledge base by importing data from openHAB.
openhab:
  # The URL from which to import and at which openHAB is running
  url: "127.0.0.1:8080"
  # The metadata namespace for items
  metadataNamespace: fuenf

# Get updates from openHAB into the knowledge base. Default: true.
mqttUpdate: true

# Method to initialize model. Possible values: "load", "openhab". Default: "load".
initModelWith: load
```

Anzupassen ist folgendes:

- referenzierte Dateien (initiales Modell `load.file = starter.eraser`, ML-Modell für Aktivitätserkennung `activity.file = activity.eg` und ML-Modell für Präferenzen `preference.file = preference.eg`) 
- referenzierte Items für die Präferenzen (`preference.items` und `preference.affectedItem`)
- URL von openHAB, inklusive Port. Optional der Namespace für Metadaten.

Die in der Konfigurations-Datei referenzierte Modell-Datei `starter.eraser` enthält das initiale Modell in der dafür vorgesehenen [DSL](https://git-st.inf.tu-dresden.de/OpenLicht/eraser/wikis/DSL). Ein Beispiel-Modell sieht wie folgt aus:

```
Color Item: id="iris1_item" label="Iris 1" state="121,88,68" topic="iris1_item";
Number Item: id="datetime_month" label="Month" state="1" topic="datetime_month";
Number Item: id="datetime_day" label="Day" state="31" topic="datetime_day";
Number Item: id="datetime_hour" label="Hour" state="13" topic="datetime_hour";
Number Item: id="datetime_minute" label="Minute" state="37" topic="datetime_minute";
Number Item: id="bias" label="bias item" state="1" ;
Activity Item: id="activity" ;

Group: id="Lights" items=["iris1_item"];
Group: id="Datetime" items=["datetime_month", "datetime_day", "datetime_hour", "datetime_minute"];

Mqtt: incoming="oh2/out/" outgoing="oh2/in/" host="localhost:2883" ;

Influx: host="172.22.1.152" ;

ML: activities={
  0: "Open door in empty room",
  1: "Door closed in empty room",
  2: "Open door with person in room",
  3: "Door closed with person in room",
  4: "Working",
  5: "Watch TV",
  6: "Reading",
  7: "Listening to music",
  8: "Going to sleep",
  9: "Wake up"
} ;
```

Im Statement zu `Mqtt` muss die Verbindung zum MQTT-Broker, der auch im openHAB verwendet wird, angegeben werden. Gegebenfalls muss eine Port-Weiterleitung eingerichtet werden.
Das Statement zu Influx ist optional.

Zum Starter der Knowledge-Base ist die Konfigurationsdatei anzugeben (hier im Beispiel `./starter-setting.yaml`):

```bash
./eraser.starter-1.0.0-SNAPSHOT/bin/eraser.starter -f starter-setting.yaml
```

Zum Beenden sollte (wie auch beim Starten im Log zu lesen) ein POST-Request auf `localhost:3467/system/exit` gesendet werden. Das ist auch über die [Swagger-UI](#swagger-ui-fur-die-rest-api-der-knowledge-base) möglich.

## openHAB-Seite

Grundlage auf der openHAB-Seite ist ein installiertes MQTT-Binding in Version 2, und das Eraser-Binding in Version `2.3.0.201904121612`.

In der PaperUI muss ein MQTT-Broker-Thing angelegt werden, entweder ein SystemBroker oder ein lokaler Broker, wie im Beispiel gezeigt:

![MQTT Binding Config](img/config-mqtt-binding.png)

Zusätzlich muss ein Eraser-Thing angelegt werden, welches den Namen des oben genannten Brokers in den Einstellung gesetzt hat (hier im Beispiel `local-mqtt-broker`).
Wahlweise können Updates von allen Items (`Publish All` aktiviert), oder nur von Items in einer Gruppe (`Publish All` deaktiviert und `Publish Group` ausgewählt) an Eraser gesendet werden.

Weiterhin ist zu beachten, dass die MQTT-Topics hier und bei der Eraser-Konfiguration übereinstimmen, d.h.

- `openhab.Base-Topic == eraser.outgoing` (im Beispiel `oh2/in`)
- `openhab.Outgoing-Topic == eraser.incoming` (im Beispiel `oh2/out`)

![Eraser Binding Config](img/config-eraser-binding.png)

## Swagger-UI für die REST-API der Knowledge-Base

Zur einfacheren Verwendung der REST-API wurde eine Swagger-UI in Python entwickelt, da das verwendete leichtgewichtige REST-Framework ([SparkJava](http://sparkjava.com/)) dies nicht unterstützt.
Zum Starten wird das Python-Skript gestartet, welches auf `localhost:5000` läuft und nach `localhost:4567` (Standard-Einstellung in der Knowledge-Base unter `rest.port`) weiterleitet.

```bash
python forward.py
```

Abhängigkeiten sind `flask`, `flask_restplus` und `requests`.
Nach Starten sieht die Oberfläche aktuell wie folgt aus:

![Swagger-UI Startseite](img/swagger-ui.png)
