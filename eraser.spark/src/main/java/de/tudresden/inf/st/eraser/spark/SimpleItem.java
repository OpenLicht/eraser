package de.tudresden.inf.st.eraser.spark;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Safe representation of an item
 *
 * @author rschoene - Initial contribution
 */
@Data(staticConstructor = "of")
public class SimpleItem {
  public final String ID;
  public final String label;
  public final String topic;
  public final boolean sendState;
  public final Map<String, String> metaData;
}
