package de.tudresden.inf.st.eraser.spark;

import de.tudresden.inf.st.eraser.jastadd.model.Activity;
import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * A recognizable activity.
 *
 * @author rschoene - Initial contribution
 */
@Data(staticConstructor = "of")
public class SimpleActivity {
  public final int identifier;
  public final String description;
  public final List<String> items;

  static SimpleActivity createFrom(Activity activity) {
    return SimpleActivity.of(activity.getIdentifier(), activity.getLabel(), Collections.emptyList());
  }
}
