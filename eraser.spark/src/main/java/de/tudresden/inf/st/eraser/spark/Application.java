package de.tudresden.inf.st.eraser.spark;

import beaver.Parser;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.JavaUtils;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The main class to start the rest service.
 *
 * @author rschoene - Initial contribution
 */
public class Application {

  private static final Logger logger = LogManager.getLogger(Application.class);
  private final Root root;
  private final Lock lock;
  private final Condition quitCondition;

  private Application(Root root, Lock lock, Condition quitCondition) {
    this.root = root;
    this.lock = lock;
    this.quitCondition = quitCondition;
  }

  private void createRules() {
    ObjectMapper mapper = new ObjectMapper();

    Spark.path("/", () -> Spark.before((request, response) -> logger.debug("{}: {}", request.pathInfo(), request.body())));

    Spark.path("/activity", () -> {

      //--- GET /activity ---
      Spark.get("",
          (request, response) -> wrapActivityList(root.getMachineLearningRoot().getActivityList()),
          mapper::writeValueAsString);

      //--- GET /activity/current ---
      Spark.get("/current",
          (request, response) -> JavaUtils.ifPresentOrElseReturn(root.currentActivity(),
              this::wrapActivity,
              () -> makeError(response, 204, "No activity recognized.")),
          mapper::writeValueAsString);

      //--- PUT /activity/current ---
      Spark.put("/current", (request, response) -> {
        logger.info("request body: '{}', params: '{}', length={}", request.body(), request.params(), request.contentLength());
        if (!root.getMachineLearningRoot().hasActivityRecognition()) {
          return makeError(response, 404, "No activity recognition model found");
        }
        MachineLearningModel activityRecognition = root.getMachineLearningRoot().getActivityRecognition();
        if (activityRecognition.canSetActivity()) {
          activityRecognition.setActivity(Integer.valueOf(request.body()));
          return "OK";
        } else {
          return makeError(response, 501, "Activity not editable for " + activityRecognition.getClass().getSimpleName());
        }
      });

      //--- GET /activity/:identifier ---
      Spark.get("/:identifier",
          (request, response) ->
              JavaUtils.ifPresentOrElseReturn(root.resolveActivity(paramAsInt(request, "identifier")),
                  this::wrapActivity,
                  () -> makeError(response, 404, "No activity for identifier " + request.params("identifier"))),
          mapper::writeValueAsString);
    });

    Spark.path("/events", () -> {

      //--- GET /events ---
      Spark.get("",
          (request, response) -> wrapChangeEventList(root.getMachineLearningRoot().getChangeEventList()),
          mapper::writeValueAsString);

      //--- GET /events/:identifier ---
      Spark.get("/:identifier",
          (request, response) ->
              JavaUtils.ifPresentOrElseReturn(root.resolveChangeEvent(paramAsInt(request, "identifier")),
                  this::wrapChangeEvent,
                  () -> makeError(response, 404, "No event for identifier " + request.params("identifier"))),
          mapper::writeValueAsString);
    });

    Spark.path("/model", () -> {

      //--- GET /model/full ---
      Spark.get("/full", (request, response) -> {
        response.type("text/plain");
        return root.prettyPrint();
      });
      Spark.path("/items", () -> {

        //--- GET /model/items ---
        Spark.get("",
            (request, response) -> wrapItemList(root.getSmartHomeEntityModel().items()),
            mapper::writeValueAsString);
        Spark.path("/:identifier", () -> {

          //--- PUT /model/items/:identifier ---
          Spark.put("", (request, response) -> {
            SmartHomeEntityModel smartHomeEntityModel = root.getSmartHomeEntityModel();
            Item item;
            try {
              item = ParserUtils.parseItem(request.body());
            } catch (IllegalArgumentException | IOException | Parser.Exception e) {
              logger.catching(e);
              return makeError(response, 400, "Could not create item. Error message: " + e.getMessage());
            }
            if (!smartHomeEntityModel.resolveItem(item.getID()).isPresent()) {
              root.getSmartHomeEntityModel().addNewItem(item);
              response.status(201);
              return "OK";
            } else {
              return makeError(response, 409, "Item already exists.");
            }
          });

          //--- GET /model/items/:identifier/state ---
          Spark.get("/state", (request, response) ->
              safeItemRoute(request, response, Item::getStateAsString));

          //--- PUT /model/items/:identifier/state ---
          Spark.put("/state", (request, response) -> {
            logger.info("request body: '{}', params: '{}', length={}", request.body(), request.params(), request.contentLength());
            return safeItemRoute(request, response,
                item -> {
                  try {
                    item.setStateFromString(request.body());
                    return "OK";
                  } catch (Exception e) {
                    logger.catching(e);
                    return makeError(response, 500, e.getMessage());
                  }
                });
          });
        });

        //--- GET /model/items/:identifier/history ---
        Spark.get("/:identifier/history",
            (request, response) -> {
              logger.info("request body: '{}', params: '{}', length={}", request.body(), request.params(), request.contentLength());
              return safeItemRoute(request, response, item -> makeHistory(item, response));
            });
      });
    });

    Spark.put("/feedback/:identifier/newDefault",
        (request, response) -> {
          logger.info("request body: '{}', params: '{}', length={}", request.body(), request.params(), request.contentLength());
          return safeItemRoute(request, response, item -> {
            logger.info("Would set for {} new default '{}'", item.getID(), request.body());
            return "OK";
        });
    });

    //--- POST /system/exit ---
    Spark.post("/system/exit", (request, response) -> {
      try {
        lock.lock();
        quitCondition.signalAll();
        Spark.stop();
      } finally {
        lock.unlock();
      }
      return makeError(response, 204, "Exiting.");
    });
  }

  private Object safeItemRoute(Request request, Response response, Function<Item, String> action) {
    return JavaUtils.ifPresentOrElseReturn(root.getSmartHomeEntityModel().resolveItem(request.params("identifier")), action,
        () -> makeError(response, 404, "Item '" + request.params("identifier") + "' not found"));
  }

  private String makeHistory(Item item, Response response) {
    response.type("text/plain");
    InfluxAdapter influxAdapter = root.getInfluxRoot().influxAdapter();
    influxAdapter.disableAsyncQuery();
    List<? extends AbstractItemPoint> list;
    if (item.asColorItem() != null) {
      list = item.asColorItem().getHistory();
    } else if (item.asDateTimeItem() != null) {
      list = item.asDateTimeItem().getHistory();
    } else if (item.asItemWithBooleanState() != null) {
      list = item.asItemWithBooleanState().getHistory();
    } else if (item.asItemWithDoubleState() != null) {
      list = item.asItemWithDoubleState().getHistory();
    } else if (item.asItemWithStringState() != null) {
      list = item.asItemWithStringState().getHistory();
    } else {
      String message = "Can not make history for item of unknown type.";
      logger.warn(message);
      influxAdapter.enableAsyncQuery();
      return message;
    }
    influxAdapter.enableAsyncQuery();
    return list.stream().map(AbstractItemPoint::toString).collect(Collectors.joining("\n"));
  }

  private String makeError(Response response, int status_code, String message) {
    response.status(status_code);
    return message;
  }

  private SimpleActivity wrapActivity(Activity modelActivity) {
    return SimpleActivity.createFrom(modelActivity);
  }

  private SimpleChangeEvent wrapChangeEvent(ChangeEvent event) {
    if (event instanceof ManualChangeEvent) {
      return SimpleManualChangeEvent.createFrom((ManualChangeEvent) event);
    } else {
      return SimpleRecognitionEvent.createFrom((RecognitionEvent) event);
    }
  }

  private List<SimpleActivity> wrapActivityList(JastAddList<Activity> activities) {
    return JavaUtils.toStream(activities).map(this::wrapActivity).collect(Collectors.toList());
  }

  private List<SimpleChangeEvent> wrapChangeEventList(JastAddList<ChangeEvent> events) {
    return JavaUtils.toStream(events).map(this::wrapChangeEvent).collect(Collectors.toList());
  }

  private SimpleItem wrapItem(Item item) {
    return SimpleItem.of(item.getID(),
        item.getLabel(),
        item.getTopic() != null ? item.getTopic().getTopicString() : null,
        item.isSendState(),
        wrapMetaData(item.getMetaData()));
  }

  private Map<String, String> wrapMetaData(MetaData metaData) {
    Map<String, String> result = new HashMap<>();
    for (KeyValuePair keyValuePair : metaData.getKeyValuePairList()) {
      result.put(keyValuePair.getKey(), keyValuePair.getValue());
    }
    return result;
  }

  private List<SimpleItem> wrapItemList(List<Item> items) {
    return items.stream().map(this::wrapItem).collect(Collectors.toList());
  }

  private int paramAsInt(Request request, String paramName) {
    return Integer.parseInt(request.params(paramName));
  }

  public static void start(int port, Root model, boolean createDummyMLData, Lock lock, Condition quit) {
    try {
      Spark.port(port);
      Application application = new Application(model, lock, quit);
      if (createDummyMLData) {
        new DummyDataCreator(model).addDummyDataToModel();
      }
      application.createRules();
    } catch (Exception e) {
      logger.catching(e);
      try {
        lock.lock();
        quit.signalAll();
      } finally {
        lock.unlock();
      }
    }
  }
}
