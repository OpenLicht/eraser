package de.tudresden.inf.st.eraser.spark;

import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.JavaUtils;

import java.time.Duration;
import java.time.Instant;

/**
 * Creates some activities and change events.
 *
 * @author rschoene - Initial contribution
 */
class DummyDataCreator {

  private final Root root;

  DummyDataCreator(Root root) {
    this.root = root;
  }

  private void addDummyActivitiesToModel() {
    MachineLearningRoot mlRoot = root.getMachineLearningRoot();
    mlRoot.addActivity(new Activity(1, "Sitting in armchair"));
    mlRoot.addActivity(new Activity(2, "Going to sleep"));
    mlRoot.addActivity(new Activity(3, "Entering house"));
  }

  private void addDummyChangeEventsToModel() {
    MachineLearningRoot mlRoot = root.getMachineLearningRoot();
    Item iris1 = getOrCreateColorItem("iris1", "Hue Iris 1");
    Item go1 = getOrCreateColorItem("go1", "Hue Go 1");
    Item go2 = getOrCreateColorItem("go2", "Hue Go 2");
    Instant now = Instant.now();
    mlRoot.addChangeEvent(newRecognitionEvent(1, now, 1,
        new ChangedItem("green", iris1), new ChangedItem("green", go1)));
    mlRoot.addChangeEvent(newRecognitionEvent(2, now.plusSeconds(1), 1));
    mlRoot.addChangeEvent(newRecognitionEvent(3, now.plusSeconds(3), 2,
        new ChangedItem("off", go2)));
    mlRoot.addChangeEvent(newManualChangeEvent(4, now.plusSeconds(17),
        new ChangedItem("green", iris1),
        new ChangedItem("red", go1),
        new ChangedItem("#EE7F00", go2)));
  }

  private Item getOrCreateColorItem(String name, String label) {
    return root.getSmartHomeEntityModel().resolveItem(name).orElseGet(() -> {
      ColorItem result = new ColorItem();
      result.setID(name);
      result.setLabel(label);
      result.disableSendState();
      result.setState(TupleHSB.of(0, 0, 0));
      result.enableSendState();
      return result;
    });
  }

  private RecognitionEvent newRecognitionEvent(int identifier, Instant when, int activityIdentifier, ChangedItem... changedItems) {
    RecognitionEvent result = new RecognitionEvent();
    JavaUtils.ifPresentOrElse(root.resolveActivity(activityIdentifier), result::setActivity, () -> { throw new RuntimeException("No activity found for identifier " + activityIdentifier); });
    initChangeEvent(result, identifier, when, changedItems);
    return result;
  }

  @SuppressWarnings("SameParameterValue")
  private ManualChangeEvent newManualChangeEvent(int identifier, Instant when, ChangedItem... changedItems) {
    ManualChangeEvent result = new ManualChangeEvent();
    initChangeEvent(result, identifier, when, changedItems);
    return result;
  }

  private void initChangeEvent(ChangeEvent result, int identifier, Instant when, ChangedItem... changedItems) {
    result.setIdentifier(identifier);
    result.setCreated(when);
    for (ChangedItem changedItem : changedItems) {
      result.addChangedItem(changedItem);
    }
  }

  void addDummyDataToModel() {
    addDummyActivitiesToModel();
    addDummyChangeEventsToModel();
  }
}
