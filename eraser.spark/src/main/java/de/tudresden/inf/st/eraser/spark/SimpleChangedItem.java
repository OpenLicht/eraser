package de.tudresden.inf.st.eraser.spark;

import de.tudresden.inf.st.eraser.jastadd.model.ChangedItem;
import lombok.Data;

/**
 * One changed item and its new state.
 *
 * @author rschoene - Initial contribution
 */
@Data
public class SimpleChangedItem {
  public final String name;
  public final Object state;
  public final String label;

  static SimpleChangedItem createFrom(ChangedItem changedItem) {
    return new SimpleChangedItem(changedItem.getItem().getID(), changedItem.getNewStateAsString(),
        changedItem.getItem().getLabel());
  }
}
