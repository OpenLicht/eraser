package de.tudresden.inf.st.eraser.spark;

import de.tudresden.inf.st.eraser.jastadd.model.RecognitionEvent;
import de.tudresden.inf.st.eraser.util.JavaUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Change of items automatically done by the system.
 *
 * @author rschoene - Initial contribution
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SimpleRecognitionEvent extends SimpleChangeEvent {
  public final int activity;
  public final String description;
  public final List<SimpleChangedItem> relevant_items;
  public final String type = "recognition";

  public SimpleRecognitionEvent(Instant created, int identifier, List<SimpleChangedItem> changedItems, List<SimpleChangedItem> relevantItems, int activity, String description) {
    super(created.getEpochSecond(), identifier, changedItems);
    this.activity = activity;
    this.description = description;
    this.relevant_items = relevantItems;
  }

  static SimpleRecognitionEvent createFrom(RecognitionEvent event) {
    return new SimpleRecognitionEvent(event.getCreated(), event.getIdentifier(),
        JavaUtils.toStream(event.getChangedItems())
            .filter(changedItem -> !changedItem.getItem().getID().isEmpty())
            .map(SimpleChangedItem::createFrom)
            .collect(Collectors.toList()),
        JavaUtils.toStream(event.getRelevantItems()).map(SimpleChangedItem::createFrom).collect(Collectors.toList()),
        event.getActivity().getIdentifier(), event.getActivity().getLabel());
  }

}
