package de.tudresden.inf.st.eraser.spark;

import de.tudresden.inf.st.eraser.jastadd.model.ManualChangeEvent;
import de.tudresden.inf.st.eraser.util.JavaUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Manual change of items in the system by an user.
 *
 * @author rschoene - Initial contribution
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SimpleManualChangeEvent extends SimpleChangeEvent {
  public final String type = "manual";

  public SimpleManualChangeEvent(Instant created, int identifier, List<SimpleChangedItem> changedItems) {
    super(created.getEpochSecond(), identifier, changedItems);
  }

  static SimpleManualChangeEvent createFrom(ManualChangeEvent event) {
    return new SimpleManualChangeEvent(event.getCreated(), event.getIdentifier(),
        JavaUtils.toStream(event.getChangedItems()).map(SimpleChangedItem::createFrom).collect(Collectors.toList()));
  }
}
