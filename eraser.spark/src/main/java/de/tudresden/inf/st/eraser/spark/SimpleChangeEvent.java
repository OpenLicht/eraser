package de.tudresden.inf.st.eraser.spark;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;
import java.util.List;

/**
 * Some system change event.
 *
 * @author rschoene - Initial contribution
 */
@Data
@AllArgsConstructor
public abstract class SimpleChangeEvent {
  public final long timestamp;
  public final int identifier;
  public final List<SimpleChangedItem> changed_items;
}
