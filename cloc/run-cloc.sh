#!/bin/sh
#  --force-lang=Java,jrag --force-lang=Java,jadd
DEF_FILE=my_definitions.txt
echo "Export language definitions"
cloc --write-lang-def="$DEF_FILE"
for f in cloc-def-*.txt;
do
	cat $f >> "$DEF_FILE"
done
echo "Running cloc with new definitions"
#  --ignored=bad-files.txt
CLOC_CMD="cloc --exclude-lang=JSON --read-lang-def=my_definitions.txt --exclude-list-file=.clocignore --quiet"
$CLOC_CMD --report-file=eraser-base-src.txt ../eraser-base/src/main
$CLOC_CMD --report-file=eraser-base-gen.txt ../eraser-base/src/gen/
$CLOC_CMD --report-file=feedbackloop.txt ../feedbackloop.*
$CLOC_CMD --sum-reports --report_file=eraser-base eraser-base-src.txt eraser-base-gen.txt feedbackloop.txt
cat eraser-base-src.txt
cat eraser-base.file
