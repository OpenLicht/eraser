package de.tudresden.inf.st.eraser.feedbackloop.analyze;

import de.tudresden.inf.st.eraser.feedbackloop.api.Analyze;
import de.tudresden.inf.st.eraser.feedbackloop.api.Plan;
import de.tudresden.inf.st.eraser.jastadd.model.Activity;
import de.tudresden.inf.st.eraser.jastadd.model.ItemUpdate;
import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningResult;
import de.tudresden.inf.st.eraser.jastadd.model.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Reference implementation for Analyze.
 *
 * @author rschoene - Initial contribution
 */
public class AnalyzeImpl implements Analyze {

  private Root knowledgeBase;
  private Plan plan;
  private Activity mostRecentActivity;
  private MachineLearningResult mostRecentPreferences;
  private Logger logger = LogManager.getLogger(AnalyzeImpl.class);

  public AnalyzeImpl() {
    this.mostRecentActivity = null;
    this.mostRecentPreferences = null;
  }

  @Override
  public void setKnowledgeBase(Root knowledgeBase) {
    this.knowledgeBase = knowledgeBase;
  }

  @Override
  public void setPlan(Plan plan) {
    this.plan = plan;
  }

  @Override
  public Plan getPlan() {
    return plan;
  }

  @Override
  public void analyzeLatestChanges() {
    MachineLearningResult recognitionResult = knowledgeBase.getMachineLearningRoot().getActivityRecognition().classify();
    recognitionResult.getItemUpdates().forEach(ItemUpdate::apply);
    knowledgeBase.currentActivity().ifPresent(activity -> {
      MachineLearningResult newMLResult = knowledgeBase.getMachineLearningRoot().getPreferenceLearning().classify();
      // check if activity has changed
      if (!activity.equals(mostRecentActivity)) {
        // new! inform plan!
        logger.info("Found new activity '{}'", activity.getLabel());
        try {
          informPlan(activity);
        } catch (Exception e) {
          logger.catching(e);
        }
      } else {
        // if no change, also check, if preferences have changed
        if (!newMLResult.equals(mostRecentPreferences)) {
          logger.info("Preferences have changed for same activity '{}'", activity.getLabel());
          try {
            informPlan(activity);
          } catch (Exception e) {
            logger.catching(e);
          }
        }
      }
      mostRecentActivity = activity;
      mostRecentPreferences = newMLResult;
    });
  }
}
