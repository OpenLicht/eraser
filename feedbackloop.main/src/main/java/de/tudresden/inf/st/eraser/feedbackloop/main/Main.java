package de.tudresden.inf.st.eraser.feedbackloop.main;

import beaver.Parser;
import de.tudresden.inf.st.eraser.feedbackloop.analyze.AnalyzeImpl;
import de.tudresden.inf.st.eraser.feedbackloop.api.Analyze;
import de.tudresden.inf.st.eraser.feedbackloop.api.Execute;
import de.tudresden.inf.st.eraser.feedbackloop.api.Monitor;
import de.tudresden.inf.st.eraser.feedbackloop.api.Plan;
import de.tudresden.inf.st.eraser.feedbackloop.execute.ExecuteImpl;
import de.tudresden.inf.st.eraser.feedbackloop.monitor.MonitorImpl;
import de.tudresden.inf.st.eraser.feedbackloop.plan.PlanImpl;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {

  private static Logger logger = LogManager.getLogger(Main.class);

  // ignore result of System.in.read()
  @SuppressWarnings("ResultOfMethodCallIgnored")
  public static void main(String[] args) throws IOException, Parser.Exception {
    logger.info("Reading model");
    Root knowledgeBase = ParserUtils.load("skywriter-hue.eraser", Main.class);
    logger.info("Create MAPE");
    Monitor monitor = new MonitorImpl();
    Analyze analyze = new AnalyzeImpl();
    Plan plan = new PlanImpl();
    Execute execute = new ExecuteImpl();

    monitor.setAnalyze(analyze);
    analyze.setPlan(plan);
    plan.setExecute(execute);

    monitor.setKnowledgeBase(knowledgeBase);
    analyze.setKnowledgeBase(knowledgeBase);
    plan.setKnowledgeBase(knowledgeBase);
    execute.setKnowledgeBase(knowledgeBase);

    analyze.startAsThread(1, TimeUnit.SECONDS);

    System.out.println("Press [Enter] to exit!");
    System.in.read();

    System.out.println("Stopping...");
    analyze.stop();
  }
}
