package de.tudresden.inf.st.eraser.feedbackloop.plan;

import de.tudresden.inf.st.eraser.feedbackloop.api.Execute;
import de.tudresden.inf.st.eraser.feedbackloop.api.Plan;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Reference implementation for Plan.
 *
 * @author rschoene - Initial contribution
 */
public class PlanImpl implements Plan {

  private Root knowledgeBase;
  private Execute execute;
  private static final Logger logger = LogManager.getLogger(PlanImpl.class);

  @Override
  public void setKnowledgeBase(Root knowledgeBase) {
    this.knowledgeBase = knowledgeBase;
  }

  @Override
  public void setExecute(Execute execute) {
    this.execute = execute;
  }

  @Override
  public Execute getExecute() {
    return execute;
  }

  @Override
  public void planToMatchPreferences(Activity activity) {
    logger.info("Plan got new activity [{}]: {}", activity.getIdentifier(), activity.getLabel());
    MachineLearningResult mlResult = knowledgeBase.getMachineLearningRoot().getPreferenceLearning().getLastResult();
    knowledgeBase.getMachineLearningRoot().addChangeEvent(createRecognitionEvent(activity, mlResult.getItemUpdates()));
    informExecute(mlResult.getItemUpdates());
  }

  private ChangeEvent createRecognitionEvent(Activity activity, Iterable<ItemUpdate> updates) {
    RecognitionEvent result = RecognitionEvent.createRecognitionEvent(knowledgeBase.getMachineLearningRoot().getActivityRecognition());
    result.setActivity(activity);
    for (ItemUpdate update : updates) {
      result.addChangedItem(ChangedItem.newFrom(update));
    }
    return result;
  }
}
