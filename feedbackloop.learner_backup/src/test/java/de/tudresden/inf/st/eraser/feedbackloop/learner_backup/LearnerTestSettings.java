package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.LearnerTestUtils.CheckUpdate;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.LearnerScenarioDefinition;
import de.tudresden.inf.st.eraser.jastadd.model.Item;
import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Data
@Accessors(chain = true)
class LearnerTestSettings {
  private URL configURL;
  private URL dataURL;
  private Function<List<String>, String> expectedOutput = targetValues -> targetValues.get(0);
  private final Map<String, BiConsumer<Item, String>> specialInputHandler = new HashMap<>();
  private Supplier<Item> outputItemProvider;
  private Function<Item, String> stateOfOutputItem;
  private CheckUpdate checkUpdate = String::equals;
  private MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget factoryTarget;
  private boolean singleUpdateList;
  private boolean verbose = false;

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient LearnerScenarioDefinition scenarioSettings;

  @SuppressWarnings("SameParameterValue")
  LearnerTestSettings putSpecialInputHandler(String itemName, BiConsumer<Item, String> handler) {
    specialInputHandler.put(itemName, handler);
    return this;
  }

  LearnerTestSettings setConfigURL(URL configURL) throws IOException {
    scenarioSettings = LearnerScenarioDefinition.loadFrom(configURL);
    this.configURL = configURL;
    return this;
  }

  LearnerScenarioDefinition getScenarioSettings() {
    return scenarioSettings;
  }
}
