package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.LearnerScenarioDefinition;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.LearnerSettings;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.SimpleColumnDefinition;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.ml.data.versatile.columns.ColumnType;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static de.tudresden.inf.st.eraser.feedbackloop.learner_backup.LearnerTestConstants.COLOR_WEIGHTS;
import static de.tudresden.inf.st.eraser.feedbackloop.learner_backup.LearnerTestConstants.MAX_COLOR_DIFFERENCE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Utility methods to keep test code clean.
 *
 * @author rschoene - Initial contribution
 */
public class LearnerTestUtils {

  private static final Logger logger = LogManager.getLogger(LearnerTestUtils.class);

  static Root createKnowledgeBase(LearnerTestSettings settings) {
    Root result = Root.createEmptyRoot();
    Group group = new Group();
    result.getSmartHomeEntityModel().addGroup(group);
    // init items
    Stream.concat(settings.getScenarioSettings().relevantItemNames.stream(),
                  settings.getScenarioSettings().targetItemNames.stream())
        .distinct().forEach(
        itemName -> {
          if (itemName.equals("activity")) return;
          Item item = createItem(itemName);
          group.addItem(item);
          item.setTopic(result.getMqttRoot().getOrCreateMqttTopic(itemName));
        }
    );
    // init activities
    for (int i = 0; i < LearnerTestConstants.ACTIVITY_NAMES.length; i++) {
      result.getMachineLearningRoot().addActivity(new Activity(i, LearnerTestConstants.ACTIVITY_NAMES[i]));
    }
    return result;
  }

  private static Item createItem(String itemName) {
    Item item;
    if (itemName.contains("OpenClose")) {
      // contact item
      item = new ContactItem();
    } else if (itemName.contains("Fibaro")) {
      // boolean item
      item = new SwitchItem();
    } else {
      switch (itemName) {
        case "work_device_online_state":
          item = new SwitchItem();
          break;
        case LearnerTestConstants.PREFERENCE_OUTPUT_ITEM_NAME:
          item = new ColorItem();
          break;
        case "w_brightness":
          item = new StringItem();
          break;
        default: item = new NumberItem();
      }
    }
    item.setID(itemName);
    return item;
  }

  static void testLearner(LearnerSubjectUnderTest sut, LearnerTestSettings settings) {
    sut.init(settings);
    // maybe use factory.createModel() here instead
    // go through same csv as for training and test some of the values
    int correct = 0, wrong = 0;
    try(InputStream is = settings.getDataURL().openStream();
        Reader reader = new InputStreamReader(is);
        CSVReader csvreader = createCSVReader(reader, settings)) {
      sut.initFor(settings.getFactoryTarget(), settings.getConfigURL());
      int index = 0;
      LearnerScenarioDefinition scenarioSettings = settings.getScenarioSettings();
      LearnerSettings definition = LearnerSettings.loadFrom(scenarioSettings.getDefinitionFileAsURL());
      List<String> targetValues;
      for (String[] line : csvreader) {
        // only check every 10th line, push an update for every input column
        if (++index % 10 == 0) {
          // Attention: Not every column might be relevant
          targetValues = new ArrayList<>();
          int lineSize = line.length;
          int inputSize = scenarioSettings.relevantItemNames.size();
          List<Item> itemsToUpdate = new ArrayList<>(inputSize);
          for (int i = 0; i < lineSize; i++) {
            SimpleColumnDefinition column = definition.columns.get(i);
            switch (column.kind) {
              case input:
                // do nothing
                break;
              case target:
                targetValues.add(line[i]);
                continue;
              case ignored:
                continue;
            }
            if (column.type == ColumnType.ignore) {
              continue;
            }
            // use itemName == name of column (or a non-trivial mapping, if any)
            String itemName = scenarioSettings.nonTrivialOutputMappings.getOrDefault(column.name, column.name);
            Item item = sut.root.getSmartHomeEntityModel().resolveItem(itemName)
                .orElseThrow(() -> new AssertionError("Item " + itemName + " not found"));
            if (settings.getSpecialInputHandler().containsKey(itemName)) {
              if (settings.isVerbose()) {
                logger.debug("Setting {} {} using special handler and value '{}' (column {})",
                    item, item.getID(), line[i], i);
              }
              settings.getSpecialInputHandler().get(itemName).accept(item, line[i]);
            } else {
              if (settings.isVerbose()) {
                logger.debug("Setting {} {} using '{}' (column {})", item, item.getID(), line[i], i);
              }
              item.setStateFromString(line[i]);
            }
            itemsToUpdate.add(item);
          }
          if (settings.isSingleUpdateList()) {
            sut.encoder.newData(itemsToUpdate);
          } else {
            itemsToUpdate.forEach(item -> sut.encoder.newData(Collections.singletonList(item)));
          }
          MachineLearningResult result = sut.decoder.classify();
          // check if only one item is to be updated
          assertEquals(1, result.getNumItemUpdate(), "Not one item update!");
          ItemUpdate update = result.getItemUpdate(0);
          // check that the output item is to be updated
          assertEquals(settings.getOutputItemProvider().get(), update.getItem(),
              "Output item not to be updated!");
          update.apply();
          // check if the correct new state was set
          assertThat(targetValues).isNotEmpty();
          String expected = settings.getExpectedOutput().apply(targetValues);
          String actual = settings.getStateOfOutputItem().apply(update.getItem());
          if (settings.getCheckUpdate().assertEquals(expected, actual)) {
            correct++;
          } else {
            wrong++;
            if (settings.isVerbose()) {
              logger.debug("Result not equal, expected '{}' but was '{}'", expected, actual);
            }
          }
        } // end if index % 10 == 0
      } // end for
    } catch (IOException | ClassNotFoundException e) {
      throw new AssertionError(e);
    } finally {
      sut.shutdown();
    }
    assertThat(correct + wrong).isGreaterThan(0);
    double accuracy = correct * 1.0 / (correct + wrong);
    logger.info("Accuracy: {}", accuracy);
    assertThat(accuracy).isGreaterThan(LearnerTestConstants.MIN_ACCURACY);
  }

  private static CSVReader createCSVReader(Reader reader, LearnerTestSettings settings) {
    char separator;
    switch(settings.getScenarioSettings().csvFormat) {
      case "DECIMAL_POINT": separator=','; break;
      case "DECIMAL_COMMA": separator=';'; break;
      default:
        logger.warn("Unknown CSV format, using default comma as separator");
        separator=',';
    }
    return new CSVReaderBuilder(reader)
        .withCSVParser(new CSVParserBuilder().withSeparator(separator).build())
        .build();
  }

  @FunctionalInterface
  public interface CheckUpdate {
    boolean assertEquals(String expected, String actual);
  }

  static String decodeOutput(List<String> targetValues) {
    int color = Integer.parseInt(targetValues.get(0));
    int brightness = Integer.parseInt(targetValues.get(1));
    return TupleHSB.of(color, 100, brightness).toString();
  }

  private static int hueDistance(int hue1, int hue2) {
    int d = Math.abs(hue1 - hue2);
    return d > 180 ? 360 - d : d;
  }

  /**
   * Compares two colours given as strings of the form "HUE,SATURATION,BRIGHTNESS"
   * @param expected the expected colour
   * @param actual   the computed, actual colour
   * @return <code>true</code>, if both a colours are similar
   */
  static boolean colorSimilar(String expected, String actual) {
    TupleHSB expectedTuple = TupleHSB.parse(expected);
    TupleHSB actualTuple = TupleHSB.parse(actual);
    int diffHue = hueDistance(expectedTuple.getHue(), actualTuple.getHue());
    int diffSaturation = Math.abs(expectedTuple.getSaturation() - actualTuple.getSaturation());
    int diffBrightness = Math.abs(expectedTuple.getBrightness() - actualTuple.getBrightness());
    double total = diffHue * COLOR_WEIGHTS[0] +
        diffSaturation * COLOR_WEIGHTS[1] +
        diffBrightness * COLOR_WEIGHTS[2];
//    logger.debug("Diff expected {} and actual {}: H={} + S={} + B={} -> {} < {} ?", expected, actual,
//        diffHue, diffSaturation, diffBrightness, total, MAX_COLOR_DIFFERENCE);
    return total < MAX_COLOR_DIFFERENCE;
  }
}
