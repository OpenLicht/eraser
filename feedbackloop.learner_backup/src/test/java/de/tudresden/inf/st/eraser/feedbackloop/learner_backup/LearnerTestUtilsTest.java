package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import de.tudresden.inf.st.eraser.jastadd.model.TupleHSB;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the utility methods.
 *
 * @author rschoene - Initial contribution
 */
public class LearnerTestUtilsTest {

  @Test
  public void testColorSimilar() {
    Map<String, TupleHSB> colors = new HashMap<>();

    // reddish target colors
    colors.put("pink", TupleHSB.of(350, 100, 82));
    colors.put("orangeRed", TupleHSB.of(16, 100, 45));
    colors.put("lightPink", TupleHSB.of(351, 100, 80));
    colors.put("darkSalmon", TupleHSB.of(15, 71, 67));
    colors.put("lightCoral", TupleHSB.of(0, 78, 63));
    colors.put("darkRed", TupleHSB.of(0, 100, 16));
    colors.put("indianRed", TupleHSB.of(0, 53, 49));
    colors.put("lavenderBlush", TupleHSB.of(340, 100, 95));
    colors.put("lavender", TupleHSB.of(240, 66, 90));
    String[] targetColors = new String[]{"pink", "orangeRed", "lightPink", "darkSalmon", "lightCoral",
        "darkRed", "indianRed", "lavenderBlush", "lavender"};

    // reference colors
    colors.put("blue", TupleHSB.of(240, 100, 11));
    colors.put("blueViolet", TupleHSB.of(271, 75, 36));
    colors.put("magenta", TupleHSB.of(300, 100, 41));
    colors.put("purple", TupleHSB.of(300, 100, 20));
    colors.put("red", TupleHSB.of(0, 100, 29));
    colors.put("tomato", TupleHSB.of(9, 100, 55));
    colors.put("orange", TupleHSB.of(39, 100, 67));
    colors.put("yellow", TupleHSB.of(60, 100, 88));
    colors.put("yellowGreen", TupleHSB.of(80, 60, 67));
    colors.put("green", TupleHSB.of(120, 100, 29));
    colors.put("springGreen", TupleHSB.of(150, 100, 64));
    colors.put("cyan", TupleHSB.of(180, 100, 69));
    colors.put("ivory", TupleHSB.of(60, 100, 98));

    String[] referenceColors = new String[]{"blue", "blueViolet", "magenta", "purple", "red", "tomato",
        "orange", "yellow", "yellowGreen", "green", "springGreen", "cyan", "ivory"};

    /* Code to help producing similarity matrix */
//    for (String target : targetColors) {
//      String tmp = "";
//      for (String reference : referenceColors) {
//        tmp += assertColorSimilar(colors, target, reference) ? "x" : " ";
//        tmp += ",";
//      }
//      System.out.println( "***" + target + ": " + tmp);
//    }

    String[] similarityMatrix = new String[]{
        "blue, blueViolet, magenta, purple, red, tomato, orange, yellow, yellowGreen, green, springGreen, cyan, ivory", // <- reference colors
        "    ,           ,    x   ,   x   ,  x ,    x  ,   x   ,   x   ,            ,      ,            ,     ,   x  ", // pink
        "    ,           ,    x   ,   x   ,  x ,    x  ,   x   ,   x   ,            ,      ,            ,     ,   x  ", // orangeRed
        "    ,           ,    x   ,   x   ,  x ,    x  ,   x   ,   x   ,            ,      ,            ,     ,   x  ", // lightPink
        "    ,           ,        ,       ,  x ,    x  ,   x   ,   x   ,      x     ,      ,            ,     ,   x  ", // darkSalmon
        "    ,           ,    x   ,   x   ,  x ,    x  ,   x   ,   x   ,      x     ,      ,            ,     ,   x  ", // lightCoral
        "    ,           ,    x   ,   x   ,  x ,    x  ,   x   ,       ,            ,      ,            ,     ,      ", // darkRed
        "    ,           ,    x   ,       ,  x ,    x  ,   x   ,       ,            ,      ,            ,     ,      ", // indianRed
        "    ,           ,    x   ,   x   ,  x ,    x  ,   x   ,   x   ,            ,      ,            ,     ,   x  ", // lavenderBlush
        " x  ,     x     ,        ,       ,    ,       ,       ,       ,            ,      ,            ,  x  ,      "}; // lavender

    for (int targetIndex = 0; targetIndex < targetColors.length; targetIndex++) {
      String target = targetColors[targetIndex];
      String[] expectedValues = similarityMatrix[targetIndex + 1].split(",");
      for (int referenceIndex = 0; referenceIndex < referenceColors.length; referenceIndex++) {
        String reference = referenceColors[referenceIndex];
        boolean expectedToBeSimilar = expectedValues[referenceIndex].contains("x");
        String message = String.format("%s iss%s expected to be similar to %s, but %s!",
            target, expectedToBeSimilar ? "" : " not",
            reference, expectedToBeSimilar ? "differs" : "it was");
        assertEquals(expectedToBeSimilar,
            LearnerTestUtils.colorSimilar(
                colors.get(reference).toString(),
                colors.get(target).toString()),
            message);
      }
    }
  }
}
