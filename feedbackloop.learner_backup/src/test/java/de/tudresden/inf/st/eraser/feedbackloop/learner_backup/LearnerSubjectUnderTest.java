package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningDecoder;
import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningEncoder;
import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory;
import de.tudresden.inf.st.eraser.jastadd.model.Root;

import java.io.IOException;
import java.net.URL;

/**
 * Data object for the test.
 *
 * @author rschoene - Initial contribution
 */
class LearnerSubjectUnderTest {
  MachineLearningEncoder encoder;
  MachineLearningDecoder decoder;
  Root root;
  private MachineLearningHandlerFactoryImpl factory;

  void init(LearnerTestSettings settings) {
    root = LearnerTestUtils.createKnowledgeBase(settings);
    factory = new MachineLearningHandlerFactoryImpl();
    factory.setKnowledgeBaseRoot(root);
  }

  void initFor(MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget factoryTarget, URL configURL) throws IOException, ClassNotFoundException {
    factory.initializeFor(factoryTarget, configURL);
    encoder = factory.createEncoder();
    decoder = factory.createDecoder();
  }

  void shutdown() {
    factory.shutdown();
  }
}
