package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

/**
 * Constants for the Learner tests.
 *
 * @author rschoene - Initial contribution
 */
public interface LearnerTestConstants {
  /** Minimal accuracy (correct / total  classifications) */
  double MIN_ACCURACY = 0.8;
  /** Maximum difference when comparing colors */
  double MAX_COLOR_DIFFERENCE = 0.2;
  /** Weights for difference (in order: Hue, Saturation, Brightness) when comparing colors */
  double[] COLOR_WEIGHTS = new double[]{0.8/360, 0.1/100, 0.1/100};
  /** Name of the item which is targeted by preference learning, in test data */
  String PREFERENCE_OUTPUT_ITEM_NAME = "iris1_item";
  /** Labels of activities, in test data */
  String[] ACTIVITY_NAMES = new String[]{
      "working",
      "walking",
      "dancing",
      "lying",
      "getting up",
      "reading",
      "DoorClosedPIn",
      "DoorClosedPOut",
      "DoorOpenedPIn",
      "DoorOpenedPOut",
      "Reading",
      "TVWatching",
      "Working",
  };
}
