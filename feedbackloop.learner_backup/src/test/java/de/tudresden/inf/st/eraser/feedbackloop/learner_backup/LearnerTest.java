package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import de.tudresden.inf.st.eraser.jastadd.model.Item;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import static de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget.ACTIVITY_RECOGNITION;
import static de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget.PREFERENCE_LEARNING;

/**
 * Testing the learner.
 *
 * @author rschoene - Initial contribution
 */
public class LearnerTest {

  private static final Path base = Paths.get("src", "test", "resources");
  private static URL ACTIVITY_CONFIG;
  private static URL LOADED_ACTIVITY_CONFIG;
  private static URL ACTIVITY_DATA;
  private static URL PREFERENCE_CONFIG;
  private static URL LOADED_PREFERENCE_CONFIG;
  private static URL PREFERENCE_DATA;
  private static URL OCT_ACTIVITY_CONFIG;
  private static URL OCT_LOADED_ACTIVITY_CONFIG;
  private static URL OCT_ACTIVITY_DATA;
  private LearnerSubjectUnderTest sut;

  @BeforeAll
  public static void setData() throws MalformedURLException {
    ACTIVITY_CONFIG = resolveFromBaseURL("learner_activity_phone_and_watch.json");
    LOADED_ACTIVITY_CONFIG = resolveFromBaseURL("loaded_learner_activity_phone_and_watch.json");
    ACTIVITY_DATA = resolveFromBaseURL("activity_data.csv");
    PREFERENCE_CONFIG = resolveFromBaseURL("learner_preferences_brightness_iris.json");
    LOADED_PREFERENCE_CONFIG = resolveFromBaseURL("loaded_learner_preferences_brightness_iris.json");
    PREFERENCE_DATA = resolveFromBaseURL("preference_data.csv");

    OCT_ACTIVITY_CONFIG = resolveFromBaseURL("2019-oct-28-learner.json");
    OCT_LOADED_ACTIVITY_CONFIG = resolveFromBaseURL("2019-oct-28-loaded_learner.json");
    OCT_ACTIVITY_DATA = resolveFromBaseURL("activity_data/28_08_2019_H14_14/result_all_items_EVERYTHING.csv");
  }

  private static URL resolveFromBaseURL(String filename) throws MalformedURLException {
    return base.resolve(filename).toUri().toURL();
  }

  @BeforeEach
  public void initLearner() {
    sut = new LearnerSubjectUnderTest();
  }

  @Test
  public void testActivities() throws IOException {
    LearnerTestUtils.testLearner(sut, settingsActivities());
  }

  @Disabled("Was ignored before")
  @Test
  public void testPreferences() throws IOException {
    LearnerTestUtils.testLearner(sut, settingsPreferences());
  }

  @Test
  public void testLoadedActivities() throws IOException {
    LearnerTestUtils.testLearner(sut, settingsActivities()
        .setConfigURL(LOADED_ACTIVITY_CONFIG));
  }

  @Test
  public void testLoadedPreferences() throws IOException {
    LearnerTestUtils.testLearner(sut, settingsPreferences()
        .setConfigURL(LOADED_PREFERENCE_CONFIG));
  }

  @Disabled("takes longer than 10min")
  @Test
  public void testOctoberActivities() throws IOException {
    LearnerTestUtils.testLearner(sut, settingsActivities()
        .setConfigURL(OCT_ACTIVITY_CONFIG)
        .setDataURL(OCT_ACTIVITY_DATA));
  }

  @Disabled("not working currently")
  @Test
  public void testLoadedOctoberActivities() throws IOException {
    LearnerTestUtils.testLearner(sut, settingsActivities()
        .setConfigURL(OCT_LOADED_ACTIVITY_CONFIG)
        .setDataURL(OCT_ACTIVITY_DATA)
        .setVerbose(true));
  }

  private LearnerTestSettings settingsActivities() throws IOException {
    return new LearnerTestSettings()
        .setConfigURL(ACTIVITY_CONFIG)
        .setDataURL(ACTIVITY_DATA)
        .setOutputItemProvider(() -> sut.root.getSmartHomeEntityModel().getActivityItem())
        .setStateOfOutputItem(item -> sut.root.currentActivityName())
        .setFactoryTarget(ACTIVITY_RECOGNITION)
        .setSingleUpdateList(false);
  }

  private LearnerTestSettings settingsPreferences() throws IOException {
    return new LearnerTestSettings()
        .setConfigURL(PREFERENCE_CONFIG)
        .setDataURL(PREFERENCE_DATA)
        .setExpectedOutput(LearnerTestUtils::decodeOutput)
        .putSpecialInputHandler("activity", (item, value) -> item.setStateFromLong(
            sut.root.resolveActivity(value)
                .orElseThrow(() -> new AssertionError("Activity " + value + " not found"))
                .getIdentifier()))
        .setOutputItemProvider(() -> sut.root.getSmartHomeEntityModel()
            .resolveItem(LearnerTestConstants.PREFERENCE_OUTPUT_ITEM_NAME)
            .orElseThrow(() -> new AssertionError(
                "Item " + LearnerTestConstants.PREFERENCE_OUTPUT_ITEM_NAME + " not found")))
        .setStateOfOutputItem(Item::getStateAsString)
        .setCheckUpdate(LearnerTestUtils::colorSimilar)
        .setFactoryTarget(PREFERENCE_LEARNING)
        .setSingleUpdateList(true);
  }

}
