package de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.LearnerKind;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.MachineLearningImpl;
import de.tudresden.inf.st.eraser.util.ParserUtils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Settings to initialize {@link MachineLearningImpl}.
 *
 * @author rschoene - Initial contribution
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class LearnerScenarioDefinition {
  public List<String> relevantItemNames;
  public List<String> targetItemNames;
  public Map<String, String> nonTrivialOutputMappings = new HashMap<>();
  public String definitionFile;
  public String csvFormat = "DECIMAL_POINT";
  public List<String> dataFiles;
  public LearnerKind kind = LearnerKind.normal;
  /** Save models at shutdown */
  public boolean saveModels = false;
  public transient URL myBaseLocation;

  public static LearnerScenarioDefinition loadFrom(URL location) throws IOException {
    LearnerScenarioDefinition result = ParserUtils.loadFrom(location, LearnerScenarioDefinition.class);
    result.myBaseLocation = new URL(location.getProtocol(), location.getHost(), location.getPort(), new File(location.getFile()).getParent(), null);
    return result;
  }

  public URL getDefinitionFileAsURL() throws MalformedURLException {
    return filenameToURL(definitionFile);
  }

  public List<URL> getDataFilesAsURL() throws MalformedURLException {
    List<URL> result = new ArrayList<>();
    for (String dataFile : dataFiles) {
      URL url = filenameToURL(dataFile);
      result.add(url);
    }
    return result;
  }

  private URL filenameToURL(URL base, String filename) throws MalformedURLException {
    // construct new URL with same content except for file part
    String newFilePart;
    if (filename.startsWith("/")) {
      newFilePart = filename;
    } else {
      newFilePart = (base.getFile().endsWith("/") ? base.getFile() : base.getFile() + "/" ) + filename;
    }
    return new URL(base.getProtocol(), base.getHost(), base.getPort(), newFilePart, null);
  }

  private URL filenameToURL(String filename) throws MalformedURLException {
    return filenameToURL(myBaseLocation, filename);
  }
}
