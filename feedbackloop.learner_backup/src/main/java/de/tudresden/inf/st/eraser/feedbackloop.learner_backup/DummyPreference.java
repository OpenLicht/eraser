package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;
import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DummyPreference {
    private static String activity;  //Activity: walking, reading, working, dancing, lying, getting up
    private static Random random = new Random();

    public static void main(String[] args) {
        creator();
    }
    private static void creator(){
        try {
            FileWriter writer = new FileWriter("datasets/backup/preference_data.csv",true);
            CSVWriter csv_writer = new CSVWriter(writer, ',',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            //activity="walking" green
            activity ="walking";

           // activity ="reading";
            csv_writer.writeAll(generator("walking","green"));
            csv_writer.writeAll(generator("reading","sky blue"));
            csv_writer.writeAll(generator("working","blue"));
            csv_writer.writeAll(generator("dancing","purple"));
            csv_writer.writeAll(generator("lying","red"));
            csv_writer.writeAll(generator("getting up","yellow"));
            csv_writer.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate random data
     * @param activity_input    input for activity
     * @param color             red 7; green 120; blue 240; yellow 60; sky blue 180; purple 300;
     * @return generated data
     */
    private static List<String[]> generator(String activity_input, String color){
        List<String[]> data = new ArrayList<>();
        //dark: <45; dimmer 45-70; bright >70;
        //1-100**/
        String brightness_output;
        String watch_brightness;
        activity = activity_input;
        //
        //100 walking with different lighting intensity
        for (int i=0; i<100; i++) {
            String[] add_data = new String[4];
            int brightness = random.nextInt(3000);
            System.out.println(brightness);
            if (brightness<45) {
                watch_brightness = "dark";
                brightness_output ="100";
            } else if(brightness < 200) {
                watch_brightness = "dimmer";
                brightness_output ="40";
            } else if(brightness < 1000) {
                watch_brightness = "medium";
                brightness_output ="70";
            } else {
                watch_brightness = "bright";
                brightness_output ="0";
            }
            add_data[0] = activity;
            add_data[1] = watch_brightness;
            add_data[2] = color;
            add_data[3] = brightness_output;
            data.add(add_data);
        }
        return data;
    }
}
