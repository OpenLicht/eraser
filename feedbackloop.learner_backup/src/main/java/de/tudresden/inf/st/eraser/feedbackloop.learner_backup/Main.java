package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.LearnerScenarioDefinition;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.LearnerSettings;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.ml.data.versatile.NormalizationHelper;
import org.encog.util.csv.CSVFormat;
import org.encog.util.csv.ReadCSV;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory.MachineLearningHandlerFactoryTarget.ACTIVITY_RECOGNITION;

@SuppressWarnings("unused")
public class Main {

  private static final Logger logger = LogManager.getLogger(Main.class);

  public static void main(String[] args) {
//    ReaderCSV reader = new ReaderCSV("datasets/backup/activity_data.csv","preference");
//    reader.updater();
//    Learner learner=new Learner();
//    learner.preference_train("../datasets/backup/preference_data.csv");
//    learner.train("datasets/backup/activity_data.csv","datasets/backup/preference_data.csv");
//    activity_validation_learner();
//    testSettings();
//    testLoadNormalizationHelper();
    testLearnerWithDatasetFromMunich();
  }

  private static void testLearnerWithDatasetFromMunich() {
    MachineLearningHandlerFactory factory = new MachineLearningHandlerFactoryImpl();
    try {
      URL configURL = Paths.get("src", "main", "resources", "activity_definition-2019-oct-28.json").toUri().toURL();
      LearnerScenarioDefinition scenarioDefinition = LearnerScenarioDefinition.loadFrom(configURL);
      Root model = createRootWithItemsFrom(scenarioDefinition);
      factory.setKnowledgeBaseRoot(model);
      factory.initializeFor(ACTIVITY_RECOGNITION, configURL);
      MachineLearningModel mlModel = factory.createModel();
      List<Item> changedItems = new ArrayList<>(); // TODO
      mlModel.getEncoder().newData(changedItems);
      MachineLearningResult result = mlModel.getDecoder().classify();
    } catch (IOException | ClassNotFoundException e) {
      logger.catching(e);
    }
  }

  private static Root createRootWithItemsFrom(LearnerScenarioDefinition scenarioDefinition) {
    Root result = Root.createEmptyRoot();
    ParserUtils.addToUnknownGroup(
        result.getSmartHomeEntityModel(),
        scenarioDefinition.relevantItemNames.stream().map(Main::createItem).collect(Collectors.toList()));
    return result;
  }

  private static Item createItem(String itemName) {
    Item item;
    if (itemName.contains("OpenClose")) {
      // contact item
      item = new ContactItem();
    } else if (itemName.contains("Fibaro")) {
      // boolean item
      item = new SwitchItem();
    } else {
      // double item
      item = new NumberItem();
    }
    item.setID(itemName);
    return item;
  }

  private static void testLoadNormalizationHelper() {
    try {
      URL binaryURL = Paths.get("src", "test", "resources", "activity_normalizer.bin").toUri().toURL();
      NormalizationHelper normalizer = LearnerPersistenceUtils.loadNormalizationHelperFrom(binaryURL, false);
      System.out.println("loaded");
    } catch (IOException | ClassNotFoundException e) {
      logger.catching(e);
    }
  }

  private static void testSettings() {
    ObjectMapper mapper = new ObjectMapper();
    File settingsFile = Paths.get("src", "main", "resources", "activity_definition.json").toFile();
    LearnerSettings settings;
    try {
      settings = mapper.readValue(settingsFile, LearnerSettings.class);
    } catch (IOException e) {
      logger.catching(e);
      return;
    }
    System.out.println("settings.name = " + settings.name);
    System.out.println("settings.columns = " + settings.columns
        .stream()
        .map(col -> "(" + col.kind + ": " + col.name + "," + col.type + ")")
        .collect(Collectors.joining(";")));
  }

  private static void activity_validation_learner() throws IOException {
    ReadCSV csv = new ReadCSV("../datasets/backup/activity_data.csv", true, CSVFormat.DECIMAL_POINT);
    String[] line = new String[11];
    Learner learner = new Learner(new ObjectMapper().readValue(
        Paths.get("src", "main", "resources", "activity_definition.json").toFile(),
        LearnerSettings.class));
    learner.train(Paths.get("src", "test", "activity_data.csv").toUri().toURL(), "DECIMAL_POINT");
//    learner.preference_train("../datasets/backup/preference_data.csv");
    int wrong = 0;
    int right = 0;
    int i = 0;
    while (csv.next()) {
      if (i == 0) {
        i++;
      } else {
        StringBuilder result = new StringBuilder();
        line[0] = csv.get(0);
        line[1] = csv.get(1);
        line[2] = csv.get(2);
        line[3] = csv.get(3);
        line[4] = csv.get(4);
        line[5] = csv.get(5);
        line[6] = csv.get(6);
        line[7] = csv.get(7);
        line[8] = csv.get(8);
        line[9] = csv.get(9);
        line[10] = csv.get(10);
        //line[11] = csv.get(11);
        String correct = csv.get(11);
        String irisChosen = learner.predictor(line)[0];
        result.append(Arrays.toString(line));
        result.append(" -> predicted: ");
        result.append(irisChosen);
        result.append("(correct: ");
        result.append(correct);
        result.append(")");
        if (!irisChosen.equals(correct)) {
          System.out.println(correct);
          System.out.println(irisChosen);
          ++wrong;
        } else {
          ++right;
        }
        System.out.println(result.toString());
      }
      System.out.println("wrong number" + wrong);
      System.out.println("right number" + right);
    }

    learner.shutdown();
    //double validation = (double(right))/(double(wrong+right));
    //System.out.println("%.2f"+validation);
  }
}
