package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.LearnerScenarioDefinition;
import de.tudresden.inf.st.eraser.jastadd.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class MachineLearningImpl implements MachineLearningDecoder, MachineLearningEncoder {

//  public static final int GOAL_ACTIVITY_PHONE_AND_WATCH = 1;
//  public static final int GOAL_PREFERENCE_BRIGHTNESS_IRIS = 2;

  private final Logger logger = LogManager.getLogger(MachineLearningImpl.class);
  private final Learner learner;
  private final LearnerGoal goal;
  private final LearnerScenarioDefinition scenarioDefinition;

  private List<Item> target_item_list;
  private List<Item> relevant_item_list;
  private String[] preference_result;

  private String[] a_new_data = new String[12];
  private String[] p_new_data = new String[2];

  private int a_length = 0;
  private boolean empty;

  private Root root;
  private String activity_result;
  private Instant lastModelUpdate;

  /**
   * Create a new MachineLearning handler.
   * @param goal      The goal of the learner
   * @param configURL The URL for the {@link LearnerScenarioDefinition}
   * @throws IOException If an error occurs during initialization of the Learner
   */
  public MachineLearningImpl(LearnerGoal goal, URL configURL) throws IOException {
    scenarioDefinition = LearnerScenarioDefinition.loadFrom(configURL);
    URL learnerSettingsURL = scenarioDefinition.getDefinitionFileAsURL();
    this.learner = new Learner(learnerSettingsURL);
    this.goal = goal;
  }

  @Override
  public void setKnowledgeBaseRoot(Root root) {
    this.root = root;
    SmartHomeEntityModel model = this.root.getSmartHomeEntityModel();
    List<String> targetItemNames, relevantItemNames;
    switch (this.goal) {
      case ACTIVITY_PHONE_AND_WATCH:
      case PREFERENCE_BRIGHTNESS_IRIS:
        targetItemNames = scenarioDefinition.targetItemNames;
        relevantItemNames = scenarioDefinition.relevantItemNames;
        break;
      default:
        logger.error("Unknown goal value ({}) set", this.goal);
        targetItemNames = Collections.emptyList();
        relevantItemNames = Collections.emptyList();
    }
    target_item_list = targetItemNames.stream()
        .map(name -> resolve(model, name))
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
    relevant_item_list = relevantItemNames.stream()
        .map(name -> resolve(model, name))
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  private Item resolve(SmartHomeEntityModel model, String id) {
    Optional<Item> maybeItem = model.resolveItem(id);
    if (maybeItem.isPresent()) {
      return maybeItem.get();
    } else {
      logger.warn("Could not find item with id {}", id);
      return null;
    }
  }

  @Override
  public void newData(List<Item> changedItems) {
    /* FIXME either save state of unchanged items here (if only changed items are reported) <- pull model
             or let knowledge base pass all relevant items <- push model
    */
    switch (this.goal) {
      case ACTIVITY_PHONE_AND_WATCH:
        for (Item item : changedItems) {
          if (item.getID().equals("m_accel_x")) {
            a_length = 0;
          }
          int i = 0;
          for (Item item1 : relevant_item_list) {
            if (item.getTopic().toString().equals(item1.getTopic().toString())) {
              this.a_new_data[i] = item.getStateAsString();
            }
            i++;
          }
          a_length++;
        }
        if (a_length == 12) {
          for (String value : a_new_data) {
            if (value == null) {
              empty = true;
              break;
            }
          }
          if (!empty) {
            this.activity_result = learner.predictor(a_new_data)[0];
          }
          a_length = 0;
          Arrays.fill(this.a_new_data, null);
        }
        break;
      case PREFERENCE_BRIGHTNESS_IRIS:
        for (Item item : changedItems) {
          if (root.getSmartHomeEntityModel().getActivityItem().equals(item)) {
            String test = item.getStateAsString();
            int index = Math.round(Float.parseFloat(test));
            this.p_new_data[0] = root.getMachineLearningRoot().getActivity(index).getLabel();
          }
          if (item.getID().equals("w_brightness")) {
            this.p_new_data[1] = item.getStateAsString();
          }
        }
        this.preference_result = learner.predictor(this.p_new_data);
        break;
      default:
        logger.error("Unknown goal value ({}) set in newData", this.goal);
    }
  }

  @Override
  public List<Item> getTargets() {
    return target_item_list;
  }

  @Override
  public List<Item> getRelevantItems() {
    return relevant_item_list;
  }

  @Override
  public void triggerTraining() {
    logger.debug("Ignore trigger training call");
  }

  @Override
  public Instant lastModelUpdate() {
    return this.lastModelUpdate;
  }

  @Override
  public MachineLearningResult classify() {
    MachineLearningResult result = new MachineLearningResult();
    switch (this.goal) {
      case ACTIVITY_PHONE_AND_WATCH:
        if (activity_result == null) {
          return result;
        }
        Item activityItem = resolve(this.root.getSmartHomeEntityModel(), "activity");
        this.root.resolveActivity(activity_result).ifPresent(
            activity -> result.addItemUpdate(new ItemUpdateDouble(activityItem, activity.getIdentifier())));
        break;
      case PREFERENCE_BRIGHTNESS_IRIS:
        Item iris1 = resolve(this.root.getSmartHomeEntityModel(), "iris1_item");
        int color = 0;
        int brightness = 0;
        if (preference_result != null) {
          color = Math.round(Float.parseFloat(preference_result[0]));
          brightness = Math.round(Float.parseFloat(preference_result[1]));
        }
        result.addItemUpdate(new ItemUpdateColor(iris1, TupleHSB.of(color, 100, brightness)));
        break;
      default:
        logger.error("Unknown goal value ({}) set in classify", this.goal);
    }
    return result;
  }

  void startTraining() throws IOException, ClassNotFoundException {
    switch (this.scenarioDefinition.kind) {
      case normal:
        learner.train(this.scenarioDefinition.getDataFilesAsURL().get(0), this.scenarioDefinition.csvFormat);
        break;
      case loaded:
        List<URL> dataFilesAsURL = this.scenarioDefinition.getDataFilesAsURL();
        learner.load(dataFilesAsURL.get(0), dataFilesAsURL.get(1), true);
    }
    this.lastModelUpdate = Instant.now();
  }

  void shutdown() {
    if (this.scenarioDefinition.saveModels) {
      learner.save();
    }
    learner.shutdown();
  }
}
