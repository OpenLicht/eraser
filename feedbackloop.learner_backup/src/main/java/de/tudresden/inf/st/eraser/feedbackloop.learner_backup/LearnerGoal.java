package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

/**
 * Goal of the Learner.
 *
 * @author rschoene - Initial contribution
 */
public enum LearnerGoal {
  ACTIVITY_PHONE_AND_WATCH,
  PREFERENCE_BRIGHTNESS_IRIS
}
