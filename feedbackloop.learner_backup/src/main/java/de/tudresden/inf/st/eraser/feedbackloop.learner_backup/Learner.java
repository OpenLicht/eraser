package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.LearnerSettings;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.SimpleColumnDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.ConsoleStatusReportable;
import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.ml.data.versatile.NormalizationHelper;
import org.encog.ml.data.versatile.VersatileMLDataSet;
import org.encog.ml.data.versatile.columns.ColumnDefinition;
import org.encog.ml.data.versatile.sources.CSVDataSource;
import org.encog.ml.data.versatile.sources.VersatileDataSource;
import org.encog.ml.model.EncogModel;
import org.encog.neural.networks.BasicNetwork;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.util.csv.CSVFormat;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Internal class for Neural Networks using Encog.
 * Expects either a CSV file with data points to learn from,
 * or serialized versions of a trained network and its normalization helpers.
 *
 * @author rschoene - Initial contribution
 */
public class Learner {

  private File modelFile;
  private NormalizationHelper normalizationHelper;
  private BasicNetwork network;
  private static final Logger logger = LogManager.getLogger(Learner.class);

  private final LearnerSettings settings;

  /**
   * Creates a new learner object using the configuration at the given URL.
   * @param configURL the location of the configuration
   * @throws IOException if an error occurs in {@link ObjectMapper#readValue(URL, Class)}
   */
  Learner(URL configURL) throws IOException {
    this(LearnerSettings.loadFrom(configURL));
  }

  Learner(LearnerSettings settings) {
    this.settings = settings;
    try {
      modelFile = File.createTempFile(settings.name + "_model", "eg");
    } catch (IOException e) {
      // use local alternative
      modelFile = new File(settings.name + "_model.eg");
    }
    modelFile.deleteOnExit();
  }

//  /**
//   * Begin training using the training set specified in settings.
//   * @throws MalformedURLException if the location of the training set in the settings is malformed
//   */
//  void train() throws MalformedURLException {
//    URL location = new File(settings.initialDataFile).toURI().toURL();
//    train(location);
//  }

  /**
   * Begin training with the given initial training set.
   * @param location the location of the training set
   */
  void train(URL location, String csvFormatString) {
    logger.info("Training for {} begins using {}", settings.name, location);
    VersatileDataSource source;
    File csvFile = new File(location.getFile());
    CSVFormat csvFormat;
    switch (csvFormatString) {
      case "DECIMAL_POINT": csvFormat = CSVFormat.DECIMAL_POINT; break;
      case "DECIMAL_COMMA": csvFormat = CSVFormat.DECIMAL_COMMA; break;
      default:
        logger.warn("Unknown CSV format, using default decimal point");
        csvFormat = CSVFormat.DECIMAL_POINT;
    }
    source = new CSVDataSource(csvFile, true, csvFormat);
    VersatileMLDataSet data = new VersatileMLDataSet(source);
    List<ColumnDefinition> targets = new ArrayList<>();
    final int inputSize = settings.columns.size();
    for (int index = 0; index < inputSize; index++) {
      SimpleColumnDefinition columnDefinition = settings.columns.get(index);
      ColumnDefinition sourceColumn = data.defineSourceColumn(columnDefinition.name, index, columnDefinition.type);
      if (columnDefinition.kind == SimpleColumnDefinition.ColumnKind.target) {
        targets.add(sourceColumn);
      }
    }
    if (targets.isEmpty()) {
      logger.warn("No targets specified for {}!", settings.name);
    }
    if (targets.size() == 1) {
      data.defineSingleOutputOthersInput(targets.get(0));
    } else {
      data.defineMultipleOutputsOthersInput(targets.toArray(new ColumnDefinition[0]));
    }
    data.analyze();
    EncogModel model = new EncogModel(data);
    if (settings.verboseTraining) {
      model.setReport(new ConsoleStatusReportable());
    }
    model.selectMethod(data, settings.trainingMethod);
    data.normalize();
    normalizationHelper = data.getNormHelper();
    model.holdBackValidation(settings.validationPercent, settings.shuffleForValidation, settings.validationSeed);
    model.selectTrainingType(data);
    network = (BasicNetwork) model.crossvalidate(settings.validationFolds, settings.shuffleForValidation);
    EncogDirectoryPersistence.saveObject(modelFile, network);
    logger.info("Training for {} finished", settings.name);
  }

  String[] predictor(String[] newData) {
    String[] result;
    MLData input = normalizationHelper.allocateInputVector();
    normalizationHelper.normalizeInputVector(newData, input.getData(), false);
    MLData output = network.compute(input);
    result = normalizationHelper.denormalizeOutputVectorToString(output);
    logger.debug("Result prediction for {} applied on {} is {}:", settings.name, newData, result);
    return result;
  }

  void load(URL networkLocation, URL normalizationHelperLocation, boolean useJsonFormat)
      throws IOException, ClassNotFoundException {
    this.network = (BasicNetwork) EncogDirectoryPersistence.loadObject(networkLocation.openStream());
    this.normalizationHelper = LearnerPersistenceUtils.loadNormalizationHelperFrom(
        normalizationHelperLocation, useJsonFormat);
  }

  void shutdown() {
    Encog.getInstance().shutdown();
  }

  public void save() {
    EncogDirectoryPersistence.saveObject(new File(settings.name + "_network.eg"), this.network);
    try {
      LearnerPersistenceUtils.saveNormalizationHelper(this.normalizationHelper,
          new File(settings.name + "_normalizer.json").toURI().toURL(), true);
    } catch (IOException e) {
      logger.catching(e);
    }
  }
}
