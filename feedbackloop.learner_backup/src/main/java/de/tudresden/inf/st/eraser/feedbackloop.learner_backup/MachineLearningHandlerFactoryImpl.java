package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningDecoder;
import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningEncoder;
import de.tudresden.inf.st.eraser.jastadd.model.MachineLearningHandlerFactory;

import java.io.IOException;
import java.net.URL;

import static de.tudresden.inf.st.eraser.feedbackloop.learner_backup.LearnerGoal.ACTIVITY_PHONE_AND_WATCH;
import static de.tudresden.inf.st.eraser.feedbackloop.learner_backup.LearnerGoal.PREFERENCE_BRIGHTNESS_IRIS;

/**
 * Factory to create handlers of type {@link MachineLearningHandlerFactoryImpl}.
 *
 * @author rschoene - Initial contribution
 */
public class MachineLearningHandlerFactoryImpl extends MachineLearningHandlerFactory {

  private MachineLearningImpl handler;

  @Override
  public void initializeFor(MachineLearningHandlerFactoryTarget target, URL configUrl) throws IOException, ClassNotFoundException {
    switch (target) {
      case ACTIVITY_RECOGNITION:
        handler = new MachineLearningImpl(ACTIVITY_PHONE_AND_WATCH, configUrl);
        handler.setKnowledgeBaseRoot(knowledgeBase);
        break;
      case PREFERENCE_LEARNING:
        handler = new MachineLearningImpl(PREFERENCE_BRIGHTNESS_IRIS, configUrl);
        handler.setKnowledgeBaseRoot(knowledgeBase);
        break;
      default:
        throw new UnsupportedOperationException("Target " + target + " is not supported");
    }
    handler.startTraining();
  }

  @Override
  public MachineLearningEncoder createEncoder() {
    return handler;
  }

  @Override
  public MachineLearningDecoder createDecoder() {
    return handler;
  }

  @Override
  public void shutdown() {
    if (handler != null) {
      handler.shutdown();
    }
  }
}
