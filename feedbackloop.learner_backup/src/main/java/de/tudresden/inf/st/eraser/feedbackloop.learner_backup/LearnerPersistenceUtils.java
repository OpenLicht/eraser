package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.NormalizationHelperData;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data.SimpleColumnDefinition;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.EncogError;
import org.encog.ml.data.versatile.NormalizationHelper;
import org.encog.ml.data.versatile.columns.ColumnDefinition;
import org.encog.ml.data.versatile.columns.ColumnType;
import org.encog.ml.data.versatile.missing.MeanMissingHandler;
import org.encog.ml.data.versatile.normalizers.Normalizer;
import org.encog.ml.data.versatile.normalizers.RangeNormalizer;
import org.encog.ml.data.versatile.normalizers.strategies.BasicNormalizationStrategy;
import org.encog.ml.data.versatile.normalizers.strategies.NormalizationStrategy;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Handles objects storing to and loading from files.
 *
 * @author rschoene - Initial contribution
 */
public class LearnerPersistenceUtils {

  private static final Logger logger = LogManager.getLogger(LearnerPersistenceUtils.class);

  static NormalizationHelper loadNormalizationHelperFrom(URL location, boolean useJsonFormat) throws IOException, ClassNotFoundException {
    NormalizationHelper result;
    if (useJsonFormat) {
      NormalizationHelperData data = NormalizationHelperData.loadFrom(location);
      result = new NormalizationHelper();
      result.setSourceColumns(mapSimpleColumnDefs(data.sourceColumns));
      result.setInputColumns(mapSimpleColumnDefs(data.inputColumns));
      result.setOutputColumns(mapSimpleColumnDefs(data.outputColumns));
      result.setNormStrategy(new BasicNormalizationStrategy(
          data.inputLow, data.inputHigh, data.outputLow, data.outputHigh));
      data.unknownValues.forEach(result::defineUnknownValue);
      data.missingToMean.forEach((name, mean) -> findColumnDef(name, result).ifPresent(
          columnDefinition -> result.defineMissingHandler(columnDefinition, new MeanMissingHandler())));
    } else {
      URLConnection connection = location.openConnection();
      ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
      result = (NormalizationHelper) in.readObject();
    }
    return result;
  }

  private static Optional<ColumnDefinition> findColumnDef(String name, NormalizationHelper result) {
    List<ColumnDefinition> allDefs = new ArrayList<>();
    allDefs.addAll(result.getSourceColumns());
    allDefs.addAll(result.getInputColumns());
    allDefs.addAll(result.getOutputColumns());
    for(ColumnDefinition colDef : allDefs) {
      if (colDef.getName().equals(name)) {
        return Optional.of(colDef);
      }
    }
    logger.warn("Could not find column with name '{}'", name);
    return Optional.empty();
  }

  private static List<ColumnDefinition> mapSimpleColumnDefs(List<SimpleColumnDefinition> columnDefinitions) {
    return columnDefinitions.stream()
        .map(colDef -> {
          ColumnDefinition result = new ColumnDefinition(colDef.name, colDef.type);
          result.setLow(colDef.low);
          result.setHigh(colDef.high);
          result.setSd(colDef.sd);
          result.setMean(colDef.mean);
          result.setCount(colDef.count);
          result.setIndex(colDef.index);
          colDef.classes.forEach(result::defineClass);
          return result;
        })
        .collect(Collectors.toList());
  }

  private static List<SimpleColumnDefinition> mapColumnDefs(List<ColumnDefinition> columnDefinitions) {
    return columnDefinitions.stream()
        .map(colDef -> {
          SimpleColumnDefinition result = new SimpleColumnDefinition();
          result.name = colDef.getName();
          result.type = colDef.getDataType();
          result.low = colDef.getLow();
          result.high = colDef.getHigh();
          result.sd = colDef.getSd();
          result.mean = colDef.getMean();
          result.count = colDef.getCount();
          result.index = colDef.getIndex();
          result.classes = colDef.getClasses();
          return result;
        })
        .collect(Collectors.toList());
  }

  static void saveNormalizationHelper(NormalizationHelper normalizationHelper, URL targetLocation, boolean useJsonFormat) throws IOException {
    ObjectOutputStream out;
    if (targetLocation.getProtocol().equals("file")) {
      // we want to write to a file, thus can not use URL directly (FileURLConnection does not support output)
      out = new ObjectOutputStream(new FileOutputStream(targetLocation.getFile()));
    } else {
      URLConnection connection = targetLocation.openConnection();
      connection.setDoOutput(true);

      out = new ObjectOutputStream(connection.getOutputStream());
    }
    if (useJsonFormat) {
      NormalizationHelperData data = new NormalizationHelperData();
      data.sourceColumns = mapColumnDefs(normalizationHelper.getSourceColumns());
      data.inputColumns = mapColumnDefs(normalizationHelper.getInputColumns());
      data.outputColumns = mapColumnDefs(normalizationHelper.getOutputColumns());
      NormalizationStrategy normStrategy = normalizationHelper.getNormStrategy();
      if (normStrategy instanceof BasicNormalizationStrategy) {
        BasicNormalizationStrategy basicNormStrategy = (BasicNormalizationStrategy) normStrategy;
        double[] lowAndHigh = extractLowAndHigh(basicNormStrategy.getInputNormalizers().get(ColumnType.continuous));
        data.inputLow = lowAndHigh[0];
        data.inputHigh = lowAndHigh[1];
        lowAndHigh = extractLowAndHigh(basicNormStrategy.getOutputNormalizers().get(ColumnType.continuous));
        data.outputLow = lowAndHigh[0];
        data.outputHigh = lowAndHigh[1];
      } else {
        logger.warn("Unknown normalization strategy, can not serialize {}", normStrategy);
      }
      data.unknownValues = normalizationHelper.getUnknownValues();
      testForMissingHandlers(normalizationHelper, data);
      ParserUtils.saveTo(data, targetLocation);
    } else {
      out.writeObject(normalizationHelper);
    }
    out.close();
  }

  private static void testForMissingHandlers(NormalizationHelper normalizationHelper, NormalizationHelperData data) {
    List<ColumnDefinition> allDefs = new ArrayList<>();
    allDefs.addAll(normalizationHelper.getSourceColumns());
    allDefs.addAll(normalizationHelper.getInputColumns());
    allDefs.addAll(normalizationHelper.getOutputColumns());
    String firstUnknown;
    if (normalizationHelper.getUnknownValues().isEmpty()) {
      normalizationHelper.defineUnknownValue(null);
      firstUnknown = null;
    } else {
      firstUnknown = normalizationHelper.getUnknownValues().get(0);
    }
    double[] buffer = new double[1];
    for (ColumnDefinition colDef : allDefs) {
      try {
        normalizationHelper.normalizeToVector(colDef, 0, buffer, false, firstUnknown);
        // there was a missing handler defined
        data.missingToMean.put(colDef.getName(), colDef.getMean());
      } catch (EncogError e) {
        // missing handler is not defined, so move on
      }
    }
  }

  private static double[] extractLowAndHigh(Normalizer norm) {
    if (norm instanceof RangeNormalizer) {
      /* reconstruct low and high from the normalizer, as we know the normalization formula is
          result = ((value - col.low) / (col.high - col.low)) * (normHigh - normLow) + normLow
         if the first part of the product is zero, we get normLow
         if the first part of the product is one, we get normHigh
       */
      double[] buffer = new double[2];
      ColumnDefinition colDef = new ColumnDefinition("dummy", ColumnType.continuous);
      colDef.setLow(0);
      colDef.setHigh(1);
      // now we have (value / 1) as first part of the product
      norm.normalizeColumn(colDef, 0, buffer, 0);
      norm.normalizeColumn(colDef, 1, buffer, 1);
      return buffer;
    } else {
      logger.error("Unknown normalizer, can not serialize {}", norm);
      return new double[2];
    }

  }

}
