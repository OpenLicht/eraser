package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

import com.opencsv.CSVReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.util.Arrays;

public class ReaderCSV{

    //read every 5 s from csv
    //activity CSV
    /*
     * Col 1: smartphone acceleration x
     * Col 2: smartphone acceleration y
     * Col 3: smartphone acceleration z
     * Col 4: smartphone rotation x
     * Col 5: smartphone rotation y
     * Col 6: smartphone rotation z
     * Col 7: watch acceleration x
     * Col 8: watch acceleration y
     * Col 9: watch acceleration z
     * Col 10: watch rotation x
     * Col 11: watch rotation y
     * Col 12: watch rotation z/*/
    //preference CSV
    /*
     * Col 1: Activity
     * Col 2: watch brightness range "bright, medium, dimmer, dark"*/

    private static int TIME_PERIOD = 5000; //5 second update new value
    private String csv_file_path;
    private String csv_typ;
    private File file;
    private FileReader file_reader;
    private CSVReader csv_reader;
    private String[] next_record;
    private static final Logger logger = LogManager.getLogger(ReaderCSV.class);
    private static final String ERASER_ITEM_URI = "http://localhost:4567/model/items/";
    //TODO ITEM_NAME HAS TO BE DISCUSSED
    private static final String[] ACTIVITY_ITEM_NAME = {
            "m_accel_x", "m_accel_y", "m_accel_z", "m_rotation_x",
            "m_rotation_y", "m_rotation_z", "w_accel_x", "w_accel_y",
            "w_accel_z", "w_rotation_x", "w_rotation_y", "w_rotation_z"};
    private static final String[] PREFERENCE_ITEM_NAME = {
           "w_brightness"
    };

    //csv_type is activity or preference
    public ReaderCSV(String csv_file_path, String csv_type) {
        this.csv_file_path = csv_file_path;
        this.csv_typ = csv_type;
    }

    public void updater(){
        file=new File(csv_file_path);
        try {
            file_reader =new FileReader(file);
            csv_reader = new CSVReader(file_reader);
            while ((next_record = csv_reader.readNext()) != null) {
                Thread.sleep(TIME_PERIOD);
                if (csv_typ =="activity"){
                    String[] values = Arrays.copyOf(next_record,12);
                    setNewValue(values, ACTIVITY_ITEM_NAME);

               } else {
                    String[] values = Arrays.copyOf(next_record,2);
                    setNewValue(values, PREFERENCE_ITEM_NAME);
                    }
                }
            }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setNewValue(String[] values, String[] name){
        if(this.csv_typ.equals("activity"))
        {
            int i = 0;
            for(String value : values){
                String uri = ERASER_ITEM_URI + name[i] + "/state";
                logger.info("reader:",value);
                try {
                    HttpResponse httpResponse = Request.Put(uri)
                            .bodyString(value, ContentType.TEXT_PLAIN)
                            .execute().returnResponse();
                }catch (Exception e){
                    e.printStackTrace();
                }
                i+=1;
            }

        }else{
            String uri= ERASER_ITEM_URI + "w_brightness" +"/state";
            logger.info("reader:",values[1]);
            try {
                HttpResponse httpResponse = Request.Put(uri)
                        .bodyString(values[1], ContentType.TEXT_PLAIN)
                        .execute().returnResponse();
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }
}
