package de.tudresden.inf.st.eraser.feedbackloop.learner_backup;

/**
 * Kind of Learner.
 *
 * @author rschoene - Initial contribution
 */
public enum LearnerKind {
  /** The standard learner, getting a CSV for initial training */
  normal,
  /** A learner loaded a pre-trained model, getting a BasicNetwork and a NormalizationHelper */
  loaded
}
