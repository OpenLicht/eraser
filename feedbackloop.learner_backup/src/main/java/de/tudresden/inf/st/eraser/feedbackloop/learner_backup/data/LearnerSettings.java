package de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.tudresden.inf.st.eraser.feedbackloop.learner_backup.Learner;
import de.tudresden.inf.st.eraser.util.ParserUtils;
import org.encog.ml.factory.MLMethodFactory;
import org.encog.ml.model.EncogModel;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Settings to initialize the {@link Learner}.
 *
 * @author rschoene - Initial contribution
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class LearnerSettings {
  /** Description what this learner is about to learn */
  public String name;
  /** All available columns (of the CSV used for training, may be set to be ignored) */
  public List<SimpleColumnDefinition> columns;
  /** Whether to be verbose while training */
  public boolean verboseTraining = false;
  /** Training method */
  public String trainingMethod = MLMethodFactory.TYPE_FEEDFORWARD;
  /** Training parameter. Used in {@link EncogModel#holdBackValidation(double, boolean, int)}  */
  public double validationPercent = 0.3;
  /** Training parameter. Used in {@link EncogModel#holdBackValidation(double, boolean, int)}
   *  and {@link EncogModel#crossvalidate(int, boolean)} */
  public boolean shuffleForValidation = true;
  /** Training parameter. Used in {@link EncogModel#holdBackValidation(double, boolean, int)}  */
  public int validationSeed = 1001;
  /** Training parameter. Used in {@link EncogModel#crossvalidate(int, boolean)}  */
  public int validationFolds = 5;

  public static LearnerSettings loadFrom(URL location) throws IOException {
    return ParserUtils.loadFrom(location, LearnerSettings.class);
  }
}
