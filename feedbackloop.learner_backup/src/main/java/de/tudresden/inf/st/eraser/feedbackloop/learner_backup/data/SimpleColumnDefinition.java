package de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.encog.ml.data.versatile.columns.ColumnType;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple, serializable representation of a {@link org.encog.ml.data.versatile.columns.ColumnDefinition}.
 *
 * @author rschoene - Initial contribution
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SimpleColumnDefinition {
  public String name;
  /**	{@link ColumnType} is either: nominal, ordinal, continuous, or ignore */
  public ColumnType type;
  public int count = -1;
  public int index = -1;
  public double low = Double.NaN;
  public double high = Double.NaN;
  public double mean = Double.NaN;
  public double sd = Double.NaN;
  public List<String> classes = new ArrayList<>();
  /** {@link ColumnKind} is either input, target, or ignored */
  public ColumnKind kind;

  public enum ColumnKind {
    input,
    target,
    ignored
  }
}
