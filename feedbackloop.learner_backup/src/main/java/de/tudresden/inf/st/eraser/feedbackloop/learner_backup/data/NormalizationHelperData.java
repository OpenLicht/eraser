package de.tudresden.inf.st.eraser.feedbackloop.learner_backup.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Settings to initialize {@link org.encog.ml.data.versatile.NormalizationHelper}.
 *
 * @author rschoene - Initial contribution
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class NormalizationHelperData {
  public List<SimpleColumnDefinition> sourceColumns;
  public List<SimpleColumnDefinition> inputColumns;
  public List<SimpleColumnDefinition> outputColumns;
  public double inputLow;
  public double inputHigh;
  public double outputLow;
  public double outputHigh;
  public List<String> unknownValues = new ArrayList<>();
  public Map<String, Double> missingToMean = new HashMap<>();

  public static NormalizationHelperData loadFrom(URL location) throws IOException {
    return new ObjectMapper().readValue(location, NormalizationHelperData.class);
  }
}
