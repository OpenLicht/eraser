import glob
import os
import untangle

print(f'Current path: {os.path.abspath(os.curdir)}')
missed, covered = 0, 0
for f in glob.iglob(os.getenv('JACOCO_REPORT')):
    print(f'Checking {f}')
    obj = untangle.parse(f)
    instructions = [o for o in obj.report.counter if o['type'] == 'INSTRUCTION'][0]
    missed += int(instructions['missed'])
    covered += int(instructions['covered'])
# print missed / (missed + covered)
if missed == covered == 0:
    covered = 1
print('Covered %.2f%% of instructions for all projects.' % (covered * 100.0 / (missed + covered)))
