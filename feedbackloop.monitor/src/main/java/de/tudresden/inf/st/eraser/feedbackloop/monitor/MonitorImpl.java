package de.tudresden.inf.st.eraser.feedbackloop.monitor;

import de.tudresden.inf.st.eraser.feedbackloop.api.Analyze;
import de.tudresden.inf.st.eraser.feedbackloop.api.Monitor;
import de.tudresden.inf.st.eraser.jastadd.model.Root;

/**
 * Reference implementation for Monitor.
 *
 * @author rschoene - Initial contribution
 */
public class MonitorImpl implements Monitor {

  private Root knowledgeBase;
  private Analyze analyze;

  @Override
  public void setKnowledgeBase(Root knowledgeBase) {
    this.knowledgeBase = knowledgeBase;
  }

  @Override
  public void setAnalyze(Analyze analyze) {
    this.analyze = analyze;
  }

  @Override
  public Analyze getAnalyze() {
    return analyze;
  }
}
