#!/usr/bin/env python2.7
import argparse
import os
import shutil
import subprocess

TEMPLATE = 'project-template'
FILES_TO_SED = ['build.gradle',
                'src/main/java/de/tudresden/inf/st/eraser/projectName/Main.java',
                'src/test/java/de/tudresden/inf/st/eraser/projectName/ATest.java']
SRC_PARENT_DIRS_TO_MOVE = ['src/main/java/de/tudresden/inf/st/eraser/',
                           'src/test/java/de/tudresden/inf/st/eraser/']


def main(args):
    projectName = args.projectName.replace('/', '').replace('-', '_')

    # copy
    shutil.copytree(TEMPLATE, projectName)

    # fill in template names
    for file in FILES_TO_SED:
        cmds = ["sed", "-i", "s|projectName|" + projectName + "|",
                os.path.join(projectName, file)]
        print('cmds', cmds)
        subprocess.call(cmds)

    # correct packages
    for package in SRC_PARENT_DIRS_TO_MOVE:
        shutil.move(os.path.join(projectName, package, "projectName/"),
                    os.path.join(projectName, package, projectName))

    # add project to settings.gradle
    with open('settings.gradle', 'a') as fdr:
        fdr.write("include ':" + projectName + "'\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('projectName', help='Name of the project to create')
    args = parser.parse_args()
    main(args)
